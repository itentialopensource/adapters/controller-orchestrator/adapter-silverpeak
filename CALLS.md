## Using this Adapter

The `adapter.js` file contains the calls the adapter makes available to the rest of the Itential Platform. The API detailed for these calls should be available through JSDOC. The following is a brief summary of the calls.

### Generic Adapter Calls

These are adapter methods that IAP or you might use. There are some other methods not shown here that might be used for internal adapter functionality.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">connect()</td>
    <td style="padding:15px">This call is run when the Adapter is first loaded by he Itential Platform. It validates the properties have been provided correctly.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">healthCheck(callback)</td>
    <td style="padding:15px">This call ensures that the adapter can communicate with Adapter for Silver Peak SD-WAN Orchestrator. The actual call that is used is defined in the adapter properties and .system entities action.json file.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">refreshProperties(properties)</td>
    <td style="padding:15px">This call provides the adapter the ability to accept property changes without having to restart the adapter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">encryptProperty(property, technique, callback)</td>
    <td style="padding:15px">This call will take the provided property and technique, and return the property encrypted with the technique. This allows the property to be used in the adapterProps section for the credential password so that the password does not have to be in clear text. The adapter will decrypt the property as needed for communications with Adapter for Silver Peak SD-WAN Orchestrator.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUpdateAdapterConfiguration(configFile, changes, entity, type, action, callback)</td>
    <td style="padding:15px">This call provides the ability to update the adapter configuration from IAP - includes actions, schema, mockdata and other configurations.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapSuspendAdapter(mode, callback)</td>
    <td style="padding:15px">This call provides the ability to suspend the adapter and either have requests rejected or put into a queue to be processed after the adapter is resumed.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapUnsuspendAdapter(callback)</td>
    <td style="padding:15px">This call provides the ability to resume a suspended adapter. Any requests in queue will be processed before new requests.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterQueue(callback)</td>
    <td style="padding:15px">This call will return the requests that are waiting in the queue if throttling is enabled.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapFindAdapterPath(apiPath, callback)</td>
    <td style="padding:15px">This call provides the ability to see if a particular API path is supported by the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapTroubleshootAdapter(props, persistFlag, adapter, callback)</td>
    <td style="padding:15px">This call can be used to check on the performance of the adapter - it checks connectivity, healthcheck and basic get calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterHealthcheck(adapter, callback)</td>
    <td style="padding:15px">This call will return the results of a healthcheck.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterConnectivity(callback)</td>
    <td style="padding:15px">This call will return the results of a connectivity check.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterBasicGet(callback)</td>
    <td style="padding:15px">This call will return the results of running basic get API calls.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapMoveAdapterEntitiesToDB(callback)</td>
    <td style="padding:15px">This call will push the adapter configuration from the entities directory into the Adapter or IAP Database.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapDeactivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to remove tasks from the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapActivateTasks(tasks, callback)</td>
    <td style="padding:15px">This call provides the ability to add deactivated tasks back into the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapExpandedGenericAdapterRequest(metadata, uriPath, restMethod, pathVars, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This is an expanded Generic Call. The metadata object allows us to provide many new capabilities within the generic request.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequest(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call allows you to provide the path to have the adapter call. It is an easy way to incorporate paths that have not been built into the adapter yet.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">genericAdapterRequestNoBasePath(uriPath, restMethod, queryData, requestBody, addlHeaders, callback)</td>
    <td style="padding:15px">This call is the same as the genericAdapterRequest only it does not add a base_path or version to the call.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterLint(callback)</td>
    <td style="padding:15px">Runs lint on the addapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRunAdapterTests(callback)</td>
    <td style="padding:15px">Runs baseunit and unit tests on the adapter and provides the information back.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetAdapterInventory(callback)</td>
    <td style="padding:15px">This call provides some inventory related information about the adapter.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Cache Calls

These are adapter methods that are used for adapter caching. If configured, the adapter will cache based on the interval provided. However, you can force a population of the cache manually as well.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">iapPopulateEntityCache(entityTypes, callback)</td>
    <td style="padding:15px">This call populates the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">iapRetrieveEntitiesCache(entityType, options, callback)</td>
    <td style="padding:15px">This call retrieves the specific items from the adapter cache.</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
  
### Adapter Broker Calls

These are adapter methods that are used to integrate to IAP Brokers. This adapter currently supports the following broker calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">hasEntities(entityType, entityList, callback)</td>
    <td style="padding:15px">This call is utilized by the IAP Device Broker to determine if the adapter has a specific entity and item of the entity.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevice(deviceName, callback)</td>
    <td style="padding:15px">This call returns the details of the requested device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getDevicesFiltered(options, callback)</td>
    <td style="padding:15px">This call returns the list of devices that match the criteria provided in the options filter.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">isAlive(deviceName, callback)</td>
    <td style="padding:15px">This call returns whether the device status is active</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">getConfig(deviceName, format, callback)</td>
    <td style="padding:15px">This call returns the configuration for the selected device.</td>
    <td style="padding:15px">No</td>
  </tr>
  <tr>
    <td style="padding:15px">iapGetDeviceCount(callback)</td>
    <td style="padding:15px">This call returns the count of devices.</td>
    <td style="padding:15px">No</td>
  </tr>
</table>
<br>

### Specific Adapter Calls

Specific adapter calls are built based on the API of the Silver Peak. The Adapter Builder creates the proper method comments for generating JS-DOC for the adapter. This is the best way to get information on the calls.

<table border="1" class="bordered-table">
  <tr>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Method Signature</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Description</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Path</span></th>
    <th bgcolor="lightgrey" style="padding:15px"><span style="font-size:12.0pt">Workflow?</span></th>
  </tr>
  <tr>
    <td style="padding:15px">actionAndAuditLogEntries(startTime, endTime, limit, logLevel, appliance, username, callback)</td>
    <td style="padding:15px">Get action/audit log entries.</td>
    <td style="padding:15px">{base_path}/{version}/action?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionCancel(key, callback)</td>
    <td style="padding:15px">Cancel action guid key</td>
    <td style="padding:15px">{base_path}/{version}/action/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">actionStatus(key, callback)</td>
    <td style="padding:15px">Get status of a particular action using guid key</td>
    <td style="padding:15px">{base_path}/{version}/action/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmAcknowledgement(neId, alarmAcknowledgementBody, callback)</td>
    <td style="padding:15px">Acknowledges alarms on appliance whose sequence IDs are provided in the request body</td>
    <td style="padding:15px">{base_path}/{version}/alarm/acknowledgement/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsAlarmAcknowledgement(gmsAlarmAcknowledgementBody, callback)</td>
    <td style="padding:15px">Acknowledges alarms whose IDs are provided in the request body</td>
    <td style="padding:15px">{base_path}/{version}/alarm/acknowledgement/gms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmApplianceGetPost(applianceAlarmGetPostBody, view = 'active', severity = 'warning', orderBySeverity, maxAlarms, from, to, callback)</td>
    <td style="padding:15px">Returns active, historical, or all alarms for appliances whose ids provided in the request body</td>
    <td style="padding:15px">{base_path}/{version}/alarm/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmClearance(neId, alarmClearanceBody, callback)</td>
    <td style="padding:15px">Clears alarms whose sequence IDs are provided in the request body</td>
    <td style="padding:15px">{base_path}/{version}/alarm/clearance/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsAlarmClearance(gmsAlarmClearanceBody, callback)</td>
    <td style="padding:15px">Clears alarms whose IDs are provided in the request body</td>
    <td style="padding:15px">{base_path}/{version}/alarm/clearance/gms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allApplianceAlarmSummary(callback)</td>
    <td style="padding:15px">Returns summary of active alarms for each appliance</td>
    <td style="padding:15px">{base_path}/{version}/alarm/count/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applianceAlarmSummary(neId, callback)</td>
    <td style="padding:15px">Returns summary of active alarms for the appliance</td>
    <td style="padding:15px">{base_path}/{version}/alarm/count/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customSeverityDelete(callback)</td>
    <td style="padding:15px">Delete all customized alarm severity</td>
    <td style="padding:15px">{base_path}/{version}/alarm/customization/severity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customSeverityGet(callback)</td>
    <td style="padding:15px">Get all customized alarm severity</td>
    <td style="padding:15px">{base_path}/{version}/alarm/customization/severity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customSeverityPOST(requestBody, callback)</td>
    <td style="padding:15px">Create customized alarm severities</td>
    <td style="padding:15px">{base_path}/{version}/alarm/customization/severity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">customSeverityPut(requestBody, callback)</td>
    <td style="padding:15px">Update customized alarm severities</td>
    <td style="padding:15px">{base_path}/{version}/alarm/customization/severity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">singleCustomSeverityDelete(alarmTypeId, callback)</td>
    <td style="padding:15px">Delete alarm severity config by alarmTypeId</td>
    <td style="padding:15px">{base_path}/{version}/alarm/customization/severity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">singleCustomSeverityGet(alarmTypeId, callback)</td>
    <td style="padding:15px">Get alarm severity config by alarmTypeId</td>
    <td style="padding:15px">{base_path}/{version}/alarm/customization/severity/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmEmailDelayDelete(callback)</td>
    <td style="padding:15px">Delete alarm email delay configuration</td>
    <td style="padding:15px">{base_path}/{version}/alarm/delayEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmEmailDelayGet(callback)</td>
    <td style="padding:15px">Get alarm email delay configuration</td>
    <td style="padding:15px">{base_path}/{version}/alarm/delayEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmEmailDelayPost(requestBody, callback)</td>
    <td style="padding:15px">Create alarm email delay configuration</td>
    <td style="padding:15px">{base_path}/{version}/alarm/delayEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmEmailDelayPut(requestBody, callback)</td>
    <td style="padding:15px">Update alarm email delay configuration</td>
    <td style="padding:15px">{base_path}/{version}/alarm/delayEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmDescGet(format, defaultParam, callback)</td>
    <td style="padding:15px">Get Orchestrator and appliance alarm descriptions</td>
    <td style="padding:15px">{base_path}/{version}/alarm/description2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmGms(view = 'active', severity = 'warning', from, to, callback)</td>
    <td style="padding:15px">Returns active, closed/historical, or all Orchestrator alarms</td>
    <td style="padding:15px">{base_path}/{version}/alarm/gms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmNotification(callback)</td>
    <td style="padding:15px">Returns alarm notification</td>
    <td style="padding:15px">{base_path}/{version}/alarm/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAlarmNotification(alarmNotification, callback)</td>
    <td style="padding:15px">Set alarm notification</td>
    <td style="padding:15px">{base_path}/{version}/alarm/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmSummary(callback)</td>
    <td style="padding:15px">Returns summary of active Orchestrator alarms as well as summary of active alarms across all appliances</td>
    <td style="padding:15px">{base_path}/{version}/alarm/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">alarmSummaryType(type = 'gms', callback)</td>
    <td style="padding:15px">Returns summary of active Orchestrator alarms or summary of active alarms across all appliances</td>
    <td style="padding:15px">{base_path}/{version}/alarm/summary/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteDisabledAlarmsConfig(callback)</td>
    <td style="padding:15px">Delete disabled alarms configuration</td>
    <td style="padding:15px">{base_path}/{version}/alarm/suppress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDisabledAlarmsConfig(callback)</td>
    <td style="padding:15px">Get all disabled alarms configuration</td>
    <td style="padding:15px">{base_path}/{version}/alarm/suppress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDisabledAlarmsConfig(requestBody, callback)</td>
    <td style="padding:15px">Update disabled alarms configuration</td>
    <td style="padding:15px">{base_path}/{version}/alarm/suppress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applianceGET(callback)</td>
    <td style="padding:15px">Returns information about appliance(s) managed by Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">adminDistanceConfig(neId, cached, callback)</td>
    <td style="padding:15px">Gets Admin Distance configurations</td>
    <td style="padding:15px">{base_path}/{version}/appliance/adminDistance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">approvedAppliance(callback)</td>
    <td style="padding:15px">Get the all approved appliances</td>
    <td style="padding:15px">{base_path}/{version}/appliance/approved?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">backupPost(operation, callback)</td>
    <td style="padding:15px">Backup appliance configuration to Orchestrator database</td>
    <td style="padding:15px">{base_path}/{version}/appliance/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">backupDelete(backupFilePk, callback)</td>
    <td style="padding:15px">Delete one specified backup record from the database table.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/backup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">backupGet(neId, callback)</td>
    <td style="padding:15px">Get backup history.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/backup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applianceChgGr(groupPk, appliancePrimaryKeys, callback)</td>
    <td style="padding:15px">Change one or more appliances' group</td>
    <td style="padding:15px">{base_path}/{version}/appliance/changeGroup/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">changeAppliancePassword(nePk, username, password, callback)</td>
    <td style="padding:15px">Change a user's password on appliance.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/changePassword/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteForRediscovery(nePk, callback)</td>
    <td style="padding:15px">Delete for rediscovery.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/deleteForDiscovery/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deniedAppliance(callback)</td>
    <td style="padding:15px">Get the all denied appliances</td>
    <td style="padding:15px">{base_path}/{version}/appliance/denied?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllDiscoveredAppliances(callback)</td>
    <td style="padding:15px">Returns the all discovered appliances</td>
    <td style="padding:15px">{base_path}/{version}/appliance/discovered?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addDiscoveredApplianceToOrchestrator(key, id, callback)</td>
    <td style="padding:15px">Add discovered appliances to Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/appliance/discovered/add/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">approveDiscoveredAppliances(key, id, callback)</td>
    <td style="padding:15px">Add and approve discovered appliances</td>
    <td style="padding:15px">{base_path}/{version}/appliance/discovered/approve/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">denyDiscoveredAppliances(id, callback)</td>
    <td style="padding:15px">Deny discovered appliances.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/discovered/deny/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDiscoveredAppliances(callback)</td>
    <td style="padding:15px">Trigger discovered appliances update</td>
    <td style="padding:15px">{base_path}/{version}/appliance/discovered/update?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsCacheConfig(neId, cached, callback)</td>
    <td style="padding:15px">Gets DNS Cache configurations</td>
    <td style="padding:15px">{base_path}/{version}/appliance/dnsCache/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplianceExtraInfo(nePk, callback)</td>
    <td style="padding:15px">Delete the extra information of an appliance</td>
    <td style="padding:15px">{base_path}/{version}/appliance/extraInfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceExtraInfo(nePk, callback)</td>
    <td style="padding:15px">Get the extra information of an appliance</td>
    <td style="padding:15px">{base_path}/{version}/appliance/extraInfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveApplianceExtraInfo(applianceExtraInfo, nePk, callback)</td>
    <td style="padding:15px">Saves the extra information for the appliance</td>
    <td style="padding:15px">{base_path}/{version}/appliance/extraInfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bridgeInterfaceState(neId, cached, callback)</td>
    <td style="padding:15px">Lists the Bridge Interfaces State and pass through Tx Interface</td>
    <td style="padding:15px">{base_path}/{version}/appliance/interface/bridge/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkRoleAndSiteGet(neId, callback)</td>
    <td style="padding:15px">To verify or change a Network Role (mesh / hub / spoke) for appliances</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networkRoleAndSite/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">networkRoleAndSitePost(neId, postNetworkRulesAndSites, callback)</td>
    <td style="padding:15px">To verify or change a Network Role (mesh / hub / spoke) for appliances and to assign them to a site</td>
    <td style="padding:15px">{base_path}/{version}/appliance/networkRoleAndSite/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">peerPriorityGet(neId, cached, callback)</td>
    <td style="padding:15px">Get all peer priority data on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/appliance/peerPriorityList/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkAppliancesQueuedForDeletion(callback)</td>
    <td style="padding:15px">Lists appliances ( appliance primary keys) that are queued on the Orchestrator for deletion.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/queuedForDeletion?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applianceGetAPI(nePk, url, callback)</td>
    <td style="padding:15px">To communicate with appliance GET APIs directly.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/rest/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appliancePostAPI(nePk, url, data, callback)</td>
    <td style="padding:15px">To communicate with appliance POST APIs directly.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/rest/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applianceRestore(operation, nePk, callback)</td>
    <td style="padding:15px">Restore the appliance from the specified back.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/restore/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveChanges(saveChangesPostBody, callback)</td>
    <td style="padding:15px">Save the configuration changes in memory to a file for an array of appliances in Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/appliance/saveChanges?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplianceSaveChangesNePk(nePk, saveChangesPostBody, callback)</td>
    <td style="padding:15px">Save the configuration changes in memory to a file for an appliances in Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/appliance/saveChanges/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsConfigGet(callback)</td>
    <td style="padding:15px">To get stats config which will be synchronized to appliances.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/statsConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsConfigPost(statsConfigPostData, callback)</td>
    <td style="padding:15px">To modify stats config which will be synchronized to appliances.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/statsConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsConfigDefaultGet(callback)</td>
    <td style="padding:15px">To get default stats config which will be synchronized to appliances.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/statsConfig/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">wanNextHopHealthGet(neId, cached, callback)</td>
    <td style="padding:15px">Get wan next hop health config on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/appliance/wanNextHopHealth/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applianceDel(nePk, callback)</td>
    <td style="padding:15px">Delete an appliance from network.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applianceGetOne(nePk, callback)</td>
    <td style="padding:15px">Get appliance information</td>
    <td style="padding:15px">{base_path}/{version}/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applianceChg(nePk, applianceInfo, callback)</td>
    <td style="padding:15px">Modify an appliance's IP address, username, password, networkRole, and webProtocol.</td>
    <td style="padding:15px">{base_path}/{version}/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resyncAppliance(applianceKeys, callback)</td>
    <td style="padding:15px">Synchronize a list of appliances.</td>
    <td style="padding:15px">{base_path}/{version}/applianceResync?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appliancesSoftwareVersions(nePK, cached, callback)</td>
    <td style="padding:15px">Returns VXOA software versions information</td>
    <td style="padding:15px">{base_path}/{version}/appliancesSoftwareVersions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGet(callback)</td>
    <td style="padding:15px">Get all built-in and user-defined applications</td>
    <td style="padding:15px">{base_path}/{version}/application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">builtin(callback)</td>
    <td style="padding:15px">Get all built-in applications from all appliance</td>
    <td style="padding:15px">{base_path}/{version}/application/builtin?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userDefined(callback)</td>
    <td style="padding:15px">Get all userDefined applications from all appliance</td>
    <td style="padding:15px">{base_path}/{version}/application/userDefined?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userDefinedConfig(neId, cached, callback)</td>
    <td style="padding:15px">Get all user-defined application group on appliance</td>
    <td style="padding:15px">{base_path}/{version}/application/userDefinedConfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appTagsGet(resourceKey, callback)</td>
    <td style="padding:15px">return user defined application groups</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/applicationTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appTagsPost(appTagsBody, callback)</td>
    <td style="padding:15px">Create user defined application groups</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/applicationTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appGroupSearchPost(searchWildcard, callback)</td>
    <td style="padding:15px">Search application groups based on wildcard and limit</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/applicationTags/wildcard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appTagsModePost(mode, callback)</td>
    <td style="padding:15px">Change mode to application groups and user defined applications</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/applicationTags/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appSearchPost(searchWildcard, callback)</td>
    <td style="padding:15px">Search applications based on wildcard and limit</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/applications/wildcard?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">compoundGet(resourceKey, callback)</td>
    <td style="padding:15px">return user defined data for Compound classification</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/compoundClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">compoundReorderPost(compoundIdsConfig, callback)</td>
    <td style="padding:15px">Change the order of user defined application related to Compound data</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/compoundClassification/reorder?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">compoundDelete(id, callback)</td>
    <td style="padding:15px">Delete user defined application related to Compound data</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/compoundClassification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">compoundPost(id, compoundConfig, callback)</td>
    <td style="padding:15px">Create/modify user defined application related to Compound data</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/compoundClassification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsGet(resourceKey, callback)</td>
    <td style="padding:15px">return user defined data for Domain Name classification</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/dnsClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsDelete(domain, callback)</td>
    <td style="padding:15px">Delete user defined application related to Domain Name</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/dnsClassification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsPost(domain, dnsConfig, callback)</td>
    <td style="padding:15px">Create/modify user defined application related to Domain Name</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/dnsClassification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipIntelligenceGet(resourceKey, callback)</td>
    <td style="padding:15px">return user defined data for Address Map classification</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/ipIntelligenceClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipIntelligenceDelete(ipStart, ipEnd, callback)</td>
    <td style="padding:15px">Delete user defined application related to Address Map</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/ipIntelligenceClassification/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipIntelligencePost(ipStart, ipEnd, ipIntelligenceConfig, callback)</td>
    <td style="padding:15px">Create/modify user defined application related to Address Map</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/ipIntelligenceClassification/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">legacyUdaGet(callback)</td>
    <td style="padding:15px">return legacy UDA configuration on each appliance</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/legacyAppliancesUdas?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meterFlowGet(resourceKey, callback)</td>
    <td style="padding:15px">return user defined data for Meter Flow classification</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/meterFlowClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meterFlowDelete(flowType, mid, callback)</td>
    <td style="padding:15px">Delete user defined application related to Meter Flow</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/meterFlowClassification/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meterFlowPost(flowType, mid, meterFlowConfig, callback)</td>
    <td style="padding:15px">Create/modify user defined application related to Meter Flow</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/meterFlowClassification/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portProtocolGet(resourceKey, callback)</td>
    <td style="padding:15px">return user defined data for IP/TCP/UDP classification</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/portProtocolClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portProtocolDelete(port, protocol, callback)</td>
    <td style="padding:15px">Delete user defined application related to IP Protocol, TCP Port, UDP Port</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/portProtocolClassification/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portProtocolPost(port, protocol, portProtocolConfig, callback)</td>
    <td style="padding:15px">Create/modify user defined application related to IP Protocol, TCP Port, UDP Port</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/portProtocolClassification/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saasGet(resourceKey, callback)</td>
    <td style="padding:15px">return user defined data for SaaS classification</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/saasClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saasDelete(id, callback)</td>
    <td style="padding:15px">Delete user defined application related to SaaS data</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/saasClassification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postApplicationDefinitionSaasClassificationId(id, saasConfig, callback)</td>
    <td style="padding:15px">Create/modify user defined application related to SaaS data</td>
    <td style="padding:15px">{base_path}/{version}/applicationDefinition/saasClassification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationGroups(neId, cached, callback)</td>
    <td style="padding:15px">Get all applicationGroups on a appliance</td>
    <td style="padding:15px">{base_path}/{version}/applicationGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applicationTrends(startDate, endDate, traffic = 'optimized_traffic', bound = 'inbound', nePKList, customRangeUsed, callback)</td>
    <td style="padding:15px">Returns bandwidth stats about applications</td>
    <td style="padding:15px">{base_path}/{version}/applicationTrends?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">auth(neId, cached, callback)</td>
    <td style="padding:15px">Get the authentication order and authorization information</td>
    <td style="padding:15px">{base_path}/{version}/authRadiusTacacs/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">authentication(user, password, callback)</td>
    <td style="padding:15px">Use this api to login to Orchestrator using FORM post. Commonly used to embed orchestrator UI or proxy orchestrator login via a web proxy.</td>
    <td style="padding:15px">{base_path}/{version}/authentication/login?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationLoginStatus(callback)</td>
    <td style="padding:15px">Get the current authentication status of the HTTP session</td>
    <td style="padding:15px">{base_path}/{version}/authentication/loginStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthenticationLoginToken(requestLoginToken, callback)</td>
    <td style="padding:15px">Send a 2-factor code to user's mailbox for 2-factor authentication</td>
    <td style="padding:15px">{base_path}/{version}/authentication/loginToken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAuthenticationLogout(callback)</td>
    <td style="padding:15px">Logout of current HTTP session</td>
    <td style="padding:15px">{base_path}/{version}/authentication/logout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAuthenticationPasswordValidation(loginToken, callback)</td>
    <td style="padding:15px">Authenticate user's password only, but doesn't mark user's status as login</td>
    <td style="padding:15px">{base_path}/{version}/authentication/password/validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userAuthType(loginToken, callback)</td>
    <td style="padding:15px">Check the two factor authentication methods the user requires to login</td>
    <td style="padding:15px">{base_path}/{version}/authentication/userAuthType?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userAuthTypeToken(resetPasswordToken, callback)</td>
    <td style="padding:15px">Check the two factor authentication methods the user has active using a reset password token</td>
    <td style="padding:15px">{base_path}/{version}/authentication/userAuthTypeToken?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">avcMode(callback)</td>
    <td style="padding:15px">Returns Orchestrator avc Mode</td>
    <td style="padding:15px">{base_path}/{version}/avcMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bannerInfo(neId, cached, callback)</td>
    <td style="padding:15px">Lists the banner messages on each appliance</td>
    <td style="padding:15px">{base_path}/{version}/banners/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNeighbor(neId, callback)</td>
    <td style="padding:15px">Get specific neighbor configuration data.</td>
    <td style="padding:15px">{base_path}/{version}/bgp/config/neighbor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBGPSystemConfig(neId, callback)</td>
    <td style="padding:15px">Get BGP system configuration data.</td>
    <td style="padding:15px">{base_path}/{version}/bgp/config/system/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bGPState(neId, callback)</td>
    <td style="padding:15px">Get specific appliance bgp state details.</td>
    <td style="padding:15px">{base_path}/{version}/bgp/state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">textCustomizationConfigDelete(callback)</td>
    <td style="padding:15px">Delete all text brand customization configurations</td>
    <td style="padding:15px">{base_path}/{version}/brandCustomization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">textCustomizationConfigGet(callback)</td>
    <td style="padding:15px">Get text brand customization configurations</td>
    <td style="padding:15px">{base_path}/{version}/brandCustomization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">textCustomizationConfigPost(requestBody, callback)</td>
    <td style="padding:15px">Create new text brand customization configurations</td>
    <td style="padding:15px">{base_path}/{version}/brandCustomization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">textCustomizationConfigPut(requestBody, callback)</td>
    <td style="padding:15px">Update text brand customization configurations</td>
    <td style="padding:15px">{base_path}/{version}/brandCustomization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fileNameGet(metaData, callback)</td>
    <td style="padding:15px">Return file names for all the uploaded images</td>
    <td style="padding:15px">{base_path}/{version}/brandCustomization/image/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imageDelete(type, callback)</td>
    <td style="padding:15px">Delete image for given type of brand customization and restore default</td>
    <td style="padding:15px">{base_path}/{version}/brandCustomization/image/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">imagePost(type, qqfile, qqfileFormData, callback)</td>
    <td style="padding:15px">Upload image for given type of brand customization</td>
    <td style="padding:15px">{base_path}/{version}/brandCustomization/image/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putBrandCustomizationImageType(type, qqfile, qqfileFormData, callback)</td>
    <td style="padding:15px">Update image for given type of brand customization</td>
    <td style="padding:15px">{base_path}/{version}/brandCustomization/image/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">broadcastCliPost(operation, callback)</td>
    <td style="padding:15px">Broadcasts CLI commands to all the selected appliances</td>
    <td style="padding:15px">{base_path}/{version}/broadcastCli?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bypassPost(operation, callback)</td>
    <td style="padding:15px">Toggles bypass mode - only effective on NX appliances</td>
    <td style="padding:15px">{base_path}/{version}/bypass?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">bypassGet(nePk, cached, callback)</td>
    <td style="padding:15px">Get bypass mode settings of one appliance in Orchestrator.</td>
    <td style="padding:15px">{base_path}/{version}/bypass/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">builtinAppGet(callback)</td>
    <td style="padding:15px">Get all built in applications</td>
    <td style="padding:15px">{base_path}/{version}/cache/builtinApps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interfaceGet(callback)</td>
    <td style="padding:15px">Get all appliances' interface endpoints' information including IP addresses.</td>
    <td style="padding:15px">{base_path}/{version}/cache/interfaceEndpoints?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">userAppGet(callback)</td>
    <td style="padding:15px">Gets the user applications</td>
    <td style="padding:15px">{base_path}/{version}/cache/userApps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloudAppsConfig(neId, cached, callback)</td>
    <td style="padding:15px">Get SaaS Optimization configuration information for appliance</td>
    <td style="padding:15px">{base_path}/{version}/cloudApplications/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cloudAppsMonitor(neId, cached, callback)</td>
    <td style="padding:15px">Get reachability/RTT values for SaaS App subnets enabled for advertisement</td>
    <td style="padding:15px">{base_path}/{version}/cloudApplications/monitor/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">disks(neId, callback)</td>
    <td style="padding:15px">Get disks information.</td>
    <td style="padding:15px">{base_path}/{version}/configReportDisk/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dbPartition(table, partition, callback)</td>
    <td style="padding:15px">Gets Orchestrator database partition details</td>
    <td style="padding:15px">{base_path}/{version}/dbPartition/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePartition(table, partition, callback)</td>
    <td style="padding:15px">Deletes the table's partition</td>
    <td style="padding:15px">{base_path}/{version}/dbPartition/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">cancel(callback)</td>
    <td style="padding:15px">Cancel download the file</td>
    <td style="padding:15px">{base_path}/{version}/debugFiles/cancel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGMSDebugFile(deleteGMSDebugFile, callback)</td>
    <td style="padding:15px">Delete the Orchestrator debug file</td>
    <td style="padding:15px">{base_path}/{version}/debugFiles/delete?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteNeFile(nePk, deleteInfoClass, callback)</td>
    <td style="padding:15px">Delete the debug file on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/debugFiles/delete/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getProxyConfig(callback)</td>
    <td style="padding:15px">Get the proxy config settings</td>
    <td style="padding:15px">{base_path}/{version}/debugFiles/proxyConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setProxyConfig(proxyConfig, callback)</td>
    <td style="padding:15px">Set the proxy config settings</td>
    <td style="padding:15px">{base_path}/{version}/debugFiles/proxyConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">debugFiles(nePk, callback)</td>
    <td style="padding:15px">Get the debug files</td>
    <td style="padding:15px">{base_path}/{version}/debugFiles/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateDeployment(nePk, deployment, callback)</td>
    <td style="padding:15px">Validate deployment configuration</td>
    <td style="padding:15px">{base_path}/{version}/deployment/validate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateDiscoveredDeployment(discoveredApplianceId, deployment, callback)</td>
    <td style="padding:15px">Validate deployment configuration for a discovered appliance.</td>
    <td style="padding:15px">{base_path}/{version}/deployment/validateDiscovered/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceDeployment(nePk, callback)</td>
    <td style="padding:15px">Get the deployment configuration of the appliance, does a direct call to the appliance.</td>
    <td style="padding:15px">{base_path}/{version}/deployment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpConfigGet(callback)</td>
    <td style="padding:15px">Get orchestrator DHCP settings</td>
    <td style="padding:15px">{base_path}/{version}/dhcpConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpConfigPost(dhcpConfigPost, callback)</td>
    <td style="padding:15px">Save orchestrator DHCP settings</td>
    <td style="padding:15px">{base_path}/{version}/dhcpConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpdLeases(neId, cached, callback)</td>
    <td style="padding:15px">Get DHCP leases information.</td>
    <td style="padding:15px">{base_path}/{version}/dhcpConfig/leases/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpReservationsGet(callback)</td>
    <td style="padding:15px">Get list of reserved subnets from the DHCP pool</td>
    <td style="padding:15px">{base_path}/{version}/dhcpConfig/reservations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpReservationsPost(dhcpConfigPost, callback)</td>
    <td style="padding:15px">Reserve a subnet in the DHCP pool</td>
    <td style="padding:15px">{base_path}/{version}/dhcpConfig/reservations?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpReservationsGmsPost(dhcpConfigGmsPost, callback)</td>
    <td style="padding:15px">Update DHCP pool reservations list. This api can be used to clear up local reservations (that have not been configured on appliance yet)</td>
    <td style="padding:15px">{base_path}/{version}/dhcpConfig/reservations/gms?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpConfigResetPost(callback)</td>
    <td style="padding:15px">Remove all DHCP pool reservations</td>
    <td style="padding:15px">{base_path}/{version}/dhcpConfig/reset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dhcpdFailoverStates(neId, cached, callback)</td>
    <td style="padding:15px">Get dhcpd leases report of failover state</td>
    <td style="padding:15px">{base_path}/{version}/dhcpConfig/state/failover/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsProxyConfigGet(neId, cached, callback)</td>
    <td style="padding:15px">Get the DNS proxy config</td>
    <td style="padding:15px">{base_path}/{version}/dnsProxy/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelExceptionDelete(callback)</td>
    <td style="padding:15px">Delete all tunnel exceptions</td>
    <td style="padding:15px">{base_path}/{version}/exception/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelExceptionGet(callback)</td>
    <td style="padding:15px">Get all tunnel exceptions</td>
    <td style="padding:15px">{base_path}/{version}/exception/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelExceptionPost(requestBody, callback)</td>
    <td style="padding:15px">Create tunnel exceptions</td>
    <td style="padding:15px">{base_path}/{version}/exception/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelExceptionPut(requestBody, callback)</td>
    <td style="padding:15px">Update tunnel exceptions</td>
    <td style="padding:15px">{base_path}/{version}/exception/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelExceptionSingleDelete(id, callback)</td>
    <td style="padding:15px">Delete single tunnel exceptions</td>
    <td style="padding:15px">{base_path}/{version}/exception/tunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelExceptionSingleUpdate(id, requestBody, callback)</td>
    <td style="padding:15px">Update a single tunnel exceptions</td>
    <td style="padding:15px">{base_path}/{version}/exception/tunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">fileCreation(type = 'upgradeGms', qqfile, qqfileFormData, callback)</td>
    <td style="padding:15px">This API is to be used with fineuploader.js plugin. See fineuploader.com</td>
    <td style="padding:15px">{base_path}/{version}/fileCreation/fineUploader?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flowBandwidthStats(neId, id, seq, callback)</td>
    <td style="padding:15px">Returns the so far accumulated bandwidth stats about the flow</td>
    <td style="padding:15px">{base_path}/{version}/flow/flowBandwidthStats/{pathv1}/q?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flowDetails(neId, id, seq, callback)</td>
    <td style="padding:15px">Returns flow details</td>
    <td style="padding:15px">{base_path}/{version}/flow/flowDetails/{pathv1}/q?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flowDetails2(neId, id, seq, callback)</td>
    <td style="padding:15px">Returns more detailed info about the flow</td>
    <td style="padding:15px">{base_path}/{version}/flow/flowDetails2/{pathv1}/q?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flowsReclassification(neId, flowsReClassificationBody, callback)</td>
    <td style="padding:15px">Reclassifies the flow(s)</td>
    <td style="padding:15px">{base_path}/{version}/flow/flowReClassification/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flowsReset(neId, flowsResetBody, callback)</td>
    <td style="padding:15px">Resets the flow(s)</td>
    <td style="padding:15px">{base_path}/{version}/flow/flowReset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">flows(neId, ip1, mask1, port1, ip2, mask2, port2, ipEitherFlag, portEitherFlag, application = '3par', applicationGroup = 'Accommodation_and_Hotels', protocol = 'ip', vlan, dscp, overlays, transport, services, zone1, zone2, zoneEither, filter = 'all', edgeHA = 'false', builtIn = 'false', uptime = 'anytime', bytes = 'total', duration, anytimeSlowFlows, callback)</td>
    <td style="padding:15px">Returns active, inactive, or both types of flows on the appliance based on the query parameters supplied</td>
    <td style="padding:15px">{base_path}/{version}/flow/{pathv1}/q?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAdvProp(callback)</td>
    <td style="padding:15px">Get Orchestrator advanced properties</td>
    <td style="padding:15px">{base_path}/{version}/gms/advancedProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAdvProp(advancedProperties, callback)</td>
    <td style="padding:15px">Update Orchestrator advanced properties</td>
    <td style="padding:15px">{base_path}/{version}/gms/advancedProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGmsAdvancedPropertiesMetadata(callback)</td>
    <td style="padding:15px">Get Orchestrator metadata of the advanced properties</td>
    <td style="padding:15px">{base_path}/{version}/gms/advancedProperties/metadata?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPreconfigurations(filter = 'names', callback)</td>
    <td style="padding:15px">Get all preconfigurations</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createPreconfiguration(createPreconfig, callback)</td>
    <td style="padding:15px">Create a preconfiguration</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultPreconfigurations(callback)</td>
    <td style="padding:15px">Get the default preconfiguration, this contains the template YAML which has all possible configuration items. You will have to base64 decode configData to see the YAML in plain text</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMatchingPreconfiguration(matchPreconfig, callback)</td>
    <td style="padding:15px">Get the first matching preconfiguration, matching by serial then tag</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration/findMatch?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGmsAppliancePreconfigurationValidate(createPreconfig, callback)</td>
    <td style="padding:15px">Get the first matching preconfiguration, matching by serial then tag</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration/validate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deletePreconfiguration(preconfigId, callback)</td>
    <td style="padding:15px">Delete one preconfiguration</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPreconfiguration(preconfigId, callback)</td>
    <td style="padding:15px">Get one preconfiguration</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyPreconfiguration(createPreconfig, preconfigId, callback)</td>
    <td style="padding:15px">Modify one preconfiguration</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPreconfigurationApplyStatus(preconfigId, callback)</td>
    <td style="padding:15px">Get the apply status of a preconfiguration</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration/{pathv1}/apply?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyDiscoveredPreconfiguration(preconfigId, discoveredId, callback)</td>
    <td style="padding:15px">Apply preconfiguration to discovered appliance</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration/{pathv1}/apply/discovered/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyManagedPreconfiguration(preconfigId, nePk, callback)</td>
    <td style="padding:15px">Apply preconfiguration to an already managed appliance</td>
    <td style="padding:15px">{base_path}/{version}/gms/appliance/preconfiguration/{pathv1}/apply/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">crashHistory(action, callback)</td>
    <td style="padding:15px">Return or sends crash history of all the appliances</td>
    <td style="padding:15px">{base_path}/{version}/gms/applianceCrashHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rebootHistory(action, callback)</td>
    <td style="padding:15px">Return or sends reboot history of all the appliances</td>
    <td style="padding:15px">{base_path}/{version}/gms/applianceRebootHistory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplyApplianceWizard(nePk, callback)</td>
    <td style="padding:15px">Get the handle for the currently applying wizard.</td>
    <td style="padding:15px">{base_path}/{version}/gms/applianceWizard/apply/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyApplianceWizard(applyWizardPost, nePk, callback)</td>
    <td style="padding:15px">Applies the wizard to an appliance in the background</td>
    <td style="padding:15px">{base_path}/{version}/gms/applianceWizard/apply/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsBackupGet(callback)</td>
    <td style="padding:15px">Returns orchestrator backup information</td>
    <td style="padding:15px">{base_path}/{version}/gms/backup?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsBackupConfig(gmsBackConfig, callback)</td>
    <td style="padding:15px">add or update orchestrator backup config data</td>
    <td style="padding:15px">{base_path}/{version}/gms/backup/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">goldenOrchestratorExport(mode, download, callback)</td>
    <td style="padding:15px">Create blueprint template</td>
    <td style="padding:15px">{base_path}/{version}/gms/backup/exportTemplate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsBackupTestConnection(gmsBackConfig, callback)</td>
    <td style="padding:15px">Test the orchestrator backup remote server whether can connect.</td>
    <td style="padding:15px">{base_path}/{version}/gms/backup/testConnection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDiscoveryConfig(callback)</td>
    <td style="padding:15px">Returns the configuration for Discovery</td>
    <td style="padding:15px">{base_path}/{version}/gms/discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDiscoveryConfig(emails, callback)</td>
    <td style="padding:15px">Updates the discovery configuration with the object passed. Note: Replaces the whole configuration with the configuration passed</td>
    <td style="padding:15px">{base_path}/{version}/gms/discovery?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDynamicTopologyConfig(userName, callback)</td>
    <td style="padding:15px">Returns the configuration for the dynamic topology tab.</td>
    <td style="padding:15px">{base_path}/{version}/gms/dynamicTopologyConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateDynamicTopologyConfig(userName, request, callback)</td>
    <td style="padding:15px">Updates the dynamic topology configuration for the given user</td>
    <td style="padding:15px">{base_path}/{version}/gms/dynamicTopologyConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserDynamicTopologyConfig(username, callback)</td>
    <td style="padding:15px">Deprecated. Use the GET /gms/dynamicTopologyConfig with the userName query parameter</td>
    <td style="padding:15px">{base_path}/{version}/gms/dynamicTopologyConfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveDynamicTopologyConfig(username, request, callback)</td>
    <td style="padding:15px">Deprecated. Use POST /gms/dynamicTopologyConfig with the username query parameter</td>
    <td style="padding:15px">{base_path}/{version}/gms/dynamicTopologyConfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mgmInterfaceByGet(callback)</td>
    <td style="padding:15px">Gets setting for GMS registration on appliance</td>
    <td style="padding:15px">{base_path}/{version}/gms/gmsRegistration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mgmInterfaceByPost(dnsDetails, callback)</td>
    <td style="padding:15px">update setting for GMS registration on appliance</td>
    <td style="padding:15px">{base_path}/{version}/gms/gmsRegistration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGmsGmsRegistration2(callback)</td>
    <td style="padding:15px">Gets setting for GMS registration on appliance</td>
    <td style="padding:15px">{base_path}/{version}/gms/gmsRegistration2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGmsGmsRegistration2(dnsDetails, callback)</td>
    <td style="padding:15px">update setting for GMS registration on appliance</td>
    <td style="padding:15px">{base_path}/{version}/gms/gmsRegistration2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allGRNodeGet(callback)</td>
    <td style="padding:15px">Get appliance positions on a map for topology</td>
    <td style="padding:15px">{base_path}/{version}/gms/grNode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">grNodeUpdateByNePk(nePk, gRNodeUpdatePostBody, callback)</td>
    <td style="padding:15px">Update appliance position by nePk</td>
    <td style="padding:15px">{base_path}/{version}/gms/grNode/forNePk/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">grNodeGet(grNodePk, callback)</td>
    <td style="padding:15px">Get appliance position by graphical node primary key</td>
    <td style="padding:15px">{base_path}/{version}/gms/grNode/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">grNodeUpdate(grNodePk, gRNodeUpdatePostBody, callback)</td>
    <td style="padding:15px">Update appliance position</td>
    <td style="padding:15px">{base_path}/{version}/gms/grNode/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">allGroupsGet(callback)</td>
    <td style="padding:15px">Get all orchestrator groups. A group is a logical collection of appliances managed by orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/gms/group?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupAdd(groupNewPostBody, callback)</td>
    <td style="padding:15px">Add new group</td>
    <td style="padding:15px">{base_path}/{version}/gms/group/new?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rootGroupGet(callback)</td>
    <td style="padding:15px">Get root group</td>
    <td style="padding:15px">{base_path}/{version}/gms/group/root?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupDelete(id, callback)</td>
    <td style="padding:15px">Delete a group</td>
    <td style="padding:15px">{base_path}/{version}/gms/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupGet(id, callback)</td>
    <td style="padding:15px">Get a single group</td>
    <td style="padding:15px">{base_path}/{version}/gms/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupUpdate(id, groupUpdatePostBody, callback)</td>
    <td style="padding:15px">Update a group</td>
    <td style="padding:15px">{base_path}/{version}/gms/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">httpsCertificateValidation(certificateKeyData, callback)</td>
    <td style="padding:15px">upload the intermediate certs, certificate and key files</td>
    <td style="padding:15px">{base_path}/{version}/gms/httpsCertificate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGmsHttpsCertificateValidation(certificateKeyData, callback)</td>
    <td style="padding:15px">Validate the certificate and key</td>
    <td style="padding:15px">{base_path}/{version}/gms/httpsCertificate/validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceLabels(active, callback)</td>
    <td style="padding:15px">Returns all the interface labels saved</td>
    <td style="padding:15px">{base_path}/{version}/gms/interfaceLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postInterfaceLabels(newLabels, deleteDependencies, callback)</td>
    <td style="padding:15px">Save interface labels, will completely replace the current implementation.</td>
    <td style="padding:15px">{base_path}/{version}/gms/interfaceLabels?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLabelsForType(type = 'wan', active, callback)</td>
    <td style="padding:15px">Returns all the labels for the given type</td>
    <td style="padding:15px">{base_path}/{version}/gms/interfaceLabels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInternalSubnets(callback)</td>
    <td style="padding:15px">Get the internal subnets</td>
    <td style="padding:15px">{base_path}/{version}/gms/internalSubnets2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveInternalSubnets(body, callback)</td>
    <td style="padding:15px">Save internal subnets</td>
    <td style="padding:15px">{base_path}/{version}/gms/internalSubnets2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIPWhitelistDrops(callback)</td>
    <td style="padding:15px">Get the IP addresses of the dropped requests to this server</td>
    <td style="padding:15px">{base_path}/{version}/gms/ipwhitelist/drops?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getExternalIPWhitelist(callback)</td>
    <td style="padding:15px">Get the external IP/mask white list</td>
    <td style="padding:15px">{base_path}/{version}/gms/ipwhitelist/external?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setExternalIPWhitelist(subnets, callback)</td>
    <td style="padding:15px">Set the external IP/mask white list</td>
    <td style="padding:15px">{base_path}/{version}/gms/ipwhitelist/external?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduledJobsGet(callback)</td>
    <td style="padding:15px">Get all the scheduled jobs in Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/gms/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduledJob2Post(scheduledJobs2Post, callback)</td>
    <td style="padding:15px">Schedule a new job in Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/gms/job?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">historicalJobsGet(lastXDays, scheduleId, latest, callback)</td>
    <td style="padding:15px">Get the historical jobs by parameter. If all parameter is empty. Get 1000 latest historical jobs entities.</td>
    <td style="padding:15px">{base_path}/{version}/gms/job/historical?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">historicalJobsGetById(jobId, callback)</td>
    <td style="padding:15px">Get the historical job by Id.</td>
    <td style="padding:15px">{base_path}/{version}/gms/job/historical/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduledJobDelete(jobId, callback)</td>
    <td style="padding:15px">Deletes a schedule job</td>
    <td style="padding:15px">{base_path}/{version}/gms/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduledJob2Get(jobId, callback)</td>
    <td style="padding:15px">Get scheduled jobs list for one specified appliance based on the nePk Id</td>
    <td style="padding:15px">{base_path}/{version}/gms/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">scheduledJobModify(jobId, scheduledJob2Post, callback)</td>
    <td style="padding:15px">Modify a scheduled job.</td>
    <td style="padding:15px">{base_path}/{version}/gms/job/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">stopJob(jobId, callback)</td>
    <td style="padding:15px">Stop a scheduled job</td>
    <td style="padding:15px">{base_path}/{version}/gms/job/{pathv1}/stop?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllMenuItems(callback)</td>
    <td style="padding:15px">All the checked menu items with different menu type names</td>
    <td style="padding:15px">{base_path}/{version}/gms/menuCustomization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addMenuType(newMenuType, callback)</td>
    <td style="padding:15px">Store or update the checked menu items for a special menu type name</td>
    <td style="padding:15px">{base_path}/{version}/gms/menuCustomization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateMenuTypeItems(usersUpdate, callback)</td>
    <td style="padding:15px">Configure users for menu types and set default menu type for monitoring users</td>
    <td style="padding:15px">{base_path}/{version}/gms/menuCustomization?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExistingMenuType(menuTypeName, callback)</td>
    <td style="padding:15px">Delete the a special menu type, including its checked menu items and configured users</td>
    <td style="padding:15px">{base_path}/{version}/gms/menuCustomization/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOverlayApplianceAssociations(callback)</td>
    <td style="padding:15px">Get the list of appliances associated with all overlays</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addOverlayApplianceAssociations(associations, callback)</td>
    <td style="padding:15px">Add appliances to overlays</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">removeAppliancesFromOverlays(appliancesToRemove, callback)</td>
    <td style="padding:15px">Remove appliances from overlays</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/association/remove?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancesForOverlay(overlayId, callback)</td>
    <td style="padding:15px">Get appliances in an overlay</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/association/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteOverlayApplianceAssociation(overlayId, nePk, callback)</td>
    <td style="padding:15px">Remove an appliance from an overlay</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/association/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllOverlays(callback)</td>
    <td style="padding:15px">All the overlays stored in the Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNewOverlay(newOverlay, callback)</td>
    <td style="padding:15px">Saves a new overlay to the Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaxNumberOfOverlays(callback)</td>
    <td style="padding:15px">Get the max number of overlays</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/config/maxNumOfOverlays?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRegionalOverlays(callback)</td>
    <td style="padding:15px">All the overlays stored in the Orchestrator keyed by overlayId and regionId</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/config/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postAllRegionalOverlays(allOverlays, callback)</td>
    <td style="padding:15px">Post regional overlay configurations</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/config/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegionalOverlay(overlayId, regionId, callback)</td>
    <td style="padding:15px">Get regional overlay by overlayId and regionId</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/config/regions/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">modifyRegionalOverlay(overlayId, regionId, overlayConfig, callback)</td>
    <td style="padding:15px">Modify regional overlay</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/config/regions/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExistingOverlay(overlayId, callback)</td>
    <td style="padding:15px">Deletes the overlay from the Orchestrator. Removes any appliances from the overlay and deletes any reports specifically for this overlay.</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOverlay(overlayId, callback)</td>
    <td style="padding:15px">Returns one overlay that has the id of the passed overlayId parameter</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExistingOverlay(overlayId, updatedOverlay, callback)</td>
    <td style="padding:15px">Update the properties of an existing overlay</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOverlayPriorityMap(callback)</td>
    <td style="padding:15px">Get the overlay priority order</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/priority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveOverlayPriority(overlayPriority, callback)</td>
    <td style="padding:15px">Change the overlay priorities</td>
    <td style="padding:15px">{base_path}/{version}/gms/overlays/priority?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getScheduleTimezone(callback)</td>
    <td style="padding:15px">Returns the configured timezone for scheduled jobs and reports</td>
    <td style="padding:15px">{base_path}/{version}/gms/scheduleTimezone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateScheduleTimezone(defaultTimezone, callback)</td>
    <td style="padding:15px">Updates the schedule timezone used with for the scheduled jobs and reports.</td>
    <td style="padding:15px">{base_path}/{version}/gms/scheduleTimezone?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getServices(callback)</td>
    <td style="padding:15px">Returns all the services</td>
    <td style="padding:15px">{base_path}/{version}/gms/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveServices(newServices, callback)</td>
    <td style="padding:15px">Save the service list</td>
    <td style="padding:15px">{base_path}/{version}/gms/services?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sessionTimeout(callback)</td>
    <td style="padding:15px">Returns information of auto logout and max login session</td>
    <td style="padding:15px">{base_path}/{version}/gms/sessionTimeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putGmsSessionTimeout(sessionDetail, callback)</td>
    <td style="padding:15px">Update auto logout and max login session</td>
    <td style="padding:15px">{base_path}/{version}/gms/sessionTimeout?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsCollection(callback)</td>
    <td style="padding:15px">Gets stats collection enable/disable details</td>
    <td style="padding:15px">{base_path}/{version}/gms/statsCollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGmsStatsCollection(statsCollection, callback)</td>
    <td style="padding:15px">Used to enable/disable stats collection by orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/gms/statsCollection?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsCollectionDefault(callback)</td>
    <td style="padding:15px">To get default values for stats collection</td>
    <td style="padding:15px">{base_path}/{version}/gms/statsCollection/default?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThirdPartyServices(callback)</td>
    <td style="padding:15px">Returns all the Third Party Services</td>
    <td style="padding:15px">{base_path}/{version}/gms/thirdPartyServices?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getDefaultTopologyConfig(userName, callback)</td>
    <td style="padding:15px">Returns the configuration for the topology tab. The key in the configuration will be the username.</td>
    <td style="padding:15px">{base_path}/{version}/gms/topologyConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateTopologyConfig(userName, request, callback)</td>
    <td style="padding:15px">Updates the topology configuration for the given user</td>
    <td style="padding:15px">{base_path}/{version}/gms/topologyConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveMapImage(defaultParam, callback)</td>
    <td style="padding:15px">Update the map image</td>
    <td style="padding:15px">{base_path}/{version}/gms/topologyConfig/map?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserTopologyConfig(username, callback)</td>
    <td style="padding:15px">Deprecated. Use GET /gms/topologyConfig with the username query parameter</td>
    <td style="padding:15px">{base_path}/{version}/gms/topologyConfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveTopologyConfig(username, request, callback)</td>
    <td style="padding:15px">Deprecated. Use POST /gms/topologyConfig with the username query parameter</td>
    <td style="padding:15px">{base_path}/{version}/gms/topologyConfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTunnelGroupApplianceAssociations(callback)</td>
    <td style="padding:15px">Get the list of all tunnel group appliance lists</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addTunnelGroupApplianceAssociations(associations, callback)</td>
    <td style="padding:15px">Add appliances to tunnel groups</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/association?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAppliancesForTunnelGroup(tunnelGroupId, callback)</td>
    <td style="padding:15px">Get appliances in a tunnel group</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/association/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTunnelGroupApplianceAssociation(tunnelGroupId, nePk, callback)</td>
    <td style="padding:15px">Remove an appliance from a tunnel group</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/association/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllTunnelGroups(callback)</td>
    <td style="padding:15px">Returns all the tunnel group configurations stored in the Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addNewTunnelGroup(newTunnelGroup, callback)</td>
    <td style="padding:15px">Saves a new tunnel group configuration to the Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteExistingTunnelGroup(id, callback)</td>
    <td style="padding:15px">Deletes the tunnel group from the Orchestrator. Removes any appliances from the tunnel group.</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnelGroup(id, callback)</td>
    <td style="padding:15px">Returns one tunnel group that matches the ID given</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">updateExistingTunnelGroup(id, updatedTunnelGroup, callback)</td>
    <td style="padding:15px">Update the properties of an existing tunnel group</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnelGroupProperties(callback)</td>
    <td style="padding:15px">Get the tunnel group management properties</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setTunnelGroupProperties(tunnelGroupProperties, callback)</td>
    <td style="padding:15px">Set the tunnel group management properties</td>
    <td style="padding:15px">{base_path}/{version}/gms/tunnelGroups/properties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">versionInfo(callback)</td>
    <td style="padding:15px">Returns available orchestrator versions</td>
    <td style="padding:15px">{base_path}/{version}/gms/versions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsConfig(callback)</td>
    <td style="padding:15px">Returns all Orchestrator configs</td>
    <td style="padding:15px">{base_path}/{version}/gmsConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postGmsConfig(gmsConfigCreation, callback)</td>
    <td style="padding:15px">Creates an Orchestrator config</td>
    <td style="padding:15px">{base_path}/{version}/gmsConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteGmsConfigBase(base, resourceKey, callback)</td>
    <td style="padding:15px">Deletes an Orchestrator config</td>
    <td style="padding:15px">{base_path}/{version}/gmsConfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGmsConfigBase(base, resourceKey, callback)</td>
    <td style="padding:15px">Returns specific Orchestrator config. But a specific Orchestrator config could have multiple sub-resources, that's why it returns an array. However, If "resourceKey" is  provided, response will be an object(sub-resource) instead of an array</td>
    <td style="padding:15px">{base_path}/{version}/gmsConfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">putGmsConfigBase(base, resourceKey, gmsConfigUpdate, callback)</td>
    <td style="padding:15px">Updates a versioned Orchestrator config. Current resource version number needs to be provided when updating</td>
    <td style="padding:15px">{base_path}/{version}/gmsConfig/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hostnameByGet(callback)</td>
    <td style="padding:15px">Get the host name of orchestrator.</td>
    <td style="padding:15px">{base_path}/{version}/gmsHostname?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">licenseByGet(callback)</td>
    <td style="padding:15px">Get current license key and information</td>
    <td style="padding:15px">{base_path}/{version}/gmsLicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">licenseByPut(licenseKey, callback)</td>
    <td style="padding:15px">Set Orchestrator license key</td>
    <td style="padding:15px">{base_path}/{version}/gmsLicense?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">validateKey(licenseKey, callback)</td>
    <td style="padding:15px">Validate a new license key</td>
    <td style="padding:15px">{base_path}/{version}/gmsLicense/validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">osInfo(callback)</td>
    <td style="padding:15px">Returns Orchestrator Operating system type</td>
    <td style="padding:15px">{base_path}/{version}/gmsOperatingSystem?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsRemoteAuthGet(callback)</td>
    <td style="padding:15px">Get current Radius or TACACS+ configuration</td>
    <td style="padding:15px">{base_path}/{version}/gmsRemoteAuth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsRemoteAuthPost(remoteAuthData, callback)</td>
    <td style="padding:15px">Modify Radius or TACACS+ configuration</td>
    <td style="padding:15px">{base_path}/{version}/gmsRemoteAuth?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sMTP(callback)</td>
    <td style="padding:15px">Delete custom SMTP</td>
    <td style="padding:15px">{base_path}/{version}/gmsSMTP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getGmsSMTP(callback)</td>
    <td style="padding:15px">Gets SMTP details</td>
    <td style="padding:15px">{base_path}/{version}/gmsSMTP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sMTPPost(sMTPDetails, callback)</td>
    <td style="padding:15px">used to set SMTP settings</td>
    <td style="padding:15px">{base_path}/{version}/gmsSMTP?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delUnverifiedEmails(emails, callback)</td>
    <td style="padding:15px">Delete unverified emails</td>
    <td style="padding:15px">{base_path}/{version}/gmsSMTP/delUnverifiedEmails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sendVerificationEmail(address, callback)</td>
    <td style="padding:15px">Send a verification email to an email address</td>
    <td style="padding:15px">{base_path}/{version}/gmsSMTP/sendVerificationEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sMTPTestMail(sMTPDetails, callback)</td>
    <td style="padding:15px">used to send test mail</td>
    <td style="padding:15px">{base_path}/{version}/gmsSMTP/testEmail?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">listUnverifiedEmails(callback)</td>
    <td style="padding:15px">Get unverified email addresses</td>
    <td style="padding:15px">{base_path}/{version}/gmsSMTP/unverifiedEmails?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">verifyEmailAddress(id, callback)</td>
    <td style="padding:15px">verify an email address</td>
    <td style="padding:15px">{base_path}/{version}/gmsSMTP/verifyAddress?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsBriefInfo(callback)</td>
    <td style="padding:15px">Returns orchestrator server information such as used disk hostname, up time, version etc...</td>
    <td style="padding:15px">{base_path}/{version}/gmsserver/briefInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">hello(callback)</td>
    <td style="padding:15px">Returns hello message.</td>
    <td style="padding:15px">{base_path}/{version}/gmsserver/hello?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsServerInfo(callback)</td>
    <td style="padding:15px">Returns orchestrator server information such as used disk space, hostname,release etc...</td>
    <td style="padding:15px">{base_path}/{version}/gmsserver/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsPingInfo(callback)</td>
    <td style="padding:15px">Returns orchestrator server information such as used disk hostname, up time, version etc...</td>
    <td style="padding:15px">{base_path}/{version}/gmsserver/ping?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">haGroupsGet(callback)</td>
    <td style="padding:15px">Returns a collection of appliances paired in a HA configuration</td>
    <td style="padding:15px">{base_path}/{version}/haGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">haGroupsPost(haGroups, callback)</td>
    <td style="padding:15px">Modify appliances paired in a HA configuration</td>
    <td style="padding:15px">{base_path}/{version}/haGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">healthSummary(healthSummaryGetPostBody, callback)</td>
    <td style="padding:15px">Returns health summary of all appliances</td>
    <td style="padding:15px">{base_path}/{version}/health?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getHealthAlarmPeriodSummary(from, to, applianceId, callback)</td>
    <td style="padding:15px">Returns summary of alarms for a given appliance for that time period</td>
    <td style="padding:15px">{base_path}/{version}/health/alarmPeriodSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jitterPeriodSummary(time, overlayId, applianceId, callback)</td>
    <td style="padding:15px">Returns summary of jitter for a given appliance for that time period</td>
    <td style="padding:15px">{base_path}/{version}/health/jitterPeriodSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">latencyPeriodSummary(time, overlayId, applianceId, callback)</td>
    <td style="padding:15px">Returns summary of latency for a given appliance for that time period</td>
    <td style="padding:15px">{base_path}/{version}/health/latencyPeriodSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">lossPeriodSummary(time, overlayId, applianceId, callback)</td>
    <td style="padding:15px">Returns summary of loss for a given appliance for that time period</td>
    <td style="padding:15px">{base_path}/{version}/health/lossPeriodSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mosPeriodSummary(time, overlayId, applianceId, callback)</td>
    <td style="padding:15px">Returns summary of mos for a given appliance for that time period</td>
    <td style="padding:15px">{base_path}/{version}/health/mosPeriodSummary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyHostname(nePk, hostName, callback)</td>
    <td style="padding:15px">add/updates the gms hostname</td>
    <td style="padding:15px">{base_path}/{version}/hostname/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">idleTime(callback)</td>
    <td style="padding:15px">Clear idle time</td>
    <td style="padding:15px">{base_path}/{version}/idle/clear?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getIdleIncrement(callback)</td>
    <td style="padding:15px">Increment idle time</td>
    <td style="padding:15px">{base_path}/{version}/idle/increment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ikeless(callback)</td>
    <td style="padding:15px">Get UDP IPSec key status for all appliances</td>
    <td style="padding:15px">{base_path}/{version}/ikelessSeedStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyLabelsToAppliance(nePk, callback)</td>
    <td style="padding:15px">Pushes the active interface labels on Orchestrator to appliance</td>
    <td style="padding:15px">{base_path}/{version}/interfaceLabels/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interfaceState(cached, neId, callback)</td>
    <td style="padding:15px">Get node configuration data from Orchestrator database or from the specified appliance.</td>
    <td style="padding:15px">{base_path}/{version}/interfaceState/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">licensing(callback)</td>
    <td style="padding:15px">Retrieves NX licensed appliances</td>
    <td style="padding:15px">{base_path}/{version}/license/nx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicensePortalAppliance(callback)</td>
    <td style="padding:15px">Retrieves portal licensed appliances</td>
    <td style="padding:15px">{base_path}/{version}/license/portal/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">grant(nePk, callback)</td>
    <td style="padding:15px">Grant an appliance a base license via Cloud Portal</td>
    <td style="padding:15px">{base_path}/{version}/license/portal/appliance/grant/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteLicenseToken(nePk, callback)</td>
    <td style="padding:15px">Delete an appliance's license token via the appliance</td>
    <td style="padding:15px">{base_path}/{version}/license/portal/appliance/license/token/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLicensePortalApplianceRevokeNePk(nePk, callback)</td>
    <td style="padding:15px">Revoke an appliance a base license via Cloud Portal</td>
    <td style="padding:15px">{base_path}/{version}/license/portal/appliance/revoke/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postLicensePortalEcNePk(nePk, applianceInfo, callback)</td>
    <td style="padding:15px">Changes appliance boost and plus settings</td>
    <td style="padding:15px">{base_path}/{version}/license/portal/ec/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicensePortalSummary(callback)</td>
    <td style="padding:15px">Retrieves summary of portal licensed appliances</td>
    <td style="padding:15px">{base_path}/{version}/license/portal/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLicenseVx(callback)</td>
    <td style="padding:15px">Retrieves VX licensed appliances</td>
    <td style="padding:15px">{base_path}/{version}/license/vx?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">linkIntegrityTestRun(linkIntegrityTestRun, callback)</td>
    <td style="padding:15px">POST operation to start link integrity test between two appliances.</td>
    <td style="padding:15px">{base_path}/{version}/linkIntegrityTest/run?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">linkIntegrityStatus(neId, callback)</td>
    <td style="padding:15px">Operation to retrieve current link integrity test status for desired appliance.</td>
    <td style="padding:15px">{base_path}/{version}/linkIntegrityTest/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">addressLookup(address, callback)</td>
    <td style="padding:15px">Lookup the latitude,longitude of an address</td>
    <td style="padding:15px">{base_path}/{version}/location/addressToLocation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getLoggingSettings(neId, cached, callback)</td>
    <td style="padding:15px">Get logging settings form appliance or gmsdb</td>
    <td style="padding:15px">{base_path}/{version}/logging/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sessions(callback)</td>
    <td style="padding:15px">Returns all the current sessions</td>
    <td style="padding:15px">{base_path}/{version}/loginSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getMaintenanceMode(callback)</td>
    <td style="padding:15px">Get maintenance mode appliances</td>
    <td style="padding:15px">{base_path}/{version}/maintenanceMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setMaintenanceMode(maintenanceModePostBody, callback)</td>
    <td style="padding:15px">Set maintenance mode for appliances</td>
    <td style="padding:15px">{base_path}/{version}/maintenanceMode?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUploadedMap(imageName, callback)</td>
    <td style="padding:15px">Delete certain uploaded map</td>
    <td style="padding:15px">{base_path}/{version}/maps/deleteUploadedMap?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUploadedMaps(callback)</td>
    <td style="padding:15px">Get list of uploaded maps</td>
    <td style="padding:15px">{base_path}/{version}/maps/getUploadedMaps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multicastConfigGet(neId, cached, callback)</td>
    <td style="padding:15px">Get multicast config info on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/multicast/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multicastEnableGet(neId, cached, callback)</td>
    <td style="padding:15px">Get multicast enabled info on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/multicast/enable/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multicastInterfaceStateGet(neId, callback)</td>
    <td style="padding:15px">Get multicast interface state info on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/multicast/state/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multicastNeighborStateGet(neId, callback)</td>
    <td style="padding:15px">Get multicast neighbor state  info on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/multicast/state/neighbors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">multicastRouteStateGet(neId, callback)</td>
    <td style="padding:15px">Get multicast route state  info on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/multicast/state/routes/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllNat(neId, cached, callback)</td>
    <td style="padding:15px">Returns all NAT Config</td>
    <td style="padding:15px">{base_path}/{version}/nat/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">branchNatMapsGet(neId, cached, callback)</td>
    <td style="padding:15px">Get NAT Maps</td>
    <td style="padding:15px">{base_path}/{version}/nat/{pathv1}/maps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllNatPools(neId, cached, callback)</td>
    <td style="padding:15px">Returns all NAT Pools</td>
    <td style="padding:15px">{base_path}/{version}/nat/{pathv1}/natPools?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNetFlowData(neId, cached, callback)</td>
    <td style="padding:15px">Get flow export data from appliance or gmsdb</td>
    <td style="padding:15px">{base_path}/{version}/netFlow/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">eraseNmPost(applianceKeys, callback)</td>
    <td style="padding:15px">Erase Network Memory on one or more appliances.</td>
    <td style="padding:15px">{base_path}/{version}/networkMemory?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delNotification(callback)</td>
    <td style="padding:15px">Remove the message of notification banner.</td>
    <td style="padding:15px">{base_path}/{version}/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getNotification(callback)</td>
    <td style="padding:15px">Get the message of notification banner.</td>
    <td style="padding:15px">{base_path}/{version}/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postNotification(notificationConfig, callback)</td>
    <td style="padding:15px">Add or update the message of notification banner.</td>
    <td style="padding:15px">{base_path}/{version}/notification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getInterfaceConfigDta(neId, callback)</td>
    <td style="padding:15px">Get OSPF interfaces configuration data.</td>
    <td style="padding:15px">{base_path}/{version}/ospf/config/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOSPFSystemConfig(neId, callback)</td>
    <td style="padding:15px">Get appliance's OSPF system level configuration data.</td>
    <td style="padding:15px">{base_path}/{version}/ospf/config/system/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oSPFInterfaceStateObj(neId, callback)</td>
    <td style="padding:15px">Gets the state of the OSPF interfaces.</td>
    <td style="padding:15px">{base_path}/{version}/ospf/state/interfaces/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oSPFNeighborsStateObj(neId, callback)</td>
    <td style="padding:15px">Gets the state of the OSPF neighbors.</td>
    <td style="padding:15px">{base_path}/{version}/ospf/state/neighbors/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">oSPFStateObj(neId, callback)</td>
    <td style="padding:15px">Gets the state of the OSPF.</td>
    <td style="padding:15px">{base_path}/{version}/ospf/state/system/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOverlayMngProps(callback)</td>
    <td style="padding:15px">Get the overlay manager properties in the Orchestrator.</td>
    <td style="padding:15px">{base_path}/{version}/overlayManagerProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setOverlayMngProps(overlayManagerProperties, callback)</td>
    <td style="padding:15px">Set the overlay manager properties in the Orchestrator.</td>
    <td style="padding:15px">{base_path}/{version}/overlayManagerProperties?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portForwardingGet(neId, callback)</td>
    <td style="padding:15px">Get port forwarding rules on a VXOA.</td>
    <td style="padding:15px">{base_path}/{version}/portForwarding/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portProfilesGet(callback)</td>
    <td style="padding:15px">Get a list of all Deployment Profile templates</td>
    <td style="padding:15px">{base_path}/{version}/portProfiles/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portProfilesPost(portProfilesPost, callback)</td>
    <td style="padding:15px">Create a new Deployment Profile template</td>
    <td style="padding:15px">{base_path}/{version}/portProfiles/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portProfilesDelete(portProfileId, callback)</td>
    <td style="padding:15px">Delete a Deployment Profile template matching the id</td>
    <td style="padding:15px">{base_path}/{version}/portProfiles/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPortProfilesConfigPortProfileId(portProfileId, callback)</td>
    <td style="padding:15px">Get a Deployment Profile template matching the id</td>
    <td style="padding:15px">{base_path}/{version}/portProfiles/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portProfilesPut(portProfileId, portProfilesPost, callback)</td>
    <td style="padding:15px">Update a Deployment Profile template matching the id</td>
    <td style="padding:15px">{base_path}/{version}/portProfiles/config/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portProfilesLabelInUseGet(labelId, labelSide = 'lan', callback)</td>
    <td style="padding:15px">Check if an interface label is in use by any of the existing deployment profiles</td>
    <td style="padding:15px">{base_path}/{version}/portProfiles/isLabelInUse?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllApplianceAccessGroups(assigned, callback)</td>
    <td style="padding:15px">Get all appliance access groups / assets</td>
    <td style="padding:15px">{base_path}/{version}/rbac/asset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateApplianceAccessGroup(body, callback)</td>
    <td style="padding:15px">Create or update appliance access group / asset.</td>
    <td style="padding:15px">{base_path}/{version}/rbac/asset?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteApplianceAccessGroupByName(applianceAccessGroupName, callback)</td>
    <td style="padding:15px">Delete appliance access group / asset by name</td>
    <td style="padding:15px">{base_path}/{version}/rbac/asset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getApplianceAccessGroupByName(applianceAccessGroupName, callback)</td>
    <td style="padding:15px">Get appliance access group / asset by name</td>
    <td style="padding:15px">{base_path}/{version}/rbac/asset/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRbacAssignments(callback)</td>
    <td style="padding:15px">Get all rbac assignments</td>
    <td style="padding:15px">{base_path}/{version}/rbac/assignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">createOrUpdateRbacAssignment(body, callback)</td>
    <td style="padding:15px">Create or update rbacAssignment.</td>
    <td style="padding:15px">{base_path}/{version}/rbac/assignment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRbacAssignment(username, callback)</td>
    <td style="padding:15px">Delete rbacAssignment by username</td>
    <td style="padding:15px">{base_path}/{version}/rbac/assignment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRbacAssignmentByUsername(username, callback)</td>
    <td style="padding:15px">Get rbacAssignment by username</td>
    <td style="padding:15px">{base_path}/{version}/rbac/assignment/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAllRoles(callback)</td>
    <td style="padding:15px">Get all roles.</td>
    <td style="padding:15px">{base_path}/{version}/rbac/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saveRole(body, callback)</td>
    <td style="padding:15px">Create role or Update existing role.</td>
    <td style="padding:15px">{base_path}/{version}/rbac/role?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getAssignedMenus(callback)</td>
    <td style="padding:15px">Get list of accessible menus</td>
    <td style="padding:15px">{base_path}/{version}/rbac/role/menuAssigned?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteRoleByRoleName(roleName, callback)</td>
    <td style="padding:15px">Delete role By name. If role is assigned to one or more users then API will return HTTP 423</td>
    <td style="padding:15px">{base_path}/{version}/rbac/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRoleByRoleName(roleName, callback)</td>
    <td style="padding:15px">Get role by name if exists</td>
    <td style="padding:15px">{base_path}/{version}/rbac/role/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reachabilityFromAppliance(neId, callback)</td>
    <td style="padding:15px">Get the reachability status from the appliance</td>
    <td style="padding:15px">{base_path}/{version}/reachability/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">reachabilityFromGMS(neId, callback)</td>
    <td style="padding:15px">Get the reachability status from the Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/reachability/gms/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">realtimeStats(nePk, body, callback)</td>
    <td style="padding:15px">Get per second statistics from a VXOA appliance</td>
    <td style="padding:15px">{base_path}/{version}/realtimeStats/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regionGet(callback)</td>
    <td style="padding:15px">Get all regions</td>
    <td style="padding:15px">{base_path}/{version}/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regionPost(requestBody, callback)</td>
    <td style="padding:15px">Create region</td>
    <td style="padding:15px">{base_path}/{version}/regions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regionAssociationGET(callback)</td>
    <td style="padding:15px">Get all region association</td>
    <td style="padding:15px">{base_path}/{version}/regions/appliances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regionAssociationPost(requestBody, callback)</td>
    <td style="padding:15px">Create region association</td>
    <td style="padding:15px">{base_path}/{version}/regions/appliances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegionsAppliancesNePkNePK(nePK, callback)</td>
    <td style="padding:15px">Get region association by nePK</td>
    <td style="padding:15px">{base_path}/{version}/regions/appliances/nePk/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRegionsAppliancesRegionIdRegionId(regionId, callback)</td>
    <td style="padding:15px">Get region association by region ID</td>
    <td style="padding:15px">{base_path}/{version}/regions/appliances/regionId/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regionAssociationPut(nePK, requestBody, callback)</td>
    <td style="padding:15px">Update region association</td>
    <td style="padding:15px">{base_path}/{version}/regions/appliances/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regionDelete(regionId, callback)</td>
    <td style="padding:15px">Delete region by regionId</td>
    <td style="padding:15px">{base_path}/{version}/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regionSingleGet(regionId, callback)</td>
    <td style="padding:15px">Get region by regionId</td>
    <td style="padding:15px">{base_path}/{version}/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">regionPut(regionId, requestBody, callback)</td>
    <td style="padding:15px">Update regions</td>
    <td style="padding:15px">{base_path}/{version}/regions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releases(filter, callback)</td>
    <td style="padding:15px">Gets all the Releases for orchestrator and vxoa</td>
    <td style="padding:15px">{base_path}/{version}/release?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">releaseNotifications(callback)</td>
    <td style="padding:15px">Get release notifications</td>
    <td style="padding:15px">{base_path}/{version}/release/notifications?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">delayReleaseNotification(version, delayRequest, callback)</td>
    <td style="padding:15px">Delay a release notification</td>
    <td style="padding:15px">{base_path}/{version}/release/notifications/delay/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dismissReleaseNotification(version, callback)</td>
    <td style="padding:15px">Dismiss a release notification</td>
    <td style="padding:15px">{base_path}/{version}/release/notifications/dismiss/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remoteLogReceiverGet(callback)</td>
    <td style="padding:15px">To get all remote log receiver configurations</td>
    <td style="padding:15px">{base_path}/{version}/remoteLogReceiver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remoteLogReceiverAdd(requestBody, callback)</td>
    <td style="padding:15px">Add remote log receiver(s) with configuration provided in the request body</td>
    <td style="padding:15px">{base_path}/{version}/remoteLogReceiver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remoteLogReceiverPut(requestBody, callback)</td>
    <td style="padding:15px">Update remote log receiver(s) with data provided in the request body</td>
    <td style="padding:15px">{base_path}/{version}/remoteLogReceiver?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remoteLogReceiverDelete(receiverId, callback)</td>
    <td style="padding:15px">Delete a remote log receiver</td>
    <td style="padding:15px">{base_path}/{version}/remoteLogReceiver/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">remoteLogReceiverSubscribe(receiverId, fetchingParam, callback)</td>
    <td style="padding:15px">To query specific log(s) with log type and sequence id(s) provided in the request body</td>
    <td style="padding:15px">{base_path}/{version}/remoteLogReceiver/{pathv1}/subscribe?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dns(neId, cached, callback)</td>
    <td style="padding:15px">Get the current DNS IP address and domain names configured for an appliance.</td>
    <td style="padding:15px">{base_path}/{version}/resolver/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restApiConfigGet(callback)</td>
    <td style="padding:15px">Get the REST API config</td>
    <td style="padding:15px">{base_path}/{version}/restApiConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">restApiConfigPost(restApiConfigPost, callback)</td>
    <td style="padding:15px">Set the REST API config</td>
    <td style="padding:15px">{base_path}/{version}/restApiConfig?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestRequestTimeStatsSummary(nePk, resource, portalWS, timedout, from, to, callback)</td>
    <td style="padding:15px">Returns summary of time used info of rest requests sent to appliances</td>
    <td style="padding:15px">{base_path}/{version}/restRequestTimeStats/summary?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getRestRequestTimeStatsDetails(nePk, resource, portalWS, method = 'GET', from, to, callback)</td>
    <td style="padding:15px">The time used details of all rest requests sent to an appliance's specific resource through portal web socket or web socket</td>
    <td style="padding:15px">{base_path}/{version}/restRequestTimeStats/{pathv1}/%2F{pathv2}/{pathv3}/{pathv4}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">applyRmaWizard(applyRmaWizard, nePk, callback)</td>
    <td style="padding:15px">Applies the RMA wizard</td>
    <td style="padding:15px">{base_path}/{version}/rmaWizard/apply/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">samap(nePk, callback)</td>
    <td style="padding:15px">Get built-in policy information</td>
    <td style="padding:15px">{base_path}/{version}/saMap/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityMaps(neId, cached, callback)</td>
    <td style="padding:15px">Get Security Policies configured on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/securityMaps/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSessionActiveSessions(callback)</td>
    <td style="padding:15px">Returns all the current active sessions</td>
    <td style="padding:15px">{base_path}/{version}/session/activeSessions?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">snmpGet(nePk, cached, callback)</td>
    <td style="padding:15px">Get SNMP information.</td>
    <td style="padding:15px">{base_path}/{version}/snmp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalAccountKeyChangeCount(callback)</td>
    <td style="padding:15px">Number of times account key has been changed</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/account/key/changeCount?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalAccountKeyChangeStatus(callback)</td>
    <td style="padding:15px">Current account key change status</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/account/key/changeStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalAccountKeyGeneratePut(callback)</td>
    <td style="padding:15px">Request Portal to generate a new account key</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/account/key/generate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalECSPLicenseAssign(spPortalECSPLicenseAssign, callback)</td>
    <td style="padding:15px">Assign EC-SP licenses on Portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/account/license/appliance/ecsp/assign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalECSPLicensesGet(callback)</td>
    <td style="padding:15px">Get EC-SP licenses from Portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/account/license/appliance/ecsp/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalECSPLicenseUnassign(spPortalECSPLicenseUnassign, callback)</td>
    <td style="padding:15px">Unassign EC-SP licenses on Portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/account/license/appliance/ecsp/unassign?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalAccountLicenseFeatureGet(callback)</td>
    <td style="padding:15px">Get account license feature from Portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/account/license/feature?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalAccountLicenseTypeGet(callback)</td>
    <td style="padding:15px">Get account license type from Portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/account/license/type?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalDeleteOldKey(callback)</td>
    <td style="padding:15px">Delete old account key from Portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/account/oldKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">checkApplianceReachabilityUsingWSGet(neId, callback)</td>
    <td style="padding:15px">Test if appliance is reachable via websocket from orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/applianceWSStatus/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appGroupTmpGet(callback)</td>
    <td style="padding:15px">Get application groups from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/applicationTags?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appGroupTmpHashCodeGet(callback)</td>
    <td style="padding:15px">Get the hash code for application groups data from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/applicationTags/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">compoundTmpGet(callback)</td>
    <td style="padding:15px">Get application definition for Compound data from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/compoundClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">compoundTmpHashCodeGet(callback)</td>
    <td style="padding:15px">Get the hash code for application definition data for Compound data from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/compoundClassification/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalConfigGet(callback)</td>
    <td style="padding:15px">Get Silver Peak Cloud Portal registration information</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalConfigPost(spPortalConfig, callback)</td>
    <td style="padding:15px">Post Silver Peak Cloud Portal registration information</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/config?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">connectivityGet(callback)</td>
    <td style="padding:15px">Get Orchestrator to Silver Peak Portal connectivity status</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/connectivity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalCreateCasePost(spPortalCreateCaseWithPortal, callback)</td>
    <td style="padding:15px">Create a case</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/createCaseWithPortal?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsTmpGet(callback)</td>
    <td style="padding:15px">Get application definition for Domain Name from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/dnsClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dnsTmpHashCodeGet(callback)</td>
    <td style="padding:15px">Get the hash code for application definition data for Domain Name from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/dnsClassification/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">geoLocationPost(geoLocationPostBody, callback)</td>
    <td style="padding:15px">Get geolocation information for multiple ip's from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/internetDb/geoLocateIp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">geoLocationGet(ip, callback)</td>
    <td style="padding:15px">Get geolocation information for a single ip from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/internetDb/geoLocateIp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipIntelligenceTmpGet(start, limit, callback)</td>
    <td style="padding:15px">Get application definition data for Address Map from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/internetDb/ipIntelligence?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipIntelligenceTmpTimeGet(callback)</td>
    <td style="padding:15px">Get the last update time for application definition data for Address Map from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/internetDb/ipIntelligence/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSpPortalInternetDbIpIntelligenceSearch(ip, filter, callback)</td>
    <td style="padding:15px">Search application definition data for Address Map from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/internetDb/ipIntelligence/search?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipIntelligenceTmpTotal(callback)</td>
    <td style="padding:15px">Get count for application definition data for Address Map from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/internetDb/ipIntelligence/total?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceIdToSaasIdGet(matchAny, org, serviceIds, top, callback)</td>
    <td style="padding:15px">Get service id to service details mapping</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/internetDb/serviceIdToSaasId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceIdToSaasIdCountGet(callback)</td>
    <td style="padding:15px">Get count of internet services on this appliance</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/internetDb/serviceIdToSaasId/count?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceIdToSaasIdCountriesGet(callback)</td>
    <td style="padding:15px">Get a list of unique countries in the internet services database</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/internetDb/serviceIdToSaasId/countries?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">serviceIdToSaasIdSaasAppGet(callback)</td>
    <td style="padding:15px">Get a list of unique saas applications in the internet services database</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/internetDb/serviceIdToSaasId/saasApps?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">ipProtocolNumbersGet(callback)</td>
    <td style="padding:15px">Get IP Protocol Numbers data</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/ipProtocolNumbers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meterFlowTmpGet(callback)</td>
    <td style="padding:15px">Get application definition for Meter Flow from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/meterFlowClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">meterFlowTmpHashCodeGet(callback)</td>
    <td style="padding:15px">Get the hash code for application definition data for Meter Flow from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/meterFlowClassification/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portProtocolTmpGet(callback)</td>
    <td style="padding:15px">Get application definition for IP Protocol, TCP Port, UDP Port from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/portProtocolClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portProtocolTmpHashCodeGet(callback)</td>
    <td style="padding:15px">Get the hash code for application definition data for IP Protocol, TCP Port, UDP Port from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/portProtocolClassification/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsPortalRegistrationGet(callback)</td>
    <td style="padding:15px">Get current GMS-Portal registration status</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/registration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">gmsPortalRegistrationPost(callback)</td>
    <td style="padding:15px">Initiate GMS-Portal Registration</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/registration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saasTmpGet(callback)</td>
    <td style="padding:15px">Get application definition for SaaS data from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/saasClassification?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">saasTmpHashCodeGet(callback)</td>
    <td style="padding:15px">Get the hash code for application definition data for SaaS data from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/saasClassification/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">spPortalStatusGet(callback)</td>
    <td style="padding:15px">Debug API to view current status of all Portal related services</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/status?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tcpUdpPortsGet(callback)</td>
    <td style="padding:15px">Get TCP/UDP ports data</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/tcpUdpPorts?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">topSitesGet(callback)</td>
    <td style="padding:15px">Get list of top internet sites</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/topSites?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tbTmpGet(callback)</td>
    <td style="padding:15px">Get traffic behavior categories from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/trafficBehavior?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tbTmpHashCodeGet(callback)</td>
    <td style="padding:15px">Get the hash code for traffic behavior categories data from portal</td>
    <td style="padding:15px">{base_path}/{version}/spPortal/trafficBehavior/info?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sslCertsGet(neId, cached, callback)</td>
    <td style="padding:15px">Get all SSL certificates on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/ssl/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sslCACertGetTextPost(sslCACertsFile, callback)</td>
    <td style="padding:15px">This API will take certificate date and return certificate information in text</td>
    <td style="padding:15px">{base_path}/{version}/sslCACertificate/getText?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sslCACertValidation(sslCACertValidation, callback)</td>
    <td style="padding:15px">Validate the SSL CA certificate file.</td>
    <td style="padding:15px">{base_path}/{version}/sslCACertificate/validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sslCACerts(neId, cached, callback)</td>
    <td style="padding:15px">Get all CA SSL certificates on the appliance.</td>
    <td style="padding:15px">{base_path}/{version}/sslCACertificate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sslCertGetInfoPost(sslCertGetInfoPost, callback)</td>
    <td style="padding:15px">This API will take certificate date and return certificate information</td>
    <td style="padding:15px">{base_path}/{version}/sslCertificate/getInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sslCertGetTextPost(sslCertGetTextPost, callback)</td>
    <td style="padding:15px">This API will take certificate date and return certificate information in text</td>
    <td style="padding:15px">{base_path}/{version}/sslCertificate/getText?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sslSubstituteCertValidation(sslSubstituteCertValidation, callback)</td>
    <td style="padding:15px">Validate the SSL Substitute certificate</td>
    <td style="padding:15px">{base_path}/{version}/sslSubstituteCertificate/validation?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">sslSubstituteCertGet(id, cached, callback)</td>
    <td style="padding:15px">Get all SSL Substitute certificates on the appliance.</td>
    <td style="padding:15px">{base_path}/{version}/sslSubstituteCertificate/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsAggregateTunnel(startTime, endTime, granularity = 'minute', groupPk, trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'CSV', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate appliance stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsAggregateAppliance(applianceIDs, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate appliance stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateApplianceNePk(nePk, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate appliance stats data for a single appliance filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsAggregateApplication(startTime, endTime, granularity = 'minute', groupPk, application, trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'CSV', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate application stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsAggregateApplication(applianceIDs, startTime, endTime, granularity = 'minute', application, trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate application stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateApplicationNePk(nePk, startTime, endTime, granularity = 'minute', application, trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate application stats data for a single appliance filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/application/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateApplication2(startTime, endTime, groupPk, application, top, format = 'CSV', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate application stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/application2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsAggregateApplication2(applianceIDs, startTime, endTime, application, top, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate application stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/application2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateApplication2NePk(nePk, startTime, endTime, application, top, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate application stats data for a single appliance filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/application2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">boostAggregateStats(applianceIDs, startTime, endTime, granularity = 'minute', top, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate boost stats data</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/boost?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateBoostNePk(nePk, startTime, endTime, granularity = 'minute', tunnelName, top, overlay, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate boost stats data</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/boost/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">dNSStatsAggregate(startTime, endTime, isSource = '0', splitType, top, splitByNe, groupBy, groupBySubDomains, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate dns stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsAggregateDns(applianceIDs, startTime, endTime, isSource = '0', splitType, top, splitByNe, groupBy, groupBySubDomains, callback)</td>
    <td style="padding:15px">Get aggregate dns stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/dns?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateDnsNePk(nePk, startTime, endTime, isSource = '0', splitType, top, splitByNe, groupBy, groupBySubDomains, callback)</td>
    <td style="padding:15px">Get aggregate dns stats data for a single appliance filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/dns/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsAggregateDrcTunnel(startTime, endTime, granularity = 'minute', groupPk, top, overlay, format = 'CSV', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate tunnel drc stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/drc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsDrcAggregateTunnel(applianceIDs, startTime, endTime, granularity = 'minute', top, overlay, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate tunnel drc stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/drc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateDrcNePk(nePk, startTime, endTime, granularity = 'minute', tunnelName, top, overlay, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate tunnel drc stats data for a single appliance filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/drc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsAggregateDscp(startTime, endTime, granularity = 'minute', groupPk, dscp, trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'CSV', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate dscp stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/dscp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsAggregateDscp(applianceIDs, startTime, endTime, granularity = 'minute', dscp, trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate dscp stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/dscp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateDscpNePk(nePk, startTime, endTime, trafficType = 'optimized_traffic', granularity = 'minute', trafficClass, dscp, ip, metric = 'throughput', top, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate dscp stats data for a single appliance filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/dscp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsAggregateFlow(startTime, endTime, granularity = 'minute', groupPk, flow = 'TCP_ACCELERATED', trafficType = 'optimized_traffic', ip, format = 'CSV', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate flow stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/flow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsAggregateFlow(applianceIDs, startTime, endTime, granularity = 'minute', flow = 'TCP_ACCELERATED', trafficType = 'optimized_traffic', ip, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate flow stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/flow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getActiveFlowCounts(applianceIDs, top, callback)</td>
    <td style="padding:15px">Get active flow counts by NE id</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/flow/active?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateFlowNePk(nePk, startTime, endTime, granularity = 'minute', trafficClass, flow = 'TCP_ACCELERATED', ip, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate flow stats data for a single appliance filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/flow/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interfaceAggregateStats(startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate interface stats data for a all appliance filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsAggregateInterface(applianceIDs, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate interface stats data</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/interface?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interfaceOverlayTransportAggregateStats(applianceIDs, startTime, endTime, granularity = 'minute', interfaceName, overlay, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate interface overlay transport stats data</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/interfaceOverlay?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">jitterStatsAggregate(startTime, endTime, granularity = 'minute', ip, overlay, groupByNE, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate jitter stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/jitter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsAggregateJitter(applianceIDs, startTime, endTime, granularity = 'minute', ip, top, overlay, groupByNE, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate jitter stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/jitter?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateJitterNePk(nePk, startTime, endTime, granularity = 'minute', ip, overlay, groupByNE, tunnelName, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate jitter stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/jitter/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mosAggregateStats(applianceIDs, startTime, endTime, granularity = 'minute', top, overlay, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate Mean Opinion Score stats data</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/mos?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateMosNePk(nePk, startTime, endTime, granularity = 'minute', tunnelName, top, overlay, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate Mean Opinion Score stats data</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/mos/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">portStatsAggregate(startTime, endTime, isSource = '0', isKnown = '0', protocol, port, top, splitByNe, lastHour, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate ports stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsAggregatePorts(applianceIDs, startTime, endTime, isSource = '0', isKnown = '0', protocol, port, top, splitByNe, lastHour, callback)</td>
    <td style="padding:15px">Get aggregate ports stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/ports?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregatePortsNePk(startTime, endTime, isSource = '0', isKnown = '0', protocol, port, top, splitByNe, lastHour, callback)</td>
    <td style="padding:15px">Get aggregate ports stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/ports/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityPolicyAggregateStats(applianceIDs, startTime, endTime, granularity = 'minute', top, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate security policy stats data</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/securityPolicy?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateSecurityPolicyNePk(nePk, startTime, endTime, granularity = 'minute', fromZone, toZone, top, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate security policy stats data</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/securityPolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">topTalkersStatsAggregate(startTime, endTime, top, splitByNe, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate topTalkers stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/topTalkers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsAggregateTopTalkers(applianceIDs, startTime, endTime, top, splitByNe, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate topTalkers stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/topTalkers?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateTopTalkersSplitNePk(nePk, startTime, endTime, sourceIp, callback)</td>
    <td style="padding:15px">Get aggregate topTalkers stats data for a single appliance filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/topTalkers/split/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateTopTalkersNePk(nePk, startTime, endTime, top, callback)</td>
    <td style="padding:15px">Get aggregate topTalkers stats data for a single appliance filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/topTalkers/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">trafficBehavior(startTime, endTime, groupPk, behavioralCate, format = 'CSV', callback)</td>
    <td style="padding:15px">Get aggregate Traffic Behavioral stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/trafficBehavior?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsAggregateTrafficBehavior(applianceIDs, startTime, endTime, behavioralCate, application, format = 'CSV', top, lastHour, isAggregated, callback)</td>
    <td style="padding:15px">Get aggregate Traffic Behavioral stats data for a single appliance filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/trafficBehavior?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateTrafficBehaviorNePk(nePk, startTime, endTime, behavioralCate, application, top, lastHour, callback)</td>
    <td style="padding:15px">Get aggregate Traffic Behavioral stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/trafficBehavior/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsAggregateTrafficClass(startTime, endTime, granularity = 'minute', groupPk, trafficClass, trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'CSV', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate traffic class stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/trafficClass?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsAggregateTrafficClass(applianceIDs, startTime, endTime, granularity = 'minute', trafficClass, trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate traffic class stats data filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/trafficClass?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateTrafficClassNePk(nePk, startTime, endTime, granularity = 'minute', trafficClass, trafficType = 'optimized_traffic', ip, metric = 'throughput', top, format = 'csv', callback)</td>
    <td style="padding:15px">Get aggregate trafficClass stats data for a single appliance filter by query parameters</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/trafficClass/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateTunnel(startTime, endTime, granularity = 'minute', groupPk, ip, metric = 'throughput', top, overlay, format = 'CSV', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate tunnel stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsAggregateTunnel(applianceIDs, startTime, endTime, granularity = 'minute', ip, metric = 'throughput', top, overlay, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate tunnel stats data filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsAggregateTunnelNePk(nePk, startTime, endTime, granularity = 'minute', tunnelName, ip, metric = 'throughput', top, overlay, format = 'csv', groupByNE, callback)</td>
    <td style="padding:15px">Get aggregate tunnel stats data for a single appliance filter by query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/aggregate/tunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsDnsInfo(ip, callback)</td>
    <td style="padding:15px">Get domain name by IP address from dns stats table</td>
    <td style="padding:15px">{base_path}/{version}/stats/dnsInfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsTimeseriesAppliance(startTime, endTime, granularity = 'minute', groupPk, trafficType = 'optimized_traffic', limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get appliance time series stats data filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsTimeseriesAppliance(applianceIDs, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get appliance time series stats data filter by certain query parameters and certain appliance IDs.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/appliance?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsTimeseriesApplianceNePk(nePk, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get appliance time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/appliance/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsOfApplianceProcessState(nePk, callback)</td>
    <td style="padding:15px">Stats of appliance Process State.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/applianceProcessState/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsTimeseriesApplication(startTime, endTime, granularity = 'minute', groupPk, trafficType = 'optimized_traffic', application, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get application time series stats data filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsTimeseriesApplication(applianceIDs, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', application, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get application time series stats data filter by certain query parameters and certain appliance IDs.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/application?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsTimeseriesApplicationNePk(nePk, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', application, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get application time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/application/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsTimeseriesApplication2(startTime, endTime, application, groupPk, format = 'CSV', total, latest, callback)</td>
    <td style="padding:15px">Get new application time series stats data filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/application2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsTimeseriesApplication2(applianceIDs, startTime, endTime, application, format = 'CSV', total, latest, callback)</td>
    <td style="padding:15px">Get new application time series stats data filter by certain query parameters and certain appliance IDs.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/application2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsTimeseriesApplication2NePk(nePk, startTime, endTime, application, total, format = 'CSV', callback)</td>
    <td style="padding:15px">Get new application time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/application2/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">boostTimeSeriesStats(nePk, startTime, endTime, granularity = 'minute', limit, callback)</td>
    <td style="padding:15px">Get boost time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/boost/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsTimeseriesDrcTunnel(startTime, endTime, granularity = 'minute', groupPk, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get tunnel drc time series stats data filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/drc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsTimeseriesDrc(applianceIDs, startTime, endTime, granularity = 'minute', limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get tunnel time series drc stats data filter by certain query parameters and certain appliance IDs.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/drc?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsTimeseriesDrcNePk(nePk, startTime, endTime, granularity = 'minute', tunnelName, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get tunnel time series drc stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/drc/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsTimeseriesDscp(startTime, endTime, granularity = 'minute', groupPk, trafficType = 'optimized_traffic', dscp, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get dscp time series stats data filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/dscp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsTimeseriesDscp(applianceIDs, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', dscp, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get dscp time series stats data filter by certain query parameters and certain appliance IDs.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/dscp?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsTimeseriesDscpNePk(nePk, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', dscp, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get dscp time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/dscp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsTimeseriesFlow(startTime, endTime, granularity = 'minute', groupPk, trafficType = 'optimized_traffic', flowType = 'TCP_ACCELERATED', limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get flow time series stats data filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/flow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsTimeseriesFlow(applianceIDs, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', flowType = 'TCP_ACCELERATED', limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get flow time series stats data filter by certain query parameters and certain appliance IDs.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/flow?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsTimeseriesFlowNePk(nePk, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', flowType = 'TCP_ACCELERATED', limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get flow time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/flow/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interfaceTimeseriesStats(nePk, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', interfaceName, limit, callback)</td>
    <td style="padding:15px">Get interface time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/interface/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">interfaceOverlayTransportTimeseriesStats(nePk, startTime, endTime, granularity = 'minute', overlay, callback)</td>
    <td style="padding:15px">Get interface overlay transport time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/interfaceOverlay/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsTimeseriesInternalDrops(nePk, startTime, endTime, granularity = 'minute', callback)</td>
    <td style="padding:15px">Get internalDrops time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/internalDrops/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsTimeseriesTunnel(startTime, endTime, key, callback)</td>
    <td style="padding:15px">Get tunnel time series stats data filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/metrics?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">mOSTimeSeriesStats(nePk, startTime, endTime, granularity = 'minute', tunnel, limit, callback)</td>
    <td style="padding:15px">Get Mean Opinion Score time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/mos/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">securityPolicyTimeSeriesStats(nePk, startTime, endTime, granularity = 'minute', fromZone, toZone, callback)</td>
    <td style="padding:15px">Get security policy time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/securityPolicy/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">shaperTimeseriesStats(groupPk, startTime, endTime, granularity = 'minute', trafficClass, direction, ip, format = 'CSV', callback)</td>
    <td style="padding:15px">Get Shaper time series stats data filter by certain query parameters and certain appliance IDs.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/shaper?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsTimeseriesShaper(applianceIDs, startTime, endTime, granularity = 'minute', trafficClass, direction, ip, format = 'CSV', callback)</td>
    <td style="padding:15px">Get Shaper time series stats data filter by certain query parameters and certain appliance IDs.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/shaper?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">statsTimeseriesTrafficClass(startTime, endTime, granularity = 'minute', groupPk, trafficType = 'optimized_traffic', trafficClass, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get traffic class time series stats data filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/trafficClass?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsTimeseriesTrafficClass(applianceIDs, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', trafficClass, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get traffic class time series stats data filter by certain query parameters and certain appliance IDs.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/trafficClass?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsTimeseriesTrafficClassNePk(nePk, startTime, endTime, granularity = 'minute', trafficType = 'optimized_traffic', trafficClass, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get traffic class time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/trafficClass/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsTimeseriesTunnel(startTime, endTime, granularity = 'minute', groupPk, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get tunnel time series stats data filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postStatsTimeseriesTunnel(applianceIDs, startTime, endTime, granularity = 'minute', limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get tunnel time series stats data filter by certain query parameters and certain appliance IDs.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/tunnel?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getStatsTimeseriesTunnelNePk(nePk, startTime, endTime, granularity = 'minute', tunnelName, limit, format = 'CSV', ip, latest, callback)</td>
    <td style="padding:15px">Get tunnel time series stats data of a single appliance filter by certain query parameters.</td>
    <td style="padding:15px">{base_path}/{version}/stats/timeseries/tunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">confSubnets(neId, operation, callback)</td>
    <td style="padding:15px">Configure an appliance's subnets.</td>
    <td style="padding:15px">{base_path}/{version}/subnets/configured/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnetsForDiscoveredAppliance(discoveredId, callback)</td>
    <td style="padding:15px">Get a discovered appliance's subnets information.</td>
    <td style="padding:15px">{base_path}/{version}/subnets/forDiscovered/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">setSubnetSharingOptions(neId, subnetsSystemJson, callback)</td>
    <td style="padding:15px">Configure appliance's subnet sharing options</td>
    <td style="padding:15px">{base_path}/{version}/subnets/setSubnetSharingOptions/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSubnets(getCachedData, neId, subnet, callback)</td>
    <td style="padding:15px">Get an appliance's subnets information.</td>
    <td style="padding:15px">{base_path}/{version}/subnets/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appSystemDeployInfo(neId, cached, callback)</td>
    <td style="padding:15px">Get Appliance System  Deployment Information</td>
    <td style="padding:15px">{base_path}/{version}/systemInfo/system/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">discoveredAppSystemDeployInfo(discoveredId, callback)</td>
    <td style="padding:15px">Discovered  Appliance System  Deployment Information</td>
    <td style="padding:15px">{base_path}/{version}/systemInfo/systemForDiscovered/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">appSystemStateInfo(neId, cached, callback)</td>
    <td style="padding:15px">Get Appliance System State Information</td>
    <td style="padding:15px">{base_path}/{version}/systemInfo/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnelTca(neId, cached, callback)</td>
    <td style="padding:15px">Get tunnel threshold crossing alerts</td>
    <td style="padding:15px">{base_path}/{version}/tca/tunnel/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getSystemTca(neId, cached, callback)</td>
    <td style="padding:15px">Get system threshold crossing alerts</td>
    <td style="padding:15px">{base_path}/{version}/tca/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tcpdumpRun(postRunConfig, callback)</td>
    <td style="padding:15px">POST operation to start the packet capture process.</td>
    <td style="padding:15px">{base_path}/{version}/tcpdump/run/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tcpdumpStatus(neId, callback)</td>
    <td style="padding:15px">GET operation to show current packet capture process status.</td>
    <td style="padding:15px">{base_path}/{version}/tcpdump/status/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTcpdumpTcpdumpStatus(callback)</td>
    <td style="padding:15px">Get the primary keys of all running tcp dumps.</td>
    <td style="padding:15px">{base_path}/{version}/tcpdump/tcpdumpStatus?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associationAll(callback)</td>
    <td style="padding:15px">Returns the complete association map of appliances to template groups</td>
    <td style="padding:15px">{base_path}/{version}/template/applianceAssociation/?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associationOne(nePk, callback)</td>
    <td style="padding:15px">Returns the association map of template groups for a single appliance</td>
    <td style="padding:15px">{base_path}/{version}/template/applianceAssociation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">associationOnePost(body, nePk, callback)</td>
    <td style="padding:15px">Associates the templates with a specific appliance. The array posted is the complete association for that appliance.</td>
    <td style="padding:15px">{base_path}/{version}/template/applianceAssociation/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">template(nePk, callback)</td>
    <td style="padding:15px">Returns the list of template groups applied to the appliance</td>
    <td style="padding:15px">{base_path}/{version}/template/history/groupList/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateHistoryNePk(nePk, latestOnly, callback)</td>
    <td style="padding:15px">Returns history of applied templates by nePk</td>
    <td style="padding:15px">{base_path}/{version}/template/history/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTemplateTemplateCreate(body, callback)</td>
    <td style="padding:15px">Creates a new template group</td>
    <td style="padding:15px">{base_path}/{version}/template/templateCreate?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateTemplateGroups(callback)</td>
    <td style="padding:15px">Returns template configurations for all template groups</td>
    <td style="padding:15px">{base_path}/{version}/template/templateGroups?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteTemplateTemplateGroupsTemplateGroup(templateGroup, callback)</td>
    <td style="padding:15px">Deletes a template group</td>
    <td style="padding:15px">{base_path}/{version}/template/templateGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateTemplateGroupsTemplateGroup(templateGroup, callback)</td>
    <td style="padding:15px">Returns template configurations for the specified template group only</td>
    <td style="padding:15px">{base_path}/{version}/template/templateGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTemplateTemplateGroupsTemplateGroup(templateGroup, body, callback)</td>
    <td style="padding:15px">Creates a new template group from an existing one or updates the configuration of the requested template group</td>
    <td style="padding:15px">{base_path}/{version}/template/templateGroups/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupPrioGet(callback)</td>
    <td style="padding:15px">Returns the order that template groups will be applied in</td>
    <td style="padding:15px">{base_path}/{version}/template/templateGroupsPriorities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">groupPrioPost(body, callback)</td>
    <td style="padding:15px">Post the order that template groups should be applied in, groups not listed are not in a deterministic order</td>
    <td style="padding:15px">{base_path}/{version}/template/templateGroupsPriorities?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTemplateTemplateSelectionTemplateGroup(templateGroup, callback)</td>
    <td style="padding:15px">Returns the selected templates in the template group</td>
    <td style="padding:15px">{base_path}/{version}/template/templateSelection/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postTemplateTemplateSelectionTemplateGroup(templateGroup, body, callback)</td>
    <td style="padding:15px">Selects the provided templates in the request body</td>
    <td style="padding:15px">{base_path}/{version}/template/templateSelection/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">rdPartyLicenses(format, callback)</td>
    <td style="padding:15px">Gets all third party licenses information</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyLicenses?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerConfigurationGet(callback)</td>
    <td style="padding:15px">Returns Zscaler configuration, including tunnel setting, selected interface label order</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerConfigurationPost(configuration, callback)</td>
    <td style="padding:15px">Update Returns Zscaler configuration, including tunnel setting, selected interface label order</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/configuration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerConnectivityGet(callback)</td>
    <td style="padding:15px">Returns Zscaler connectivity status</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/connectivity?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerDefaultTunnelSettingGet(callback)</td>
    <td style="padding:15px">Returns recommend tunnel setting for Zscaler</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/defaultTunnelSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerPauseOrchestrationGet(callback)</td>
    <td style="padding:15px">Returns Pause Zscaler Orchestration configuration</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/pauseOrchestration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerPauseOrchestrationPost(zscalerPauseObj, callback)</td>
    <td style="padding:15px">update pause Zscaler Orchestration configuration</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/pauseOrchestration?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerRemoteEndpointExceptionGet(callback)</td>
    <td style="padding:15px">Returns all user configured ZEN Override data</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/remoteEndpointException?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerSubscriptionDelete(callback)</td>
    <td style="padding:15px">Delete Subscription</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerSubscriptionGet(callback)</td>
    <td style="padding:15px">Returns Zscaler subscription</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerSubscriptionPost(subscription, callback)</td>
    <td style="padding:15px">Add/Update Subscription</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/subscription?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zscalerVpnLocationExceptionPost(applianceList, callback)</td>
    <td style="padding:15px">Get all VPN, Location, discovered and configured ZEN override data for one or more appliances</td>
    <td style="padding:15px">{base_path}/{version}/thirdPartyServices/zscaler/vpnLocationEndpointExceptionSetting?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBondedTunnelsConfigurationAndStates(neIds, overlayId, callback)</td>
    <td style="padding:15px">Deprecated</td>
    <td style="padding:15px">{base_path}/{version}/tunnels/bonded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBondedTunnelsStates(neIds, callback)</td>
    <td style="padding:15px">Get the current list of tunnels state</td>
    <td style="padding:15px">{base_path}/{version}/tunnels/bonded/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnelsConfigurationAndStates(ids, state, callback)</td>
    <td style="padding:15px">Deprecated. Get the current list of tunnels configuration and their state information</td>
    <td style="padding:15px">{base_path}/{version}/tunnels/physical?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnelsStates(neIds, state, callback)</td>
    <td style="padding:15px">Get the current list of tunnels state</td>
    <td style="padding:15px">{base_path}/{version}/tunnels/physical/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">initiateTraceroute(id, nePk, callback)</td>
    <td style="padding:15px">Initiate a traceroute on a tunnel</td>
    <td style="padding:15px">{base_path}/{version}/tunnels/physical/traceroute/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tracerouteState(id, nePk, callback)</td>
    <td style="padding:15px">Get traceroute state for a tunnel</td>
    <td style="padding:15px">{base_path}/{version}/tunnels/physical/tracerouteState/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThirdPartyTunnelsConfigurationAndStates(neIds, state, callback)</td>
    <td style="padding:15px">Deprecated. Get the current list of tunnels configuration and their state information</td>
    <td style="padding:15px">{base_path}/{version}/tunnels/thirdParty?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getThirdPartyTunnelsStates(neIds, state, callback)</td>
    <td style="padding:15px">Get the current list of tunnels state</td>
    <td style="padding:15px">{base_path}/{version}/tunnels/thirdParty/state?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelsCount(metaData, callback)</td>
    <td style="padding:15px">Search total tunnel count, all appliances</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAllBondedTunnels(limit, matchingAlias, overlayId, state, callback)</td>
    <td style="padding:15px">Get bonded tunnels for all appliances</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/bonded?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchBondedTunnelsForAppliance(nePk, limit, matchingAlias, overlayId, state, callback)</td>
    <td style="padding:15px">Get bonded tunnels for one appliance</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/bonded/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOneBondedTunnel(bondedTunnelId, nePk, callback)</td>
    <td style="padding:15px">Get a specific bonded tunnel for one appliance</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/bonded/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getBondedTunnelsWithPhysicalTunnels(nePk, physicalTunnelId, state, callback)</td>
    <td style="padding:15px">Get bonded tunnels a physical tunnel belongs to</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/bondedTunnelsWithPhysicalTunnel/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnelsBetweenAppliances(ids, limit, matchingAlias, overlayId, state, callback)</td>
    <td style="padding:15px">Get all tunnels between appliances</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/getTunnelsBetweenAppliances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAllPassThroughTunnels(limit, matchingAlias, matchingService, state, callback)</td>
    <td style="padding:15px">Search pass through tunnels</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/passThrough?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAppliancePassthroughTunnels(nePk, limit, matchingAlias, matchingService, state, callback)</td>
    <td style="padding:15px">Search pass through tunnels for one appliance</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/passThrough/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getPassThroughTunnel(nePk, passThroughTunnelId, callback)</td>
    <td style="padding:15px">Get pass through tunnel</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/passThrough/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">searchAllPhysicalTunnels(limit, matchingAlias, state, callback)</td>
    <td style="padding:15px">Search physical tunnels, all appliances</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/physical?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getTunnels2PhysicalNePk(nePk, limit, matchingAlias, state, callback)</td>
    <td style="padding:15px">Search physical tunnels for one appliance</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/physical/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getOnePhysicalTunnel(nePk, tunnelId, callback)</td>
    <td style="padding:15px">Get physical tunnel</td>
    <td style="padding:15px">{base_path}/{version}/tunnels2/physical/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelsDeploymentInfo(callback)</td>
    <td style="padding:15px">Get deployment information for appliances managed by the Orchestrator.</td>
    <td style="padding:15px">{base_path}/{version}/tunnelsConfiguration/deployment?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">tunnelsOverlayInfo(callback)</td>
    <td style="padding:15px">Get the tunnels in each overlay</td>
    <td style="padding:15px">{base_path}/{version}/tunnelsConfiguration/overlayInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">passThroughTunnelsInfo(callback)</td>
    <td style="padding:15px">Get all pass through tunnels from all appliances</td>
    <td style="padding:15px">{base_path}/{version}/tunnelsConfiguration/passThroughTunnelsInfo?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">uiUsageStats(uiName, callback)</td>
    <td style="padding:15px">Add usage count for one UI feature</td>
    <td style="padding:15px">{base_path}/{version}/uiUsageStats/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">upgradeAppliances(applianceUpgradeEntry, callback)</td>
    <td style="padding:15px">Upgrade appliances with option</td>
    <td style="padding:15px">{base_path}/{version}/upgradeAppliances?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUserAccount(neId, cached, callback)</td>
    <td style="padding:15px">Gets all user details and sessions of appliance</td>
    <td style="padding:15px">{base_path}/{version}/userAccount/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">users(callback)</td>
    <td style="padding:15px">Returns all the users in the system</td>
    <td style="padding:15px">{base_path}/{version}/users?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">forgotPassword(forgotPasswordBody, callback)</td>
    <td style="padding:15px">Forgot password</td>
    <td style="padding:15px">{base_path}/{version}/users/forgotPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">newTfaKey(callback)</td>
    <td style="padding:15px">Returns a barcode to scan with an authentication application to set up app based two factor authentication for your account</td>
    <td style="padding:15px">{base_path}/{version}/users/newTfaKey?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">resetPassword(resetPasswordBody, callback)</td>
    <td style="padding:15px">Resets user password</td>
    <td style="padding:15px">{base_path}/{version}/users/resetPassword?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersNewUser(newUser, saveUpdateUserBody, callback)</td>
    <td style="padding:15px">Creates a new user or updates it. 'Status: Active/Inactive. Role: Admin Manager/Network Monitor'</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">deleteUsersUserIdUserName(userId, userName, callback)</td>
    <td style="padding:15px">Deletes the specified user</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/{pathv2}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">getUsersUserName(userName, callback)</td>
    <td style="padding:15px">Gets user by username</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">postUsersUsernamePassword(username, changePasswordBody, callback)</td>
    <td style="padding:15px">Changes user password</td>
    <td style="padding:15px">{base_path}/{version}/users/{pathv1}/password?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">loopbackGet(neId, cached, callback)</td>
    <td style="padding:15px">Get loopback interface configuration on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/virtualif/loopback/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vtiGet(neId, cached, callback)</td>
    <td style="padding:15px">Get VTI interface configuration on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/virtualif/vti/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vrrpGet(neId, cached, callback)</td>
    <td style="padding:15px">Get a list of VRRP instances configured on the appliance</td>
    <td style="padding:15px">{base_path}/{version}/vrrp/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vXOAImagesGet(callback)</td>
    <td style="padding:15px">Get VXOA images</td>
    <td style="padding:15px">{base_path}/{version}/vxoaImages?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">vXOAImagesDelete(imageFile, callback)</td>
    <td style="padding:15px">Delete VXOA image</td>
    <td style="padding:15px">{base_path}/{version}/vxoaImages/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">wccpConfigGroup(neId, cached, callback)</td>
    <td style="padding:15px">Get WCCP service groups settings for the appliance.</td>
    <td style="padding:15px">{base_path}/{version}/wccp/config/group/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">wccpConfigSystem(neId, cached, callback)</td>
    <td style="padding:15px">Get WCCP system settings for the appliance</td>
    <td style="padding:15px">{base_path}/{version}/wccp/config/system/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">wccpState(neId, cached, callback)</td>
    <td style="padding:15px">Get WCCP state</td>
    <td style="padding:15px">{base_path}/{version}/wccp/state/{pathv1}?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zonesGet(callback)</td>
    <td style="padding:15px">Returns all Zones defined in Orchestrator</td>
    <td style="padding:15px">{base_path}/{version}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">zonesPost(zones, deleteDependencies, callback)</td>
    <td style="padding:15px">Add/Edit Zones</td>
    <td style="padding:15px">{base_path}/{version}/zones?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nextIdGet(callback)</td>
    <td style="padding:15px">Returns Next Id that should be assigned to a new Zone</td>
    <td style="padding:15px">{base_path}/{version}/zones/nextId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
  <tr>
    <td style="padding:15px">nextIdPost(nextIdPost, callback)</td>
    <td style="padding:15px">Update Next Id</td>
    <td style="padding:15px">{base_path}/{version}/zones/nextId?{query}</td>
    <td style="padding:15px">Yes</td>
  </tr>
</table>
<br>
