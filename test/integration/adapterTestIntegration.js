/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
samProps.devicebroker.enabled = true;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-silverpeak',
      type: 'Silverpeak',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Silverpeak = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Silverpeak Adapter Test', () => {
  describe('Silverpeak Class Tests', () => {
    const a = new Silverpeak(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    const actionKey = 'fakedata';
    describe('#actionCancel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.actionCancel(actionKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Action', 'actionCancel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let actionStartTime = 'fakedata';
    let actionEndTime = 'fakedata';
    describe('#actionStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.actionStatus(actionKey, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.completionStatus);
                assert.equal('string', data.response.description);
                assert.equal(5, data.response.endTime);
                assert.equal('string', data.response.guid);
                assert.equal(1, data.response.id);
                assert.equal(10, data.response.intTaskStatus);
                assert.equal('string', data.response.ipAddress);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.nepk);
                assert.equal(5, data.response.percentComplete);
                assert.equal(2, data.response.queuedTime);
                assert.equal('string', data.response.result);
                assert.equal(2, data.response.startTime);
                assert.equal('string', data.response.taskStatus);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              actionStartTime = data.response.startTime;
              actionEndTime = data.response.endTime;
              saveMockData('Action', 'actionStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const actionLimit = 555;
    const actionLogLevel = 555;
    describe('#actionAndAuditLogEntries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.actionAndAuditLogEntries(actionStartTime, actionEndTime, actionLimit, actionLogLevel, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Action', 'actionAndAuditLogEntries', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmNeId = 'fakedata';
    const alarmAlarmAcknowledgementBodyParam = {
      acknowledge: true
    };
    describe('#alarmAcknowledgement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.alarmAcknowledgement(alarmNeId, alarmAlarmAcknowledgementBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmAcknowledgement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmGmsAlarmAcknowledgementBodyParam = {
      acknowledge: false,
      ids: [
        4
      ]
    };
    describe('#gmsAlarmAcknowledgement - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.gmsAlarmAcknowledgement(alarmGmsAlarmAcknowledgementBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'gmsAlarmAcknowledgement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmAlarmApplianceGetPostBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#alarmApplianceGetPost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.alarmApplianceGetPost(alarmAlarmApplianceGetPostBodyParam, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmApplianceGetPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmAlarmClearanceBodyParam = {
      actions: [
        {
          id: 5,
          sequenceId: 10,
          source: 'string'
        }
      ]
    };
    describe('#alarmClearance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.alarmClearance(alarmNeId, alarmAlarmClearanceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmClearance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmGmsAlarmClearanceBodyParam = {
      ids: [
        2
      ]
    };
    describe('#gmsAlarmClearance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.gmsAlarmClearance(alarmGmsAlarmClearanceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'gmsAlarmClearance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmCustomSeverityPOSTBodyParam = {
      65536: 'string'
    };
    describe('#customSeverityPOST - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customSeverityPOST(alarmCustomSeverityPOSTBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'customSeverityPOST', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmAlarmEmailDelayPostBodyParam = {
      duration: 1
    };
    describe('#alarmEmailDelayPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.alarmEmailDelayPost(alarmAlarmEmailDelayPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmEmailDelayPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmPostAlarmNotificationBodyParam = {
      enable: false
    };
    describe('#postAlarmNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAlarmNotification(alarmPostAlarmNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'postAlarmNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmUpdateDisabledAlarmsConfigBodyParam = {
      action: 'string',
      alarmTypeIds: [
        3
      ],
      applianceIds: [
        'string'
      ]
    };
    describe('#updateDisabledAlarmsConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDisabledAlarmsConfig(alarmUpdateDisabledAlarmsConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'updateDisabledAlarmsConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allApplianceAlarmSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allApplianceAlarmSummary((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'allApplianceAlarmSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceAlarmSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applianceAlarmSummary(alarmNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(5, data.response.numOfCriticalAlarms);
                assert.equal(6, data.response.numOfMajorAlarms);
                assert.equal(1, data.response.numOfMinorAlarms);
                assert.equal(6, data.response.numOfWarningAlarms);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'applianceAlarmSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmCustomSeverityPutBodyParam = {
      65536: 'string'
    };
    describe('#customSeverityPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customSeverityPut(alarmCustomSeverityPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'customSeverityPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customSeverityGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.customSeverityGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response['65536']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'customSeverityGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmAlarmTypeId = 555;
    describe('#singleCustomSeverityGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.singleCustomSeverityGet(alarmAlarmTypeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response['65536']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'singleCustomSeverityGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmAlarmEmailDelayPutBodyParam = {
      duration: 10
    };
    describe('#alarmEmailDelayPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.alarmEmailDelayPut(alarmAlarmEmailDelayPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmEmailDelayPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmEmailDelayGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.alarmEmailDelayGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(6, data.response.duration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmEmailDelayGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmDescGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.alarmDescGet(null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(10, data.response.alarmType);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.recommendedAction);
                assert.equal(false, data.response.serviceAffecting);
                assert.equal('string', data.response.severity);
                assert.equal('string', data.response.source);
                assert.equal(5, data.response.sourceType);
                assert.equal(5, data.response.systemType);
                assert.equal(9, data.response.typeId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmDescGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmGms - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.alarmGms(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmGms', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.alarmNotification((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.enable);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.alarmSummary((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(5, data.response.numOfCriticalAlarms);
                assert.equal(4, data.response.numOfMajorAlarms);
                assert.equal(5, data.response.numOfMinorAlarms);
                assert.equal(8, data.response.numOfWarningAlarms);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const alarmType = 'fakedata';
    describe('#alarmSummaryType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.alarmSummaryType(alarmType, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(10, data.response.numOfCriticalAlarms);
                assert.equal(3, data.response.numOfMajorAlarms);
                assert.equal(6, data.response.numOfMinorAlarms);
                assert.equal(5, data.response.numOfWarningAlarms);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmSummaryType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDisabledAlarmsConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDisabledAlarmsConfig((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.alarmTypeIds));
                assert.equal(true, Array.isArray(data.response.applianceIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'getAllDisabledAlarmsConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceBackupPostBodyParam = {
      comment: 'string'
    };
    describe('#backupPost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.backupPost(applianceBackupPostBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.clientKey);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'backupPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceGroupPk = 'fakedata';
    const applianceApplianceChgGrBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#applianceChgGr - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applianceChgGr(applianceGroupPk, applianceApplianceChgGrBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'applianceChgGr', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceNePk = 'fakedata';
    const applianceUsername = 'fakedata';
    const applianceChangeAppliancePasswordBodyParam = {
      password: 'string'
    };
    describe('#changeAppliancePassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.changeAppliancePassword(applianceNePk, applianceUsername, applianceChangeAppliancePasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'changeAppliancePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceKey = 'fakedata';
    const applianceId = {};
    const applianceAddDiscoveredApplianceToOrchestratorBodyParam = {
      id: 'string'
    };
    describe('#addDiscoveredApplianceToOrchestrator - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addDiscoveredApplianceToOrchestrator(applianceKey, applianceAddDiscoveredApplianceToOrchestratorBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'addDiscoveredApplianceToOrchestrator', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceApproveDiscoveredAppliancesBodyParam = {
      id: 'string'
    };
    describe('#approveDiscoveredAppliances - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.approveDiscoveredAppliances(applianceKey, applianceApproveDiscoveredAppliancesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'approveDiscoveredAppliances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#denyDiscoveredAppliances - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.denyDiscoveredAppliances(applianceId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'denyDiscoveredAppliances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceSaveApplianceExtraInfoBodyParam = {
      contact: {
        email: 'string',
        name: 'string',
        phoneNumber: 'string'
      },
      location: {
        address: 'string',
        address2: 'string',
        city: 'string',
        country: 'string',
        state: 'string',
        zipCode: 'string'
      },
      overlaySettings: {
        ipsecUdpPort: 'string',
        isUserDefinedIPSecUDPPort: false
      }
    };
    describe('#saveApplianceExtraInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveApplianceExtraInfo(applianceSaveApplianceExtraInfoBodyParam, applianceNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'saveApplianceExtraInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceNeId = 'fakedata';
    const applianceNetworkRoleAndSitePostBodyParam = {
      id: 'string',
      networkRole: 1,
      site: 'string',
      sitePriority: 7
    };
    describe('#networkRoleAndSitePost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.networkRoleAndSitePost(applianceNeId, applianceNetworkRoleAndSitePostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'networkRoleAndSitePost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceUrl = 'fakedata';
    const applianceAppliancePostAPIBodyParam = {};
    describe('#appliancePostAPI - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appliancePostAPI(applianceNePk, applianceUrl, applianceAppliancePostAPIBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'appliancePostAPI', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceApplianceRestoreBodyParam = {
      backupFilePk: 8
    };
    describe('#applianceRestore - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applianceRestore(applianceApplianceRestoreBodyParam, applianceNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.clientKey);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'applianceRestore', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceSaveChangesBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#saveChanges - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveChanges(applianceSaveChangesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'saveChanges', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appliancePostApplianceSaveChangesNePkBodyParam = {};
    describe('#postApplianceSaveChangesNePk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postApplianceSaveChangesNePk(applianceNePk, appliancePostApplianceSaveChangesNePkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'postApplianceSaveChangesNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceStatsConfigPostBodyParam = {
      app: {},
      dns: {},
      flows_csv_enable: false,
      ip: {},
      minuteRetention: 2,
      port: {},
      verticalRetention: 10
    };
    describe('#statsConfigPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.statsConfigPost(applianceStatsConfigPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'statsConfigPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceApplianceChgBodyParam = {
      IP: 'string',
      networkRole: 'string',
      password: 'string',
      userName: 'string',
      webProtocolType: 5
    };
    describe('#applianceChg - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applianceChg(applianceNePk, applianceApplianceChgBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'applianceChg', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceGET - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applianceGET((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'applianceGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceCached = true;
    describe('#adminDistanceConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.adminDistanceConfig(applianceNeId, applianceCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(9, data.response.bgp_br);
                assert.equal(5, data.response.bgp_pe);
                assert.equal(10, data.response.bgp_tr);
                assert.equal(6, data.response.local);
                assert.equal(10, data.response.ospf);
                assert.equal(1, data.response.sub_shared);
                assert.equal(9, data.response.sub_shared_bgp);
                assert.equal(2, data.response.sub_shared_ospf);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'adminDistanceConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approvedAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.approvedAppliance((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'approvedAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#backupGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.backupGet(applianceNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'backupGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deniedAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.deniedAppliance((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'deniedAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDiscoveredAppliances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllDiscoveredAppliances((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'getAllDiscoveredAppliances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDiscoveredAppliances - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDiscoveredAppliances((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'updateDiscoveredAppliances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsCacheConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dnsCacheConfig(applianceNeId, applianceCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.http);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'dnsCacheConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceExtraInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplianceExtraInfo(applianceNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.contact);
                assert.equal('object', typeof data.response.location);
                assert.equal('object', typeof data.response.overlaySettings);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'getApplianceExtraInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bridgeInterfaceState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bridgeInterfaceState(applianceNeId, applianceCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.bridge);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'bridgeInterfaceState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkRoleAndSiteGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.networkRoleAndSiteGet(applianceNeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'networkRoleAndSiteGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#peerPriorityGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.peerPriorityGet(applianceNeId, applianceCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.peerlist);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'peerPriorityGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkAppliancesQueuedForDeletion - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.checkAppliancesQueuedForDeletion((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'checkAppliancesQueuedForDeletion', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceGetAPI - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applianceGetAPI(applianceNePk, applianceUrl, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'applianceGetAPI', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsConfigGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsConfigGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.app);
                assert.equal('object', typeof data.response.dns);
                assert.equal(false, data.response.flows_csv_enable);
                assert.equal('object', typeof data.response.ip);
                assert.equal(10, data.response.minuteRetention);
                assert.equal('object', typeof data.response.port);
                assert.equal(1, data.response.verticalRetention);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'statsConfigGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsConfigDefaultGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsConfigDefaultGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.app);
                assert.equal('object', typeof data.response.dns);
                assert.equal(true, data.response.flows_csv_enable);
                assert.equal('object', typeof data.response.ip);
                assert.equal(9, data.response.minuteRetention);
                assert.equal('object', typeof data.response.port);
                assert.equal(3, data.response.verticalRetention);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'statsConfigDefaultGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wanNextHopHealthGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.wanNextHopHealthGet(applianceNeId, applianceCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.enable);
                assert.equal(1, data.response.hold_down_count);
                assert.equal(7, data.response.interval);
                assert.equal(7, data.response.retry_count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'wanNextHopHealthGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceGetOne - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applianceGetOne(applianceNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.IP);
                assert.equal(8, data.response.applianceId);
                assert.equal(true, data.response.bypass);
                assert.equal(2, data.response.discoveredFrom);
                assert.equal('string', data.response.dynamicUuid);
                assert.equal('string', data.response.groupId);
                assert.equal('string', data.response.hardwareRevision);
                assert.equal(true, data.response.hasUnsavedChanges);
                assert.equal('string', data.response.hostName);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.ip);
                assert.equal(8, data.response.latitude);
                assert.equal(2, data.response.longitude);
                assert.equal('string', data.response.mode);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.nePk);
                assert.equal('string', data.response.networkRole);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.portalObjectId);
                assert.equal(5, data.response.reachabilityChannel);
                assert.equal(true, data.response.rebootRequired);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.site);
                assert.equal(9, data.response.sitePriority);
                assert.equal('string', data.response.softwareVersion);
                assert.equal(3, data.response.startupTime);
                assert.equal(5, data.response.state);
                assert.equal(1, data.response.systemBandwidth);
                assert.equal('string', data.response.userName);
                assert.equal('string', data.response.uuid);
                assert.equal('string', data.response.webProtocol);
                assert.equal(5, data.response.webProtocolType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'applianceGetOne', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceResyncResyncApplianceBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#resyncAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.resyncAppliance(applianceResyncResyncApplianceBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplianceResync', 'resyncAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const appliancesSoftwareVersionsNePK = 'fakedata';
    const appliancesSoftwareVersionsCached = true;
    describe('#appliancesSoftwareVersions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.appliancesSoftwareVersions(appliancesSoftwareVersionsNePK, appliancesSoftwareVersionsCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AppliancesSoftwareVersions', 'appliancesSoftwareVersions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'applicationGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#builtin - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.builtin((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'builtin', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userDefined - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userDefined((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'userDefined', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationNeId = 'fakedata';
    const applicationCached = true;
    describe('#userDefinedConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userDefinedConfig(applicationNeId, applicationCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<priorityId>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Application', 'userDefinedConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionAppTagsPostBodyParam = {
      '<userDefinedGroupName>': {
        apps: [
          'string'
        ],
        parentGroup: [
          'string'
        ]
      }
    };
    describe('#appTagsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appTagsPost(applicationDefinitionAppTagsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'appTagsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionAppGroupSearchPostBodyParam = {
      pattern: 'string'
    };
    describe('#appGroupSearchPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appGroupSearchPost(applicationDefinitionAppGroupSearchPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'appGroupSearchPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionMode = 'fakedata';
    describe('#appTagsModePost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appTagsModePost(applicationDefinitionMode, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'appTagsModePost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionAppSearchPostBodyParam = {
      pattern: 'string'
    };
    describe('#appSearchPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appSearchPost(applicationDefinitionAppSearchPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'appSearchPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionCompoundReorderPostBodyParam = [
      5
    ];
    describe('#compoundReorderPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.compoundReorderPost(applicationDefinitionCompoundReorderPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'compoundReorderPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionId = 555;
    const applicationDefinitionCompoundPostBodyParam = {
      confidence: 10,
      description: 'string',
      disabled: true,
      dscp: 'string',
      dst_dns: 'string',
      dst_geo: 'string',
      dst_ip: 'string',
      dst_port: 'string',
      dst_service: 'string',
      either_dns: 'string',
      either_geo: 'string',
      either_ip: 'string',
      either_port: 'string',
      either_service: 'string',
      id: 2,
      name: 'string',
      protocol: 'string',
      src_dns: 'string',
      src_geo: 'string',
      src_ip: 'string',
      src_port: 'string',
      src_service: 'string',
      vlan: 'string'
    };
    describe('#compoundPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.compoundPost(applicationDefinitionId, applicationDefinitionCompoundPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'compoundPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionDomain = 'fakedata';
    const applicationDefinitionDnsPostBodyParam = {
      description: 'string',
      disabled: false,
      domain: 'string',
      name: 'string',
      priority: 7
    };
    describe('#dnsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dnsPost(applicationDefinitionDomain, applicationDefinitionDnsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'dnsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionIpStart = 555;
    const applicationDefinitionIpEnd = 555;
    const applicationDefinitionIpIntelligencePostBodyParam = {
      country: 'string',
      country_code: 'string',
      description: 'string',
      name: 'string',
      org: 'string',
      priority: 10
    };
    describe('#ipIntelligencePost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ipIntelligencePost(applicationDefinitionIpStart, applicationDefinitionIpEnd, applicationDefinitionIpIntelligencePostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'ipIntelligencePost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionFlowType = 'fakedata';
    const applicationDefinitionMid = 555;
    const applicationDefinitionMeterFlowPostBodyParam = {
      description: 'string',
      disabled: true,
      flowType: 'string',
      mid: 9,
      name: 'string',
      priority: 7
    };
    describe('#meterFlowPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meterFlowPost(applicationDefinitionFlowType, applicationDefinitionMid, applicationDefinitionMeterFlowPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'meterFlowPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionPort = 555;
    const applicationDefinitionProtocol = 555;
    const applicationDefinitionPortProtocolPostBodyParam = {
      description: 'string',
      disabled: false,
      name: 'string',
      port: 7,
      priority: 2,
      protocol: 10
    };
    describe('#portProtocolPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.portProtocolPost(applicationDefinitionPort, applicationDefinitionProtocol, applicationDefinitionPortProtocolPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'portProtocolPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionPostApplicationDefinitionSaasClassificationIdBodyParam = {
      addresses: [
        {
          pingMethod: 'string',
          pingPort: 6,
          reachableIp: 'string',
          subnet: 'string'
        }
      ],
      application: 'string',
      domains: [
        'string'
      ],
      enabled: false,
      ports: [
        'string'
      ],
      processedAt: 'string',
      saasId: 2,
      threshold: 8
    };
    describe('#postApplicationDefinitionSaasClassificationId - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postApplicationDefinitionSaasClassificationId(applicationDefinitionId, applicationDefinitionPostApplicationDefinitionSaasClassificationIdBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'postApplicationDefinitionSaasClassificationId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationDefinitionResourceKey = 'fakedata';
    describe('#appTagsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appTagsGet(applicationDefinitionResourceKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'appTagsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#compoundGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.compoundGet(applicationDefinitionResourceKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'compoundGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dnsGet(applicationDefinitionResourceKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'dnsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipIntelligenceGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ipIntelligenceGet(applicationDefinitionResourceKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'ipIntelligenceGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#legacyUdaGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.legacyUdaGet((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'legacyUdaGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meterFlowGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meterFlowGet(applicationDefinitionResourceKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'meterFlowGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProtocolGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.portProtocolGet(applicationDefinitionResourceKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'portProtocolGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saasGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saasGet(applicationDefinitionResourceKey, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'saasGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationGroupsNeId = 'fakedata';
    const applicationGroupsCached = true;
    describe('#applicationGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationGroups(applicationGroupsNeId, applicationGroupsCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.citrix);
                assert.equal('object', typeof data.response.encrypted);
                assert.equal('object', typeof data.response.interactive);
                assert.equal('object', typeof data.response['real-time']);
                assert.equal('object', typeof data.response.replication);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationGroups', 'applicationGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applicationTrendsStartDate = 555;
    const applicationTrendsEndDate = 555;
    const applicationTrendsTraffic = 'fakedata';
    const applicationTrendsBound = 'fakedata';
    const applicationTrendsNePKList = 'fakedata';
    describe('#applicationTrends - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applicationTrends(applicationTrendsStartDate, applicationTrendsEndDate, applicationTrendsTraffic, applicationTrendsBound, applicationTrendsNePKList, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.applicationStatsOfAppliances);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationTrends', 'applicationTrends', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authRadiusTacacsNeId = 'fakedata';
    const authRadiusTacacsCached = true;
    describe('#auth - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.auth(authRadiusTacacsNeId, authRadiusTacacsCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.aaa);
                assert.equal('object', typeof data.response.radius);
                assert.equal('object', typeof data.response.tacacs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AuthRadiusTacacs', 'auth', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationUser = 'fakedata';
    const authenticationPassword = 'fakedata';
    describe('#authentication - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.authentication(authenticationUser, authenticationPassword, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'authentication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationPostAuthenticationLoginTokenBodyParam = {
      password: 'string',
      user: 'string'
    };
    describe('#postAuthenticationLoginToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAuthenticationLoginToken(authenticationPostAuthenticationLoginTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'postAuthenticationLoginToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationPostAuthenticationPasswordValidationBodyParam = {
      password: 'string',
      user: 'string'
    };
    describe('#postAuthenticationPasswordValidation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAuthenticationPasswordValidation(authenticationPostAuthenticationPasswordValidationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'postAuthenticationPasswordValidation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationUserAuthTypeBodyParam = {
      password: 'string',
      user: 'string'
    };
    describe('#userAuthType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userAuthType(authenticationUserAuthTypeBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.authType);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'userAuthType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const authenticationUserAuthTypeTokenBodyParam = {
      token: 'string'
    };
    describe('#userAuthTypeToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.userAuthTypeToken(authenticationUserAuthTypeTokenBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'userAuthTypeToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationLoginStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAuthenticationLoginStatus((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.isLoggedIn);
                assert.equal('string', data.response.user);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'getAuthenticationLoginStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationLogout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getAuthenticationLogout((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Authentication', 'getAuthenticationLogout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#avcMode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.avcMode((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.avc);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('AvcMode', 'avcMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bannersNeId = 'fakedata';
    const bannersCached = true;
    describe('#bannerInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bannerInfo(bannersNeId, bannersCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.issue);
                assert.equal('string', data.response.motd);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Banners', 'bannerInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bgpNeId = 'fakedata';
    describe('#getNeighbor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNeighbor(bgpNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.IpAddress1);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgp', 'getNeighbor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBGPSystemConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBGPSystemConfig(bgpNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(10, data.response.asn);
                assert.equal(true, data.response.enable);
                assert.equal(false, data.response.graceful_restart_en);
                assert.equal(4, data.response.max_restart_time);
                assert.equal(false, data.response.redist_ospf);
                assert.equal(8, data.response.redist_ospf_filter);
                assert.equal(true, data.response.remote_as_path_advertise);
                assert.equal('string', data.response.rtr_id);
                assert.equal(6, data.response.stale_path_time);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgp', 'getBGPSystemConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bGPState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bGPState(bgpNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.neighbor);
                assert.equal('object', typeof data.response.summary);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bgp', 'bGPState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const brandCustomizationTextCustomizationConfigPostBodyParam = {
      footer: {
        custom: 'string',
        useDefault: true
      }
    };
    describe('#textCustomizationConfigPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.textCustomizationConfigPost(brandCustomizationTextCustomizationConfigPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BrandCustomization', 'textCustomizationConfigPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const brandCustomizationType = 'fakedata';
    const brandCustomizationQqfile = 'fakedata';
    const brandCustomizationQqfileFormData = 'fakedata';
    describe('#imagePost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imagePost(brandCustomizationType, brandCustomizationQqfile, brandCustomizationQqfileFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BrandCustomization', 'imagePost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const brandCustomizationTextCustomizationConfigPutBodyParam = {
      footer: {
        custom: 'string',
        useDefault: true
      }
    };
    describe('#textCustomizationConfigPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.textCustomizationConfigPut(brandCustomizationTextCustomizationConfigPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BrandCustomization', 'textCustomizationConfigPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#textCustomizationConfigGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.textCustomizationConfigGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.footer);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BrandCustomization', 'textCustomizationConfigGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const brandCustomizationMetaData = true;
    describe('#fileNameGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.fileNameGet(brandCustomizationMetaData, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.backgroundImage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BrandCustomization', 'fileNameGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putBrandCustomizationImageType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putBrandCustomizationImageType(brandCustomizationType, brandCustomizationQqfile, brandCustomizationQqfileFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BrandCustomization', 'putBrandCustomizationImageType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const broadcastCliBroadcastCliPostBodyParam = {
      cmdList: [
        'string'
      ],
      neList: [
        'string'
      ]
    };
    describe('#broadcastCliPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.broadcastCliPost(broadcastCliBroadcastCliPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BroadcastCli', 'broadcastCliPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bypassBypassPostBodyParam = {
      enable: false,
      neList: [
        'string'
      ]
    };
    describe('#bypassPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.bypassPost(bypassBypassPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bypass', 'bypassPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const bypassNePk = 'fakedata';
    const bypassCached = true;
    describe('#bypassGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.bypassGet(bypassNePk, bypassCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.bypass_actual);
                assert.equal(false, data.response.bypass_config);
                assert.equal('BYPASS', data.response.status);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Bypass', 'bypassGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#builtinAppGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.builtinAppGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response['<appName&Type>']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cache', 'builtinAppGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interfaceGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.interfaceGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<NameOfTheInterface1>']);
                assert.equal('object', typeof data.response['NameOfTheInterface2>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cache', 'interfaceGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userAppGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.userAppGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response['<appName&Type>']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Cache', 'userAppGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const cloudApplicationsNeId = 'fakedata';
    const cloudApplicationsCached = true;
    describe('#cloudAppsConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloudAppsConfig(cloudApplicationsNeId, cloudApplicationsCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.application);
                assert.equal(false, data.response.enable);
                assert.equal(4, data.response.num_pings);
                assert.equal('string', data.response.ping_src_intf);
                assert.equal(3, data.response.rtt_interval);
                assert.equal(true, data.response.user_modifed);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudApplications', 'cloudAppsConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloudAppsMonitor - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.cloudAppsMonitor(cloudApplicationsNeId, cloudApplicationsCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.domains));
                assert.equal(true, Array.isArray(data.response.subnets));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('CloudApplications', 'cloudAppsMonitor', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const configReportDiskNeId = 'fakedata';
    describe('#disks - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.disks(configReportDiskNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.controller);
                assert.equal('string', data.response.diskImage);
                assert.equal('object', typeof data.response.disks);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ConfigReportDisk', 'disks', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dbPartitionTable = 'fakedata';
    const dbPartitionPartition = 'fakedata';
    describe('#dbPartition - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dbPartition(dbPartitionTable, dbPartitionPartition, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DbPartition', 'dbPartition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.cancel((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebugFiles', 'cancel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const debugFilesDeleteGMSDebugFileBodyParam = {
      fileName: 'string'
    };
    describe('#deleteGMSDebugFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGMSDebugFile(debugFilesDeleteGMSDebugFileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebugFiles', 'deleteGMSDebugFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const debugFilesNePk = 'fakedata';
    const debugFilesDeleteNeFileBodyParam = {
      group: 'string',
      local_filename: 'string'
    };
    describe('#deleteNeFile - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteNeFile(debugFilesNePk, debugFilesDeleteNeFileBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebugFiles', 'deleteNeFile', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const debugFilesSetProxyConfigBodyParam = {
      proxyHost: 'string',
      proxyPassword: 'string',
      proxyPort: 80,
      proxyUser: 'string',
      useProxy: false
    };
    describe('#setProxyConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setProxyConfig(debugFilesSetProxyConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebugFiles', 'setProxyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProxyConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getProxyConfig((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.proxyHost);
                assert.equal('string', data.response.proxyPassword);
                assert.equal(80, data.response.proxyPort);
                assert.equal('string', data.response.proxyUser);
                assert.equal(true, data.response.useProxy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebugFiles', 'getProxyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#debugFiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.debugFiles(debugFilesNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.debugDump));
                assert.equal(true, Array.isArray(data.response.debugFile));
                assert.equal(true, Array.isArray(data.response.log));
                assert.equal(true, Array.isArray(data.response.snapshots));
                assert.equal(true, Array.isArray(data.response.tcpDump));
                assert.equal(true, Array.isArray(data.response.techDump));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DebugFiles', 'debugFiles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentNePk = 'fakedata';
    const deploymentValidateDeploymentBodyParam = {
      dpRoutes: [
        {}
      ],
      modeIfs: [
        {}
      ],
      sysConfig: {}
    };
    describe('#validateDeployment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateDeployment(deploymentNePk, deploymentValidateDeploymentBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.err);
                assert.equal(true, data.response.rebootRequired);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployment', 'validateDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const deploymentDiscoveredApplianceId = 'fakedata';
    const deploymentValidateDiscoveredDeploymentBodyParam = {
      dpRoutes: [
        {}
      ],
      modeIfs: [
        {}
      ],
      sysConfig: {}
    };
    describe('#validateDiscoveredDeployment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateDiscoveredDeployment(deploymentDiscoveredApplianceId, deploymentValidateDiscoveredDeploymentBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.err);
                assert.equal(false, data.response.rebootRequired);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployment', 'validateDiscoveredDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceDeployment - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplianceDeployment(deploymentNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.dhcpFailover);
                assert.equal(true, Array.isArray(data.response.dpRoutes));
                assert.equal('object', typeof data.response.mgmtIfData);
                assert.equal(true, Array.isArray(data.response.modeIfs));
                assert.equal('string', data.response.scalars);
                assert.equal('object', typeof data.response.sysConfig);
                assert.equal('object', typeof data.response.vifs);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Deployment', 'getApplianceDeployment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpConfigDhcpConfigPostBodyParam = {
      dhcpFoSetting: {
        load_bal_max: 9,
        max_resp_delay: 3,
        max_unack_updates: 9,
        mclt: 6,
        my_ip: 'string',
        my_port: 6,
        peer_ip: 'string',
        peer_port: 2,
        role: 'secondary',
        split: 7
      },
      mode: 'string',
      relayConfig: {
        destDhcpServer: 'string',
        opt82: true,
        opt82Policy: 'string'
      },
      serverConfig: {
        applianceReservations: [
          {
            ifName: 'string',
            mask: 8,
            neId: 'string',
            startIp: 'string'
          }
        ],
        blockMask: 'string',
        blockStartIp: 'string',
        defaultGateway: 'string',
        defaultGatewayEnabled: true,
        defaultLease: 3,
        defaultSubnetMask: 'string',
        dnsServers: [
          'string'
        ],
        endIp: 'string',
        endOffset: 2,
        failover: true,
        localReservations: [
          {
            ifName: 'string',
            mask: 6,
            neId: 'string',
            startIp: 'string'
          }
        ],
        maxLease: 8,
        netBiosNodeType: 'string',
        netBiosServers: [
          'string'
        ],
        ntpServers: [
          'string'
        ],
        reservations: [
          {
            ifName: 'string',
            mask: 5,
            neId: 'string',
            startIp: 'string'
          }
        ],
        startIp: 'string',
        startOffset: 7
      }
    };
    describe('#dhcpConfigPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dhcpConfigPost(dhcpConfigDhcpConfigPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DhcpConfig', 'dhcpConfigPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpConfigDhcpReservationsPostBodyParam = {
      ifName: 'string',
      mask: 1,
      neId: 'string',
      startIp: 'string'
    };
    describe('#dhcpReservationsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dhcpReservationsPost(dhcpConfigDhcpReservationsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DhcpConfig', 'dhcpReservationsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpConfigDhcpReservationsGmsPostBodyParam = [
      {
        ifName: 'string',
        ip: 'string',
        mask: 3,
        neId: 'string',
        source: 'string'
      }
    ];
    describe('#dhcpReservationsGmsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dhcpReservationsGmsPost(dhcpConfigDhcpReservationsGmsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DhcpConfig', 'dhcpReservationsGmsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpConfigResetPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dhcpConfigResetPost((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DhcpConfig', 'dhcpConfigResetPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpConfigGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dhcpConfigGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.dhcpFoSetting);
                assert.equal('string', data.response.mode);
                assert.equal('object', typeof data.response.relayConfig);
                assert.equal('object', typeof data.response.serverConfig);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DhcpConfig', 'dhcpConfigGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dhcpConfigNeId = 'fakedata';
    const dhcpConfigCached = true;
    describe('#dhcpdLeases - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dhcpdLeases(dhcpConfigNeId, dhcpConfigCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<leaseIpAddress>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DhcpConfig', 'dhcpdLeases', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpReservationsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dhcpReservationsGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DhcpConfig', 'dhcpReservationsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpdFailoverStates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dhcpdFailoverStates(dhcpConfigNeId, dhcpConfigCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<failoverstate>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DhcpConfig', 'dhcpdFailoverStates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const dnsProxyNeId = 'fakedata';
    const dnsProxyCached = true;
    describe('#dnsProxyConfigGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dnsProxyConfigGet(dnsProxyNeId, dnsProxyCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.active);
                assert.equal('object', typeof data.response.domaingroup);
                assert.equal(false, data.response.enable);
                assert.equal('string', data.response.interface);
                assert.equal('object', typeof data.response.map);
                assert.equal('object', typeof data.response.profile);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DnsProxy', 'dnsProxyConfigGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let exceptionId = 'fakedata';
    const exceptionTunnelExceptionPostBodyParam = [
      {
        appliance_id_1: 'string',
        appliance_id_2: 'string',
        description: 'string',
        id: 2,
        interface_label_1: 'string',
        interface_label_2: 'string'
      }
    ];
    describe('#tunnelExceptionPost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.tunnelExceptionPost(exceptionTunnelExceptionPostBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.appliance_id_1);
                assert.equal('string', data.response.appliance_id_2);
                assert.equal('string', data.response.description);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.interface_label_1);
                assert.equal('string', data.response.interface_label_2);
              } else {
                runCommonAsserts(data, error);
              }
              exceptionId = data.response.id;
              saveMockData('Exception', 'tunnelExceptionPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionTunnelExceptionPutBodyParam = [
      {
        appliance_id_1: 'string',
        appliance_id_2: 'string',
        description: 'string',
        id: 8,
        interface_label_1: 'string',
        interface_label_2: 'string'
      }
    ];
    describe('#tunnelExceptionPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tunnelExceptionPut(exceptionTunnelExceptionPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Exception', 'tunnelExceptionPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelExceptionGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.tunnelExceptionGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.appliance_id_1);
                assert.equal('string', data.response.appliance_id_2);
                assert.equal('string', data.response.description);
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.interface_label_1);
                assert.equal('string', data.response.interface_label_2);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Exception', 'tunnelExceptionGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const exceptionTunnelExceptionSingleUpdateBodyParam = {
      appliance_id_1: 'string',
      appliance_id_2: 'string',
      description: 'string',
      id: 5,
      interface_label_1: 'string',
      interface_label_2: 'string'
    };
    describe('#tunnelExceptionSingleUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tunnelExceptionSingleUpdate(exceptionId, exceptionTunnelExceptionSingleUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Exception', 'tunnelExceptionSingleUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const fileCreationType = 'fakedata';
    const fileCreationQqfile = 'fakedata';
    const fileCreationQqfileFormData = 'fakedata';
    describe('#fileCreation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.fileCreation(fileCreationType, fileCreationQqfile, fileCreationQqfileFormData, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('FileCreation', 'fileCreation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowNeId = 'fakedata';
    const flowFlowsReclassificationBodyParam = {
      spIds: [
        8
      ]
    };
    describe('#flowsReclassification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.flowsReclassification(flowNeId, flowFlowsReclassificationBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(0, data.response.rc);
                assert.equal(true, Array.isArray(data.response.results));
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flow', 'flowsReclassification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowFlowsResetBodyParam = {
      spIds: [
        7
      ]
    };
    describe('#flowsReset - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.flowsReset(flowNeId, flowFlowsResetBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(-1, data.response.rc);
                assert.equal(true, Array.isArray(data.response.results));
                assert.equal('string', data.response.value);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flow', 'flowsReset', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const flowId = 555;
    const flowSeq = 555;
    describe('#flowBandwidthStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.flowBandwidthStats(flowNeId, flowId, flowSeq, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flow', 'flowBandwidthStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.flowDetails(flowNeId, flowId, flowSeq, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flow', 'flowDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowDetails2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.flowDetails2(flowNeId, flowId, flowSeq, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flow', 'flowDetails2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flows - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.flows(flowNeId, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.active);
                assert.equal('string', data.response.err_msg);
                assert.equal(true, Array.isArray(data.response.flows));
                assert.equal(10, data.response.flows_management);
                assert.equal(9, data.response.flows_optimized);
                assert.equal(8, data.response.flows_passthrough);
                assert.equal(9, data.response.flows_with_ignores);
                assert.equal(6, data.response.flows_with_issues);
                assert.equal('object', typeof data.response.inactive);
                assert.equal(8, data.response.inconsistent_flows);
                assert.equal(4, data.response.matched_flows);
                assert.equal(7, data.response.now);
                assert.equal(6, data.response.ret_code);
                assert.equal(1, data.response.returned_flows);
                assert.equal(1, data.response.stale_flows);
                assert.equal(1, data.response.total_flows);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Flow', 'flows', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let gmsId = 'fakedata';
    let gmsPreconfigId = 'fakedata';
    const gmsCreatePreconfigurationBodyParam = {
      autoApply: false,
      comment: 'string',
      configData: 'string',
      name: 'string',
      serialNum: 'string',
      tag: 'string'
    };
    describe('#createPreconfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.createPreconfiguration(gmsCreatePreconfigurationBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }
              gmsId = data.response.id;
              gmsPreconfigId = data.response.id;
              saveMockData('Gms', 'createPreconfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let gmsDiscoveredId = 'fakedata';
    const gmsGetMatchingPreconfigurationBodyParam = {
      serial: 'string',
      tag: 'string'
    };
    describe('#getMatchingPreconfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMatchingPreconfiguration(gmsGetMatchingPreconfigurationBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.autoApply);
                assert.equal('string', data.response.comment);
                assert.equal(true, data.response.completionStatus);
                assert.equal('string', data.response.configData);
                assert.equal(10, data.response.createdtime);
                assert.equal('string', data.response.discoveredId);
                assert.equal(10, data.response.endtime);
                assert.equal('string', data.response.guid);
                assert.equal(4, data.response.id);
                assert.equal(4, data.response.modifiedtime);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.nepk);
                assert.equal(true, Array.isArray(data.response.result));
                assert.equal('string', data.response.serialNum);
                assert.equal(8, data.response.starttime);
                assert.equal('string', data.response.tag);
                assert.equal(4, data.response.taskStatus);
              } else {
                runCommonAsserts(data, error);
              }
              gmsDiscoveredId = data.response.discoveredId;
              saveMockData('Gms', 'getMatchingPreconfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsPostGmsAppliancePreconfigurationValidateBodyParam = {
      autoApply: false,
      comment: 'string',
      configData: 'string',
      name: 'string',
      serialNum: 'string',
      tag: 'string'
    };
    describe('#postGmsAppliancePreconfigurationValidate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postGmsAppliancePreconfigurationValidate(gmsPostGmsAppliancePreconfigurationValidateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'postGmsAppliancePreconfigurationValidate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsModifyPreconfigurationBodyParam = {
      autoApply: true,
      comment: 'string',
      configData: 'string',
      name: 'string',
      serialNum: 'string',
      tag: 'string'
    };
    describe('#modifyPreconfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyPreconfiguration(gmsModifyPreconfigurationBodyParam, gmsPreconfigId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'modifyPreconfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyDiscoveredPreconfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applyDiscoveredPreconfiguration(gmsPreconfigId, gmsDiscoveredId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'applyDiscoveredPreconfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsNePk = 'fakedata';
    describe('#applyManagedPreconfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applyManagedPreconfiguration(gmsPreconfigId, gmsNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'applyManagedPreconfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsApplyApplianceWizardBodyParam = {
      wizardData: {
        applianceExtraInfo: {
          contact: {
            email: 'string',
            name: 'string',
            phoneNumber: 'string'
          },
          location: {
            address: 'string',
            address2: 'string',
            city: 'string',
            country: 'string',
            state: 'string',
            zipCode: 'string'
          },
          overlaySettings: {
            ipsecUdpPort: 'string',
            isUserDefinedIPSecUDPPort: false
          }
        },
        applianceImage: 'string',
        approve: {},
        coordinates: {
          latitude: 2,
          longitude: 1,
          wx: 5,
          wy: 4
        },
        deployment: {
          dpRoutes: [
            {
              intf: 'string',
              metric: 6,
              nexthop: 'string',
              prefix: 'string',
              type: 'string'
            }
          ],
          modeIfs: [
            {
              applianceIPs: [
                {
                  behindNAT: 'string',
                  brifs: {
                    '(ifname)': {
                      comment: 'string',
                      harden: 'string',
                      label: 'string',
                      lanSide: true,
                      wanSide: true
                    }
                  },
                  comment: 'string',
                  dhcp: false,
                  dhcpd: {
                    relay: {
                      dhcpserver: [
                        'string'
                      ],
                      option82: false,
                      option82_policy: 'string'
                    },
                    server: {
                      host: {
                        '(hostName)': {
                          ip: 'string',
                          mac: 'string'
                        }
                      },
                      defaultLease: 8,
                      dns: [
                        'string'
                      ],
                      failover: false,
                      gw: [
                        'string'
                      ],
                      ipEnd: 'string',
                      ipStart: 'string',
                      maxLease: 10,
                      netbios: [
                        'string'
                      ],
                      netbiosNodeType: 'string',
                      ntpd: [
                        'string'
                      ],
                      options: {
                        '(dhcpOptionId)': 'string'
                      },
                      prefix: 'string'
                    },
                    type: 'string'
                  },
                  harden: 10,
                  ip: 'string',
                  label: 'string',
                  lanSide: true,
                  mask: 6,
                  maxBW: {
                    inbound: 9,
                    outbound: 8
                  },
                  subif: 'string',
                  vlan: 'string',
                  wanNexthop: 'string',
                  wanSide: true,
                  zone: 9
                }
              ],
              devNum: 'string',
              ifName: 'string'
            }
          ],
          sysConfig: {
            bonding: true,
            haIf: 'string',
            ifLabels: {
              lan: [
                {
                  id: 6,
                  name: 'string'
                }
              ],
              wan: [
                {
                  id: 6,
                  name: 'string'
                }
              ]
            },
            inline: true,
            licence: {
              ecBoost: true,
              ecBoostBW: 9,
              ecMini: true,
              ecPlus: false,
              ecTier: 'string',
              ecTierBW: 8
            },
            maxBW: 10,
            maxInBW: 7,
            maxInBWEnabled: false,
            mode: 'router',
            propagateLinkDown: true,
            singleBridge: true,
            tenG: true,
            useMgmt0: true,
            zones: [
              'string'
            ]
          }
        },
        ecLicense: 'string',
        hostname: 'string',
        labels: {},
        overlayRegion: 'string',
        overlays: [
          6
        ],
        password: {
          password: 'string'
        },
        subnets: {
          data: {
            prefix: {
              'x.x.x.x/x': {
                advert: false,
                advert_bgp: true,
                exclude: false,
                nhop: {
                  'x.x.x.x': {
                    comment: 'string',
                    dir: 'string',
                    metric: 4
                  }
                }
              },
              'y.y.y.y/y': {
                advert: true,
                advert_bgp: true,
                exclude: true,
                nhop: {
                  'x.x.x.x': {
                    comment: 'string',
                    dir: 'string',
                    metric: 5
                  }
                }
              }
            }
          },
          options: {
            auto_subnet: {
              add_local: true,
              add_local_lan: false,
              add_local_wan: false,
              self: false
            }
          }
        },
        templates: [
          'string'
        ]
      }
    };
    describe('#applyApplianceWizard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applyApplianceWizard(gmsApplyApplianceWizardBodyParam, gmsNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.actionGroupId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'applyApplianceWizard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsGmsBackupConfigBodyParam = {
      directory: 'string',
      hostname: 'string',
      maxBackups: 10,
      password: 'string',
      port: 21,
      protocol: 2,
      username: 'string'
    };
    describe('#gmsBackupConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.gmsBackupConfig(gmsGmsBackupConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'gmsBackupConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsGmsBackupTestConnectionBodyParam = {
      directory: 'string',
      hostname: 'string',
      maxBackups: 10,
      password: 'string',
      port: 21,
      protocol: 2,
      username: 'string'
    };
    describe('#gmsBackupTestConnection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gmsBackupTestConnection(gmsGmsBackupTestConnectionBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.message);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'gmsBackupTestConnection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsUpdateDiscoveryConfigBodyParam = [
      'string'
    ];
    describe('#updateDiscoveryConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDiscoveryConfig(gmsUpdateDiscoveryConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'updateDiscoveryConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsUserName = 'fakedata';
    const gmsUpdateDynamicTopologyConfigBodyParam = {
      username: {}
    };
    describe('#updateDynamicTopologyConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateDynamicTopologyConfig(gmsUserName, gmsUpdateDynamicTopologyConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'updateDynamicTopologyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsUsername = 'fakedata';
    const gmsSaveDynamicTopologyConfigBodyParam = {
      username: {}
    };
    describe('#saveDynamicTopologyConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveDynamicTopologyConfig(gmsUsername, gmsSaveDynamicTopologyConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'saveDynamicTopologyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsMgmInterfaceByPostBodyParam = {
      defaultIPOrDns: 'string'
    };
    describe('#mgmInterfaceByPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.mgmInterfaceByPost(gmsMgmInterfaceByPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'mgmInterfaceByPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsPostGmsGmsRegistration2BodyParam = {
      customDefaultIP: 'string'
    };
    describe('#postGmsGmsRegistration2 - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postGmsGmsRegistration2(gmsPostGmsGmsRegistration2BodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'postGmsGmsRegistration2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsGrNodeUpdateByNePkBodyParam = {
      latitude: 3,
      longitude: 9,
      wx: 5,
      wy: 4
    };
    describe('#grNodeUpdateByNePk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.grNodeUpdateByNePk(gmsNePk, gmsGrNodeUpdateByNePkBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'grNodeUpdateByNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsGrNodePk = 'fakedata';
    const gmsGrNodeUpdateBodyParam = {
      latitude: 2,
      longitude: 3,
      wx: 10,
      wy: 10
    };
    describe('#grNodeUpdate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.grNodeUpdate(gmsGrNodePk, gmsGrNodeUpdateBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.appliance);
                assert.equal('string', data.response.groupId);
                assert.equal('string', data.response.id);
                assert.equal(9, data.response.latitude);
                assert.equal(3, data.response.longitude);
                assert.equal('string', data.response.sourceId);
                assert.equal(4, data.response.wx);
                assert.equal(5, data.response.wy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'grNodeUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsGroupAddBodyParam = {
      name: 'string',
      parentId: 'string'
    };
    describe('#groupAdd - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupAdd(gmsGroupAddBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'groupAdd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsGroupUpdateBodyParam = {
      id: 'string',
      name: 'string'
    };
    describe('#groupUpdate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupUpdate(gmsId, gmsGroupUpdateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'groupUpdate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsHttpsCertificateValidationBodyParam = {
      certificateData: 'string',
      keyData: 'string'
    };
    describe('#httpsCertificateValidation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.httpsCertificateValidation(gmsHttpsCertificateValidationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'httpsCertificateValidation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsPostGmsHttpsCertificateValidationBodyParam = {
      certificateData: 'string',
      keyData: 'string'
    };
    describe('#postGmsHttpsCertificateValidation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postGmsHttpsCertificateValidation(gmsPostGmsHttpsCertificateValidationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'postGmsHttpsCertificateValidation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsPostInterfaceLabelsBodyParam = {
      lan: {
        1: {
          active: true,
          name: 'string',
          topology: 3
        },
        2: {
          active: true,
          name: 'string',
          topology: 1
        }
      },
      wan: {
        1: {
          active: true,
          name: 'string',
          topology: 9
        },
        2: {
          active: false,
          name: 'string',
          topology: 8
        }
      }
    };
    describe('#postInterfaceLabels - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postInterfaceLabels(gmsPostInterfaceLabelsBodyParam, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'postInterfaceLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSaveInternalSubnetsBodyParam = {};
    describe('#saveInternalSubnets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveInternalSubnets(gmsSaveInternalSubnetsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'saveInternalSubnets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSetExternalIPWhitelistBodyParam = [
      'string'
    ];
    describe('#setExternalIPWhitelist - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setExternalIPWhitelist(gmsSetExternalIPWhitelistBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'setExternalIPWhitelist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsScheduledJob2PostBodyParam = {
      config: 'string',
      description: 'string',
      jobCategory: 'string',
      schedule: 'string'
    };
    describe('#scheduledJob2Post - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.scheduledJob2Post(gmsScheduledJob2PostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'scheduledJob2Post', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsJobId = 'fakedata';
    describe('#stopJob - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.stopJob(gmsJobId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'stopJob', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsAddMenuTypeBodyParam = {
      menuTypeName: {}
    };
    describe('#addMenuType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addMenuType(gmsAddMenuTypeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'addMenuType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsAddOverlayApplianceAssociationsBodyParam = {
      1: [
        'string'
      ],
      2: [
        'string'
      ]
    };
    describe('#addOverlayApplianceAssociations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addOverlayApplianceAssociations(gmsAddOverlayApplianceAssociationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'addOverlayApplianceAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsRemoveAppliancesFromOverlaysBodyParam = {
      1: [
        'string'
      ],
      2: [
        'string'
      ]
    };
    describe('#removeAppliancesFromOverlays - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.removeAppliancesFromOverlays(gmsRemoveAppliancesFromOverlaysBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'removeAppliancesFromOverlays', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsAddNewOverlayBodyParam = {
      bondingPolicy: 10,
      boostTraffic: false,
      brownoutThresholds: {
        jitter: 7,
        latency: 8,
        loss: 7
      },
      hubInternetPolicies: {
        '<nePk>': {
          internetPolicy: {
            localBreakout: {
              backup: [
                'string'
              ],
              primary: [
                'string'
              ]
            },
            policyList: [
              'string'
            ]
          }
        }
      },
      internetPolicy: {
        localBreakout: {
          backup: [
            'string'
          ],
          primary: [
            'string'
          ]
        },
        policyList: [
          'string'
        ]
      },
      lanDscp: 'string',
      match: {
        acl: 'string',
        interfaceLabel: 'string',
        overlayAcl: 'string'
      },
      name: 'string',
      overlayFallbackOption: 'string',
      topology: {
        externalHubs: [
          'string'
        ],
        hubs: [
          'string'
        ],
        topologyType: 6,
        useRegions: false
      },
      trafficClass: 'string',
      tunnelUsagePolicy: 'string',
      useBackupOnBrownout: false,
      wanDscp: 'string',
      wanPorts: {
        backup: [
          'string'
        ],
        crossConnect: [
          'string'
        ],
        primary: [
          'string'
        ]
      }
    };
    describe('#addNewOverlay - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addNewOverlay(gmsAddNewOverlayBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'addNewOverlay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsPostAllRegionalOverlaysBodyParam = {
      '<overlayId>': {
        '<regionId>': {
          bondingPolicy: 6,
          boostTraffic: false,
          brownoutThresholds: {
            jitter: 4,
            latency: 3,
            loss: 5
          },
          hubInternetPolicies: {
            '<nePk>': {
              internetPolicy: {
                localBreakout: {
                  backup: [
                    'string'
                  ],
                  primary: [
                    'string'
                  ]
                },
                policyList: [
                  'string'
                ]
              }
            }
          },
          id: 'string',
          internetPolicy: {
            localBreakout: {
              backup: [
                'string'
              ],
              primary: [
                'string'
              ]
            },
            policyList: [
              'string'
            ]
          },
          lanDscp: 'string',
          match: {
            acl: 'string',
            interfaceLabel: 'string',
            overlayAcl: 'string'
          },
          name: 'string',
          overlayFallbackOption: 3,
          topology: {
            externalHubs: [
              'string'
            ],
            hubs: [
              'string'
            ],
            topologyType: 3,
            useRegions: false
          },
          trafficClass: 'string',
          tunnelUsagePolicy: 'string',
          useBackupOnBrownout: true,
          wanDscp: 'string',
          wanPorts: {
            backup: [
              'string'
            ],
            crossConnect: [
              'string'
            ],
            primary: [
              'string'
            ]
          }
        }
      }
    };
    describe('#postAllRegionalOverlays - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAllRegionalOverlays(gmsPostAllRegionalOverlaysBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'postAllRegionalOverlays', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSaveOverlayPriorityBodyParam = {
      1: 9,
      2: 8
    };
    describe('#saveOverlayPriority - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveOverlayPriority(gmsSaveOverlayPriorityBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'saveOverlayPriority', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsUpdateScheduleTimezoneBodyParam = {};
    describe('#updateScheduleTimezone - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateScheduleTimezone(gmsUpdateScheduleTimezoneBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'updateScheduleTimezone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSaveServicesBodyParam = {
      service_1: {
        name: 'string'
      },
      service_2: {
        name: 'string'
      }
    };
    describe('#saveServices - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveServices(gmsSaveServicesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'saveServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsPostGmsStatsCollectionBodyParam = {
      Application: false,
      Dns: false,
      Drc: false,
      Drops: true,
      Dscp: false,
      Flow: true,
      Interface: true,
      Jitter: false,
      Port: true,
      Shaper: false,
      TopTalkers: true,
      Tunnel: false
    };
    describe('#postGmsStatsCollection - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postGmsStatsCollection(gmsPostGmsStatsCollectionBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'postGmsStatsCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsUpdateTopologyConfigBodyParam = {
      username: {}
    };
    describe('#updateTopologyConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateTopologyConfig(gmsUserName, gmsUpdateTopologyConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'updateTopologyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSaveMapImageBodyParam = {
      image: 'string'
    };
    describe('#saveMapImage - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveMapImage(gmsSaveMapImageBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'saveMapImage', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSaveTopologyConfigBodyParam = {
      username: {}
    };
    describe('#saveTopologyConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveTopologyConfig(gmsUsername, gmsSaveTopologyConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'saveTopologyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsAddTunnelGroupApplianceAssociationsBodyParam = {
      1: [
        'string'
      ],
      2: [
        'string'
      ]
    };
    describe('#addTunnelGroupApplianceAssociations - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addTunnelGroupApplianceAssociations(gmsAddTunnelGroupApplianceAssociationsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'addTunnelGroupApplianceAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsAddNewTunnelGroupBodyParam = {
      crossConnect: false,
      name: 'string',
      topology: {
        externalHubs: [
          'string'
        ],
        hubs: [
          'string'
        ],
        topologyType: 7,
        useRegions: false
      },
      useAllAvailableInterfaces: false,
      wanPorts: [
        'string'
      ]
    };
    describe('#addNewTunnelGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.addNewTunnelGroup(gmsAddNewTunnelGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'addNewTunnelGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSetTunnelGroupPropertiesBodyParam = {
      enable: false
    };
    describe('#setTunnelGroupProperties - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setTunnelGroupProperties(gmsSetTunnelGroupPropertiesBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'setTunnelGroupProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsPostAdvPropBodyParam = {
      ContentSecurityPolicyHeaderEnabled: false,
      MultipleOrchestratorsForOneZscalerAccount: true,
      ParallelActionTasks: 8,
      ParallelOrchestrationTasks: 1,
      ParallelReachabilityTasks: 10,
      ParallelStatsTasks: 7,
      bondedTunnelReorderWaitTime: 8,
      bridgeCacheExpireTime: 5,
      dbPoolConnectionTimeout: 2,
      dbPoolIdleTimeout: 1,
      dbPoolLeakDetectionThreshold: 10,
      dbPoolMaxConnectionLifeTime: 4,
      dbPoolMaxConnections: 4,
      dbPoolMinimumIdleConnections: 6,
      dbPoolValidationTimeout: 7,
      denyApplianceOnDelete: true,
      emailImagesMaxSize: 5,
      excludeTables: false,
      excludedTableNames: 'string',
      failedLoginAttemptThreshold: 4,
      fileOpsChunkSize: 9,
      jettyAcceptQueueSize: 9,
      jettyIdleTimeout: 8,
      jettyMaxThreads: 6,
      jettyMinThreads: 5,
      mgmtInterface: 'string',
      modifyTunnelBatchSize: 5,
      newSoftwareReleasesNotification: true,
      restRequestStatsCollection: true,
      restRequestTimeout: 3,
      sslExcludeCiphers: 'string',
      sslExcludeProtocols: 'string',
      sslIncludeCiphers: 'string',
      sslIncludeProtocols: 'string',
      threadPoolSize: 3
    };
    describe('#postAdvProp - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postAdvProp(gmsPostAdvPropBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'postAdvProp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdvProp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAdvProp((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.ContentSecurityPolicyHeaderEnabled);
                assert.equal(true, data.response.MultipleOrchestratorsForOneZscalerAccount);
                assert.equal(5, data.response.ParallelActionTasks);
                assert.equal(9, data.response.ParallelOrchestrationTasks);
                assert.equal(8, data.response.ParallelReachabilityTasks);
                assert.equal(2, data.response.ParallelStatsTasks);
                assert.equal(10, data.response.bondedTunnelReorderWaitTime);
                assert.equal(1, data.response.bridgeCacheExpireTime);
                assert.equal(7, data.response.dbPoolConnectionTimeout);
                assert.equal(5, data.response.dbPoolIdleTimeout);
                assert.equal(1, data.response.dbPoolLeakDetectionThreshold);
                assert.equal(2, data.response.dbPoolMaxConnectionLifeTime);
                assert.equal(3, data.response.dbPoolMaxConnections);
                assert.equal(10, data.response.dbPoolMinimumIdleConnections);
                assert.equal(8, data.response.dbPoolValidationTimeout);
                assert.equal(false, data.response.denyApplianceOnDelete);
                assert.equal(2, data.response.emailImagesMaxSize);
                assert.equal(false, data.response.excludeTables);
                assert.equal('string', data.response.excludedTableNames);
                assert.equal(4, data.response.failedLoginAttemptThreshold);
                assert.equal(3, data.response.fileOpsChunkSize);
                assert.equal(1, data.response.jettyAcceptQueueSize);
                assert.equal(7, data.response.jettyIdleTimeout);
                assert.equal(3, data.response.jettyMaxThreads);
                assert.equal(3, data.response.jettyMinThreads);
                assert.equal('string', data.response.mgmtInterface);
                assert.equal(2, data.response.modifyTunnelBatchSize);
                assert.equal(true, data.response.newSoftwareReleasesNotification);
                assert.equal(false, data.response.restRequestStatsCollection);
                assert.equal(6, data.response.restRequestTimeout);
                assert.equal('string', data.response.sslExcludeCiphers);
                assert.equal('string', data.response.sslExcludeProtocols);
                assert.equal('string', data.response.sslIncludeCiphers);
                assert.equal('string', data.response.sslIncludeProtocols);
                assert.equal(2, data.response.threadPoolSize);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getAdvProp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGmsAdvancedPropertiesMetadata - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGmsAdvancedPropertiesMetadata((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.ContentSecurityPolicyHeaderEnabled);
                assert.equal('object', typeof data.response.MultipleOrchestratorsForOneZscalerAccount);
                assert.equal('object', typeof data.response.ParallelActionTasks);
                assert.equal('object', typeof data.response.ParallelOrchestrationTasks);
                assert.equal('object', typeof data.response.ParallelReachabilityTasks);
                assert.equal('object', typeof data.response.ParallelStatsTasks);
                assert.equal('object', typeof data.response.bondedTunnelReorderWaitTime);
                assert.equal('object', typeof data.response.bridgeCacheExpireTime);
                assert.equal('object', typeof data.response.dbPoolConnectionTimeout);
                assert.equal('object', typeof data.response.dbPoolIdleTimeout);
                assert.equal('object', typeof data.response.dbPoolLeakDetectionThreshold);
                assert.equal('object', typeof data.response.dbPoolMaxConnectionLifeTime);
                assert.equal('object', typeof data.response.dbPoolMaxConnections);
                assert.equal('object', typeof data.response.dbPoolMinimumIdleConnections);
                assert.equal('object', typeof data.response.dbPoolValidationTimeout);
                assert.equal('object', typeof data.response.denyApplianceOnDelete);
                assert.equal('object', typeof data.response.emailImagesMaxSize);
                assert.equal('object', typeof data.response.excludeTables);
                assert.equal('object', typeof data.response.excludedTableNames);
                assert.equal('object', typeof data.response.failedLoginAttemptThreshold);
                assert.equal('object', typeof data.response.fileOpsChunkSize);
                assert.equal('object', typeof data.response.jettyAcceptQueueSize);
                assert.equal('object', typeof data.response.jettyIdleTimeout);
                assert.equal('object', typeof data.response.jettyMaxThreads);
                assert.equal('object', typeof data.response.jettyMinThreads);
                assert.equal('object', typeof data.response.mgmtInterface);
                assert.equal('object', typeof data.response.modifyTunnelBatchSize);
                assert.equal('object', typeof data.response.newSoftwareReleasesNotification);
                assert.equal('object', typeof data.response.restRequestStatsCollection);
                assert.equal('object', typeof data.response.restRequestTimeout);
                assert.equal('object', typeof data.response.sslExcludeCiphers);
                assert.equal('object', typeof data.response.sslExcludeProtocols);
                assert.equal('object', typeof data.response.sslIncludeCiphers);
                assert.equal('object', typeof data.response.sslIncludeProtocols);
                assert.equal('object', typeof data.response.threadPoolSize);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getGmsAdvancedPropertiesMetadata', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPreconfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPreconfigurations(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getPreconfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultPreconfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDefaultPreconfigurations((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getDefaultPreconfigurations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPreconfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPreconfiguration(gmsPreconfigId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.autoApply);
                assert.equal('string', data.response.comment);
                assert.equal(true, data.response.completionStatus);
                assert.equal('string', data.response.configData);
                assert.equal(9, data.response.createdtime);
                assert.equal('string', data.response.discoveredId);
                assert.equal(10, data.response.endtime);
                assert.equal('string', data.response.guid);
                assert.equal(7, data.response.id);
                assert.equal(10, data.response.modifiedtime);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.nepk);
                assert.equal(true, Array.isArray(data.response.result));
                assert.equal('string', data.response.serialNum);
                assert.equal(2, data.response.starttime);
                assert.equal('string', data.response.tag);
                assert.equal(2, data.response.taskStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getPreconfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPreconfigurationApplyStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPreconfigurationApplyStatus(gmsPreconfigId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.completionStatus);
                assert.equal(3, data.response.endtime);
                assert.equal('string', data.response.guid);
                assert.equal(6, data.response.id);
                assert.equal(true, Array.isArray(data.response.result));
                assert.equal(1, data.response.starttime);
                assert.equal(6, data.response.taskStatus);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getPreconfigurationApplyStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#crashHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.crashHistory(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'crashHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebootHistory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rebootHistory(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'rebootHistory', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplyApplianceWizard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplyApplianceWizard(gmsNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.actionGroupId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getApplyApplianceWizard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsBackupGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gmsBackupGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.directory);
                assert.equal('string', data.response.hostname);
                assert.equal(10, data.response.maxBackups);
                assert.equal('string', data.response.password);
                assert.equal(21, data.response.port);
                assert.equal(2, data.response.protocol);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'gmsBackupGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsMode = 'fakedata';
    const gmsDownload = 'fakedata';
    describe('#goldenOrchestratorExport - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.goldenOrchestratorExport(gmsMode, gmsDownload, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'goldenOrchestratorExport', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDiscoveryConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDiscoveryConfig((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.emails));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getDiscoveryConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDynamicTopologyConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDynamicTopologyConfig(gmsUserName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getDynamicTopologyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserDynamicTopologyConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserDynamicTopologyConfig(gmsUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getUserDynamicTopologyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mgmInterfaceByGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.mgmInterfaceByGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.defaultIPOrDns);
                assert.equal('object', typeof data.response.label);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'mgmInterfaceByGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGmsGmsRegistration2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGmsGmsRegistration2((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.customDefaultIP);
                assert.equal('string', data.response.internalManagementIP);
                assert.equal('object', typeof data.response.label);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getGmsGmsRegistration2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allGRNodeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allGRNodeGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'allGRNodeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#grNodeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.grNodeGet(gmsGrNodePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.appliance);
                assert.equal('string', data.response.groupId);
                assert.equal('string', data.response.id);
                assert.equal(2, data.response.latitude);
                assert.equal(8, data.response.longitude);
                assert.equal('string', data.response.sourceId);
                assert.equal(10, data.response.wx);
                assert.equal(5, data.response.wy);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'grNodeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allGroupsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.allGroupsGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'allGroupsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rootGroupGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rootGroupGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.backgroundImage);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.parentId);
                assert.equal(8, data.response.subType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'rootGroupGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.groupGet(gmsId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.backgroundImage);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.parentId);
                assert.equal(2, data.response.subType);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'groupGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceLabels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaceLabels(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.lan);
                assert.equal('object', typeof data.response.wan);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getInterfaceLabels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsType = 'fakedata';
    describe('#getLabelsForType - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLabelsForType(gmsType, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['1']);
                assert.equal('object', typeof data.response['2']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getLabelsForType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternalSubnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInternalSubnets((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.ipv4));
                assert.equal(true, Array.isArray(data.response.ipv6));
                assert.equal(true, data.response.nonDefaultRoutes);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getInternalSubnets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPWhitelistDrops - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIPWhitelistDrops((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getIPWhitelistDrops', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalIPWhitelist - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getExternalIPWhitelist((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getExternalIPWhitelist', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduledJobsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.scheduledJobsGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'scheduledJobsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#historicalJobsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.historicalJobsGet(null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'historicalJobsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#historicalJobsGetById - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.historicalJobsGetById(gmsJobId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(3, data.response.assocId);
                assert.equal('string', data.response.config);
                assert.equal('string', data.response.description);
                assert.equal(6, data.response.endTime);
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.jobCategory);
                assert.equal(7, data.response.jobType);
                assert.equal(6, data.response.startTime);
                assert.equal(6, data.response.status);
                assert.equal(7, data.response.statusText);
                assert.equal('object', typeof data.response.targetAppliance);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'historicalJobsGetById', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsScheduledJobModifyBodyParam = {
      config: 'string',
      description: 'string',
      jobCategory: 'string',
      schedule: 'string'
    };
    describe('#scheduledJobModify - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.scheduledJobModify(gmsJobId, gmsScheduledJobModifyBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'scheduledJobModify', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduledJob2Get - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.scheduledJob2Get(gmsJobId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'scheduledJob2Get', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsUpdateMenuTypeItemsBodyParam = {
      menuTypeName: {}
    };
    describe('#updateMenuTypeItems - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateMenuTypeItems(gmsUpdateMenuTypeItemsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'updateMenuTypeItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let gmsMenuTypeName = 'fakedata';
    describe('#getAllMenuItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllMenuItems((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.menuTypeName);
              } else {
                runCommonAsserts(data, error);
              }
              gmsMenuTypeName = data.response.menuTypeName;
              saveMockData('Gms', 'getAllMenuItems', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOverlayApplianceAssociations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllOverlayApplianceAssociations((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response['1']));
                assert.equal(true, Array.isArray(data.response['2']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getAllOverlayApplianceAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsOverlayId = 'fakedata';
    describe('#getAppliancesForOverlay - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliancesForOverlay(gmsOverlayId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response['1']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getAppliancesForOverlay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOverlays - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllOverlays((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getAllOverlays', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaxNumberOfOverlays - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMaxNumberOfOverlays((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(1, data.response.max);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getMaxNumberOfOverlays', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRegionalOverlays - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRegionalOverlays((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<overlayId>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getAllRegionalOverlays', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsRegionId = 'fakedata';
    const gmsModifyRegionalOverlayBodyParam = {
      bondingPolicy: 2,
      boostTraffic: true,
      brownoutThresholds: {
        jitter: 6,
        latency: 2,
        loss: 9
      },
      hubInternetPolicies: {
        '<nePk>': {
          internetPolicy: {
            localBreakout: {
              backup: [
                'string'
              ],
              primary: [
                'string'
              ]
            },
            policyList: [
              'string'
            ]
          }
        }
      },
      id: 'string',
      internetPolicy: {
        localBreakout: {
          backup: [
            'string'
          ],
          primary: [
            'string'
          ]
        },
        policyList: [
          'string'
        ]
      },
      lanDscp: 'string',
      match: {
        acl: 'string',
        interfaceLabel: 'string',
        overlayAcl: 'string'
      },
      name: 'string',
      overlayFallbackOption: 3,
      topology: {
        externalHubs: [
          'string'
        ],
        hubs: [
          'string'
        ],
        topologyType: 1,
        useRegions: true
      },
      trafficClass: 'string',
      tunnelUsagePolicy: 'string',
      useBackupOnBrownout: false,
      wanDscp: 'string',
      wanPorts: {
        backup: [
          'string'
        ],
        crossConnect: [
          'string'
        ],
        primary: [
          'string'
        ]
      }
    };
    describe('#modifyRegionalOverlay - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.modifyRegionalOverlay(gmsOverlayId, gmsRegionId, gmsModifyRegionalOverlayBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'modifyRegionalOverlay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRegionalOverlay - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRegionalOverlay(gmsOverlayId, gmsRegionId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(6, data.response.bondingPolicy);
                assert.equal(true, data.response.boostTraffic);
                assert.equal('object', typeof data.response.brownoutThresholds);
                assert.equal('object', typeof data.response.hubInternetPolicies);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.internetPolicy);
                assert.equal('string', data.response.lanDscp);
                assert.equal('object', typeof data.response.match);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.overlayFallbackOption);
                assert.equal('object', typeof data.response.topology);
                assert.equal('string', data.response.trafficClass);
                assert.equal('string', data.response.tunnelUsagePolicy);
                assert.equal(false, data.response.useBackupOnBrownout);
                assert.equal('string', data.response.wanDscp);
                assert.equal('object', typeof data.response.wanPorts);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getRegionalOverlay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsUpdateExistingOverlayBodyParam = {
      bondingPolicy: 1,
      boostTraffic: true,
      brownoutThresholds: {
        jitter: 5,
        latency: 7,
        loss: 9
      },
      hubInternetPolicies: {
        '<nePk>': {
          internetPolicy: {
            localBreakout: {
              backup: [
                'string'
              ],
              primary: [
                'string'
              ]
            },
            policyList: [
              'string'
            ]
          }
        }
      },
      id: 'string',
      internetPolicy: {
        localBreakout: {
          backup: [
            'string'
          ],
          primary: [
            'string'
          ]
        },
        policyList: [
          'string'
        ]
      },
      lanDscp: 'string',
      match: {
        acl: 'string',
        interfaceLabel: 'string',
        overlayAcl: 'string'
      },
      name: 'string',
      overlayFallbackOption: 8,
      topology: {
        externalHubs: [
          'string'
        ],
        hubs: [
          'string'
        ],
        topologyType: 1,
        useRegions: true
      },
      trafficClass: 'string',
      tunnelUsagePolicy: 'string',
      useBackupOnBrownout: false,
      wanDscp: 'string',
      wanPorts: {
        backup: [
          'string'
        ],
        crossConnect: [
          'string'
        ],
        primary: [
          'string'
        ]
      }
    };
    describe('#updateExistingOverlay - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateExistingOverlay(gmsOverlayId, gmsUpdateExistingOverlayBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'updateExistingOverlay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOverlay - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOverlay(gmsOverlayId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(9, data.response.bondingPolicy);
                assert.equal(false, data.response.boostTraffic);
                assert.equal('object', typeof data.response.brownoutThresholds);
                assert.equal('object', typeof data.response.hubInternetPolicies);
                assert.equal('string', data.response.id);
                assert.equal('object', typeof data.response.internetPolicy);
                assert.equal('string', data.response.lanDscp);
                assert.equal('object', typeof data.response.match);
                assert.equal('string', data.response.name);
                assert.equal(10, data.response.overlayFallbackOption);
                assert.equal('object', typeof data.response.topology);
                assert.equal('string', data.response.trafficClass);
                assert.equal('string', data.response.tunnelUsagePolicy);
                assert.equal(false, data.response.useBackupOnBrownout);
                assert.equal('string', data.response.wanDscp);
                assert.equal('object', typeof data.response.wanPorts);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getOverlay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOverlayPriorityMap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOverlayPriorityMap((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(3, data.response['1']);
                assert.equal(2, data.response['2']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getOverlayPriorityMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleTimezone - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getScheduleTimezone((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.defaultTimezone);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getScheduleTimezone', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getServices((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.service_1);
                assert.equal('object', typeof data.response.service_2);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsPutGmsSessionTimeoutBodyParam = {
      autoLogout: 2
    };
    describe('#putGmsSessionTimeout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putGmsSessionTimeout(gmsPutGmsSessionTimeoutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'putGmsSessionTimeout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sessionTimeout - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.sessionTimeout((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.configData);
                assert.equal('string', data.response.resourceBase);
                assert.equal('string', data.response.resourceKey);
                assert.equal(6, data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'sessionTimeout', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsCollection - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsCollection((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.Application);
                assert.equal(true, data.response.Dns);
                assert.equal(false, data.response.Drc);
                assert.equal(true, data.response.Drops);
                assert.equal(true, data.response.Dscp);
                assert.equal(false, data.response.Flow);
                assert.equal(true, data.response.Interface);
                assert.equal(true, data.response.Jitter);
                assert.equal(false, data.response.Port);
                assert.equal(true, data.response.Shaper);
                assert.equal(false, data.response.TopTalkers);
                assert.equal(true, data.response.Tunnel);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'statsCollection', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsCollectionDefault - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsCollectionDefault((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.Application);
                assert.equal(true, data.response.Dns);
                assert.equal(false, data.response.Drc);
                assert.equal(false, data.response.Drops);
                assert.equal(true, data.response.Dscp);
                assert.equal(false, data.response.Flow);
                assert.equal(true, data.response.Interface);
                assert.equal(false, data.response.Jitter);
                assert.equal(false, data.response.Port);
                assert.equal(false, data.response.Shaper);
                assert.equal(false, data.response.TopTalkers);
                assert.equal(false, data.response.Tunnel);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'statsCollectionDefault', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getThirdPartyServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getThirdPartyServices((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<service_id>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getThirdPartyServices', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultTopologyConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getDefaultTopologyConfig(gmsUserName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getDefaultTopologyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserTopologyConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserTopologyConfig(gmsUsername, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.key);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getUserTopologyConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTunnelGroupApplianceAssociations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllTunnelGroupApplianceAssociations((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response['1']));
                assert.equal(true, Array.isArray(data.response['2']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getAllTunnelGroupApplianceAssociations', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsTunnelGroupId = 'fakedata';
    describe('#getAppliancesForTunnelGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAppliancesForTunnelGroup(gmsTunnelGroupId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response['1']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getAppliancesForTunnelGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTunnelGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllTunnelGroups((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getAllTunnelGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsUpdateExistingTunnelGroupBodyParam = {
      crossConnect: true,
      id: 'string',
      name: 'string',
      topology: {
        externalHubs: [
          'string'
        ],
        hubs: [
          'string'
        ],
        topologyType: 8,
        useRegions: true
      },
      useAllAvailableInterfaces: false,
      wanPorts: [
        'string'
      ]
    };
    describe('#updateExistingTunnelGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.updateExistingTunnelGroup(gmsId, gmsUpdateExistingTunnelGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'updateExistingTunnelGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnelGroup(gmsId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.crossConnect);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.name);
                assert.equal('object', typeof data.response.topology);
                assert.equal(true, data.response.useAllAvailableInterfaces);
                assert.equal(true, Array.isArray(data.response.wanPorts));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getTunnelGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelGroupProperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnelGroupProperties((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.enable);
                assert.equal(false, data.response.tunnelGroupPaused);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'getTunnelGroupProperties', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#versionInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.versionInfo((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.current);
                assert.equal(true, Array.isArray(data.response.installed));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'versionInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsConfigPostGmsConfigBodyParam = {
      configData: {},
      resourceBase: 'string'
    };
    describe('#postGmsConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postGmsConfig(gmsConfigPostGmsConfigBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsConfig', 'postGmsConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gmsConfig((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsConfig', 'gmsConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsConfigBase = 'fakedata';
    const gmsConfigPutGmsConfigBaseBodyParam = {
      configData: {},
      version: 1
    };
    describe('#putGmsConfigBase - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.putGmsConfigBase(gmsConfigBase, null, gmsConfigPutGmsConfigBaseBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsConfig', 'putGmsConfigBase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGmsConfigBase - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGmsConfigBase(gmsConfigBase, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsConfig', 'getGmsConfigBase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostnameByGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.hostnameByGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.gms_hostname);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsHostname', 'hostnameByGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsLicenseLicenseByPutBodyParam = {
      licenseKey: 'string'
    };
    describe('#licenseByPut - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.licenseByPut(gmsLicenseLicenseByPutBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(6, data.response.availableAppliances);
                assert.equal(10, data.response.code);
                assert.equal(7, data.response.daysToExpiration);
                assert.equal('string', data.response.expirationDate);
                assert.equal('string', data.response.licenseKey);
                assert.equal(8, data.response.maxAppliances);
                assert.equal('string', data.response.message);
                assert.equal(3, data.response.rc);
                assert.equal('string', data.response.serialNumber);
                assert.equal(1, data.response.usedAppliances);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsLicense', 'licenseByPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let gmsLicenseLicenseKey = 'fakedata';
    describe('#licenseByGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.licenseByGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(1, data.response.availableAppliances);
                assert.equal(2, data.response.code);
                assert.equal(2, data.response.daysToExpiration);
                assert.equal('string', data.response.expirationDate);
                assert.equal('string', data.response.licenseKey);
                assert.equal(4, data.response.maxAppliances);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.serialNumber);
                assert.equal(7, data.response.usedAppliances);
              } else {
                runCommonAsserts(data, error);
              }
              gmsLicenseLicenseKey = data.response.licenseKey;
              saveMockData('GmsLicense', 'licenseByGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.validateKey(gmsLicenseLicenseKey, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(1, data.response.code);
                assert.equal('string', data.response.licenseKey);
                assert.equal(3, data.response.maxAppliances);
                assert.equal('string', data.response.message);
                assert.equal('string', data.response.serialNumber);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsLicense', 'validateKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#osInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.osInfo((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsOperatingSystem', 'osInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsRemoteAuthGmsRemoteAuthPostBodyParam = {
      authOrderStr: 'string',
      authenticationOrder: [
        'string'
      ],
      hasChanges: true,
      hasError: true,
      netReadPrivilege: 10,
      netReadWritePrivilege: 5,
      primaryHost: 'string',
      primaryPort: 'string',
      primaryRadiusKey: 'string',
      privileges: [
        7
      ],
      protocolStr: 'Local',
      radiusAuth: [
        'string'
      ],
      radiusType: 'string',
      secondaryHost: 'string',
      secondaryPort: 'string',
      secondaryRadiusKey: 'string',
      tacascAuth: [
        'string'
      ],
      type: 'string'
    };
    describe('#gmsRemoteAuthPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.gmsRemoteAuthPost(gmsRemoteAuthGmsRemoteAuthPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsRemoteAuth', 'gmsRemoteAuthPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsRemoteAuthGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gmsRemoteAuthGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(1, data.response.authOrder);
                assert.equal('string', data.response.authOrderStr);
                assert.equal(1, data.response.netReadPrivilege);
                assert.equal(10, data.response.netReadWritePrivilege);
                assert.equal('string', data.response.primaryHost);
                assert.equal('string', data.response.primaryPort);
                assert.equal('string', data.response.primaryRadiusKey);
                assert.equal(4, data.response.protocol);
                assert.equal('string', data.response.protocolStr);
                assert.equal('string', data.response.radiusType);
                assert.equal('string', data.response.secondaryHost);
                assert.equal('string', data.response.secondaryPort);
                assert.equal('string', data.response.secondaryRadiusKey);
                assert.equal('string', data.response.type);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsRemoteAuth', 'gmsRemoteAuthGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSMTPSMTPPostBodyParam = {
      emailAuthentication: false,
      emailSender: 'string',
      emailSsl: false,
      password: 'string',
      requireEmailVerification: true,
      server: 'string',
      smtpPort: 7,
      userID: 'string'
    };
    describe('#sMTPPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sMTPPost(gmsSMTPSMTPPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsSMTP', 'sMTPPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSMTPDelUnverifiedEmailsBodyParam = [
      'string'
    ];
    describe('#delUnverifiedEmails - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delUnverifiedEmails(gmsSMTPDelUnverifiedEmailsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsSMTP', 'delUnverifiedEmails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSMTPAddress = 'fakedata';
    describe('#sendVerificationEmail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sendVerificationEmail(gmsSMTPAddress, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsSMTP', 'sendVerificationEmail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSMTPSMTPTestMailBodyParam = {
      emailRecepients: 'string',
      smtp: {}
    };
    describe('#sMTPTestMail - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sMTPTestMail(gmsSMTPSMTPTestMailBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsSMTP', 'sMTPTestMail', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGmsSMTP - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getGmsSMTP((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.smtp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsSMTP', 'getGmsSMTP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUnverifiedEmails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.listUnverifiedEmails((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
                assert.equal('string', data.response[2]);
                assert.equal('string', data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsSMTP', 'listUnverifiedEmails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const gmsSMTPId = 'fakedata';
    describe('#verifyEmailAddress - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.verifyEmailAddress(gmsSMTPId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsSMTP', 'verifyEmailAddress', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsBriefInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gmsBriefInfo((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.hostName);
                assert.equal('string', data.response.ip);
                assert.equal('string', data.response.time);
                assert.equal('string', data.response.uptime);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gmsserver', 'gmsBriefInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hello - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.hello((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gmsserver', 'hello', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsServerInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gmsServerInfo((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.host);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.freeDiskSpace);
                assert.equal('string', data.response.hostName);
                assert.equal('string', data.response.hwRev);
                assert.equal(true, data.response.inContainerMode);
                assert.equal(10, data.response.loadAverage);
                assert.equal(7, data.response.memSize);
                assert.equal('string', data.response.model);
                assert.equal(9, data.response.numActiveUsers);
                assert.equal(10, data.response.numCpus);
                assert.equal('string', data.response.osRev);
                assert.equal('string', data.response.release);
                assert.equal(8, data.response.role);
                assert.equal('string', data.response.serialNumber);
                assert.equal(5, data.response.time);
                assert.equal('string', data.response.uptime);
                assert.equal('string', data.response.usedDiskSpace);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gmsserver', 'gmsServerInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsPingInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gmsPingInfo((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.hostName);
                assert.equal('string', data.response.message);
                assert.equal(7, data.response.time);
                assert.equal('string', data.response.timeStr);
                assert.equal('string', data.response.uptime);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gmsserver', 'gmsPingInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const haGroupsHaGroupsPostBodyParam = {
      '<indexNumber>': {
        appliances: [
          {
            nePk: 'string'
          }
        ],
        mask: 8,
        subnet: 'string',
        vlanStart: 6
      }
    };
    describe('#haGroupsPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.haGroupsPost(haGroupsHaGroupsPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HaGroups', 'haGroupsPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#haGroupsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.haGroupsGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<indexNumber>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('HaGroups', 'haGroupsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthHealthSummaryBodyParam = {
      applianceIds: [
        'string'
      ],
      from: 5,
      overlayId: 6,
      to: 2,
      verticals: [
        'string'
      ]
    };
    describe('#healthSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.healthSummary(healthHealthSummaryBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response['appliance-id']));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'healthSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthFrom = 555;
    const healthTo = 555;
    const healthApplianceId = 'fakedata';
    describe('#getHealthAlarmPeriodSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getHealthAlarmPeriodSummary(healthFrom, healthTo, healthApplianceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.alarmCountsInPeriod);
                assert.equal('object', typeof data.response.critical);
                assert.equal('object', typeof data.response.major);
                assert.equal('object', typeof data.response.minor);
                assert.equal('object', typeof data.response.warning);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'getHealthAlarmPeriodSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const healthTime = 555;
    const healthOverlayId = 555;
    describe('#jitterPeriodSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jitterPeriodSummary(healthTime, healthOverlayId, healthApplianceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(7, data.response.criticalMins);
                assert.equal('WARNING', data.response.healthStatus);
                assert.equal(1, data.response.majorMins);
                assert.equal(4, data.response.max);
                assert.equal(1, data.response.normalMins);
                assert.equal('string', data.response.tunnelId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'jitterPeriodSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#latencyPeriodSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.latencyPeriodSummary(healthTime, healthOverlayId, healthApplianceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(6, data.response.criticalMins);
                assert.equal('WARNING', data.response.healthStatus);
                assert.equal(6, data.response.majorMins);
                assert.equal(2, data.response.max);
                assert.equal(1, data.response.normalMins);
                assert.equal('string', data.response.tunnelId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'latencyPeriodSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lossPeriodSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.lossPeriodSummary(healthTime, healthOverlayId, healthApplianceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(4, data.response.criticalMins);
                assert.equal('CRITICAL', data.response.healthStatus);
                assert.equal(10, data.response.majorMins);
                assert.equal(2, data.response.maxLossPercentage);
                assert.equal(2, data.response.normalMins);
                assert.equal('string', data.response.tunnelId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'lossPeriodSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mosPeriodSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.mosPeriodSummary(healthTime, healthOverlayId, healthApplianceId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(8, data.response.criticalMins);
                assert.equal('HEALTHY', data.response.healthStatus);
                assert.equal(1, data.response.majorMins);
                assert.equal(10, data.response.max);
                assert.equal(2, data.response.normalMins);
                assert.equal('string', data.response.tunnelId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Health', 'mosPeriodSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const hostnameNePk = 'fakedata';
    const hostnameApplyHostnameBodyParam = {
      hostname: 'string'
    };
    describe('#applyHostname - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applyHostname(hostnameNePk, hostnameApplyHostnameBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Hostname', 'applyHostname', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#idleTime - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.idleTime((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Idle', 'idleTime', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdleIncrement - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getIdleIncrement((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.isTimeout);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Idle', 'getIdleIncrement', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ikeless - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ikeless((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<nePk>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('IkelessSeedStatus', 'ikeless', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceLabelsNePk = 'fakedata';
    describe('#applyLabelsToAppliance - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applyLabelsToAppliance(interfaceLabelsNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InterfaceLabels', 'applyLabelsToAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const interfaceStateCached = true;
    const interfaceStateNeId = 'fakedata';
    describe('#interfaceState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.interfaceState(interfaceStateCached, interfaceStateNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.availMACs));
                assert.equal(true, Array.isArray(data.response.ifInfo));
                assert.equal(true, Array.isArray(data.response.macIfs));
                assert.equal('object', typeof data.response.scalars);
                assert.equal('object', typeof data.response.sysConfig);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('InterfaceState', 'interfaceState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licenseNePk = 'fakedata';
    describe('#grant - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.grant(licenseNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'grant', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLicensePortalApplianceRevokeNePk - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLicensePortalApplianceRevokeNePk(licenseNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'postLicensePortalApplianceRevokeNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const licensePostLicensePortalEcNePkBodyParam = {
      license: {
        boost: {
          bandwidth: 1,
          enable: false
        },
        mini: {
          enable: true
        },
        plus: {
          enable: false
        },
        tier: {
          bandwidth: 6,
          display: 'string'
        }
      }
    };
    describe('#postLicensePortalEcNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postLicensePortalEcNePk(licenseNePk, licensePostLicensePortalEcNePkBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.license);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'postLicensePortalEcNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#licensing - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.licensing((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'licensing', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicensePortalAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicensePortalAppliance((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'getLicensePortalAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicensePortalSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicensePortalSummary((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.licenseState);
                assert.equal('object', typeof data.response.licenses);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'getLicensePortalSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseVx - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLicenseVx((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'getLicenseVx', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkIntegrityTestLinkIntegrityTestRunBodyParam = {
      DSCP: 'string',
      duration: 'string',
      testProgram: 'string'
    };
    describe('#linkIntegrityTestRun - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.linkIntegrityTestRun(linkIntegrityTestLinkIntegrityTestRunBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkIntegrityTest', 'linkIntegrityTestRun', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const linkIntegrityTestNeId = 'fakedata';
    describe('#linkIntegrityStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.linkIntegrityStatus(linkIntegrityTestNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.active);
                assert.equal('string', data.response.result);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LinkIntegrityTest', 'linkIntegrityStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const locationAddress = 'fakedata';
    describe('#addressLookup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.addressLookup(locationAddress, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Location', 'addressLookup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const loggingNeId = 'fakedata';
    const loggingCached = true;
    describe('#getLoggingSettings - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getLoggingSettings(loggingNeId, loggingCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.config);
                assert.equal('object', typeof data.response.remote);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Logging', 'getLoggingSettings', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sessions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.sessions((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('LoginSessions', 'sessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const maintenanceModeSetMaintenanceModeBodyParam = {
      action: 'string',
      data: {}
    };
    describe('#setMaintenanceMode - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setMaintenanceMode(maintenanceModeSetMaintenanceModeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceMode', 'setMaintenanceMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaintenanceMode - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getMaintenanceMode((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.pauseOrchestration));
                assert.equal(true, Array.isArray(data.response.suppressAlarm));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('MaintenanceMode', 'getMaintenanceMode', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const mapsDeleteUploadedMapBodyParam = {};
    describe('#deleteUploadedMap - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUploadedMap(mapsDeleteUploadedMapBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Maps', 'deleteUploadedMap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUploadedMaps - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getUploadedMaps((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Maps', 'getUploadedMaps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const multicastNeId = 'fakedata';
    const multicastCached = true;
    describe('#multicastConfigGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.multicastConfigGet(multicastNeId, multicastCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.igmp);
                assert.equal('object', typeof data.response.pim);
                assert.equal('string', data.response.rp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'multicastConfigGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multicastEnableGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.multicastEnableGet(multicastNeId, multicastCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.enable);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'multicastEnableGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multicastInterfaceStateGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.multicastInterfaceStateGet(multicastNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.DRIP);
                assert.equal('string', data.response.DRPriority);
                assert.equal('string', data.response.generationID);
                assert.equal('string', data.response.interface);
                assert.equal('string', data.response.interfaceIP);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'multicastInterfaceStateGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multicastNeighborStateGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.multicastNeighborStateGet(multicastNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.interface);
                assert.equal('string', data.response.neighborDRPriority);
                assert.equal('string', data.response.neighborGenerationID);
                assert.equal('string', data.response.neighborIP);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'multicastNeighborStateGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multicastRouteStateGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.multicastRouteStateGet(multicastNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.group);
                assert.equal('string', data.response.inIntf);
                assert.equal('string', data.response.outIntfList);
                assert.equal('string', data.response.source);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Multicast', 'multicastRouteStateGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const natNeId = 'fakedata';
    const natCached = true;
    describe('#getAllNat - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllNat(natNeId, natCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.maps);
                assert.equal('object', typeof data.response.natPools);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nat', 'getAllNat', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#branchNatMapsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.branchNatMapsGet(natNeId, natCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.map1);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nat', 'branchNatMapsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNatPools - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllNatPools(natNeId, natCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<NATPool Id>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Nat', 'getAllNatPools', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const netFlowNeId = 'fakedata';
    const netFlowCached = true;
    describe('#getNetFlowData - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNetFlowData(netFlowNeId, netFlowCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(5, data.response.active_timeout);
                assert.equal('string', data.response.collector_type1);
                assert.equal('string', data.response.collector_type2);
                assert.equal(false, data.response.enable);
                assert.equal(false, data.response.if_lan_rx);
                assert.equal(true, data.response.if_lan_tx);
                assert.equal(true, data.response.if_wan_rx);
                assert.equal(true, data.response.if_wan_tx);
                assert.equal('string', data.response.ipaddr1);
                assert.equal('string', data.response.ipaddr2);
                assert.equal(3, data.response.ipfix_tmplt_rfrsh_t);
                assert.equal(2, data.response.port1);
                assert.equal(3, data.response.port2);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetFlow', 'getNetFlowData', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const networkMemoryEraseNmPostBodyParam = {
      erase: false,
      neList: [
        'string'
      ]
    };
    describe('#eraseNmPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.eraseNmPost(networkMemoryEraseNmPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('NetworkMemory', 'eraseNmPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const notificationPostNotificationBodyParam = {
      author: 'string',
      message: 'string'
    };
    describe('#postNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postNotification(notificationPostNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Notification', 'postNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNotification - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getNotification((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Notification', 'getNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const ospfNeId = 'fakedata';
    describe('#getInterfaceConfigDta - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getInterfaceConfigDta(ospfNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<NameOfTheInterface1>']);
                assert.equal('object', typeof data.response['NameOfTheInterface2>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospf', 'getInterfaceConfigDta', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOSPFSystemConfig - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOSPFSystemConfig(ospfNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(5, data.response.bgpRedistMetric);
                assert.equal(9, data.response.bgpRedistMetricType);
                assert.equal(9, data.response.bgpRedistTag);
                assert.equal(false, data.response.enable);
                assert.equal(8, data.response.localRedistMetric);
                assert.equal(5, data.response.localRedistMetricType);
                assert.equal(4, data.response.localRedistTag);
                assert.equal(true, data.response.redistBgp);
                assert.equal(false, data.response.redistLocal);
                assert.equal(false, data.response.redistSubnetShare);
                assert.equal('string', data.response.routerId);
                assert.equal(7, data.response.subnetShareRedistMetric);
                assert.equal(2, data.response.subnetShareRedistMetricType);
                assert.equal(8, data.response.subnetShareRedistTag);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospf', 'getOSPFSystemConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#oSPFInterfaceStateObj - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.oSPFInterfaceStateObj(ospfNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.interfaceState));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospf', 'oSPFInterfaceStateObj', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#oSPFNeighborsStateObj - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.oSPFNeighborsStateObj(ospfNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.interfaceState));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospf', 'oSPFNeighborsStateObj', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#oSPFStateObj - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.oSPFStateObj(ospfNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(8, data.response.extnl_lsa_cnt);
                assert.equal(8, data.response.extnl_lsa_refresh_intvl);
                assert.equal(7, data.response.hold_ls_intvl);
                assert.equal(9, data.response.max_ls_intvl);
                assert.equal(3, data.response.min_lsa_arrival);
                assert.equal(4, data.response.min_lsa_interval);
                assert.equal(10, data.response.num_learned_extnl_ospf);
                assert.equal(4, data.response.num_nbrs);
                assert.equal(6, data.response.originated_new_lsas);
                assert.equal(true, data.response.ospf_enabled);
                assert.equal('string', data.response.pm_admin_state);
                assert.equal(4, data.response.proto_version);
                assert.equal(false, data.response.redist_bgp);
                assert.equal(5, data.response.redist_bgp_metric);
                assert.equal(7, data.response.redist_bgp_route_tag);
                assert.equal(9, data.response.redist_bgp_route_type);
                assert.equal(false, data.response.redist_local);
                assert.equal(7, data.response.redist_local_metric);
                assert.equal(4, data.response.redist_local_route_tag);
                assert.equal(10, data.response.redist_local_route_type);
                assert.equal(true, data.response.redist_subshared);
                assert.equal(7, data.response.redist_subshared_metric);
                assert.equal(10, data.response.redist_subshared_route_tag);
                assert.equal(1, data.response.redist_subshared_route_type);
                assert.equal(9, data.response.route_calc_max_delay);
                assert.equal('string', data.response.router_id);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ospf', 'oSPFStateObj', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const overlayManagerPropertiesSetOverlayMngPropsBodyParam = {
      autoReclassifyValues: 1,
      enable: true,
      ipsecPresharedKey: 'string',
      pollingInterval: 6,
      resetAllFlows: false,
      tunnelMode: 'gre'
    };
    describe('#setOverlayMngProps - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setOverlayMngProps(overlayManagerPropertiesSetOverlayMngPropsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OverlayManagerProperties', 'setOverlayMngProps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOverlayMngProps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOverlayMngProps((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(5, data.response.autoReclassifyValues);
                assert.equal(false, data.response.enable);
                assert.equal(true, data.response.enableAutoSaveOnVXOA);
                assert.equal(true, data.response.enableTemplateApply);
                assert.equal('string', data.response.ipsecPresharedKey);
                assert.equal(true, data.response.overlayPaused);
                assert.equal(4, data.response.pollingInterval);
                assert.equal(true, data.response.resetAllFlows);
                assert.equal('object', typeof data.response.settings);
                assert.equal('ipsec', data.response.tunnelMode);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('OverlayManagerProperties', 'getOverlayMngProps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portForwardingNeId = 'fakedata';
    describe('#portForwardingGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.portForwardingGet(portForwardingNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortForwarding', 'portForwardingGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portProfilesPortProfilesPostBodyParam = {
      id: 'string',
      name: 'string'
    };
    describe('#portProfilesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.portProfilesPost(portProfilesPortProfilesPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortProfiles', 'portProfilesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProfilesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.portProfilesGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortProfiles', 'portProfilesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portProfilesPortProfileId = 'fakedata';
    const portProfilesPortProfilesPutBodyParam = {
      id: 'string',
      name: 'string'
    };
    describe('#portProfilesPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.portProfilesPut(portProfilesPortProfileId, portProfilesPortProfilesPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortProfiles', 'portProfilesPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortProfilesConfigPortProfileId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPortProfilesConfigPortProfileId(portProfilesPortProfileId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.dhcpFailover);
                assert.equal(true, Array.isArray(data.response.dpRoutes));
                assert.equal(true, Array.isArray(data.response.modeIfs));
                assert.equal('object', typeof data.response.sysConfig);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortProfiles', 'getPortProfilesConfigPortProfileId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const portProfilesLabelId = 'fakedata';
    const portProfilesLabelSide = 'fakedata';
    describe('#portProfilesLabelInUseGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.portProfilesLabelInUseGet(portProfilesLabelId, portProfilesLabelSide, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.inUse);
                assert.equal(true, Array.isArray(data.response.portProfileIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortProfiles', 'portProfilesLabelInUseGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rbacCreateOrUpdateApplianceAccessGroupBodyParam = {
      '<applianceAccessGroupName>': {}
    };
    describe('#createOrUpdateApplianceAccessGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOrUpdateApplianceAccessGroup(rbacCreateOrUpdateApplianceAccessGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'createOrUpdateApplianceAccessGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rbacCreateOrUpdateRbacAssignmentBodyParam = {
      username: 'string'
    };
    describe('#createOrUpdateRbacAssignment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.createOrUpdateRbacAssignment(rbacCreateOrUpdateRbacAssignmentBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'createOrUpdateRbacAssignment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rbacSaveRoleBodyParam = {
      '<rolename>': {}
    };
    describe('#saveRole - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saveRole(rbacSaveRoleBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'saveRole', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplianceAccessGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllApplianceAccessGroups(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<applianceAccessGroupName1>']);
                assert.equal('object', typeof data.response['<applianceAccessGroupName2>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'getAllApplianceAccessGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rbacApplianceAccessGroupName = 'fakedata';
    describe('#getApplianceAccessGroupByName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getApplianceAccessGroupByName(rbacApplianceAccessGroupName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.applianceGroups));
                assert.equal(true, Array.isArray(data.response.applianceRegions));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'getApplianceAccessGroupByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRbacAssignments - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRbacAssignments((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'getAllRbacAssignments', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacAssignmentByUsername - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRbacAssignmentByUsername('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.asset);
                assert.equal('string', data.response.roles);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'getRbacAssignmentByUsername', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAllRoles((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<rolename>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'getAllRoles', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssignedMenus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getAssignedMenus((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.net_read));
                assert.equal(true, Array.isArray(data.response.net_read_write));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'getAssignedMenus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rbacRoleName = 'fakedata';
    describe('#getRoleByRoleName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRoleByRoleName(rbacRoleName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.menuTypeItems));
                assert.equal(true, data.response.net_read);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'getRoleByRoleName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const reachabilityNeId = 'fakedata';
    describe('#reachabilityFromAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reachabilityFromAppliance(reachabilityNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.neIP);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reachability', 'reachabilityFromAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reachabilityFromGMS - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.reachabilityFromGMS(reachabilityNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.actualWebProtocolType);
                assert.equal('string', data.response.hostName);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.neId);
                assert.equal(5, data.response.state);
                assert.equal(3, data.response.unsavedChanges);
                assert.equal('string', data.response.userName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Reachability', 'reachabilityFromGMS', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const realtimeStatsNePk = 'fakedata';
    const realtimeStatsRealtimeStatsBodyParam = {
      filter: 2,
      name: 'string',
      type: 'trafficClass'
    };
    describe('#realtimeStats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.realtimeStats(realtimeStatsNePk, realtimeStatsRealtimeStatsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RealtimeStats', 'realtimeStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const regionsRegionPostBodyParam = {
      regionName: 'string'
    };
    describe('#regionPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.regionPost(regionsRegionPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Regions', 'regionPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const regionsRegionAssociationPostBodyParam = {};
    describe('#regionAssociationPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.regionAssociationPost(regionsRegionAssociationPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Regions', 'regionAssociationPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let regionsRegionId = 'fakedata';
    describe('#regionGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.regionGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(9, data.response.regionId);
                assert.equal('string', data.response.regionName);
              } else {
                runCommonAsserts(data, error);
              }
              regionsRegionId = data.response.regionId;
              saveMockData('Regions', 'regionGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionAssociationGET - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.regionAssociationGET((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Regions', 'regionAssociationGET', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const regionsNePK = 'fakedata';
    describe('#getRegionsAppliancesNePkNePK - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRegionsAppliancesNePkNePK(regionsNePK, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.nePk);
                assert.equal(10, data.response.regionId);
                assert.equal('string', data.response.regionName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Regions', 'getRegionsAppliancesNePkNePK', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRegionsAppliancesRegionIdRegionId - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRegionsAppliancesRegionIdRegionId(regionsRegionId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.nePk);
                assert.equal(5, data.response.regionId);
                assert.equal('string', data.response.regionName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Regions', 'getRegionsAppliancesRegionIdRegionId', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const regionsRegionAssociationPutBodyParam = {
      regionId: 5
    };
    describe('#regionAssociationPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.regionAssociationPut(regionsNePK, regionsRegionAssociationPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Regions', 'regionAssociationPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const regionsRegionPutBodyParam = {
      regionName: 'string'
    };
    describe('#regionPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.regionPut(regionsRegionId, regionsRegionPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Regions', 'regionPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionSingleGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.regionSingleGet(regionsRegionId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(8, data.response.regionId);
                assert.equal('string', data.response.regionName);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Regions', 'regionSingleGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const releaseVersion = 'fakedata';
    const releaseDelayReleaseNotificationBodyParam = {
      delayByHours: 7
    };
    describe('#delayReleaseNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delayReleaseNotification(releaseVersion, releaseDelayReleaseNotificationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Release', 'delayReleaseNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dismissReleaseNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dismissReleaseNotification(releaseVersion, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Release', 'dismissReleaseNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const releaseFilter = true;
    describe('#releases - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.releases(releaseFilter, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.gmsbuilds));
                assert.equal(true, Array.isArray(data.response.vxoabuilds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Release', 'releases', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseNotifications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.releaseNotifications((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Release', 'releaseNotifications', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const remoteLogReceiverRemoteLogReceiverAddBodyParam = {};
    describe('#remoteLogReceiverAdd - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.remoteLogReceiverAdd(remoteLogReceiverRemoteLogReceiverAddBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteLogReceiver', 'remoteLogReceiverAdd', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const remoteLogReceiverReceiverId = 555;
    const remoteLogReceiverRemoteLogReceiverSubscribeBodyParam = {
      id: [
        'string'
      ],
      logType: 6
    };
    describe('#remoteLogReceiverSubscribe - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.remoteLogReceiverSubscribe(remoteLogReceiverReceiverId, remoteLogReceiverRemoteLogReceiverSubscribeBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteLogReceiver', 'remoteLogReceiverSubscribe', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const remoteLogReceiverRemoteLogReceiverPutBodyParam = {};
    describe('#remoteLogReceiverPut - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.remoteLogReceiverPut(remoteLogReceiverRemoteLogReceiverPutBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteLogReceiver', 'remoteLogReceiverPut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remoteLogReceiverGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.remoteLogReceiverGet((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteLogReceiver', 'remoteLogReceiverGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const resolverNeId = 'fakedata';
    const resolverCached = true;
    describe('#dns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dns(resolverNeId, resolverCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.domain_search);
                assert.equal('object', typeof data.response.nameserver);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Resolver', 'dns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restApiConfigRestApiConfigPostBodyParam = {
      communicateWithApplianceViaPortal: true
    };
    describe('#restApiConfigPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.restApiConfigPost(restApiConfigRestApiConfigPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RestApiConfig', 'restApiConfigPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restApiConfigGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.restApiConfigGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.communicateWithApplianceViaPortal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RestApiConfig', 'restApiConfigGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restRequestTimeStatsNePk = 'fakedata';
    const restRequestTimeStatsResource = 'fakedata';
    const restRequestTimeStatsPortalWS = true;
    const restRequestTimeStatsTimedout = true;
    const restRequestTimeStatsFrom = 555;
    const restRequestTimeStatsTo = 555;
    describe('#getRestRequestTimeStatsSummary - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestRequestTimeStatsSummary(restRequestTimeStatsNePk, restRequestTimeStatsResource, restRequestTimeStatsPortalWS, restRequestTimeStatsTimedout, restRequestTimeStatsFrom, restRequestTimeStatsTo, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RestRequestTimeStats', 'getRestRequestTimeStatsSummary', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const restRequestTimeStatsMethod = 'fakedata';
    describe('#getRestRequestTimeStatsDetails - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getRestRequestTimeStatsDetails(restRequestTimeStatsNePk, restRequestTimeStatsResource, restRequestTimeStatsPortalWS, restRequestTimeStatsMethod, restRequestTimeStatsFrom, restRequestTimeStatsTo, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RestRequestTimeStats', 'getRestRequestTimeStatsDetails', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rmaWizardNePk = 'fakedata';
    const rmaWizardApplyRmaWizardBodyParam = {
      backup: {
        backupTime: 8,
        comment: 'string',
        fileName: 'string',
        id: 5,
        runningConfig: 'string',
        swVersion: 'string'
      },
      discoveredAppliance: {
        applianceInfo: {
          group: 'string',
          hostname: 'string',
          ip: 'string',
          isLicenseRequired: 'string',
          model: 'string',
          portalLicenseType: 'string',
          reachabilityStatus: 6,
          serial: 'string',
          site: 'string',
          softwareVersion: 'string'
        },
        approved: true,
        approvedTime: 6,
        denied: false,
        deniedTime: 1,
        discoveredFrom: 2,
        discoveredTime: 4,
        dynamicUuid: 'string',
        id: 8,
        portalObjectId: 'string',
        userId: 'string',
        uuid: 'string'
      },
      existingAppliance: {
        IP: 'string',
        applianceId: 8,
        bypass: true,
        discoveredFrom: 4,
        dynamicUuid: 'string',
        groupId: 'string',
        hardwareRevision: 'string',
        hasUnsavedChanges: true,
        hostName: 'string',
        id: 'string',
        ip: 'string',
        latitude: 8,
        longitude: 2,
        mode: 'string',
        model: 'string',
        nePk: 'string',
        networkRole: 'string',
        password: 'string',
        portalObjectId: 'string',
        reachabilityChannel: 7,
        rebootRequired: false,
        serial: 'string',
        site: 'string',
        sitePriority: 10,
        softwareVersion: 'string',
        startupTime: 3,
        state: 10,
        systemBandwidth: 8,
        userName: 'string',
        uuid: 'string',
        webProtocol: 'string',
        webProtocolType: 7
      },
      selectedImage: {
        'NxImage.buildDate': 'string',
        'NxImage.fileName': 'string',
        'NxImage.version': 'string'
      }
    };
    describe('#applyRmaWizard - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.applyRmaWizard(rmaWizardApplyRmaWizardBodyParam, rmaWizardNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.backup);
                assert.equal('object', typeof data.response.discoveredAppliance);
                assert.equal('object', typeof data.response.existingAppliance);
                assert.equal('object', typeof data.response.selectedImage);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RmaWizard', 'applyRmaWizard', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const saMapNePk = 'fakedata';
    describe('#samap - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.samap(saMapNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.entries));
                assert.equal('string', data.response.mapName);
                assert.equal('string', data.response.numOfEntries);
                assert.equal('string', data.response.state);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SaMap', 'samap', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const securityMapsNeId = 'fakedata';
    const securityMapsCached = true;
    describe('#securityMaps - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.securityMaps(securityMapsNeId, securityMapsCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.map1);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SecurityMaps', 'securityMaps', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionActiveSessions - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSessionActiveSessions((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Session', 'getSessionActiveSessions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const snmpNePk = 'fakedata';
    const snmpCached = true;
    describe('#snmpGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.snmpGet(snmpNePk, snmpCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.access);
                assert.equal(false, data.response.auto_launch);
                assert.equal('object', typeof data.response.listen);
                assert.equal('string', data.response.syscontact);
                assert.equal('string', data.response.sysdescr);
                assert.equal('string', data.response.syslocation);
                assert.equal('object', typeof data.response.traps);
                assert.equal('object', typeof data.response.v3);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Snmp', 'snmpGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spPortalSpPortalECSPLicenseAssignBodyParam = {
      licenses: [
        {}
      ]
    };
    describe('#spPortalECSPLicenseAssign - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.spPortalECSPLicenseAssign(spPortalSpPortalECSPLicenseAssignBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalECSPLicenseAssign', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spPortalSpPortalECSPLicenseUnassignBodyParam = {
      licenses: [
        {}
      ]
    };
    describe('#spPortalECSPLicenseUnassign - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.spPortalECSPLicenseUnassign(spPortalSpPortalECSPLicenseUnassignBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalECSPLicenseUnassign', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spPortalSpPortalConfigPostBodyParam = {
      host: 'string',
      port: 2,
      registration: {
        account: 'string',
        accountId: 'string',
        group: 'string',
        key: 'string',
        oldKey: 'string',
        site: 'string'
      }
    };
    describe('#spPortalConfigPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.spPortalConfigPost(spPortalSpPortalConfigPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalConfigPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spPortalSpPortalCreateCasePostBodyParam = {
      caseDesc: 'string',
      caseEmail: 'string',
      caseName: 'string',
      casePhone: 'string',
      casePrio: 'string',
      caseSubject: 'string',
      orchSerialNum: 'string'
    };
    describe('#spPortalCreateCasePost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.spPortalCreateCasePost(spPortalSpPortalCreateCasePostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalCreateCasePost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    let spPortalIp = 555;
    const spPortalGeoLocationPostBodyParam = [
      7
    ];
    describe('#geoLocationPost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.geoLocationPost(spPortalGeoLocationPostBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<ip>']);
              } else {
                runCommonAsserts(data, error);
              }
              spPortalIp = data.response['<ip>'];
              saveMockData('SpPortal', 'geoLocationPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsPortalRegistrationPost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gmsPortalRegistrationPost((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.accountKey);
                assert.equal('string', data.response.accountName);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal(true, data.response.enabled);
                assert.equal('string', data.response.group);
                assert.equal(false, data.response.pendingPoll);
                assert.equal(true, data.response.registered);
                assert.equal('string', data.response.site);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'gmsPortalRegistrationPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalAccountKeyChangeCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.spPortalAccountKeyChangeCount((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(1, data.response.count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalAccountKeyChangeCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalAccountKeyChangeStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.spPortalAccountKeyChangeStatus((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(4, data.response.committedToPortalTimestamp);
                assert.equal(2, data.response.generateTimestamp);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalAccountKeyChangeStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalAccountKeyGeneratePut - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.spPortalAccountKeyGeneratePut((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.accountKey);
                assert.equal('string', data.response.oldAccountKey);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalAccountKeyGeneratePut', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalECSPLicensesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.spPortalECSPLicensesGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalECSPLicensesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalAccountLicenseFeatureGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.spPortalAccountLicenseFeatureGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalAccountLicenseFeatureGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalAccountLicenseTypeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.spPortalAccountLicenseTypeGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.displayName);
                assert.equal('string', data.response.name);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalAccountLicenseTypeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spPortalNeId = 555;
    describe('#checkApplianceReachabilityUsingWSGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.checkApplianceReachabilityUsingWSGet(spPortalNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.directWS);
                assert.equal('object', typeof data.response.portalWS);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'checkApplianceReachabilityUsingWSGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appGroupTmpGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.appGroupTmpGet((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'appGroupTmpGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appGroupTmpHashCodeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.appGroupTmpHashCodeGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.hashVal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'appGroupTmpHashCodeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#compoundTmpGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.compoundTmpGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['id#']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'compoundTmpGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#compoundTmpHashCodeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.compoundTmpHashCodeGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.hashVal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'compoundTmpHashCodeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalConfigGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.spPortalConfigGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.host);
                assert.equal(2, data.response.port);
                assert.equal('object', typeof data.response.registration);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalConfigGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectivityGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.connectivityGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(3, data.response.portal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'connectivityGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsTmpGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dnsTmpGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'dnsTmpGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsTmpHashCodeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dnsTmpHashCodeGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.hashVal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'dnsTmpHashCodeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#geoLocationGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.geoLocationGet(spPortalIp, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<ip>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'geoLocationGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const spPortalStart = 555;
    const spPortalLimit = 555;
    describe('#ipIntelligenceTmpGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ipIntelligenceTmpGet(spPortalStart, spPortalLimit, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'ipIntelligenceTmpGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipIntelligenceTmpTimeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ipIntelligenceTmpTimeGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.lastUpdateTime);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'ipIntelligenceTmpTimeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpPortalInternetDbIpIntelligenceSearch - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSpPortalInternetDbIpIntelligenceSearch(spPortalIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'getSpPortalInternetDbIpIntelligenceSearch', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipIntelligenceTmpTotal - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ipIntelligenceTmpTotal((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'ipIntelligenceTmpTotal', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceIdToSaasIdGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceIdToSaasIdGet(null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.serviceId));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'serviceIdToSaasIdGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceIdToSaasIdCountGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceIdToSaasIdCountGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(7, data.response.count);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'serviceIdToSaasIdCountGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceIdToSaasIdCountriesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceIdToSaasIdCountriesGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.country));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'serviceIdToSaasIdCountriesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceIdToSaasIdSaasAppGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.serviceIdToSaasIdSaasAppGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'serviceIdToSaasIdSaasAppGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipProtocolNumbersGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.ipProtocolNumbersGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'ipProtocolNumbersGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meterFlowTmpGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meterFlowTmpGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.flowType));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'meterFlowTmpGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meterFlowTmpHashCodeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.meterFlowTmpHashCodeGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.hashVal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'meterFlowTmpHashCodeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProtocolTmpGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.portProtocolTmpGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.applicationTags);
                assert.equal('object', typeof data.response.portProtocolWithPriority);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'portProtocolTmpGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProtocolTmpHashCodeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.portProtocolTmpHashCodeGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.hashVal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'portProtocolTmpHashCodeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsPortalRegistrationGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.gmsPortalRegistrationGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.accountKey);
                assert.equal('string', data.response.accountName);
                assert.equal(true, Array.isArray(data.response.emails));
                assert.equal(false, data.response.enabled);
                assert.equal('string', data.response.group);
                assert.equal(false, data.response.pendingPoll);
                assert.equal(false, data.response.registered);
                assert.equal('string', data.response.site);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'gmsPortalRegistrationGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saasTmpGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.saasTmpGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['id#']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'saasTmpGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saasTmpHashCodeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.saasTmpHashCodeGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.hashVal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'saasTmpHashCodeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalStatusGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.spPortalStatusGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<serviceName>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalStatusGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tcpUdpPortsGet - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tcpUdpPortsGet((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'tcpUdpPortsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#topSitesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.topSitesGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'topSitesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tbTmpGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.tbTmpGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.TB_CONFIG);
                assert.equal('object', typeof data.response.TB_NAME);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'tbTmpGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tbTmpHashCodeGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.tbTmpHashCodeGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.hashVal);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'tbTmpHashCodeGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslNeId = 'fakedata';
    const sslCached = true;
    describe('#sslCertsGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.sslCertsGet(sslNeId, sslCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.config);
                assert.equal('object', typeof data.response.state);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Ssl', 'sslCertsGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslCACertificateSslCACertGetTextPostBodyParam = {
      certificateData: 'string'
    };
    describe('#sslCACertGetTextPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sslCACertGetTextPost(sslCACertificateSslCACertGetTextPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SslCACertificate', 'sslCACertGetTextPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslCACertificateSslCACertValidationBodyParam = {
      certificateData: 'string',
      isSslCACert: false
    };
    describe('#sslCACertValidation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sslCACertValidation(sslCACertificateSslCACertValidationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SslCACertificate', 'sslCACertValidation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslCACertificateNeId = 'fakedata';
    const sslCACertificateCached = true;
    describe('#sslCACerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.sslCACerts(sslCACertificateNeId, sslCACertificateCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.auth);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SslCACertificate', 'sslCACerts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslCertificateSslCertGetInfoPostBodyParam = {
      isPFX: true,
      isSslCACert: true
    };
    describe('#sslCertGetInfoPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sslCertGetInfoPost(sslCertificateSslCertGetInfoPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SslCertificate', 'sslCertGetInfoPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslCertificateSslCertGetTextPostBodyParam = {
      certificateData: 'string'
    };
    describe('#sslCertGetTextPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sslCertGetTextPost(sslCertificateSslCertGetTextPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SslCertificate', 'sslCertGetTextPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslSubstituteCertificateSslSubstituteCertValidationBodyParam = {
      certificateData: 'string',
      isPFX: true,
      isSslCACert: false
    };
    describe('#sslSubstituteCertValidation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sslSubstituteCertValidation(sslSubstituteCertificateSslSubstituteCertValidationBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SslSubstituteCertificate', 'sslSubstituteCertValidation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const sslSubstituteCertificateId = 'fakedata';
    const sslSubstituteCertificateCached = true;
    describe('#sslSubstituteCertGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.sslSubstituteCertGet(sslSubstituteCertificateId, sslSubstituteCertificateCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.enable);
                assert.equal('object', typeof data.response.sslcert);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SslSubstituteCertificate', 'sslSubstituteCertGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsStartTime = 555;
    const statsEndTime = 555;
    const statsGranularity = 'fakedata';
    const statsTrafficType = 'fakedata';
    const statsIp = true;
    const statsPostStatsAggregateApplianceBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsAggregateAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsAggregateAppliance(statsPostStatsAggregateApplianceBodyParam, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, statsIp, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<trafficType>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsAggregateAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsApplication = 'fakedata';
    const statsPostStatsAggregateApplicationBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsAggregateApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsAggregateApplication(statsPostStatsAggregateApplicationBodyParam, statsStartTime, statsEndTime, statsGranularity, statsApplication, statsTrafficType, statsIp, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsAggregateApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsAggregateApplication2BodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsAggregateApplication2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsAggregateApplication2(statsPostStatsAggregateApplication2BodyParam, statsStartTime, statsEndTime, statsApplication, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsAggregateApplication2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsBoostAggregateStatsBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#boostAggregateStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.boostAggregateStats(statsBoostAggregateStatsBodyParam, statsStartTime, statsEndTime, statsGranularity, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
                assert.equal('string', data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'boostAggregateStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsStatsAggregateDnsBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#statsAggregateDns - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsAggregateDns(statsStatsAggregateDnsBodyParam, statsStartTime, statsEndTime, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsAggregateDns', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsStatsDrcAggregateTunnelBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#statsDrcAggregateTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsDrcAggregateTunnel(statsStatsDrcAggregateTunnelBodyParam, statsStartTime, statsEndTime, statsGranularity, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsDrcAggregateTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsDscp = 555;
    const statsPostStatsAggregateDscpBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsAggregateDscp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsAggregateDscp(statsPostStatsAggregateDscpBodyParam, statsStartTime, statsEndTime, statsGranularity, statsDscp, statsTrafficType, statsIp, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsAggregateDscp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsAggregateFlowBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsAggregateFlow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsAggregateFlow(statsPostStatsAggregateFlowBodyParam, statsStartTime, statsEndTime, statsGranularity, null, statsTrafficType, statsIp, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsAggregateFlow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsGetActiveFlowCountsBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#getActiveFlowCounts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getActiveFlowCounts(statsGetActiveFlowCountsBodyParam, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<nePk>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getActiveFlowCounts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsAggregateInterfaceBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsAggregateInterface - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsAggregateInterface(statsPostStatsAggregateInterfaceBodyParam, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsAggregateInterface', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsInterfaceOverlayTransportAggregateStatsBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#interfaceOverlayTransportAggregateStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.interfaceOverlayTransportAggregateStats(statsInterfaceOverlayTransportAggregateStatsBodyParam, statsStartTime, statsEndTime, statsGranularity, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'interfaceOverlayTransportAggregateStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsStatsAggregateJitterBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#statsAggregateJitter - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsAggregateJitter(statsStatsAggregateJitterBodyParam, statsStartTime, statsEndTime, statsGranularity, statsIp, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsAggregateJitter', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsMosAggregateStatsBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#mosAggregateStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.mosAggregateStats(statsMosAggregateStatsBodyParam, statsStartTime, statsEndTime, statsGranularity, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'mosAggregateStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsStatsAggregatePortsBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#statsAggregatePorts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsAggregatePorts(statsStatsAggregatePortsBodyParam, statsStartTime, statsEndTime, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
                assert.equal('string', data.response[2]);
                assert.equal('string', data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsAggregatePorts', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsSecurityPolicyAggregateStatsBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#securityPolicyAggregateStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.securityPolicyAggregateStats(statsSecurityPolicyAggregateStatsBodyParam, statsStartTime, statsEndTime, statsGranularity, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'securityPolicyAggregateStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsStatsAggregateTopTalkersBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#statsAggregateTopTalkers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsAggregateTopTalkers(statsStatsAggregateTopTalkersBodyParam, statsStartTime, statsEndTime, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsAggregateTopTalkers', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsAggregateTrafficBehaviorBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsAggregateTrafficBehavior - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsAggregateTrafficBehavior(statsPostStatsAggregateTrafficBehaviorBodyParam, statsStartTime, statsEndTime, null, statsApplication, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsAggregateTrafficBehavior', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsTrafficClass = 555;
    const statsPostStatsAggregateTrafficClassBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsAggregateTrafficClass - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsAggregateTrafficClass(statsPostStatsAggregateTrafficClassBodyParam, statsStartTime, statsEndTime, statsGranularity, statsTrafficClass, statsTrafficType, statsIp, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsAggregateTrafficClass', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsAggregateTunnelBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsAggregateTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsAggregateTunnel(statsPostStatsAggregateTunnelBodyParam, statsStartTime, statsEndTime, statsGranularity, statsIp, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<tunnelName>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsAggregateTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsTimeseriesApplianceBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsTimeseriesAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsTimeseriesAppliance(statsPostStatsTimeseriesApplianceBodyParam, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsTimeseriesAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsTimeseriesApplicationBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsTimeseriesApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsTimeseriesApplication(statsPostStatsTimeseriesApplicationBodyParam, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, statsApplication, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsTimeseriesApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsTimeseriesApplication2BodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsTimeseriesApplication2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsTimeseriesApplication2(statsPostStatsTimeseriesApplication2BodyParam, statsStartTime, statsEndTime, statsApplication, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsTimeseriesApplication2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsTimeseriesDrcBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsTimeseriesDrc - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsTimeseriesDrc(statsPostStatsTimeseriesDrcBodyParam, statsStartTime, statsEndTime, statsGranularity, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsTimeseriesDrc', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsTimeseriesDscpBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsTimeseriesDscp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsTimeseriesDscp(statsPostStatsTimeseriesDscpBodyParam, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, statsDscp, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsTimeseriesDscp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsFlowType = 'fakedata';
    const statsPostStatsTimeseriesFlowBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsTimeseriesFlow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsTimeseriesFlow(statsPostStatsTimeseriesFlowBodyParam, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, statsFlowType, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsTimeseriesFlow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsDirection = 555;
    const statsPostStatsTimeseriesShaperBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsTimeseriesShaper - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsTimeseriesShaper(statsPostStatsTimeseriesShaperBodyParam, statsStartTime, statsEndTime, statsGranularity, statsTrafficClass, statsDirection, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsTimeseriesShaper', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsTimeseriesTrafficClassBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsTimeseriesTrafficClass - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsTimeseriesTrafficClass(statsPostStatsTimeseriesTrafficClassBodyParam, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, statsTrafficClass, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsTimeseriesTrafficClass', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsPostStatsTimeseriesTunnelBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#postStatsTimeseriesTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postStatsTimeseriesTunnel(statsPostStatsTimeseriesTunnelBodyParam, statsStartTime, statsEndTime, statsGranularity, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'postStatsTimeseriesTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsAggregateTunnel(statsStartTime, statsEndTime, statsGranularity, null, statsTrafficType, statsIp, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<trafficType>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsAggregateTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsNePk = 'fakedata';
    describe('#getStatsAggregateApplianceNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateApplianceNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, statsIp, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<trafficType>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateApplianceNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsAggregateApplication(statsStartTime, statsEndTime, statsGranularity, null, statsApplication, statsTrafficType, statsIp, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsAggregateApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateApplicationNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateApplicationNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsApplication, statsTrafficType, statsIp, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateApplicationNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateApplication2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateApplication2(statsStartTime, statsEndTime, null, statsApplication, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateApplication2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateApplication2NePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateApplication2NePk(statsNePk, statsStartTime, statsEndTime, statsApplication, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateApplication2NePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsTunnelName = 'fakedata';
    describe('#getStatsAggregateBoostNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateBoostNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTunnelName, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateBoostNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dNSStatsAggregate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.dNSStatsAggregate(statsStartTime, statsEndTime, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'dNSStatsAggregate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateDnsNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateDnsNePk(statsNePk, statsStartTime, statsEndTime, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateDnsNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateDrcTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsAggregateDrcTunnel(statsStartTime, statsEndTime, statsGranularity, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsAggregateDrcTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateDrcNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateDrcNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTunnelName, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateDrcNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateDscp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsAggregateDscp(statsStartTime, statsEndTime, statsGranularity, null, statsDscp, statsTrafficType, statsIp, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsAggregateDscp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateDscpNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateDscpNePk(statsNePk, statsStartTime, statsEndTime, statsTrafficType, statsGranularity, statsTrafficClass, statsDscp, statsIp, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateDscpNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateFlow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsAggregateFlow(statsStartTime, statsEndTime, statsGranularity, null, null, statsTrafficType, statsIp, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsAggregateFlow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateFlowNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateFlowNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTrafficClass, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateFlowNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interfaceAggregateStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.interfaceAggregateStats(statsStartTime, statsEndTime, statsGranularity, statsTrafficType, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'interfaceAggregateStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jitterStatsAggregate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.jitterStatsAggregate(statsStartTime, statsEndTime, statsGranularity, statsIp, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
                assert.equal('string', data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'jitterStatsAggregate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateJitterNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateJitterNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsIp, null, null, statsTunnelName, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
                assert.equal('string', data.response[2]);
                assert.equal('string', data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateJitterNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateMosNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateMosNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTunnelName, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateMosNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portStatsAggregate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.portStatsAggregate(statsStartTime, statsEndTime, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'portStatsAggregate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregatePortsNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregatePortsNePk(statsStartTime, statsEndTime, null, null, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregatePortsNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsFromZone = 'fakedata';
    const statsToZone = 'fakedata';
    describe('#getStatsAggregateSecurityPolicyNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateSecurityPolicyNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsFromZone, statsToZone, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateSecurityPolicyNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#topTalkersStatsAggregate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.topTalkersStatsAggregate(statsStartTime, statsEndTime, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'topTalkersStatsAggregate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTopTalkersSplitNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateTopTalkersSplitNePk(statsNePk, statsStartTime, statsEndTime, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateTopTalkersSplitNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTopTalkersNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateTopTalkersNePk(statsNePk, statsStartTime, statsEndTime, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateTopTalkersNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#trafficBehavior - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.trafficBehavior(statsStartTime, statsEndTime, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'trafficBehavior', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTrafficBehaviorNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateTrafficBehaviorNePk(statsNePk, statsStartTime, statsEndTime, null, statsApplication, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateTrafficBehaviorNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateTrafficClass - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsAggregateTrafficClass(statsStartTime, statsEndTime, statsGranularity, null, statsTrafficClass, statsTrafficType, statsIp, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsAggregateTrafficClass', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTrafficClassNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateTrafficClassNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTrafficClass, statsTrafficType, statsIp, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<Traffic Type>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateTrafficClassNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateTunnel(statsStartTime, statsEndTime, statsGranularity, null, statsIp, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<tunnelName>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTunnelNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsAggregateTunnelNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTunnelName, statsIp, null, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<tunnelName>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsAggregateTunnelNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsDnsInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsDnsInfo(statsIp, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.dns);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsDnsInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsTimeseriesAppliance(statsStartTime, statsEndTime, statsGranularity, null, statsTrafficType, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsTimeseriesAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesApplianceNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsTimeseriesApplianceNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsTimeseriesApplianceNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsOfApplianceProcessState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsOfApplianceProcessState(statsNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsOfApplianceProcessState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesApplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsTimeseriesApplication(statsStartTime, statsEndTime, statsGranularity, null, statsTrafficType, statsApplication, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsTimeseriesApplication', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesApplicationNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsTimeseriesApplicationNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, statsApplication, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsTimeseriesApplicationNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesApplication2 - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsTimeseriesApplication2(statsStartTime, statsEndTime, statsApplication, null, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsTimeseriesApplication2', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesApplication2NePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsTimeseriesApplication2NePk(statsNePk, statsStartTime, statsEndTime, statsApplication, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsTimeseriesApplication2NePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#boostTimeSeriesStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.boostTimeSeriesStats(statsNePk, statsStartTime, statsEndTime, statsGranularity, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'boostTimeSeriesStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesDrcTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsTimeseriesDrcTunnel(statsStartTime, statsEndTime, statsGranularity, null, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsTimeseriesDrcTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesDrcNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsTimeseriesDrcNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTunnelName, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsTimeseriesDrcNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesDscp - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsTimeseriesDscp(statsStartTime, statsEndTime, statsGranularity, null, statsTrafficType, statsDscp, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsTimeseriesDscp', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesDscpNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsTimeseriesDscpNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, statsDscp, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsTimeseriesDscpNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesFlow - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsTimeseriesFlow(statsStartTime, statsEndTime, statsGranularity, null, statsTrafficType, statsFlowType, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsTimeseriesFlow', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesFlowNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsTimeseriesFlowNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, statsFlowType, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(4, data.response.COMP_L2W);
                assert.equal(7, data.response.COMP_L2WMAX);
                assert.equal(2, data.response.COMP_L2WMAX_TS);
                assert.equal(2, data.response.COMP_NOOHEAD_L2W);
                assert.equal(7, data.response.COMP_NOOHEAD_L2WMAX);
                assert.equal(5, data.response.COMP_NOOHEAD_L2WMAX_TS);
                assert.equal(10, data.response.COMP_NOOHEAD_W2L);
                assert.equal(6, data.response.COMP_NOOHEAD_W2LMAX);
                assert.equal(7, data.response.COMP_NOOHEAD_W2LMAX_TS);
                assert.equal(9, data.response.COMP_W2L);
                assert.equal(3, data.response.COMP_W2LMAX);
                assert.equal(10, data.response.COMP_W2LMAX_TS);
                assert.equal(5, data.response.CREATED);
                assert.equal(4, data.response.CREATED_MAX);
                assert.equal(10, data.response.CREATED_MAX_TS);
                assert.equal(5, data.response.DELETED);
                assert.equal(6, data.response.DELETED_MAX);
                assert.equal(9, data.response.DELETED_MAX_TS);
                assert.equal(5, data.response.EXT);
                assert.equal(6, data.response.FLOW_EXT_PEAK);
                assert.equal(4, data.response.FLOW_EXT_PEAK_TS);
                assert.equal(2, data.response.FLOW_MAX);
                assert.equal(1, data.response.FLOW_PEAK);
                assert.equal(9, data.response.FLOW_PEAK_TS);
                assert.equal(5, data.response.FLOW_TYPE);
                assert.equal(3, data.response.LRX_BYTES);
                assert.equal(2, data.response.LRX_BYTES_MAX);
                assert.equal(1, data.response.LRX_BYTES_MAX_TS);
                assert.equal(3, data.response.LRX_PKTS);
                assert.equal(5, data.response.LRX_PKTS_MAX);
                assert.equal(9, data.response.LRX_PKTS_MAX_TS);
                assert.equal(9, data.response.LTX_BYTES);
                assert.equal(3, data.response.LTX_BYTES_MAX);
                assert.equal(6, data.response.LTX_BYTES_MAX_TS);
                assert.equal(9, data.response.LTX_PKTS);
                assert.equal(8, data.response.LTX_PKTS_MAX);
                assert.equal(3, data.response.LTX_PKTS_MAX_TS);
                assert.equal(7, data.response.TIMESTAMP);
                assert.equal(4, data.response.TRAFFIC_TYPE);
                assert.equal(1, data.response.WRX_BYTES);
                assert.equal(10, data.response.WRX_BYTES_MAX);
                assert.equal(1, data.response.WRX_BYTES_MAX_TS);
                assert.equal(1, data.response.WRX_PKTS);
                assert.equal(10, data.response.WRX_PKTS_MAX);
                assert.equal(1, data.response.WRX_PKTS_MAX_TS);
                assert.equal(5, data.response.WTX_BYTES);
                assert.equal(5, data.response.WTX_BYTES_MAX);
                assert.equal(4, data.response.WTX_BYTES_MAX_TS);
                assert.equal(7, data.response.WTX_PKTS);
                assert.equal(5, data.response.WTX_PKTS_MAX);
                assert.equal(7, data.response.WTX_PKTS_MAX_TS);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsTimeseriesFlowNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interfaceTimeseriesStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.interfaceTimeseriesStats(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'interfaceTimeseriesStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interfaceOverlayTransportTimeseriesStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.interfaceOverlayTransportTimeseriesStats(statsNePk, statsStartTime, statsEndTime, statsGranularity, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'interfaceOverlayTransportTimeseriesStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesInternalDrops - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsTimeseriesInternalDrops(statsNePk, statsStartTime, statsEndTime, statsGranularity, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsTimeseriesInternalDrops', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsKey = 'fakedata';
    describe('#statsTimeseriesTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsTimeseriesTunnel(statsStartTime, statsEndTime, statsKey, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsTimeseriesTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const statsTunnel = 'fakedata';
    describe('#mOSTimeSeriesStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.mOSTimeSeriesStats(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTunnel, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'mOSTimeSeriesStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityPolicyTimeSeriesStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.securityPolicyTimeSeriesStats(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsFromZone, statsToZone, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'securityPolicyTimeSeriesStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#shaperTimeseriesStats - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.shaperTimeseriesStats(null, statsStartTime, statsEndTime, statsGranularity, statsTrafficClass, statsDirection, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'shaperTimeseriesStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesTrafficClass - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.statsTimeseriesTrafficClass(statsStartTime, statsEndTime, statsGranularity, null, statsTrafficType, statsTrafficClass, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'statsTimeseriesTrafficClass', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesTrafficClassNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsTimeseriesTrafficClassNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTrafficType, statsTrafficClass, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsTimeseriesTrafficClassNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsTimeseriesTunnel(statsStartTime, statsEndTime, statsGranularity, null, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal('object', typeof data.response.data);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsTimeseriesTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesTunnelNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getStatsTimeseriesTunnelNePk(statsNePk, statsStartTime, statsEndTime, statsGranularity, statsTunnelName, null, null, statsIp, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.column_def));
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Stats', 'getStatsTimeseriesTunnelNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subnetsNeId = 'fakedata';
    const subnetsConfSubnetsBodyParam = {
      ipv4: {}
    };
    describe('#confSubnets - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.confSubnets(subnetsNeId, subnetsConfSubnetsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnets', 'confSubnets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subnetsSetSubnetSharingOptionsBodyParam = {
      auto_subnet: {
        add_local: true,
        add_local_lan: true,
        add_local_wan: true,
        self: false
      }
    };
    describe('#setSubnetSharingOptions - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.setSubnetSharingOptions(subnetsNeId, subnetsSetSubnetSharingOptionsBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnets', 'setSubnetSharingOptions', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subnetsDiscoveredId = 'fakedata';
    describe('#getSubnetsForDiscoveredAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubnetsForDiscoveredAppliance(subnetsDiscoveredId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.moduleInfo);
                assert.equal(true, Array.isArray(data.response.peers));
                assert.equal('object', typeof data.response.subnets);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnets', 'getSubnetsForDiscoveredAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const subnetsGetCachedData = true;
    describe('#getSubnets - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSubnets(subnetsGetCachedData, subnetsNeId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.moduleInfo);
                assert.equal(true, Array.isArray(data.response.peers));
                assert.equal('object', typeof data.response.subnets);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Subnets', 'getSubnets', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemInfoNeId = 'fakedata';
    const systemInfoCached = true;
    describe('#appSystemDeployInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.appSystemDeployInfo(systemInfoNeId, systemInfoCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, data.response.auto_ipid);
                assert.equal(3, data.response.auto_pol_lookup_intvl);
                assert.equal('object', typeof data.response.auto_subnet);
                assert.equal(false, data.response.auto_syn);
                assert.equal(false, data.response.auto_tunnel);
                assert.equal(false, data.response.bridge_loop_test);
                assert.equal('object', typeof data.response.bw);
                assert.equal(true, data.response.disk_encrypt_enable);
                assert.equal('object', typeof data.response.dpc);
                assert.equal('object', typeof data.response.excess_flow);
                assert.equal('string', data.response.ha_if);
                assert.equal('object', typeof data.response.idrc);
                assert.equal(true, data.response.igmp_snooping);
                assert.equal(true, data.response.int_hairpin);
                assert.equal(false, data.response.ipsec_override);
                assert.equal(10, data.response.max_tcp_mss);
                assert.equal(false, data.response.nat);
                assert.equal(6, data.response.network_role);
                assert.equal(false, data.response.nm_fsp_enable);
                assert.equal(8, data.response.nm_media);
                assert.equal(3, data.response.nm_mode);
                assert.equal('object', typeof data.response.node_dns);
                assert.equal(9, data.response.options);
                assert.equal('string', data.response.orch_guid);
                assert.equal(false, data.response.passthru_to_sender);
                assert.equal('string', data.response.port_fwd_rules);
                assert.equal(6, data.response.quies_tun_ka_intvl);
                assert.equal('object', typeof data.response.serverMode);
                assert.equal('object', typeof data.response.shaperinbound);
                assert.equal(false, data.response.smb_signing);
                assert.equal(10, data.response.udp_inact_time);
                assert.equal('string', data.response.udp_ipsec_lcl_ports);
                assert.equal('string', data.response.udp_ipsec_peer_ports);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemInfo', 'appSystemDeployInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const systemInfoDiscoveredId = 'fakedata';
    describe('#discoveredAppSystemDeployInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.discoveredAppSystemDeployInfo(systemInfoDiscoveredId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.auto_ipid);
                assert.equal(6, data.response.auto_pol_lookup_intvl);
                assert.equal('object', typeof data.response.auto_subnet);
                assert.equal(true, data.response.auto_syn);
                assert.equal(true, data.response.auto_tunnel);
                assert.equal(false, data.response.bridge_loop_test);
                assert.equal('object', typeof data.response.bw);
                assert.equal(false, data.response.disk_encrypt_enable);
                assert.equal('object', typeof data.response.dpc);
                assert.equal('object', typeof data.response.excess_flow);
                assert.equal('string', data.response.ha_if);
                assert.equal('object', typeof data.response.idrc);
                assert.equal(false, data.response.igmp_snooping);
                assert.equal(true, data.response.int_hairpin);
                assert.equal(false, data.response.ipsec_override);
                assert.equal(7, data.response.max_tcp_mss);
                assert.equal(true, data.response.nat);
                assert.equal(10, data.response.network_role);
                assert.equal(false, data.response.nm_fsp_enable);
                assert.equal(6, data.response.nm_media);
                assert.equal(9, data.response.nm_mode);
                assert.equal('object', typeof data.response.node_dns);
                assert.equal(1, data.response.options);
                assert.equal('string', data.response.orch_guid);
                assert.equal(false, data.response.passthru_to_sender);
                assert.equal('string', data.response.port_fwd_rules);
                assert.equal(8, data.response.quies_tun_ka_intvl);
                assert.equal('object', typeof data.response.serverMode);
                assert.equal('object', typeof data.response.shaperinbound);
                assert.equal(false, data.response.smb_signing);
                assert.equal(9, data.response.udp_inact_time);
                assert.equal('string', data.response.udp_ipsec_lcl_ports);
                assert.equal('string', data.response.udp_ipsec_peer_ports);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemInfo', 'discoveredAppSystemDeployInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appSystemStateInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.appSystemStateInfo(systemInfoNeId, systemInfoCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.alarmSummary);
                assert.equal(7, data.response.applianceid);
                assert.equal('string', data.response.biosVersion);
                assert.equal('string', data.response.datetime);
                assert.equal('string', data.response.deploymentMode);
                assert.equal(9, data.response.gmtOffset);
                assert.equal(true, data.response.hasUnsavedChanges);
                assert.equal('string', data.response.hostName);
                assert.equal(true, data.response.isLicenseInstalled);
                assert.equal(2, data.response.licenseExpirationDaysLeft);
                assert.equal(1, data.response.licenseExpiryDate);
                assert.equal(true, data.response.licenseRequired);
                assert.equal('string', data.response.model);
                assert.equal('string', data.response.modelShort);
                assert.equal(true, data.response.rebootRequired);
                assert.equal('string', data.response.release);
                assert.equal('string', data.response.releaseWithoutPrefix);
                assert.equal('string', data.response.serial);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.timezone);
                assert.equal(7, data.response.uptime);
                assert.equal('string', data.response.uptimeString);
                assert.equal('string', data.response.uuid);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SystemInfo', 'appSystemStateInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tcaNeId = 'fakedata';
    const tcaCached = true;
    describe('#getTunnelTca - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnelTca(tcaNeId, tcaCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.latency);
                assert.equal('object', typeof data.response['loss-post-fec']);
                assert.equal('object', typeof data.response['loss-pre-fec']);
                assert.equal('object', typeof data.response['oop-post-poc']);
                assert.equal('object', typeof data.response['oop-pre-poc']);
                assert.equal('object', typeof data.response.reduction);
                assert.equal('object', typeof data.response.utilization);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tca', 'getTunnelTca', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemTca - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getSystemTca(tcaNeId, tcaCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['file-system-utilization']);
                assert.equal('object', typeof data.response['lan-side-rx-throughput']);
                assert.equal('object', typeof data.response.latency);
                assert.equal('object', typeof data.response['loss-post-fec']);
                assert.equal('object', typeof data.response['loss-pre-fec']);
                assert.equal('object', typeof data.response['oop-post-poc']);
                assert.equal('object', typeof data.response['oop-pre-poc']);
                assert.equal('object', typeof data.response['optimized-flows']);
                assert.equal('object', typeof data.response.reduction);
                assert.equal('object', typeof data.response['total-flows']);
                assert.equal('object', typeof data.response.utilization);
                assert.equal('object', typeof data.response['wan-side-tx-throughput']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tca', 'getSystemTca', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tcpdumpTcpdumpRunBodyParam = {
      max_packet: '1000',
      nePks: [
        'string'
      ]
    };
    describe('#tcpdumpRun - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tcpdumpRun(tcpdumpTcpdumpRunBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tcpdump', 'tcpdumpRun', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tcpdumpNeId = 'fakedata';
    describe('#tcpdumpStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.tcpdumpStatus(tcpdumpNeId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.active);
                assert.equal(false, data.response.lastOneDone);
                assert.equal(4, data.response.progress);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tcpdump', 'tcpdumpStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTcpdumpTcpdumpStatus - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getTcpdumpTcpdumpStatus((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tcpdump', 'getTcpdumpTcpdumpStatus', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateNePk = 'fakedata';
    const templateAssociationOnePostBodyParam = {
      templateIds: [
        'string'
      ]
    };
    describe('#associationOnePost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.associationOnePost(templateAssociationOnePostBodyParam, templateNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'associationOnePost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templatePostTemplateTemplateCreateBodyParam = {
      name: 'string',
      templates: [
        {
          name: 'string'
        }
      ]
    };
    describe('#postTemplateTemplateCreate - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTemplateTemplateCreate(templatePostTemplateTemplateCreateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'postTemplateTemplateCreate', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateTemplateGroup = 'fakedata';
    const templatePostTemplateTemplateGroupsTemplateGroupBodyParam = {
      name: 'string',
      templates: [
        {
          name: 'string'
        }
      ]
    };
    describe('#postTemplateTemplateGroupsTemplateGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTemplateTemplateGroupsTemplateGroup(templateTemplateGroup, templatePostTemplateTemplateGroupsTemplateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'postTemplateTemplateGroupsTemplateGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateGroupPrioPostBodyParam = {
      priorities: [
        'string'
      ]
    };
    describe('#groupPrioPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupPrioPost(templateGroupPrioPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'groupPrioPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templatePostTemplateTemplateSelectionTemplateGroupBodyParam = [
      'string'
    ];
    describe('#postTemplateTemplateSelectionTemplateGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postTemplateTemplateSelectionTemplateGroup(templateTemplateGroup, templatePostTemplateTemplateSelectionTemplateGroupBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'postTemplateTemplateSelectionTemplateGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associationAll - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associationAll((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.nepk));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'associationAll', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associationOne - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.associationOne(templateNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.templateIds));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'associationOne', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#template - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.template(templateNePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'template', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const templateLatestOnly = true;
    describe('#getTemplateHistoryNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateHistoryNePk(templateNePk, templateLatestOnly, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'getTemplateHistoryNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateTemplateGroups - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateTemplateGroups((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'getTemplateTemplateGroups', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateTemplateGroupsTemplateGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateTemplateGroupsTemplateGroup(templateTemplateGroup, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.name);
                assert.equal(true, Array.isArray(data.response.selectedTemplates));
                assert.equal(true, Array.isArray(data.response.templates));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'getTemplateTemplateGroupsTemplateGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupPrioGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.groupPrioGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.priorities));
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'groupPrioGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateTemplateSelectionTemplateGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTemplateTemplateSelectionTemplateGroup(templateTemplateGroup, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response[0]);
                assert.equal('string', data.response[1]);
                assert.equal('string', data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'getTemplateTemplateSelectionTemplateGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rdPartyLicenses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.rdPartyLicenses(null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyLicenses', 'rdPartyLicenses', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thirdPartyServicesZscalerConfigurationPostBodyParam = {
      interfaces: [
        [
          'string'
        ]
      ],
      locationSetting: {},
      tunnelSetting: {}
    };
    describe('#zscalerConfigurationPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.zscalerConfigurationPost(thirdPartyServicesZscalerConfigurationPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerConfigurationPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thirdPartyServicesZscalerPauseOrchestrationPostBodyParam = {
      paused: true
    };
    describe('#zscalerPauseOrchestrationPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.zscalerPauseOrchestrationPost(thirdPartyServicesZscalerPauseOrchestrationPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerPauseOrchestrationPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thirdPartyServicesZscalerSubscriptionPostBodyParam = {
      cloud: 'string',
      domain: 'string',
      partnerKey: 'string',
      username: 'string'
    };
    describe('#zscalerSubscriptionPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.zscalerSubscriptionPost(thirdPartyServicesZscalerSubscriptionPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerSubscriptionPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const thirdPartyServicesZscalerVpnLocationExceptionPostBodyParam = [
      'string'
    ];
    describe('#zscalerVpnLocationExceptionPost - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.zscalerVpnLocationExceptionPost(thirdPartyServicesZscalerVpnLocationExceptionPostBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerVpnLocationExceptionPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerConfigurationGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.zscalerConfigurationGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(true, Array.isArray(data.response.interfaces));
                assert.equal('object', typeof data.response.locationSetting);
                assert.equal('object', typeof data.response.tunnelSetting);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerConfigurationGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerConnectivityGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.zscalerConnectivityGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.connectivity);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerConnectivityGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerDefaultTunnelSettingGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.zscalerDefaultTunnelSettingGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.admin);
                assert.equal('string', data.response.authenticationAlgorithm);
                assert.equal(true, data.response.autoMaxBandwidthEnabled);
                assert.equal('string', data.response.dhgroup);
                assert.equal(3, data.response.dpdDelay);
                assert.equal(4, data.response.dpdRetry);
                assert.equal('string', data.response.encryptionAlgorithm);
                assert.equal('string', data.response.exchangeMode);
                assert.equal('string', data.response.idStr);
                assert.equal('string', data.response.idType);
                assert.equal('string', data.response.ikeAuthenticationAlgorithm);
                assert.equal('string', data.response.ikeEncryptionAlgorithm);
                assert.equal(3, data.response.ikeLifetime);
                assert.equal('string', data.response.ipsecAntiReplayWindow);
                assert.equal(2, data.response.lifebytes);
                assert.equal(3, data.response.lifetime);
                assert.equal('string', data.response.mode);
                assert.equal('string', data.response.natMode);
                assert.equal(true, data.response.pfs);
                assert.equal('string', data.response.pfsgroup);
                assert.equal('string', data.response.presharedKey);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerDefaultTunnelSettingGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerPauseOrchestrationGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.zscalerPauseOrchestrationGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.paused);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerPauseOrchestrationGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerRemoteEndpointExceptionGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.zscalerRemoteEndpointExceptionGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<nePk>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerRemoteEndpointExceptionGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerSubscriptionGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.zscalerSubscriptionGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.cloud);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.partnerKey);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerSubscriptionGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnelsGetBondedTunnelsConfigurationAndStatesBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#getBondedTunnelsConfigurationAndStates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBondedTunnelsConfigurationAndStates(tunnelsGetBondedTunnelsConfigurationAndStatesBodyParam, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<tunnel name>']);
                assert.equal('object', typeof data.response.allTunnelState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels', 'getBondedTunnelsConfigurationAndStates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnelsGetBondedTunnelsStatesBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#getBondedTunnelsStates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBondedTunnelsStates(tunnelsGetBondedTunnelsStatesBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<tunnel name>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels', 'getBondedTunnelsStates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnelsGetTunnelsConfigurationAndStatesBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#getTunnelsConfigurationAndStates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnelsConfigurationAndStates(tunnelsGetTunnelsConfigurationAndStatesBodyParam, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<tunnel name>']);
                assert.equal('object', typeof data.response.allTunnelState);
                assert.equal('object', typeof data.response.default);
                assert.equal('object', typeof data.response['pass-through']);
                assert.equal('object', typeof data.response['pass-through-unshaped']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels', 'getTunnelsConfigurationAndStates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnelsGetTunnelsStatesBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#getTunnelsStates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnelsStates(tunnelsGetTunnelsStatesBodyParam, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<tunnel name>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels', 'getTunnelsStates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnelsId = 'fakedata';
    const tunnelsInitiateTracerouteBodyParam = {};
    describe('#initiateTraceroute - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.initiateTraceroute(tunnelsId, tunnelsInitiateTracerouteBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels', 'initiateTraceroute', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnelsTracerouteStateBodyParam = {};
    describe('#tracerouteState - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tracerouteState(tunnelsId, tunnelsTracerouteStateBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels', 'tracerouteState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnelsGetThirdPartyTunnelsConfigurationAndStatesBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#getThirdPartyTunnelsConfigurationAndStates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getThirdPartyTunnelsConfigurationAndStates(tunnelsGetThirdPartyTunnelsConfigurationAndStatesBodyParam, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<tunnel name>']);
                assert.equal('object', typeof data.response.allTunnelState);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels', 'getThirdPartyTunnelsConfigurationAndStates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnelsGetThirdPartyTunnelsStatesBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#getThirdPartyTunnelsStates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getThirdPartyTunnelsStates(tunnelsGetThirdPartyTunnelsStatesBodyParam, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<tunnel name>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels', 'getThirdPartyTunnelsStates', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnels2Limit = 555;
    const tunnels2GetTunnelsBetweenAppliancesBodyParam = {
      ids: [
        'string'
      ]
    };
    describe('#getTunnelsBetweenAppliances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnelsBetweenAppliances(tunnels2GetTunnelsBetweenAppliancesBodyParam, tunnels2Limit, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'getTunnelsBetweenAppliances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnels2MetaData = true;
    describe('#tunnelsCount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.tunnelsCount(tunnels2MetaData, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(4, data.response.totalTunnelCount);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'tunnelsCount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAllBondedTunnels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchAllBondedTunnels(tunnels2Limit, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['1.NE']);
                assert.equal('object', typeof data.response['2.NE']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'searchAllBondedTunnels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnels2NePk = 'fakedata';
    describe('#searchBondedTunnelsForAppliance - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchBondedTunnelsForAppliance(tunnels2NePk, tunnels2Limit, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.exampleTunnelId1);
                assert.equal('object', typeof data.response.exampleTunnelId2);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'searchBondedTunnelsForAppliance', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnels2BondedTunnelId = 'fakedata';
    describe('#getOneBondedTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getOneBondedTunnel(tunnels2BondedTunnelId, tunnels2NePk, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.adminStatus);
                assert.equal('string', data.response.alias);
                assert.equal(true, Array.isArray(data.response.children));
                assert.equal('string', data.response.destNePk);
                assert.equal('string', data.response.destTunnelAlias);
                assert.equal('string', data.response.destTunnelId);
                assert.equal('string', data.response.gmsMarked);
                assert.equal('string', data.response.id);
                assert.equal('string', data.response.operStatus);
                assert.equal(3, data.response.overlayId);
                assert.equal('string', data.response.srcNePk);
                assert.equal('string', data.response.tag);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'getOneBondedTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnels2PhysicalTunnelId = 'fakedata';
    describe('#getBondedTunnelsWithPhysicalTunnels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getBondedTunnelsWithPhysicalTunnels(tunnels2NePk, tunnels2PhysicalTunnelId, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.exampleTunnelId1);
                assert.equal('object', typeof data.response.exampleTunnelId2);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'getBondedTunnelsWithPhysicalTunnels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAllPassThroughTunnels - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.searchAllPassThroughTunnels(tunnels2Limit, null, null, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'searchAllPassThroughTunnels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAppliancePassthroughTunnels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchAppliancePassthroughTunnels(tunnels2NePk, tunnels2Limit, null, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.exampleTunnelId1);
                assert.equal('object', typeof data.response.exampleTunnelId2);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'searchAppliancePassthroughTunnels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnels2PassThroughTunnelId = 'fakedata';
    describe('#getPassThroughTunnel - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getPassThroughTunnel(tunnels2NePk, tunnels2PassThroughTunnelId, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.adminStatus);
                assert.equal('string', data.response.alias);
                assert.equal('string', data.response.destIpAddress);
                assert.equal('string', data.response.destNePk);
                assert.equal(10, data.response.fecRatio);
                assert.equal('string', data.response.fecStatus);
                assert.equal(false, data.response.gmsMarked);
                assert.equal('string', data.response.id);
                assert.equal(2, data.response.max_bw);
                assert.equal(true, data.response.max_bw_auto);
                assert.equal('string', data.response.mode);
                assert.equal('string', data.response.natMode);
                assert.equal('string', data.response.operStatus);
                assert.equal('string', data.response.peerName);
                assert.equal('string', data.response.sourceIpAddress);
                assert.equal('string', data.response.srcNePk);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'getPassThroughTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAllPhysicalTunnels - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.searchAllPhysicalTunnels(tunnels2Limit, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['1.NE']);
                assert.equal('object', typeof data.response['2.NE']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'searchAllPhysicalTunnels', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnels2PhysicalNePk - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getTunnels2PhysicalNePk(tunnels2NePk, tunnels2Limit, null, null, (data, error) => {
            try {
              runCommonAsserts(data, error);
              saveMockData('Tunnels2', 'getTunnels2PhysicalNePk', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const tunnels2TunnelId = 'fakedata';
    describe('#getOnePhysicalTunnel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.getOnePhysicalTunnel(tunnels2NePk, tunnels2TunnelId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Tunnels2', 'getOnePhysicalTunnel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelsDeploymentInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.tunnelsDeploymentInfo((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.nePk);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TunnelsConfiguration', 'tunnelsDeploymentInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelsOverlayInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.tunnelsOverlayInfo((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['0']);
                assert.equal('object', typeof data.response['1-7']);
                assert.equal('object', typeof data.response.all);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TunnelsConfiguration', 'tunnelsOverlayInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#passThroughTunnelsInfo - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.passThroughTunnelsInfo((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.nePk);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('TunnelsConfiguration', 'passThroughTunnelsInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const uiUsageStatsUiName = 'fakedata';
    describe('#uiUsageStats - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.uiUsageStats(uiUsageStatsUiName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UiUsageStats', 'uiUsageStats', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const upgradeAppliancesUpgradeAppliancesBodyParam = {
      imageName: 'string',
      installOption: 'string',
      neList: [
        'string'
      ],
      version: 'string'
    };
    describe('#upgradeAppliances - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.upgradeAppliances(upgradeAppliancesUpgradeAppliancesBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.clientKey);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UpgradeAppliances', 'upgradeAppliances', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const userAccountNeId = 'fakedata';
    const userAccountCached = 'fakedata';
    describe('#getUserAccount - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUserAccount(userAccountNeId, userAccountCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.sessions);
                assert.equal('object', typeof data.response.users);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('UserAccount', 'getUserAccount', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersForgotPasswordBodyParam = {
      userName: 'string'
    };
    describe('#forgotPassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.forgotPassword(usersForgotPasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'forgotPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersResetPasswordBodyParam = {
      confirmPassword: 'string',
      id: 'string',
      password: 'string',
      tfaApp: true,
      tfaEmail: true
    };
    describe('#resetPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.resetPassword(usersResetPasswordBodyParam, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.barcodeBase64Jpeg);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'resetPassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersNewUser = true;
    const usersPostUsersNewUserBodyParam = {
      createDisplayTime: 'string',
      email: 'string',
      firstName: 'string',
      isTwoFactorEmail: false,
      isTwoFactorTime: true,
      lastName: 'string',
      password: 'string',
      phone: 'string',
      repeatPassword: 'string',
      role: 'string',
      status: 'string',
      userPk: 'string',
      username: 'string'
    };
    describe('#postUsersNewUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postUsersNewUser(usersNewUser, usersPostUsersNewUserBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postUsersNewUser', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUsername = 'fakedata';
    const usersPostUsersUsernamePasswordBodyParam = {
      newPassword: 'string',
      oldPassword: 'string'
    };
    describe('#postUsersUsernamePassword - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postUsersUsernamePassword(usersUsername, usersPostUsersUsernamePasswordBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'postUsersUsernamePassword', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#users - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.users((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'users', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#newTfaKey - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.newTfaKey((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('string', data.response.barcodeBase64Jpeg);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'newTfaKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUserName = 'fakedata';
    describe('#getUsersUserName - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.getUsersUserName(usersUserName, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(5, data.response.createTime);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.firstName);
                assert.equal(false, data.response.isTwoFactorEmail);
                assert.equal(true, data.response.isTwoFactorTime);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.phone);
                assert.equal('string', data.response.role);
                assert.equal('string', data.response.salt);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.userPk);
                assert.equal('string', data.response.username);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'getUsersUserName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const virtualifNeId = 'fakedata';
    const virtualifCached = true;
    describe('#loopbackGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.loopbackGet(virtualifNeId, virtualifCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<interface>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualif', 'loopbackGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vtiGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vtiGet(virtualifNeId, virtualifCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<interface>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Virtualif', 'vtiGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vrrpNeId = 'fakedata';
    const vrrpCached = true;
    describe('#vrrpGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vrrpGet(vrrpNeId, vrrpCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Vrrp', 'vrrpGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vXOAImagesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.vXOAImagesGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VxoaImages', 'vXOAImagesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const wccpNeId = 'fakedata';
    const wccpCached = true;
    describe('#wccpConfigGroup - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.wccpConfigGroup(wccpNeId, wccpCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['51']);
                assert.equal('object', typeof data.response['52']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wccp', 'wccpConfigGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wccpConfigSystem - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.wccpConfigSystem(wccpNeId, wccpCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(false, data.response.enable);
                assert.equal(4, data.response.mcast_ttl);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wccp', 'wccpConfigSystem', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wccpState - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.wccpState(wccpNeId, wccpCached, (data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response.group);
                assert.equal('object', typeof data.response.system);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Wccp', 'wccpState', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zonesDeleteDependencies = true;
    const zonesZonesPostBodyParam = {
      '<zoneId>': {
        name: 'string'
      }
    };
    describe('#zonesPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.zonesPost(zonesZonesPostBodyParam, zonesDeleteDependencies, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'zonesPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const zonesNextIdPostBodyParam = {
      nextId: 1
    };
    describe('#nextIdPost - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.nextIdPost(zonesNextIdPostBodyParam, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'nextIdPost', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zonesGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.zonesGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal('object', typeof data.response['<zoneId>']);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'zonesGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#nextIdGet - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.nextIdGet((data, error) => {
            try {
              runCommonAsserts(data, error);
              if (stub) {
                assert.equal(3, data.response.nextId);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Zones', 'nextIdGet', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customSeverityDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.customSeverityDelete((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'customSeverityDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#singleCustomSeverityDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.singleCustomSeverityDelete(alarmAlarmTypeId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'singleCustomSeverityDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmEmailDelayDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.alarmEmailDelayDelete((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'alarmEmailDelayDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDisabledAlarmsConfig - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteDisabledAlarmsConfig((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Alarm', 'deleteDisabledAlarmsConfig', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const applianceBackupFilePk = 555;
    describe('#backupDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.backupDelete(applianceBackupFilePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'backupDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteForRediscovery - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteForRediscovery(applianceNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'deleteForRediscovery', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplianceExtraInfo - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplianceExtraInfo(applianceNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'deleteApplianceExtraInfo', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceDel - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.applianceDel(applianceNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Appliance', 'applianceDel', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#compoundDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.compoundDelete(applicationDefinitionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'compoundDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.dnsDelete(applicationDefinitionDomain, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'dnsDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipIntelligenceDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.ipIntelligenceDelete(applicationDefinitionIpStart, applicationDefinitionIpEnd, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'ipIntelligenceDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meterFlowDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.meterFlowDelete(applicationDefinitionFlowType, applicationDefinitionMid, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'meterFlowDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProtocolDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.portProtocolDelete(applicationDefinitionPort, applicationDefinitionProtocol, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'portProtocolDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saasDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.saasDelete(applicationDefinitionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ApplicationDefinition', 'saasDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#textCustomizationConfigDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.textCustomizationConfigDelete((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BrandCustomization', 'textCustomizationConfigDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#imageDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.imageDelete(brandCustomizationType, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('BrandCustomization', 'imageDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePartition - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePartition(dbPartitionTable, dbPartitionPartition, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('DbPartition', 'deletePartition', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelExceptionDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tunnelExceptionDelete((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Exception', 'tunnelExceptionDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelExceptionSingleDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.tunnelExceptionSingleDelete(exceptionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Exception', 'tunnelExceptionSingleDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePreconfiguration - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deletePreconfiguration(gmsPreconfigId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'deletePreconfiguration', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.groupDelete(gmsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'groupDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduledJobDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.scheduledJobDelete(gmsJobId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'scheduledJobDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExistingMenuType - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExistingMenuType(gmsMenuTypeName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'deleteExistingMenuType', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOverlayApplianceAssociation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteOverlayApplianceAssociation(gmsOverlayId, gmsNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'deleteOverlayApplianceAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExistingOverlay - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExistingOverlay(gmsOverlayId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'deleteExistingOverlay', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTunnelGroupApplianceAssociation - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTunnelGroupApplianceAssociation(gmsTunnelGroupId, gmsNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'deleteTunnelGroupApplianceAssociation', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExistingTunnelGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteExistingTunnelGroup(gmsId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Gms', 'deleteExistingTunnelGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGmsConfigBase - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteGmsConfigBase(gmsConfigBase, null, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsConfig', 'deleteGmsConfigBase', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sMTP - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.sMTP((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('GmsSMTP', 'sMTP', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLicenseToken - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteLicenseToken(licenseNePk, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('License', 'deleteLicenseToken', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delNotification - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.delNotification((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Notification', 'delNotification', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProfilesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.portProfilesDelete(portProfilesPortProfileId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('PortProfiles', 'portProfilesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplianceAccessGroupByName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteApplianceAccessGroupByName(rbacApplianceAccessGroupName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'deleteApplianceAccessGroupByName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const rbacUsername = 'fakedata';
    describe('#deleteRbacAssignment - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRbacAssignment(rbacUsername, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'deleteRbacAssignment', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoleByRoleName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteRoleByRoleName(rbacRoleName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Rbac', 'deleteRoleByRoleName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.regionDelete(regionsRegionId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Regions', 'regionDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remoteLogReceiverDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.remoteLogReceiverDelete(remoteLogReceiverReceiverId, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('RemoteLogReceiver', 'remoteLogReceiverDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalDeleteOldKey - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.spPortalDeleteOldKey((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('SpPortal', 'spPortalDeleteOldKey', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateTemplateGroupsTemplateGroup - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteTemplateTemplateGroupsTemplateGroup(templateTemplateGroup, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Template', 'deleteTemplateTemplateGroupsTemplateGroup', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerSubscriptionDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.zscalerSubscriptionDelete((data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('ThirdPartyServices', 'zscalerSubscriptionDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const usersUserId = 'fakedata';
    describe('#deleteUsersUserIdUserName - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.deleteUsersUserIdUserName(usersUserId, usersUserName, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('Users', 'deleteUsersUserIdUserName', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    const vxoaImagesImageFile = 'fakedata';
    describe('#vXOAImagesDelete - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.vXOAImagesDelete(vxoaImagesImageFile, (data, error) => {
            try {
              if (stub) {
                const displayE = 'Error 400 received on request';
                runErrorAsserts(data, error, 'AD.500', 'Test-silverpeak-connectorRest-handleEndResponse', displayE);
              } else {
                runCommonAsserts(data, error);
              }
              saveMockData('VxoaImages', 'vXOAImagesDelete', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
