/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const path = require('path');
const util = require('util');
const execute = require('child_process').execSync;
const fs = require('fs-extra');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');
const Ajv = require('ajv');

const ajv = new Ajv({ strictSchema: false, allErrors: true, allowUnionTypes: true });
const anything = td.matchers.anything();
let logLevel = 'none';
const isRapidFail = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;

// uncomment if connecting
// samProps.host = 'replace.hostorip.here';
// samProps.authentication.username = 'username';
// samProps.authentication.password = 'password';
// samProps.authentication.token = 'password';
// samProps.protocol = 'http';
// samProps.port = 80;
// samProps.ssl.enabled = false;
// samProps.ssl.accept_invalid_cert = false;

samProps.request.attempt_timeout = 1200000;
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-silverpeak',
      type: 'Silverpeak',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

// require the adapter that we are going to be using
const Silverpeak = require('../../adapter');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Silverpeak Adapter Test', () => {
  describe('Silverpeak Class Tests', () => {
    const a = new Silverpeak(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#iapGetAdapterWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.iapGetAdapterWorkflowFunctions([]);

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          // Define the JSON schema for package.json
          const packageJsonSchema = {
            type: 'object',
            properties: {
              name: { type: 'string' },
              version: { type: 'string' }
              // May need to add more properties as needed
            },
            required: ['name', 'version']
          };
          const validate = ajv.compile(packageJsonSchema);
          const isValid = validate(packageDotJson);

          if (isValid === false) {
            log.error('The package.json contains errors');
            assert.equal(true, isValid);
          } else {
            assert.equal(true, isValid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json standard fields should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('silverpeak'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          assert.notEqual(undefined, packageDotJson.description);
          assert.notEqual(null, packageDotJson.description);
          assert.notEqual('', packageDotJson.description);
          assert.equal('adapter.js', packageDotJson.main);
          assert.notEqual(undefined, packageDotJson.wizardVersion);
          assert.notEqual(null, packageDotJson.wizardVersion);
          assert.notEqual('', packageDotJson.wizardVersion);
          assert.notEqual(undefined, packageDotJson.engineVersion);
          assert.notEqual(null, packageDotJson.engineVersion);
          assert.notEqual('', packageDotJson.engineVersion);
          assert.equal('http', packageDotJson.adapterType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper scripts should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.scripts);
          assert.notEqual(null, packageDotJson.scripts);
          assert.notEqual('', packageDotJson.scripts);
          assert.equal('node utils/setup.js', packageDotJson.scripts.preinstall);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js', packageDotJson.scripts.lint);
          assert.equal('node --max_old_space_size=4096 ./node_modules/eslint/bin/eslint.js . --ext .json --ext .js --quiet', packageDotJson.scripts['lint:errors']);
          assert.equal('mocha test/unit/adapterBaseTestUnit.js --LOG=error', packageDotJson.scripts['test:baseunit']);
          assert.equal('mocha test/unit/adapterTestUnit.js --LOG=error', packageDotJson.scripts['test:unit']);
          assert.equal('mocha test/integration/adapterTestIntegration.js --LOG=error', packageDotJson.scripts['test:integration']);
          assert.equal('npm run test:baseunit && npm run test:unit && npm run test:integration', packageDotJson.scripts.test);
          assert.equal('npm publish --registry=https://registry.npmjs.org --access=public', packageDotJson.scripts.deploy);
          assert.equal('npm run deploy', packageDotJson.scripts.build);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper directories should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.repository);
          assert.notEqual(null, packageDotJson.repository);
          assert.notEqual('', packageDotJson.repository);
          assert.equal('git', packageDotJson.repository.type);
          assert.equal('git@gitlab.com:itentialopensource/adapters/', packageDotJson.repository.url.substring(0, 43));
          assert.equal('https://gitlab.com/itentialopensource/adapters/', packageDotJson.homepage.substring(0, 47));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.dependencies);
          assert.notEqual(null, packageDotJson.dependencies);
          assert.notEqual('', packageDotJson.dependencies);
          assert.equal('^8.17.1', packageDotJson.dependencies.ajv);
          assert.equal('^1.7.9', packageDotJson.dependencies.axios);
          assert.equal('^11.0.0', packageDotJson.dependencies.commander);
          assert.equal('^11.2.0', packageDotJson.dependencies['fs-extra']);
          assert.equal('^10.8.2', packageDotJson.dependencies.mocha);
          assert.equal('^2.0.1', packageDotJson.dependencies['mocha-param']);
          assert.equal('^0.4.4', packageDotJson.dependencies.ping);
          assert.equal('^1.4.10', packageDotJson.dependencies['readline-sync']);
          assert.equal('^7.6.3', packageDotJson.dependencies.semver);
          assert.equal('^3.17.0', packageDotJson.dependencies.winston);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json proper dev dependencies should be provided', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(undefined, packageDotJson.devDependencies);
          assert.notEqual(null, packageDotJson.devDependencies);
          assert.notEqual('', packageDotJson.devDependencies);
          assert.equal('^4.3.7', packageDotJson.devDependencies.chai);
          assert.equal('^8.44.0', packageDotJson.devDependencies.eslint);
          assert.equal('^15.0.0', packageDotJson.devDependencies['eslint-config-airbnb-base']);
          assert.equal('^2.27.5', packageDotJson.devDependencies['eslint-plugin-import']);
          assert.equal('^3.1.0', packageDotJson.devDependencies['eslint-plugin-json']);
          assert.equal('^3.18.0', packageDotJson.devDependencies.testdouble);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('silverpeak'));
          assert.equal('Adapter', pronghornDotJson.type);
          assert.equal('Silverpeak', pronghornDotJson.export);
          assert.equal('Silverpeak', pronghornDotJson.title);
          assert.equal('adapter.js', pronghornDotJson.src);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should contain generic adapter methods', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(undefined, pronghornDotJson.methods);
          assert.notEqual(null, pronghornDotJson.methods);
          assert.notEqual('', pronghornDotJson.methods);
          assert.equal(true, Array.isArray(pronghornDotJson.methods));
          assert.notEqual(0, pronghornDotJson.methods.length);
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUpdateAdapterConfiguration'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapSuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapUnsuspendAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterQueue'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapFindAdapterPath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapTroubleshootAdapter'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterHealthcheck'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterConnectivity'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterBasicGet'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapMoveAdapterEntitiesToDB'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapDeactivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapActivateTasks'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapPopulateEntityCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRetrieveEntitiesCache'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevice'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getDevicesFiltered'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'isAlive'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'getConfig'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetDeviceCount'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapExpandedGenericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequest'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'genericAdapterRequestNoBasePath'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterLint'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapRunAdapterTests'));
          assert.notEqual(undefined, pronghornDotJson.methods.find((e) => e.name === 'iapGetAdapterInventory'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.lastIndexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json verify input/output schema objects', (done) => {
        const verifySchema = (methodName, schema) => {
          try {
            ajv.compile(schema);
          } catch (error) {
            const errorMessage = `Invalid schema found in '${methodName}' method.
          Schema => ${JSON.stringify(schema)}.
          Details => ${error.message}`;
            throw new Error(errorMessage);
          }
        };

        try {
          const pronghornDotJson = require('../../pronghorn.json');
          const { methods } = pronghornDotJson;
          for (let i = 0; i < methods.length; i += 1) {
            for (let j = 0; j < methods[i].input.length; j += 1) {
              const inputSchema = methods[i].input[j].schema;
              if (inputSchema) {
                verifySchema(methods[i].name, inputSchema);
              }
            }
            const outputSchema = methods[i].output.schema;
            if (outputSchema) {
              verifySchema(methods[i].name, outputSchema);
            }
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-silverpeak', propertiesDotJson.$id);
          assert.equal('object', propertiesDotJson.type);
          assert.equal('http://json-schema.org/draft-07/schema#', propertiesDotJson.$schema);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should contain generic adapter properties', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.notEqual(undefined, propertiesDotJson.properties);
          assert.notEqual(null, propertiesDotJson.properties);
          assert.notEqual('', propertiesDotJson.properties);
          assert.equal('string', propertiesDotJson.properties.host.type);
          assert.equal('integer', propertiesDotJson.properties.port.type);
          assert.equal('boolean', propertiesDotJson.properties.stub.type);
          assert.equal('string', propertiesDotJson.properties.protocol.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.authentication);
          assert.notEqual(null, propertiesDotJson.definitions.authentication);
          assert.notEqual('', propertiesDotJson.definitions.authentication);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.auth_method.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.invalid_token_error.type);
          assert.equal('integer', propertiesDotJson.definitions.authentication.properties.token_timeout.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.token_cache.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field.type));
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.authentication.properties.auth_field_format.type));
          assert.equal('boolean', propertiesDotJson.definitions.authentication.properties.auth_logging.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_id.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.client_secret.type);
          assert.equal('string', propertiesDotJson.definitions.authentication.properties.grant_type.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.ssl);
          assert.notEqual(null, propertiesDotJson.definitions.ssl);
          assert.notEqual('', propertiesDotJson.definitions.ssl);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ecdhCurve.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.cert_file.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.secure_protocol.type);
          assert.equal('string', propertiesDotJson.definitions.ssl.properties.ciphers.type);
          assert.equal('string', propertiesDotJson.properties.base_path.type);
          assert.equal('string', propertiesDotJson.properties.version.type);
          assert.equal('string', propertiesDotJson.properties.cache_location.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_pathvars.type);
          assert.equal('boolean', propertiesDotJson.properties.encode_queryvars.type);
          assert.equal(true, Array.isArray(propertiesDotJson.properties.save_metric.type));
          assert.notEqual(undefined, propertiesDotJson.definitions);
          assert.notEqual(null, propertiesDotJson.definitions);
          assert.notEqual('', propertiesDotJson.definitions);
          assert.notEqual(undefined, propertiesDotJson.definitions.healthcheck);
          assert.notEqual(null, propertiesDotJson.definitions.healthcheck);
          assert.notEqual('', propertiesDotJson.definitions.healthcheck);
          assert.equal('string', propertiesDotJson.definitions.healthcheck.properties.type.type);
          assert.equal('integer', propertiesDotJson.definitions.healthcheck.properties.frequency.type);
          assert.equal('object', propertiesDotJson.definitions.healthcheck.properties.query_object.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.throttle);
          assert.notEqual(null, propertiesDotJson.definitions.throttle);
          assert.notEqual('', propertiesDotJson.definitions.throttle);
          assert.equal('boolean', propertiesDotJson.definitions.throttle.properties.throttle_enabled.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.number_pronghorns.type);
          assert.equal('string', propertiesDotJson.definitions.throttle.properties.sync_async.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.max_in_queue.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.concurrent_max.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.expire_timeout.type);
          assert.equal('integer', propertiesDotJson.definitions.throttle.properties.avg_runtime.type);
          assert.equal('array', propertiesDotJson.definitions.throttle.properties.priorities.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.request);
          assert.notEqual(null, propertiesDotJson.definitions.request);
          assert.notEqual('', propertiesDotJson.definitions.request);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_redirects.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.number_retries.type);
          assert.equal(true, Array.isArray(propertiesDotJson.definitions.request.properties.limit_retry_error.type));
          assert.equal('array', propertiesDotJson.definitions.request.properties.failover_codes.type);
          assert.equal('integer', propertiesDotJson.definitions.request.properties.attempt_timeout.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.payload.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.uriOptions.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.addlHeaders.type);
          assert.equal('object', propertiesDotJson.definitions.request.properties.global_request.properties.authData.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.healthcheck_on_timeout.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_raw.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.archiving.type);
          assert.equal('boolean', propertiesDotJson.definitions.request.properties.return_request.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.proxy);
          assert.notEqual(null, propertiesDotJson.definitions.proxy);
          assert.notEqual('', propertiesDotJson.definitions.proxy);
          assert.equal('boolean', propertiesDotJson.definitions.proxy.properties.enabled.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.proxy.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.protocol.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.proxy.properties.password.type);
          assert.notEqual(undefined, propertiesDotJson.definitions.mongo);
          assert.notEqual(null, propertiesDotJson.definitions.mongo);
          assert.notEqual('', propertiesDotJson.definitions.mongo);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.host.type);
          assert.equal('integer', propertiesDotJson.definitions.mongo.properties.port.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.database.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.username.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.password.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.replSet.type);
          assert.equal('object', propertiesDotJson.definitions.mongo.properties.db_ssl.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.enabled.type);
          assert.equal('boolean', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.accept_invalid_cert.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.ca_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.key_file.type);
          assert.equal('string', propertiesDotJson.definitions.mongo.properties.db_ssl.properties.cert_file.type);
          assert.notEqual('', propertiesDotJson.definitions.devicebroker);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevice.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getDevicesFiltered.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.isAlive.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getConfig.type);
          assert.equal('array', propertiesDotJson.definitions.devicebroker.properties.getCount.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('error.json should have standard adapter errors', (done) => {
        try {
          const errorDotJson = require('../../error.json');
          assert.notEqual(undefined, errorDotJson.errors);
          assert.notEqual(null, errorDotJson.errors);
          assert.notEqual('', errorDotJson.errors);
          assert.equal(true, Array.isArray(errorDotJson.errors));
          assert.notEqual(0, errorDotJson.errors.length);
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.100'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.101'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.102'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.110'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.111'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.112'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.113'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.114'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.115'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.116'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.300'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.301'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.302'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.303'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.304'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.305'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.310'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.311'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.312'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.320'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.321'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.400'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.401'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.402'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.500'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.501'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.502'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.503'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.600'));
          assert.notEqual(undefined, errorDotJson.errors.find((e) => e.icode === 'AD.900'));
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('sampleProperties.json should contain generic adapter properties', (done) => {
        try {
          const sampleDotJson = require('../../sampleProperties.json');
          assert.notEqual(-1, sampleDotJson.id.indexOf('silverpeak'));
          assert.equal('Silverpeak', sampleDotJson.type);
          assert.notEqual(undefined, sampleDotJson.properties);
          assert.notEqual(null, sampleDotJson.properties);
          assert.notEqual('', sampleDotJson.properties);
          assert.notEqual(undefined, sampleDotJson.properties.host);
          assert.notEqual(undefined, sampleDotJson.properties.port);
          assert.notEqual(undefined, sampleDotJson.properties.stub);
          assert.notEqual(undefined, sampleDotJson.properties.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.authentication);
          assert.notEqual(null, sampleDotJson.properties.authentication);
          assert.notEqual('', sampleDotJson.properties.authentication);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_method);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.username);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.password);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.invalid_token_error);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.token_cache);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_field_format);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.auth_logging);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_id);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.client_secret);
          assert.notEqual(undefined, sampleDotJson.properties.authentication.grant_type);
          assert.notEqual(undefined, sampleDotJson.properties.ssl);
          assert.notEqual(null, sampleDotJson.properties.ssl);
          assert.notEqual('', sampleDotJson.properties.ssl);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ecdhCurve);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.secure_protocol);
          assert.notEqual(undefined, sampleDotJson.properties.ssl.ciphers);
          assert.notEqual(undefined, sampleDotJson.properties.base_path);
          assert.notEqual(undefined, sampleDotJson.properties.version);
          assert.notEqual(undefined, sampleDotJson.properties.cache_location);
          assert.notEqual(undefined, sampleDotJson.properties.encode_pathvars);
          assert.notEqual(undefined, sampleDotJson.properties.encode_queryvars);
          assert.notEqual(undefined, sampleDotJson.properties.save_metric);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck);
          assert.notEqual(null, sampleDotJson.properties.healthcheck);
          assert.notEqual('', sampleDotJson.properties.healthcheck);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.type);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.frequency);
          assert.notEqual(undefined, sampleDotJson.properties.healthcheck.query_object);
          assert.notEqual(undefined, sampleDotJson.properties.throttle);
          assert.notEqual(null, sampleDotJson.properties.throttle);
          assert.notEqual('', sampleDotJson.properties.throttle);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.throttle_enabled);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.number_pronghorns);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.sync_async);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.max_in_queue);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.concurrent_max);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.expire_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.avg_runtime);
          assert.notEqual(undefined, sampleDotJson.properties.throttle.priorities);
          assert.notEqual(undefined, sampleDotJson.properties.request);
          assert.notEqual(null, sampleDotJson.properties.request);
          assert.notEqual('', sampleDotJson.properties.request);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_redirects);
          assert.notEqual(undefined, sampleDotJson.properties.request.number_retries);
          assert.notEqual(undefined, sampleDotJson.properties.request.limit_retry_error);
          assert.notEqual(undefined, sampleDotJson.properties.request.failover_codes);
          assert.notEqual(undefined, sampleDotJson.properties.request.attempt_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.payload);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.uriOptions);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.addlHeaders);
          assert.notEqual(undefined, sampleDotJson.properties.request.global_request.authData);
          assert.notEqual(undefined, sampleDotJson.properties.request.healthcheck_on_timeout);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_raw);
          assert.notEqual(undefined, sampleDotJson.properties.request.archiving);
          assert.notEqual(undefined, sampleDotJson.properties.request.return_request);
          assert.notEqual(undefined, sampleDotJson.properties.proxy);
          assert.notEqual(null, sampleDotJson.properties.proxy);
          assert.notEqual('', sampleDotJson.properties.proxy);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.host);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.port);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.protocol);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.username);
          assert.notEqual(undefined, sampleDotJson.properties.proxy.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo);
          assert.notEqual(null, sampleDotJson.properties.mongo);
          assert.notEqual('', sampleDotJson.properties.mongo);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.host);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.port);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.database);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.username);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.password);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.replSet);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.enabled);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.accept_invalid_cert);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.ca_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.key_file);
          assert.notEqual(undefined, sampleDotJson.properties.mongo.db_ssl.cert_file);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevice);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getDevicesFiltered);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.isAlive);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getConfig);
          assert.notEqual(undefined, sampleDotJson.properties.devicebroker.getCount);
          assert.notEqual(undefined, sampleDotJson.properties.cache);
          assert.notEqual(undefined, sampleDotJson.properties.cache.entities);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUpdateAdapterConfiguration', () => {
      it('should have a iapUpdateAdapterConfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.iapUpdateAdapterConfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapSuspendAdapter', () => {
      it('should have a iapSuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapSuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapUnsuspendAdapter', () => {
      it('should have a iapUnsuspendAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapUnsuspendAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterQueue', () => {
      it('should have a iapGetAdapterQueue function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterQueue === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapFindAdapterPath', () => {
      it('should have a iapFindAdapterPath function', (done) => {
        try {
          assert.equal(true, typeof a.iapFindAdapterPath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('iapFindAdapterPath should find atleast one path that matches', (done) => {
        try {
          a.iapFindAdapterPath('{base_path}/{version}', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.equal(true, data.found);
              assert.notEqual(undefined, data.foundIn);
              assert.notEqual(null, data.foundIn);
              assert.notEqual(0, data.foundIn.length);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapTroubleshootAdapter', () => {
      it('should have a iapTroubleshootAdapter function', (done) => {
        try {
          assert.equal(true, typeof a.iapTroubleshootAdapter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterHealthcheck', () => {
      it('should have a iapRunAdapterHealthcheck function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterHealthcheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterConnectivity', () => {
      it('should have a iapRunAdapterConnectivity function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterConnectivity === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterBasicGet', () => {
      it('should have a iapRunAdapterBasicGet function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterBasicGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapMoveAdapterEntitiesToDB', () => {
      it('should have a iapMoveAdapterEntitiesToDB function', (done) => {
        try {
          assert.equal(true, typeof a.iapMoveAdapterEntitiesToDB === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapDeactivateTasks', () => {
      it('should have a iapDeactivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapDeactivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapActivateTasks', () => {
      it('should have a iapActivateTasks function', (done) => {
        try {
          assert.equal(true, typeof a.iapActivateTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapPopulateEntityCache', () => {
      it('should have a iapPopulateEntityCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapPopulateEntityCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRetrieveEntitiesCache', () => {
      it('should have a iapRetrieveEntitiesCache function', (done) => {
        try {
          assert.equal(true, typeof a.iapRetrieveEntitiesCache === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#hasEntities', () => {
      it('should have a hasEntities function', (done) => {
        try {
          assert.equal(true, typeof a.hasEntities === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevice', () => {
      it('should have a getDevice function', (done) => {
        try {
          assert.equal(true, typeof a.getDevice === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getDevicesFiltered', () => {
      it('should have a getDevicesFiltered function', (done) => {
        try {
          assert.equal(true, typeof a.getDevicesFiltered === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#isAlive', () => {
      it('should have a isAlive function', (done) => {
        try {
          assert.equal(true, typeof a.isAlive === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#getConfig', () => {
      it('should have a getConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetDeviceCount', () => {
      it('should have a iapGetDeviceCount function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetDeviceCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapExpandedGenericAdapterRequest', () => {
      it('should have a iapExpandedGenericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.iapExpandedGenericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequest', () => {
      it('should have a genericAdapterRequest function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequest === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#genericAdapterRequestNoBasePath', () => {
      it('should have a genericAdapterRequestNoBasePath function', (done) => {
        try {
          assert.equal(true, typeof a.genericAdapterRequestNoBasePath === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapRunAdapterLint', () => {
      it('should have a iapRunAdapterLint function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterLint === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the lint results', (done) => {
        try {
          a.iapRunAdapterLint((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.status);
              assert.notEqual(null, data.status);
              assert.equal('SUCCESS', data.status);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRunAdapterTests', () => {
      it('should have a iapRunAdapterTests function', (done) => {
        try {
          assert.equal(true, typeof a.iapRunAdapterTests === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#iapGetAdapterInventory', () => {
      it('should have a iapGetAdapterInventory function', (done) => {
        try {
          assert.equal(true, typeof a.iapGetAdapterInventory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('retrieve the inventory', (done) => {
        try {
          a.iapGetAdapterInventory((data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    describe('metadata.json', () => {
      it('should have a metadata.json', (done) => {
        try {
          fs.exists('metadata.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json is customized', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.equal('adapter-silverpeak', metadataDotJson.name);
          assert.notEqual(undefined, metadataDotJson.webName);
          assert.notEqual(null, metadataDotJson.webName);
          assert.notEqual('', metadataDotJson.webName);
          assert.equal('Adapter', metadataDotJson.type);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json contains accurate documentation', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.documentation);
          assert.equal('https://www.npmjs.com/package/@itentialopensource/adapter-silverpeak', metadataDotJson.documentation.npmLink);
          assert.equal('https://docs.itential.com/opensource/docs/troubleshooting-an-adapter', metadataDotJson.documentation.faqLink);
          assert.equal('https://gitlab.com/itentialopensource/adapters/contributing-guide', metadataDotJson.documentation.contributeLink);
          assert.equal('https://itential.atlassian.net/servicedesk/customer/portals', metadataDotJson.documentation.issueLink);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('metadata.json has related items', (done) => {
        try {
          const metadataDotJson = require('../../metadata.json');
          assert.notEqual(undefined, metadataDotJson.relatedItems);
          assert.notEqual(undefined, metadataDotJson.relatedItems.adapters);
          assert.notEqual(undefined, metadataDotJson.relatedItems.integrations);
          assert.notEqual(undefined, metadataDotJson.relatedItems.ecosystemApplications);
          assert.notEqual(undefined, metadataDotJson.relatedItems.workflowProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.transformationProjects);
          assert.notEqual(undefined, metadataDotJson.relatedItems.exampleProjects);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#actionAndAuditLogEntries - errors', () => {
      it('should have a actionAndAuditLogEntries function', (done) => {
        try {
          assert.equal(true, typeof a.actionAndAuditLogEntries === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.actionAndAuditLogEntries(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-actionAndAuditLogEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.actionAndAuditLogEntries('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-actionAndAuditLogEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.actionAndAuditLogEntries('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-actionAndAuditLogEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing logLevel', (done) => {
        try {
          a.actionAndAuditLogEntries('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'logLevel is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-actionAndAuditLogEntries', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#actionCancel - errors', () => {
      it('should have a actionCancel function', (done) => {
        try {
          assert.equal(true, typeof a.actionCancel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.actionCancel(null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-actionCancel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#actionStatus - errors', () => {
      it('should have a actionStatus function', (done) => {
        try {
          assert.equal(true, typeof a.actionStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.actionStatus(null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-actionStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmAcknowledgement - errors', () => {
      it('should have a alarmAcknowledgement function', (done) => {
        try {
          assert.equal(true, typeof a.alarmAcknowledgement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.alarmAcknowledgement(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-alarmAcknowledgement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmAcknowledgementBody', (done) => {
        try {
          a.alarmAcknowledgement('fakeparam', null, (data, error) => {
            try {
              const displayE = 'alarmAcknowledgementBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-alarmAcknowledgement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsAlarmAcknowledgement - errors', () => {
      it('should have a gmsAlarmAcknowledgement function', (done) => {
        try {
          assert.equal(true, typeof a.gmsAlarmAcknowledgement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gmsAlarmAcknowledgementBody', (done) => {
        try {
          a.gmsAlarmAcknowledgement(null, (data, error) => {
            try {
              const displayE = 'gmsAlarmAcknowledgementBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-gmsAlarmAcknowledgement', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmApplianceGetPost - errors', () => {
      it('should have a alarmApplianceGetPost function', (done) => {
        try {
          assert.equal(true, typeof a.alarmApplianceGetPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceAlarmGetPostBody', (done) => {
        try {
          a.alarmApplianceGetPost(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceAlarmGetPostBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-alarmApplianceGetPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmClearance - errors', () => {
      it('should have a alarmClearance function', (done) => {
        try {
          assert.equal(true, typeof a.alarmClearance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.alarmClearance(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-alarmClearance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmClearanceBody', (done) => {
        try {
          a.alarmClearance('fakeparam', null, (data, error) => {
            try {
              const displayE = 'alarmClearanceBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-alarmClearance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsAlarmClearance - errors', () => {
      it('should have a gmsAlarmClearance function', (done) => {
        try {
          assert.equal(true, typeof a.gmsAlarmClearance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gmsAlarmClearanceBody', (done) => {
        try {
          a.gmsAlarmClearance(null, (data, error) => {
            try {
              const displayE = 'gmsAlarmClearanceBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-gmsAlarmClearance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allApplianceAlarmSummary - errors', () => {
      it('should have a allApplianceAlarmSummary function', (done) => {
        try {
          assert.equal(true, typeof a.allApplianceAlarmSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceAlarmSummary - errors', () => {
      it('should have a applianceAlarmSummary function', (done) => {
        try {
          assert.equal(true, typeof a.applianceAlarmSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.applianceAlarmSummary(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceAlarmSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customSeverityDelete - errors', () => {
      it('should have a customSeverityDelete function', (done) => {
        try {
          assert.equal(true, typeof a.customSeverityDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customSeverityGet - errors', () => {
      it('should have a customSeverityGet function', (done) => {
        try {
          assert.equal(true, typeof a.customSeverityGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customSeverityPOST - errors', () => {
      it('should have a customSeverityPOST function', (done) => {
        try {
          assert.equal(true, typeof a.customSeverityPOST === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.customSeverityPOST(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-customSeverityPOST', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#customSeverityPut - errors', () => {
      it('should have a customSeverityPut function', (done) => {
        try {
          assert.equal(true, typeof a.customSeverityPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.customSeverityPut(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-customSeverityPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#singleCustomSeverityDelete - errors', () => {
      it('should have a singleCustomSeverityDelete function', (done) => {
        try {
          assert.equal(true, typeof a.singleCustomSeverityDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmTypeId', (done) => {
        try {
          a.singleCustomSeverityDelete(null, (data, error) => {
            try {
              const displayE = 'alarmTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-singleCustomSeverityDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#singleCustomSeverityGet - errors', () => {
      it('should have a singleCustomSeverityGet function', (done) => {
        try {
          assert.equal(true, typeof a.singleCustomSeverityGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmTypeId', (done) => {
        try {
          a.singleCustomSeverityGet(null, (data, error) => {
            try {
              const displayE = 'alarmTypeId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-singleCustomSeverityGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmEmailDelayDelete - errors', () => {
      it('should have a alarmEmailDelayDelete function', (done) => {
        try {
          assert.equal(true, typeof a.alarmEmailDelayDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmEmailDelayGet - errors', () => {
      it('should have a alarmEmailDelayGet function', (done) => {
        try {
          assert.equal(true, typeof a.alarmEmailDelayGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmEmailDelayPost - errors', () => {
      it('should have a alarmEmailDelayPost function', (done) => {
        try {
          assert.equal(true, typeof a.alarmEmailDelayPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.alarmEmailDelayPost(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-alarmEmailDelayPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmEmailDelayPut - errors', () => {
      it('should have a alarmEmailDelayPut function', (done) => {
        try {
          assert.equal(true, typeof a.alarmEmailDelayPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.alarmEmailDelayPut(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-alarmEmailDelayPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmDescGet - errors', () => {
      it('should have a alarmDescGet function', (done) => {
        try {
          assert.equal(true, typeof a.alarmDescGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmGms - errors', () => {
      it('should have a alarmGms function', (done) => {
        try {
          assert.equal(true, typeof a.alarmGms === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmNotification - errors', () => {
      it('should have a alarmNotification function', (done) => {
        try {
          assert.equal(true, typeof a.alarmNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAlarmNotification - errors', () => {
      it('should have a postAlarmNotification function', (done) => {
        try {
          assert.equal(true, typeof a.postAlarmNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing alarmNotification', (done) => {
        try {
          a.postAlarmNotification(null, (data, error) => {
            try {
              const displayE = 'alarmNotification is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postAlarmNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmSummary - errors', () => {
      it('should have a alarmSummary function', (done) => {
        try {
          assert.equal(true, typeof a.alarmSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#alarmSummaryType - errors', () => {
      it('should have a alarmSummaryType function', (done) => {
        try {
          assert.equal(true, typeof a.alarmSummaryType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.alarmSummaryType(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-alarmSummaryType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteDisabledAlarmsConfig - errors', () => {
      it('should have a deleteDisabledAlarmsConfig function', (done) => {
        try {
          assert.equal(true, typeof a.deleteDisabledAlarmsConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDisabledAlarmsConfig - errors', () => {
      it('should have a getAllDisabledAlarmsConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDisabledAlarmsConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDisabledAlarmsConfig - errors', () => {
      it('should have a updateDisabledAlarmsConfig function', (done) => {
        try {
          assert.equal(true, typeof a.updateDisabledAlarmsConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.updateDisabledAlarmsConfig(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateDisabledAlarmsConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceGET - errors', () => {
      it('should have a applianceGET function', (done) => {
        try {
          assert.equal(true, typeof a.applianceGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#adminDistanceConfig - errors', () => {
      it('should have a adminDistanceConfig function', (done) => {
        try {
          assert.equal(true, typeof a.adminDistanceConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.adminDistanceConfig(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-adminDistanceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.adminDistanceConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-adminDistanceConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approvedAppliance - errors', () => {
      it('should have a approvedAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.approvedAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#backupPost - errors', () => {
      it('should have a backupPost function', (done) => {
        try {
          assert.equal(true, typeof a.backupPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#backupDelete - errors', () => {
      it('should have a backupDelete function', (done) => {
        try {
          assert.equal(true, typeof a.backupDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing backupFilePk', (done) => {
        try {
          a.backupDelete(null, (data, error) => {
            try {
              const displayE = 'backupFilePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-backupDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#backupGet - errors', () => {
      it('should have a backupGet function', (done) => {
        try {
          assert.equal(true, typeof a.backupGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.backupGet(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-backupGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceChgGr - errors', () => {
      it('should have a applianceChgGr function', (done) => {
        try {
          assert.equal(true, typeof a.applianceChgGr === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupPk', (done) => {
        try {
          a.applianceChgGr(null, null, (data, error) => {
            try {
              const displayE = 'groupPk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceChgGr', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appliancePrimaryKeys', (done) => {
        try {
          a.applianceChgGr('fakeparam', null, (data, error) => {
            try {
              const displayE = 'appliancePrimaryKeys is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceChgGr', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#changeAppliancePassword - errors', () => {
      it('should have a changeAppliancePassword function', (done) => {
        try {
          assert.equal(true, typeof a.changeAppliancePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.changeAppliancePassword(null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-changeAppliancePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.changeAppliancePassword('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-changeAppliancePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.changeAppliancePassword('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-changeAppliancePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteForRediscovery - errors', () => {
      it('should have a deleteForRediscovery function', (done) => {
        try {
          assert.equal(true, typeof a.deleteForRediscovery === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.deleteForRediscovery(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteForRediscovery', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deniedAppliance - errors', () => {
      it('should have a deniedAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.deniedAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllDiscoveredAppliances - errors', () => {
      it('should have a getAllDiscoveredAppliances function', (done) => {
        try {
          assert.equal(true, typeof a.getAllDiscoveredAppliances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addDiscoveredApplianceToOrchestrator - errors', () => {
      it('should have a addDiscoveredApplianceToOrchestrator function', (done) => {
        try {
          assert.equal(true, typeof a.addDiscoveredApplianceToOrchestrator === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.addDiscoveredApplianceToOrchestrator(null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-addDiscoveredApplianceToOrchestrator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.addDiscoveredApplianceToOrchestrator('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-addDiscoveredApplianceToOrchestrator', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#approveDiscoveredAppliances - errors', () => {
      it('should have a approveDiscoveredAppliances function', (done) => {
        try {
          assert.equal(true, typeof a.approveDiscoveredAppliances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.approveDiscoveredAppliances(null, null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-approveDiscoveredAppliances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.approveDiscoveredAppliances('fakeparam', null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-approveDiscoveredAppliances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#denyDiscoveredAppliances - errors', () => {
      it('should have a denyDiscoveredAppliances function', (done) => {
        try {
          assert.equal(true, typeof a.denyDiscoveredAppliances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.denyDiscoveredAppliances(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-denyDiscoveredAppliances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDiscoveredAppliances - errors', () => {
      it('should have a updateDiscoveredAppliances function', (done) => {
        try {
          assert.equal(true, typeof a.updateDiscoveredAppliances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsCacheConfig - errors', () => {
      it('should have a dnsCacheConfig function', (done) => {
        try {
          assert.equal(true, typeof a.dnsCacheConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.dnsCacheConfig(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dnsCacheConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.dnsCacheConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dnsCacheConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplianceExtraInfo - errors', () => {
      it('should have a deleteApplianceExtraInfo function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplianceExtraInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.deleteApplianceExtraInfo(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteApplianceExtraInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceExtraInfo - errors', () => {
      it('should have a getApplianceExtraInfo function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceExtraInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getApplianceExtraInfo(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getApplianceExtraInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveApplianceExtraInfo - errors', () => {
      it('should have a saveApplianceExtraInfo function', (done) => {
        try {
          assert.equal(true, typeof a.saveApplianceExtraInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceExtraInfo', (done) => {
        try {
          a.saveApplianceExtraInfo(null, null, (data, error) => {
            try {
              const displayE = 'applianceExtraInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveApplianceExtraInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.saveApplianceExtraInfo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveApplianceExtraInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bridgeInterfaceState - errors', () => {
      it('should have a bridgeInterfaceState function', (done) => {
        try {
          assert.equal(true, typeof a.bridgeInterfaceState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.bridgeInterfaceState(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-bridgeInterfaceState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.bridgeInterfaceState('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-bridgeInterfaceState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkRoleAndSiteGet - errors', () => {
      it('should have a networkRoleAndSiteGet function', (done) => {
        try {
          assert.equal(true, typeof a.networkRoleAndSiteGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.networkRoleAndSiteGet(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-networkRoleAndSiteGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#networkRoleAndSitePost - errors', () => {
      it('should have a networkRoleAndSitePost function', (done) => {
        try {
          assert.equal(true, typeof a.networkRoleAndSitePost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.networkRoleAndSitePost(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-networkRoleAndSitePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing postNetworkRulesAndSites', (done) => {
        try {
          a.networkRoleAndSitePost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'postNetworkRulesAndSites is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-networkRoleAndSitePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#peerPriorityGet - errors', () => {
      it('should have a peerPriorityGet function', (done) => {
        try {
          assert.equal(true, typeof a.peerPriorityGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.peerPriorityGet(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-peerPriorityGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.peerPriorityGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-peerPriorityGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkAppliancesQueuedForDeletion - errors', () => {
      it('should have a checkAppliancesQueuedForDeletion function', (done) => {
        try {
          assert.equal(true, typeof a.checkAppliancesQueuedForDeletion === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceGetAPI - errors', () => {
      it('should have a applianceGetAPI function', (done) => {
        try {
          assert.equal(true, typeof a.applianceGetAPI === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.applianceGetAPI(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceGetAPI', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing url', (done) => {
        try {
          a.applianceGetAPI('fakeparam', null, (data, error) => {
            try {
              const displayE = 'url is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceGetAPI', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appliancePostAPI - errors', () => {
      it('should have a appliancePostAPI function', (done) => {
        try {
          assert.equal(true, typeof a.appliancePostAPI === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.appliancePostAPI(null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appliancePostAPI', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing url', (done) => {
        try {
          a.appliancePostAPI('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'url is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appliancePostAPI', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing data', (done) => {
        try {
          a.appliancePostAPI('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'data is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appliancePostAPI', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceRestore - errors', () => {
      it('should have a applianceRestore function', (done) => {
        try {
          assert.equal(true, typeof a.applianceRestore === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.applianceRestore(null, null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceRestore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.applianceRestore('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceRestore', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveChanges - errors', () => {
      it('should have a saveChanges function', (done) => {
        try {
          assert.equal(true, typeof a.saveChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing saveChangesPostBody', (done) => {
        try {
          a.saveChanges(null, (data, error) => {
            try {
              const displayE = 'saveChangesPostBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveChanges', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApplianceSaveChangesNePk - errors', () => {
      it('should have a postApplianceSaveChangesNePk function', (done) => {
        try {
          assert.equal(true, typeof a.postApplianceSaveChangesNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.postApplianceSaveChangesNePk(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postApplianceSaveChangesNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing saveChangesPostBody', (done) => {
        try {
          a.postApplianceSaveChangesNePk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'saveChangesPostBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postApplianceSaveChangesNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsConfigGet - errors', () => {
      it('should have a statsConfigGet function', (done) => {
        try {
          assert.equal(true, typeof a.statsConfigGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsConfigPost - errors', () => {
      it('should have a statsConfigPost function', (done) => {
        try {
          assert.equal(true, typeof a.statsConfigPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing statsConfigPostData', (done) => {
        try {
          a.statsConfigPost(null, (data, error) => {
            try {
              const displayE = 'statsConfigPostData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsConfigPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsConfigDefaultGet - errors', () => {
      it('should have a statsConfigDefaultGet function', (done) => {
        try {
          assert.equal(true, typeof a.statsConfigDefaultGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wanNextHopHealthGet - errors', () => {
      it('should have a wanNextHopHealthGet function', (done) => {
        try {
          assert.equal(true, typeof a.wanNextHopHealthGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.wanNextHopHealthGet(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-wanNextHopHealthGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.wanNextHopHealthGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-wanNextHopHealthGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceDel - errors', () => {
      it('should have a applianceDel function', (done) => {
        try {
          assert.equal(true, typeof a.applianceDel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.applianceDel(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceDel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceGetOne - errors', () => {
      it('should have a applianceGetOne function', (done) => {
        try {
          assert.equal(true, typeof a.applianceGetOne === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.applianceGetOne(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceGetOne', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applianceChg - errors', () => {
      it('should have a applianceChg function', (done) => {
        try {
          assert.equal(true, typeof a.applianceChg === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.applianceChg(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceChg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceInfo', (done) => {
        try {
          a.applianceChg('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applianceInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applianceChg', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resyncAppliance - errors', () => {
      it('should have a resyncAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.resyncAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceKeys', (done) => {
        try {
          a.resyncAppliance(null, (data, error) => {
            try {
              const displayE = 'applianceKeys is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-resyncAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appliancesSoftwareVersions - errors', () => {
      it('should have a appliancesSoftwareVersions function', (done) => {
        try {
          assert.equal(true, typeof a.appliancesSoftwareVersions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePK', (done) => {
        try {
          a.appliancesSoftwareVersions(null, null, (data, error) => {
            try {
              const displayE = 'nePK is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appliancesSoftwareVersions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.appliancesSoftwareVersions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appliancesSoftwareVersions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGet - errors', () => {
      it('should have a applicationGet function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#builtin - errors', () => {
      it('should have a builtin function', (done) => {
        try {
          assert.equal(true, typeof a.builtin === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userDefined - errors', () => {
      it('should have a userDefined function', (done) => {
        try {
          assert.equal(true, typeof a.userDefined === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userDefinedConfig - errors', () => {
      it('should have a userDefinedConfig function', (done) => {
        try {
          assert.equal(true, typeof a.userDefinedConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.userDefinedConfig(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-userDefinedConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.userDefinedConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-userDefinedConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appTagsGet - errors', () => {
      it('should have a appTagsGet function', (done) => {
        try {
          assert.equal(true, typeof a.appTagsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceKey', (done) => {
        try {
          a.appTagsGet(null, (data, error) => {
            try {
              const displayE = 'resourceKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appTagsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appTagsPost - errors', () => {
      it('should have a appTagsPost function', (done) => {
        try {
          assert.equal(true, typeof a.appTagsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appTagsBody', (done) => {
        try {
          a.appTagsPost(null, (data, error) => {
            try {
              const displayE = 'appTagsBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appTagsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appGroupSearchPost - errors', () => {
      it('should have a appGroupSearchPost function', (done) => {
        try {
          assert.equal(true, typeof a.appGroupSearchPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchWildcard', (done) => {
        try {
          a.appGroupSearchPost(null, (data, error) => {
            try {
              const displayE = 'searchWildcard is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appGroupSearchPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appTagsModePost - errors', () => {
      it('should have a appTagsModePost function', (done) => {
        try {
          assert.equal(true, typeof a.appTagsModePost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mode', (done) => {
        try {
          a.appTagsModePost(null, (data, error) => {
            try {
              const displayE = 'mode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appTagsModePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appSearchPost - errors', () => {
      it('should have a appSearchPost function', (done) => {
        try {
          assert.equal(true, typeof a.appSearchPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing searchWildcard', (done) => {
        try {
          a.appSearchPost(null, (data, error) => {
            try {
              const displayE = 'searchWildcard is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appSearchPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#compoundGet - errors', () => {
      it('should have a compoundGet function', (done) => {
        try {
          assert.equal(true, typeof a.compoundGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceKey', (done) => {
        try {
          a.compoundGet(null, (data, error) => {
            try {
              const displayE = 'resourceKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-compoundGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#compoundReorderPost - errors', () => {
      it('should have a compoundReorderPost function', (done) => {
        try {
          assert.equal(true, typeof a.compoundReorderPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing compoundIdsConfig', (done) => {
        try {
          a.compoundReorderPost(null, (data, error) => {
            try {
              const displayE = 'compoundIdsConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-compoundReorderPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#compoundDelete - errors', () => {
      it('should have a compoundDelete function', (done) => {
        try {
          assert.equal(true, typeof a.compoundDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.compoundDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-compoundDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#compoundPost - errors', () => {
      it('should have a compoundPost function', (done) => {
        try {
          assert.equal(true, typeof a.compoundPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.compoundPost(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-compoundPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing compoundConfig', (done) => {
        try {
          a.compoundPost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'compoundConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-compoundPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsGet - errors', () => {
      it('should have a dnsGet function', (done) => {
        try {
          assert.equal(true, typeof a.dnsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceKey', (done) => {
        try {
          a.dnsGet(null, (data, error) => {
            try {
              const displayE = 'resourceKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dnsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsDelete - errors', () => {
      it('should have a dnsDelete function', (done) => {
        try {
          assert.equal(true, typeof a.dnsDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domain', (done) => {
        try {
          a.dnsDelete(null, (data, error) => {
            try {
              const displayE = 'domain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dnsDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsPost - errors', () => {
      it('should have a dnsPost function', (done) => {
        try {
          assert.equal(true, typeof a.dnsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing domain', (done) => {
        try {
          a.dnsPost(null, null, (data, error) => {
            try {
              const displayE = 'domain is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dnsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dnsConfig', (done) => {
        try {
          a.dnsPost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'dnsConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dnsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipIntelligenceGet - errors', () => {
      it('should have a ipIntelligenceGet function', (done) => {
        try {
          assert.equal(true, typeof a.ipIntelligenceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceKey', (done) => {
        try {
          a.ipIntelligenceGet(null, (data, error) => {
            try {
              const displayE = 'resourceKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-ipIntelligenceGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipIntelligenceDelete - errors', () => {
      it('should have a ipIntelligenceDelete function', (done) => {
        try {
          assert.equal(true, typeof a.ipIntelligenceDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipStart', (done) => {
        try {
          a.ipIntelligenceDelete(null, null, (data, error) => {
            try {
              const displayE = 'ipStart is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-ipIntelligenceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipEnd', (done) => {
        try {
          a.ipIntelligenceDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipEnd is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-ipIntelligenceDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipIntelligencePost - errors', () => {
      it('should have a ipIntelligencePost function', (done) => {
        try {
          assert.equal(true, typeof a.ipIntelligencePost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipStart', (done) => {
        try {
          a.ipIntelligencePost(null, null, null, (data, error) => {
            try {
              const displayE = 'ipStart is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-ipIntelligencePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipEnd', (done) => {
        try {
          a.ipIntelligencePost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'ipEnd is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-ipIntelligencePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ipIntelligenceConfig', (done) => {
        try {
          a.ipIntelligencePost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'ipIntelligenceConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-ipIntelligencePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#legacyUdaGet - errors', () => {
      it('should have a legacyUdaGet function', (done) => {
        try {
          assert.equal(true, typeof a.legacyUdaGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meterFlowGet - errors', () => {
      it('should have a meterFlowGet function', (done) => {
        try {
          assert.equal(true, typeof a.meterFlowGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceKey', (done) => {
        try {
          a.meterFlowGet(null, (data, error) => {
            try {
              const displayE = 'resourceKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-meterFlowGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meterFlowDelete - errors', () => {
      it('should have a meterFlowDelete function', (done) => {
        try {
          assert.equal(true, typeof a.meterFlowDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowType', (done) => {
        try {
          a.meterFlowDelete(null, null, (data, error) => {
            try {
              const displayE = 'flowType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-meterFlowDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mid', (done) => {
        try {
          a.meterFlowDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'mid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-meterFlowDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meterFlowPost - errors', () => {
      it('should have a meterFlowPost function', (done) => {
        try {
          assert.equal(true, typeof a.meterFlowPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowType', (done) => {
        try {
          a.meterFlowPost(null, null, null, (data, error) => {
            try {
              const displayE = 'flowType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-meterFlowPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mid', (done) => {
        try {
          a.meterFlowPost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'mid is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-meterFlowPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing meterFlowConfig', (done) => {
        try {
          a.meterFlowPost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'meterFlowConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-meterFlowPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProtocolGet - errors', () => {
      it('should have a portProtocolGet function', (done) => {
        try {
          assert.equal(true, typeof a.portProtocolGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceKey', (done) => {
        try {
          a.portProtocolGet(null, (data, error) => {
            try {
              const displayE = 'resourceKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProtocolGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProtocolDelete - errors', () => {
      it('should have a portProtocolDelete function', (done) => {
        try {
          assert.equal(true, typeof a.portProtocolDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.portProtocolDelete(null, null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProtocolDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing protocol', (done) => {
        try {
          a.portProtocolDelete('fakeparam', null, (data, error) => {
            try {
              const displayE = 'protocol is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProtocolDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProtocolPost - errors', () => {
      it('should have a portProtocolPost function', (done) => {
        try {
          assert.equal(true, typeof a.portProtocolPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing port', (done) => {
        try {
          a.portProtocolPost(null, null, null, (data, error) => {
            try {
              const displayE = 'port is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProtocolPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing protocol', (done) => {
        try {
          a.portProtocolPost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'protocol is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProtocolPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portProtocolConfig', (done) => {
        try {
          a.portProtocolPost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'portProtocolConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProtocolPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saasGet - errors', () => {
      it('should have a saasGet function', (done) => {
        try {
          assert.equal(true, typeof a.saasGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resourceKey', (done) => {
        try {
          a.saasGet(null, (data, error) => {
            try {
              const displayE = 'resourceKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saasGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saasDelete - errors', () => {
      it('should have a saasDelete function', (done) => {
        try {
          assert.equal(true, typeof a.saasDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.saasDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saasDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postApplicationDefinitionSaasClassificationId - errors', () => {
      it('should have a postApplicationDefinitionSaasClassificationId function', (done) => {
        try {
          assert.equal(true, typeof a.postApplicationDefinitionSaasClassificationId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.postApplicationDefinitionSaasClassificationId(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postApplicationDefinitionSaasClassificationId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing saasConfig', (done) => {
        try {
          a.postApplicationDefinitionSaasClassificationId('fakeparam', null, (data, error) => {
            try {
              const displayE = 'saasConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postApplicationDefinitionSaasClassificationId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationGroups - errors', () => {
      it('should have a applicationGroups function', (done) => {
        try {
          assert.equal(true, typeof a.applicationGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.applicationGroups(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applicationGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.applicationGroups('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applicationGroups', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applicationTrends - errors', () => {
      it('should have a applicationTrends function', (done) => {
        try {
          assert.equal(true, typeof a.applicationTrends === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startDate', (done) => {
        try {
          a.applicationTrends(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applicationTrends', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endDate', (done) => {
        try {
          a.applicationTrends('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endDate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applicationTrends', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing traffic', (done) => {
        try {
          a.applicationTrends('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'traffic is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applicationTrends', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bound', (done) => {
        try {
          a.applicationTrends('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'bound is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applicationTrends', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePKList', (done) => {
        try {
          a.applicationTrends('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'nePKList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applicationTrends', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#auth - errors', () => {
      it('should have a auth function', (done) => {
        try {
          assert.equal(true, typeof a.auth === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.auth(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-auth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.auth('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-auth', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#authentication - errors', () => {
      it('should have a authentication function', (done) => {
        try {
          assert.equal(true, typeof a.authentication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing user', (done) => {
        try {
          a.authentication(null, null, (data, error) => {
            try {
              const displayE = 'user is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-authentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing password', (done) => {
        try {
          a.authentication('fakeparam', null, (data, error) => {
            try {
              const displayE = 'password is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-authentication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationLoginStatus - errors', () => {
      it('should have a getAuthenticationLoginStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationLoginStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthenticationLoginToken - errors', () => {
      it('should have a postAuthenticationLoginToken function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthenticationLoginToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestLoginToken', (done) => {
        try {
          a.postAuthenticationLoginToken(null, (data, error) => {
            try {
              const displayE = 'requestLoginToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postAuthenticationLoginToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthenticationLogout - errors', () => {
      it('should have a getAuthenticationLogout function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthenticationLogout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAuthenticationPasswordValidation - errors', () => {
      it('should have a postAuthenticationPasswordValidation function', (done) => {
        try {
          assert.equal(true, typeof a.postAuthenticationPasswordValidation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loginToken', (done) => {
        try {
          a.postAuthenticationPasswordValidation(null, (data, error) => {
            try {
              const displayE = 'loginToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postAuthenticationPasswordValidation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userAuthType - errors', () => {
      it('should have a userAuthType function', (done) => {
        try {
          assert.equal(true, typeof a.userAuthType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing loginToken', (done) => {
        try {
          a.userAuthType(null, (data, error) => {
            try {
              const displayE = 'loginToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-userAuthType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userAuthTypeToken - errors', () => {
      it('should have a userAuthTypeToken function', (done) => {
        try {
          assert.equal(true, typeof a.userAuthTypeToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resetPasswordToken', (done) => {
        try {
          a.userAuthTypeToken(null, (data, error) => {
            try {
              const displayE = 'resetPasswordToken is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-userAuthTypeToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#avcMode - errors', () => {
      it('should have a avcMode function', (done) => {
        try {
          assert.equal(true, typeof a.avcMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bannerInfo - errors', () => {
      it('should have a bannerInfo function', (done) => {
        try {
          assert.equal(true, typeof a.bannerInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.bannerInfo(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-bannerInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.bannerInfo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-bannerInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNeighbor - errors', () => {
      it('should have a getNeighbor function', (done) => {
        try {
          assert.equal(true, typeof a.getNeighbor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getNeighbor(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getNeighbor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBGPSystemConfig - errors', () => {
      it('should have a getBGPSystemConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getBGPSystemConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getBGPSystemConfig(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getBGPSystemConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bGPState - errors', () => {
      it('should have a bGPState function', (done) => {
        try {
          assert.equal(true, typeof a.bGPState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.bGPState(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-bGPState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#textCustomizationConfigDelete - errors', () => {
      it('should have a textCustomizationConfigDelete function', (done) => {
        try {
          assert.equal(true, typeof a.textCustomizationConfigDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#textCustomizationConfigGet - errors', () => {
      it('should have a textCustomizationConfigGet function', (done) => {
        try {
          assert.equal(true, typeof a.textCustomizationConfigGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#textCustomizationConfigPost - errors', () => {
      it('should have a textCustomizationConfigPost function', (done) => {
        try {
          assert.equal(true, typeof a.textCustomizationConfigPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.textCustomizationConfigPost(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-textCustomizationConfigPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#textCustomizationConfigPut - errors', () => {
      it('should have a textCustomizationConfigPut function', (done) => {
        try {
          assert.equal(true, typeof a.textCustomizationConfigPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.textCustomizationConfigPut(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-textCustomizationConfigPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fileNameGet - errors', () => {
      it('should have a fileNameGet function', (done) => {
        try {
          assert.equal(true, typeof a.fileNameGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metaData', (done) => {
        try {
          a.fileNameGet(null, (data, error) => {
            try {
              const displayE = 'metaData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-fileNameGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#imageDelete - errors', () => {
      it('should have a imageDelete function', (done) => {
        try {
          assert.equal(true, typeof a.imageDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.imageDelete(null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-imageDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#imagePost - errors', () => {
      it('should have a imagePost function', (done) => {
        try {
          assert.equal(true, typeof a.imagePost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.imagePost(null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-imagePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qqfile', (done) => {
        try {
          a.imagePost('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'qqfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-imagePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qqfileFormData', (done) => {
        try {
          a.imagePost('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'qqfileFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-imagePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putBrandCustomizationImageType - errors', () => {
      it('should have a putBrandCustomizationImageType function', (done) => {
        try {
          assert.equal(true, typeof a.putBrandCustomizationImageType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.putBrandCustomizationImageType(null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-putBrandCustomizationImageType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qqfile', (done) => {
        try {
          a.putBrandCustomizationImageType('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'qqfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-putBrandCustomizationImageType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qqfileFormData', (done) => {
        try {
          a.putBrandCustomizationImageType('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'qqfileFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-putBrandCustomizationImageType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#broadcastCliPost - errors', () => {
      it('should have a broadcastCliPost function', (done) => {
        try {
          assert.equal(true, typeof a.broadcastCliPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.broadcastCliPost(null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-broadcastCliPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bypassPost - errors', () => {
      it('should have a bypassPost function', (done) => {
        try {
          assert.equal(true, typeof a.bypassPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.bypassPost(null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-bypassPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#bypassGet - errors', () => {
      it('should have a bypassGet function', (done) => {
        try {
          assert.equal(true, typeof a.bypassGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.bypassGet(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-bypassGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.bypassGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-bypassGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#builtinAppGet - errors', () => {
      it('should have a builtinAppGet function', (done) => {
        try {
          assert.equal(true, typeof a.builtinAppGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interfaceGet - errors', () => {
      it('should have a interfaceGet function', (done) => {
        try {
          assert.equal(true, typeof a.interfaceGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#userAppGet - errors', () => {
      it('should have a userAppGet function', (done) => {
        try {
          assert.equal(true, typeof a.userAppGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloudAppsConfig - errors', () => {
      it('should have a cloudAppsConfig function', (done) => {
        try {
          assert.equal(true, typeof a.cloudAppsConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.cloudAppsConfig(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-cloudAppsConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.cloudAppsConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-cloudAppsConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cloudAppsMonitor - errors', () => {
      it('should have a cloudAppsMonitor function', (done) => {
        try {
          assert.equal(true, typeof a.cloudAppsMonitor === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.cloudAppsMonitor(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-cloudAppsMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.cloudAppsMonitor('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-cloudAppsMonitor', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#disks - errors', () => {
      it('should have a disks function', (done) => {
        try {
          assert.equal(true, typeof a.disks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.disks(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-disks', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dbPartition - errors', () => {
      it('should have a dbPartition function', (done) => {
        try {
          assert.equal(true, typeof a.dbPartition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePartition - errors', () => {
      it('should have a deletePartition function', (done) => {
        try {
          assert.equal(true, typeof a.deletePartition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing table', (done) => {
        try {
          a.deletePartition(null, null, (data, error) => {
            try {
              const displayE = 'table is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deletePartition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing partition', (done) => {
        try {
          a.deletePartition('fakeparam', null, (data, error) => {
            try {
              const displayE = 'partition is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deletePartition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#cancel - errors', () => {
      it('should have a cancel function', (done) => {
        try {
          assert.equal(true, typeof a.cancel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGMSDebugFile - errors', () => {
      it('should have a deleteGMSDebugFile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGMSDebugFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deleteGMSDebugFile', (done) => {
        try {
          a.deleteGMSDebugFile(null, (data, error) => {
            try {
              const displayE = 'deleteGMSDebugFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteGMSDebugFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNeFile - errors', () => {
      it('should have a deleteNeFile function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNeFile === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.deleteNeFile(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteNeFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deleteInfoClass', (done) => {
        try {
          a.deleteNeFile('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deleteInfoClass is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteNeFile', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getProxyConfig - errors', () => {
      it('should have a getProxyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getProxyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setProxyConfig - errors', () => {
      it('should have a setProxyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.setProxyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing proxyConfig', (done) => {
        try {
          a.setProxyConfig(null, (data, error) => {
            try {
              const displayE = 'proxyConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-setProxyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#debugFiles - errors', () => {
      it('should have a debugFiles function', (done) => {
        try {
          assert.equal(true, typeof a.debugFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.debugFiles(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-debugFiles', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateDeployment - errors', () => {
      it('should have a validateDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.validateDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.validateDeployment(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-validateDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deployment', (done) => {
        try {
          a.validateDeployment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deployment is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-validateDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateDiscoveredDeployment - errors', () => {
      it('should have a validateDiscoveredDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.validateDiscoveredDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discoveredApplianceId', (done) => {
        try {
          a.validateDiscoveredDeployment(null, null, (data, error) => {
            try {
              const displayE = 'discoveredApplianceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-validateDiscoveredDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deployment', (done) => {
        try {
          a.validateDiscoveredDeployment('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deployment is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-validateDiscoveredDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceDeployment - errors', () => {
      it('should have a getApplianceDeployment function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceDeployment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getApplianceDeployment(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getApplianceDeployment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpConfigGet - errors', () => {
      it('should have a dhcpConfigGet function', (done) => {
        try {
          assert.equal(true, typeof a.dhcpConfigGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpConfigPost - errors', () => {
      it('should have a dhcpConfigPost function', (done) => {
        try {
          assert.equal(true, typeof a.dhcpConfigPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpConfigPost', (done) => {
        try {
          a.dhcpConfigPost(null, (data, error) => {
            try {
              const displayE = 'dhcpConfigPost is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dhcpConfigPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpdLeases - errors', () => {
      it('should have a dhcpdLeases function', (done) => {
        try {
          assert.equal(true, typeof a.dhcpdLeases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.dhcpdLeases(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dhcpdLeases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.dhcpdLeases('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dhcpdLeases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpReservationsGet - errors', () => {
      it('should have a dhcpReservationsGet function', (done) => {
        try {
          assert.equal(true, typeof a.dhcpReservationsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpReservationsPost - errors', () => {
      it('should have a dhcpReservationsPost function', (done) => {
        try {
          assert.equal(true, typeof a.dhcpReservationsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpConfigPost', (done) => {
        try {
          a.dhcpReservationsPost(null, (data, error) => {
            try {
              const displayE = 'dhcpConfigPost is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dhcpReservationsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpReservationsGmsPost - errors', () => {
      it('should have a dhcpReservationsGmsPost function', (done) => {
        try {
          assert.equal(true, typeof a.dhcpReservationsGmsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dhcpConfigGmsPost', (done) => {
        try {
          a.dhcpReservationsGmsPost(null, (data, error) => {
            try {
              const displayE = 'dhcpConfigGmsPost is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dhcpReservationsGmsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpConfigResetPost - errors', () => {
      it('should have a dhcpConfigResetPost function', (done) => {
        try {
          assert.equal(true, typeof a.dhcpConfigResetPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dhcpdFailoverStates - errors', () => {
      it('should have a dhcpdFailoverStates function', (done) => {
        try {
          assert.equal(true, typeof a.dhcpdFailoverStates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.dhcpdFailoverStates(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dhcpdFailoverStates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.dhcpdFailoverStates('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dhcpdFailoverStates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsProxyConfigGet - errors', () => {
      it('should have a dnsProxyConfigGet function', (done) => {
        try {
          assert.equal(true, typeof a.dnsProxyConfigGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.dnsProxyConfigGet(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dnsProxyConfigGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.dnsProxyConfigGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dnsProxyConfigGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelExceptionDelete - errors', () => {
      it('should have a tunnelExceptionDelete function', (done) => {
        try {
          assert.equal(true, typeof a.tunnelExceptionDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelExceptionGet - errors', () => {
      it('should have a tunnelExceptionGet function', (done) => {
        try {
          assert.equal(true, typeof a.tunnelExceptionGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelExceptionPost - errors', () => {
      it('should have a tunnelExceptionPost function', (done) => {
        try {
          assert.equal(true, typeof a.tunnelExceptionPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.tunnelExceptionPost(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-tunnelExceptionPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelExceptionPut - errors', () => {
      it('should have a tunnelExceptionPut function', (done) => {
        try {
          assert.equal(true, typeof a.tunnelExceptionPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.tunnelExceptionPut(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-tunnelExceptionPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelExceptionSingleDelete - errors', () => {
      it('should have a tunnelExceptionSingleDelete function', (done) => {
        try {
          assert.equal(true, typeof a.tunnelExceptionSingleDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.tunnelExceptionSingleDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-tunnelExceptionSingleDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelExceptionSingleUpdate - errors', () => {
      it('should have a tunnelExceptionSingleUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.tunnelExceptionSingleUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.tunnelExceptionSingleUpdate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-tunnelExceptionSingleUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.tunnelExceptionSingleUpdate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-tunnelExceptionSingleUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#fileCreation - errors', () => {
      it('should have a fileCreation function', (done) => {
        try {
          assert.equal(true, typeof a.fileCreation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.fileCreation(null, null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-fileCreation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qqfile', (done) => {
        try {
          a.fileCreation('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'qqfile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-fileCreation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing qqfileFormData', (done) => {
        try {
          a.fileCreation('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'qqfileFormData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-fileCreation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowBandwidthStats - errors', () => {
      it('should have a flowBandwidthStats function', (done) => {
        try {
          assert.equal(true, typeof a.flowBandwidthStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.flowBandwidthStats(null, null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowBandwidthStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.flowBandwidthStats('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowBandwidthStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing seq', (done) => {
        try {
          a.flowBandwidthStats('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'seq is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowBandwidthStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowDetails - errors', () => {
      it('should have a flowDetails function', (done) => {
        try {
          assert.equal(true, typeof a.flowDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.flowDetails(null, null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.flowDetails('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing seq', (done) => {
        try {
          a.flowDetails('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'seq is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowDetails2 - errors', () => {
      it('should have a flowDetails2 function', (done) => {
        try {
          assert.equal(true, typeof a.flowDetails2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.flowDetails2(null, null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowDetails2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.flowDetails2('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowDetails2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing seq', (done) => {
        try {
          a.flowDetails2('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'seq is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowDetails2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowsReclassification - errors', () => {
      it('should have a flowsReclassification function', (done) => {
        try {
          assert.equal(true, typeof a.flowsReclassification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.flowsReclassification(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowsReclassification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowsReClassificationBody', (done) => {
        try {
          a.flowsReclassification('fakeparam', null, (data, error) => {
            try {
              const displayE = 'flowsReClassificationBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowsReclassification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flowsReset - errors', () => {
      it('should have a flowsReset function', (done) => {
        try {
          assert.equal(true, typeof a.flowsReset === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.flowsReset(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowsReset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowsResetBody', (done) => {
        try {
          a.flowsReset('fakeparam', null, (data, error) => {
            try {
              const displayE = 'flowsResetBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flowsReset', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#flows - errors', () => {
      it('should have a flows function', (done) => {
        try {
          assert.equal(true, typeof a.flows === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.flows(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-flows', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAdvProp - errors', () => {
      it('should have a getAdvProp function', (done) => {
        try {
          assert.equal(true, typeof a.getAdvProp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAdvProp - errors', () => {
      it('should have a postAdvProp function', (done) => {
        try {
          assert.equal(true, typeof a.postAdvProp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing advancedProperties', (done) => {
        try {
          a.postAdvProp(null, (data, error) => {
            try {
              const displayE = 'advancedProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postAdvProp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGmsAdvancedPropertiesMetadata - errors', () => {
      it('should have a getGmsAdvancedPropertiesMetadata function', (done) => {
        try {
          assert.equal(true, typeof a.getGmsAdvancedPropertiesMetadata === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPreconfigurations - errors', () => {
      it('should have a getPreconfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.getPreconfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPreconfiguration - errors', () => {
      it('should have a createPreconfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.createPreconfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createPreconfig', (done) => {
        try {
          a.createPreconfiguration(null, (data, error) => {
            try {
              const displayE = 'createPreconfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-createPreconfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultPreconfigurations - errors', () => {
      it('should have a getDefaultPreconfigurations function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultPreconfigurations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMatchingPreconfiguration - errors', () => {
      it('should have a getMatchingPreconfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getMatchingPreconfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing matchPreconfig', (done) => {
        try {
          a.getMatchingPreconfiguration(null, (data, error) => {
            try {
              const displayE = 'matchPreconfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getMatchingPreconfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGmsAppliancePreconfigurationValidate - errors', () => {
      it('should have a postGmsAppliancePreconfigurationValidate function', (done) => {
        try {
          assert.equal(true, typeof a.postGmsAppliancePreconfigurationValidate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createPreconfig', (done) => {
        try {
          a.postGmsAppliancePreconfigurationValidate(null, (data, error) => {
            try {
              const displayE = 'createPreconfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postGmsAppliancePreconfigurationValidate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePreconfiguration - errors', () => {
      it('should have a deletePreconfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.deletePreconfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing preconfigId', (done) => {
        try {
          a.deletePreconfiguration(null, (data, error) => {
            try {
              const displayE = 'preconfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deletePreconfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPreconfiguration - errors', () => {
      it('should have a getPreconfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.getPreconfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing preconfigId', (done) => {
        try {
          a.getPreconfiguration(null, (data, error) => {
            try {
              const displayE = 'preconfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getPreconfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyPreconfiguration - errors', () => {
      it('should have a modifyPreconfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.modifyPreconfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing createPreconfig', (done) => {
        try {
          a.modifyPreconfiguration(null, null, (data, error) => {
            try {
              const displayE = 'createPreconfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-modifyPreconfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing preconfigId', (done) => {
        try {
          a.modifyPreconfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'preconfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-modifyPreconfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPreconfigurationApplyStatus - errors', () => {
      it('should have a getPreconfigurationApplyStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getPreconfigurationApplyStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing preconfigId', (done) => {
        try {
          a.getPreconfigurationApplyStatus(null, (data, error) => {
            try {
              const displayE = 'preconfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getPreconfigurationApplyStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyDiscoveredPreconfiguration - errors', () => {
      it('should have a applyDiscoveredPreconfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.applyDiscoveredPreconfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing preconfigId', (done) => {
        try {
          a.applyDiscoveredPreconfiguration(null, null, (data, error) => {
            try {
              const displayE = 'preconfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyDiscoveredPreconfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discoveredId', (done) => {
        try {
          a.applyDiscoveredPreconfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'discoveredId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyDiscoveredPreconfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyManagedPreconfiguration - errors', () => {
      it('should have a applyManagedPreconfiguration function', (done) => {
        try {
          assert.equal(true, typeof a.applyManagedPreconfiguration === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing preconfigId', (done) => {
        try {
          a.applyManagedPreconfiguration(null, null, (data, error) => {
            try {
              const displayE = 'preconfigId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyManagedPreconfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.applyManagedPreconfiguration('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyManagedPreconfiguration', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#crashHistory - errors', () => {
      it('should have a crashHistory function', (done) => {
        try {
          assert.equal(true, typeof a.crashHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rebootHistory - errors', () => {
      it('should have a rebootHistory function', (done) => {
        try {
          assert.equal(true, typeof a.rebootHistory === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplyApplianceWizard - errors', () => {
      it('should have a getApplyApplianceWizard function', (done) => {
        try {
          assert.equal(true, typeof a.getApplyApplianceWizard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getApplyApplianceWizard(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getApplyApplianceWizard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyApplianceWizard - errors', () => {
      it('should have a applyApplianceWizard function', (done) => {
        try {
          assert.equal(true, typeof a.applyApplianceWizard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applyWizardPost', (done) => {
        try {
          a.applyApplianceWizard(null, null, (data, error) => {
            try {
              const displayE = 'applyWizardPost is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyApplianceWizard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.applyApplianceWizard('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyApplianceWizard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsBackupGet - errors', () => {
      it('should have a gmsBackupGet function', (done) => {
        try {
          assert.equal(true, typeof a.gmsBackupGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsBackupConfig - errors', () => {
      it('should have a gmsBackupConfig function', (done) => {
        try {
          assert.equal(true, typeof a.gmsBackupConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gmsBackConfig', (done) => {
        try {
          a.gmsBackupConfig(null, (data, error) => {
            try {
              const displayE = 'gmsBackConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-gmsBackupConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#goldenOrchestratorExport - errors', () => {
      it('should have a goldenOrchestratorExport function', (done) => {
        try {
          assert.equal(true, typeof a.goldenOrchestratorExport === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mode', (done) => {
        try {
          a.goldenOrchestratorExport(null, null, (data, error) => {
            try {
              const displayE = 'mode is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-goldenOrchestratorExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing download', (done) => {
        try {
          a.goldenOrchestratorExport('fakeparam', null, (data, error) => {
            try {
              const displayE = 'download is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-goldenOrchestratorExport', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsBackupTestConnection - errors', () => {
      it('should have a gmsBackupTestConnection function', (done) => {
        try {
          assert.equal(true, typeof a.gmsBackupTestConnection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gmsBackConfig', (done) => {
        try {
          a.gmsBackupTestConnection(null, (data, error) => {
            try {
              const displayE = 'gmsBackConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-gmsBackupTestConnection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDiscoveryConfig - errors', () => {
      it('should have a getDiscoveryConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getDiscoveryConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDiscoveryConfig - errors', () => {
      it('should have a updateDiscoveryConfig function', (done) => {
        try {
          assert.equal(true, typeof a.updateDiscoveryConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing emails', (done) => {
        try {
          a.updateDiscoveryConfig(null, (data, error) => {
            try {
              const displayE = 'emails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateDiscoveryConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDynamicTopologyConfig - errors', () => {
      it('should have a getDynamicTopologyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getDynamicTopologyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateDynamicTopologyConfig - errors', () => {
      it('should have a updateDynamicTopologyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.updateDynamicTopologyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userName', (done) => {
        try {
          a.updateDynamicTopologyConfig(null, null, (data, error) => {
            try {
              const displayE = 'userName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateDynamicTopologyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.updateDynamicTopologyConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateDynamicTopologyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserDynamicTopologyConfig - errors', () => {
      it('should have a getUserDynamicTopologyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getUserDynamicTopologyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getUserDynamicTopologyConfig(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getUserDynamicTopologyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveDynamicTopologyConfig - errors', () => {
      it('should have a saveDynamicTopologyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.saveDynamicTopologyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.saveDynamicTopologyConfig(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveDynamicTopologyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.saveDynamicTopologyConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveDynamicTopologyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mgmInterfaceByGet - errors', () => {
      it('should have a mgmInterfaceByGet function', (done) => {
        try {
          assert.equal(true, typeof a.mgmInterfaceByGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mgmInterfaceByPost - errors', () => {
      it('should have a mgmInterfaceByPost function', (done) => {
        try {
          assert.equal(true, typeof a.mgmInterfaceByPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dnsDetails', (done) => {
        try {
          a.mgmInterfaceByPost(null, (data, error) => {
            try {
              const displayE = 'dnsDetails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mgmInterfaceByPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGmsGmsRegistration2 - errors', () => {
      it('should have a getGmsGmsRegistration2 function', (done) => {
        try {
          assert.equal(true, typeof a.getGmsGmsRegistration2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGmsGmsRegistration2 - errors', () => {
      it('should have a postGmsGmsRegistration2 function', (done) => {
        try {
          assert.equal(true, typeof a.postGmsGmsRegistration2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dnsDetails', (done) => {
        try {
          a.postGmsGmsRegistration2(null, (data, error) => {
            try {
              const displayE = 'dnsDetails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postGmsGmsRegistration2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allGRNodeGet - errors', () => {
      it('should have a allGRNodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.allGRNodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#grNodeUpdateByNePk - errors', () => {
      it('should have a grNodeUpdateByNePk function', (done) => {
        try {
          assert.equal(true, typeof a.grNodeUpdateByNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.grNodeUpdateByNePk(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-grNodeUpdateByNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gRNodeUpdatePostBody', (done) => {
        try {
          a.grNodeUpdateByNePk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gRNodeUpdatePostBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-grNodeUpdateByNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#grNodeGet - errors', () => {
      it('should have a grNodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.grNodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing grNodePk', (done) => {
        try {
          a.grNodeGet(null, (data, error) => {
            try {
              const displayE = 'grNodePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-grNodeGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#grNodeUpdate - errors', () => {
      it('should have a grNodeUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.grNodeUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing grNodePk', (done) => {
        try {
          a.grNodeUpdate(null, null, (data, error) => {
            try {
              const displayE = 'grNodePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-grNodeUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gRNodeUpdatePostBody', (done) => {
        try {
          a.grNodeUpdate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'gRNodeUpdatePostBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-grNodeUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#allGroupsGet - errors', () => {
      it('should have a allGroupsGet function', (done) => {
        try {
          assert.equal(true, typeof a.allGroupsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupAdd - errors', () => {
      it('should have a groupAdd function', (done) => {
        try {
          assert.equal(true, typeof a.groupAdd === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupNewPostBody', (done) => {
        try {
          a.groupAdd(null, (data, error) => {
            try {
              const displayE = 'groupNewPostBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-groupAdd', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rootGroupGet - errors', () => {
      it('should have a rootGroupGet function', (done) => {
        try {
          assert.equal(true, typeof a.rootGroupGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupDelete - errors', () => {
      it('should have a groupDelete function', (done) => {
        try {
          assert.equal(true, typeof a.groupDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupDelete(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-groupDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupGet - errors', () => {
      it('should have a groupGet function', (done) => {
        try {
          assert.equal(true, typeof a.groupGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupGet(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-groupGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupUpdate - errors', () => {
      it('should have a groupUpdate function', (done) => {
        try {
          assert.equal(true, typeof a.groupUpdate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.groupUpdate(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-groupUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing groupUpdatePostBody', (done) => {
        try {
          a.groupUpdate('fakeparam', null, (data, error) => {
            try {
              const displayE = 'groupUpdatePostBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-groupUpdate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#httpsCertificateValidation - errors', () => {
      it('should have a httpsCertificateValidation function', (done) => {
        try {
          assert.equal(true, typeof a.httpsCertificateValidation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateKeyData', (done) => {
        try {
          a.httpsCertificateValidation(null, (data, error) => {
            try {
              const displayE = 'certificateKeyData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-httpsCertificateValidation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGmsHttpsCertificateValidation - errors', () => {
      it('should have a postGmsHttpsCertificateValidation function', (done) => {
        try {
          assert.equal(true, typeof a.postGmsHttpsCertificateValidation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing certificateKeyData', (done) => {
        try {
          a.postGmsHttpsCertificateValidation(null, (data, error) => {
            try {
              const displayE = 'certificateKeyData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postGmsHttpsCertificateValidation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceLabels - errors', () => {
      it('should have a getInterfaceLabels function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postInterfaceLabels - errors', () => {
      it('should have a postInterfaceLabels function', (done) => {
        try {
          assert.equal(true, typeof a.postInterfaceLabels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newLabels', (done) => {
        try {
          a.postInterfaceLabels(null, null, (data, error) => {
            try {
              const displayE = 'newLabels is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postInterfaceLabels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLabelsForType - errors', () => {
      it('should have a getLabelsForType function', (done) => {
        try {
          assert.equal(true, typeof a.getLabelsForType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing type', (done) => {
        try {
          a.getLabelsForType(null, null, (data, error) => {
            try {
              const displayE = 'type is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getLabelsForType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInternalSubnets - errors', () => {
      it('should have a getInternalSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.getInternalSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveInternalSubnets - errors', () => {
      it('should have a saveInternalSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.saveInternalSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIPWhitelistDrops - errors', () => {
      it('should have a getIPWhitelistDrops function', (done) => {
        try {
          assert.equal(true, typeof a.getIPWhitelistDrops === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExternalIPWhitelist - errors', () => {
      it('should have a getExternalIPWhitelist function', (done) => {
        try {
          assert.equal(true, typeof a.getExternalIPWhitelist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setExternalIPWhitelist - errors', () => {
      it('should have a setExternalIPWhitelist function', (done) => {
        try {
          assert.equal(true, typeof a.setExternalIPWhitelist === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnets', (done) => {
        try {
          a.setExternalIPWhitelist(null, (data, error) => {
            try {
              const displayE = 'subnets is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-setExternalIPWhitelist', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduledJobsGet - errors', () => {
      it('should have a scheduledJobsGet function', (done) => {
        try {
          assert.equal(true, typeof a.scheduledJobsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduledJob2Post - errors', () => {
      it('should have a scheduledJob2Post function', (done) => {
        try {
          assert.equal(true, typeof a.scheduledJob2Post === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduledJobs2Post', (done) => {
        try {
          a.scheduledJob2Post(null, (data, error) => {
            try {
              const displayE = 'scheduledJobs2Post is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-scheduledJob2Post', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#historicalJobsGet - errors', () => {
      it('should have a historicalJobsGet function', (done) => {
        try {
          assert.equal(true, typeof a.historicalJobsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#historicalJobsGetById - errors', () => {
      it('should have a historicalJobsGetById function', (done) => {
        try {
          assert.equal(true, typeof a.historicalJobsGetById === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.historicalJobsGetById(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-historicalJobsGetById', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduledJobDelete - errors', () => {
      it('should have a scheduledJobDelete function', (done) => {
        try {
          assert.equal(true, typeof a.scheduledJobDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.scheduledJobDelete(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-scheduledJobDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduledJob2Get - errors', () => {
      it('should have a scheduledJob2Get function', (done) => {
        try {
          assert.equal(true, typeof a.scheduledJob2Get === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.scheduledJob2Get(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-scheduledJob2Get', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#scheduledJobModify - errors', () => {
      it('should have a scheduledJobModify function', (done) => {
        try {
          assert.equal(true, typeof a.scheduledJobModify === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.scheduledJobModify(null, null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-scheduledJobModify', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing scheduledJob2Post', (done) => {
        try {
          a.scheduledJobModify('fakeparam', null, (data, error) => {
            try {
              const displayE = 'scheduledJob2Post is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-scheduledJobModify', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#stopJob - errors', () => {
      it('should have a stopJob function', (done) => {
        try {
          assert.equal(true, typeof a.stopJob === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing jobId', (done) => {
        try {
          a.stopJob(null, (data, error) => {
            try {
              const displayE = 'jobId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-stopJob', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllMenuItems - errors', () => {
      it('should have a getAllMenuItems function', (done) => {
        try {
          assert.equal(true, typeof a.getAllMenuItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addMenuType - errors', () => {
      it('should have a addMenuType function', (done) => {
        try {
          assert.equal(true, typeof a.addMenuType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newMenuType', (done) => {
        try {
          a.addMenuType(null, (data, error) => {
            try {
              const displayE = 'newMenuType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-addMenuType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateMenuTypeItems - errors', () => {
      it('should have a updateMenuTypeItems function', (done) => {
        try {
          assert.equal(true, typeof a.updateMenuTypeItems === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing usersUpdate', (done) => {
        try {
          a.updateMenuTypeItems(null, (data, error) => {
            try {
              const displayE = 'usersUpdate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateMenuTypeItems', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExistingMenuType - errors', () => {
      it('should have a deleteExistingMenuType function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExistingMenuType === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing menuTypeName', (done) => {
        try {
          a.deleteExistingMenuType(null, (data, error) => {
            try {
              const displayE = 'menuTypeName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteExistingMenuType', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOverlayApplianceAssociations - errors', () => {
      it('should have a getAllOverlayApplianceAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOverlayApplianceAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addOverlayApplianceAssociations - errors', () => {
      it('should have a addOverlayApplianceAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.addOverlayApplianceAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associations', (done) => {
        try {
          a.addOverlayApplianceAssociations(null, (data, error) => {
            try {
              const displayE = 'associations is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-addOverlayApplianceAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#removeAppliancesFromOverlays - errors', () => {
      it('should have a removeAppliancesFromOverlays function', (done) => {
        try {
          assert.equal(true, typeof a.removeAppliancesFromOverlays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing appliancesToRemove', (done) => {
        try {
          a.removeAppliancesFromOverlays(null, (data, error) => {
            try {
              const displayE = 'appliancesToRemove is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-removeAppliancesFromOverlays', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesForOverlay - errors', () => {
      it('should have a getAppliancesForOverlay function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancesForOverlay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.getAppliancesForOverlay(null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getAppliancesForOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteOverlayApplianceAssociation - errors', () => {
      it('should have a deleteOverlayApplianceAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.deleteOverlayApplianceAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.deleteOverlayApplianceAssociation(null, null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteOverlayApplianceAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.deleteOverlayApplianceAssociation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteOverlayApplianceAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllOverlays - errors', () => {
      it('should have a getAllOverlays function', (done) => {
        try {
          assert.equal(true, typeof a.getAllOverlays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNewOverlay - errors', () => {
      it('should have a addNewOverlay function', (done) => {
        try {
          assert.equal(true, typeof a.addNewOverlay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newOverlay', (done) => {
        try {
          a.addNewOverlay(null, (data, error) => {
            try {
              const displayE = 'newOverlay is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-addNewOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaxNumberOfOverlays - errors', () => {
      it('should have a getMaxNumberOfOverlays function', (done) => {
        try {
          assert.equal(true, typeof a.getMaxNumberOfOverlays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRegionalOverlays - errors', () => {
      it('should have a getAllRegionalOverlays function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRegionalOverlays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postAllRegionalOverlays - errors', () => {
      it('should have a postAllRegionalOverlays function', (done) => {
        try {
          assert.equal(true, typeof a.postAllRegionalOverlays === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing allOverlays', (done) => {
        try {
          a.postAllRegionalOverlays(null, (data, error) => {
            try {
              const displayE = 'allOverlays is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postAllRegionalOverlays', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRegionalOverlay - errors', () => {
      it('should have a getRegionalOverlay function', (done) => {
        try {
          assert.equal(true, typeof a.getRegionalOverlay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.getRegionalOverlay(null, null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRegionalOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionId', (done) => {
        try {
          a.getRegionalOverlay('fakeparam', null, (data, error) => {
            try {
              const displayE = 'regionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRegionalOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyRegionalOverlay - errors', () => {
      it('should have a modifyRegionalOverlay function', (done) => {
        try {
          assert.equal(true, typeof a.modifyRegionalOverlay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.modifyRegionalOverlay(null, null, null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-modifyRegionalOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionId', (done) => {
        try {
          a.modifyRegionalOverlay('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'regionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-modifyRegionalOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayConfig', (done) => {
        try {
          a.modifyRegionalOverlay('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'overlayConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-modifyRegionalOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExistingOverlay - errors', () => {
      it('should have a deleteExistingOverlay function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExistingOverlay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.deleteExistingOverlay(null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteExistingOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOverlay - errors', () => {
      it('should have a getOverlay function', (done) => {
        try {
          assert.equal(true, typeof a.getOverlay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.getOverlay(null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExistingOverlay - errors', () => {
      it('should have a updateExistingOverlay function', (done) => {
        try {
          assert.equal(true, typeof a.updateExistingOverlay === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.updateExistingOverlay(null, null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateExistingOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updatedOverlay', (done) => {
        try {
          a.updateExistingOverlay('fakeparam', null, (data, error) => {
            try {
              const displayE = 'updatedOverlay is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateExistingOverlay', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOverlayPriorityMap - errors', () => {
      it('should have a getOverlayPriorityMap function', (done) => {
        try {
          assert.equal(true, typeof a.getOverlayPriorityMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveOverlayPriority - errors', () => {
      it('should have a saveOverlayPriority function', (done) => {
        try {
          assert.equal(true, typeof a.saveOverlayPriority === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayPriority', (done) => {
        try {
          a.saveOverlayPriority(null, (data, error) => {
            try {
              const displayE = 'overlayPriority is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveOverlayPriority', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getScheduleTimezone - errors', () => {
      it('should have a getScheduleTimezone function', (done) => {
        try {
          assert.equal(true, typeof a.getScheduleTimezone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateScheduleTimezone - errors', () => {
      it('should have a updateScheduleTimezone function', (done) => {
        try {
          assert.equal(true, typeof a.updateScheduleTimezone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultTimezone', (done) => {
        try {
          a.updateScheduleTimezone(null, (data, error) => {
            try {
              const displayE = 'defaultTimezone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateScheduleTimezone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getServices - errors', () => {
      it('should have a getServices function', (done) => {
        try {
          assert.equal(true, typeof a.getServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveServices - errors', () => {
      it('should have a saveServices function', (done) => {
        try {
          assert.equal(true, typeof a.saveServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newServices', (done) => {
        try {
          a.saveServices(null, (data, error) => {
            try {
              const displayE = 'newServices is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sessionTimeout - errors', () => {
      it('should have a sessionTimeout function', (done) => {
        try {
          assert.equal(true, typeof a.sessionTimeout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putGmsSessionTimeout - errors', () => {
      it('should have a putGmsSessionTimeout function', (done) => {
        try {
          assert.equal(true, typeof a.putGmsSessionTimeout === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sessionDetail', (done) => {
        try {
          a.putGmsSessionTimeout(null, (data, error) => {
            try {
              const displayE = 'sessionDetail is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-putGmsSessionTimeout', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsCollection - errors', () => {
      it('should have a statsCollection function', (done) => {
        try {
          assert.equal(true, typeof a.statsCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGmsStatsCollection - errors', () => {
      it('should have a postGmsStatsCollection function', (done) => {
        try {
          assert.equal(true, typeof a.postGmsStatsCollection === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing statsCollection', (done) => {
        try {
          a.postGmsStatsCollection(null, (data, error) => {
            try {
              const displayE = 'statsCollection is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postGmsStatsCollection', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsCollectionDefault - errors', () => {
      it('should have a statsCollectionDefault function', (done) => {
        try {
          assert.equal(true, typeof a.statsCollectionDefault === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getThirdPartyServices - errors', () => {
      it('should have a getThirdPartyServices function', (done) => {
        try {
          assert.equal(true, typeof a.getThirdPartyServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDefaultTopologyConfig - errors', () => {
      it('should have a getDefaultTopologyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getDefaultTopologyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateTopologyConfig - errors', () => {
      it('should have a updateTopologyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.updateTopologyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userName', (done) => {
        try {
          a.updateTopologyConfig(null, null, (data, error) => {
            try {
              const displayE = 'userName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateTopologyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.updateTopologyConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateTopologyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveMapImage - errors', () => {
      it('should have a saveMapImage function', (done) => {
        try {
          assert.equal(true, typeof a.saveMapImage === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing defaultParam', (done) => {
        try {
          a.saveMapImage(null, (data, error) => {
            try {
              const displayE = 'defaultParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveMapImage', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserTopologyConfig - errors', () => {
      it('should have a getUserTopologyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getUserTopologyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getUserTopologyConfig(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getUserTopologyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveTopologyConfig - errors', () => {
      it('should have a saveTopologyConfig function', (done) => {
        try {
          assert.equal(true, typeof a.saveTopologyConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.saveTopologyConfig(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveTopologyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing request', (done) => {
        try {
          a.saveTopologyConfig('fakeparam', null, (data, error) => {
            try {
              const displayE = 'request is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveTopologyConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTunnelGroupApplianceAssociations - errors', () => {
      it('should have a getAllTunnelGroupApplianceAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTunnelGroupApplianceAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addTunnelGroupApplianceAssociations - errors', () => {
      it('should have a addTunnelGroupApplianceAssociations function', (done) => {
        try {
          assert.equal(true, typeof a.addTunnelGroupApplianceAssociations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing associations', (done) => {
        try {
          a.addTunnelGroupApplianceAssociations(null, (data, error) => {
            try {
              const displayE = 'associations is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-addTunnelGroupApplianceAssociations', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAppliancesForTunnelGroup - errors', () => {
      it('should have a getAppliancesForTunnelGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getAppliancesForTunnelGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelGroupId', (done) => {
        try {
          a.getAppliancesForTunnelGroup(null, (data, error) => {
            try {
              const displayE = 'tunnelGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getAppliancesForTunnelGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTunnelGroupApplianceAssociation - errors', () => {
      it('should have a deleteTunnelGroupApplianceAssociation function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTunnelGroupApplianceAssociation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelGroupId', (done) => {
        try {
          a.deleteTunnelGroupApplianceAssociation(null, null, (data, error) => {
            try {
              const displayE = 'tunnelGroupId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteTunnelGroupApplianceAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.deleteTunnelGroupApplianceAssociation('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteTunnelGroupApplianceAssociation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllTunnelGroups - errors', () => {
      it('should have a getAllTunnelGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAllTunnelGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addNewTunnelGroup - errors', () => {
      it('should have a addNewTunnelGroup function', (done) => {
        try {
          assert.equal(true, typeof a.addNewTunnelGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newTunnelGroup', (done) => {
        try {
          a.addNewTunnelGroup(null, (data, error) => {
            try {
              const displayE = 'newTunnelGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-addNewTunnelGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteExistingTunnelGroup - errors', () => {
      it('should have a deleteExistingTunnelGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteExistingTunnelGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.deleteExistingTunnelGroup(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteExistingTunnelGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelGroup - errors', () => {
      it('should have a getTunnelGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnelGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.getTunnelGroup(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTunnelGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateExistingTunnelGroup - errors', () => {
      it('should have a updateExistingTunnelGroup function', (done) => {
        try {
          assert.equal(true, typeof a.updateExistingTunnelGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.updateExistingTunnelGroup(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateExistingTunnelGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing updatedTunnelGroup', (done) => {
        try {
          a.updateExistingTunnelGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'updatedTunnelGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-updateExistingTunnelGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelGroupProperties - errors', () => {
      it('should have a getTunnelGroupProperties function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnelGroupProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setTunnelGroupProperties - errors', () => {
      it('should have a setTunnelGroupProperties function', (done) => {
        try {
          assert.equal(true, typeof a.setTunnelGroupProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelGroupProperties', (done) => {
        try {
          a.setTunnelGroupProperties(null, (data, error) => {
            try {
              const displayE = 'tunnelGroupProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-setTunnelGroupProperties', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#versionInfo - errors', () => {
      it('should have a versionInfo function', (done) => {
        try {
          assert.equal(true, typeof a.versionInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsConfig - errors', () => {
      it('should have a gmsConfig function', (done) => {
        try {
          assert.equal(true, typeof a.gmsConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGmsConfig - errors', () => {
      it('should have a postGmsConfig function', (done) => {
        try {
          assert.equal(true, typeof a.postGmsConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gmsConfigCreation', (done) => {
        try {
          a.postGmsConfig(null, (data, error) => {
            try {
              const displayE = 'gmsConfigCreation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postGmsConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteGmsConfigBase - errors', () => {
      it('should have a deleteGmsConfigBase function', (done) => {
        try {
          assert.equal(true, typeof a.deleteGmsConfigBase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing base', (done) => {
        try {
          a.deleteGmsConfigBase(null, null, (data, error) => {
            try {
              const displayE = 'base is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteGmsConfigBase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGmsConfigBase - errors', () => {
      it('should have a getGmsConfigBase function', (done) => {
        try {
          assert.equal(true, typeof a.getGmsConfigBase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing base', (done) => {
        try {
          a.getGmsConfigBase(null, null, (data, error) => {
            try {
              const displayE = 'base is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getGmsConfigBase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#putGmsConfigBase - errors', () => {
      it('should have a putGmsConfigBase function', (done) => {
        try {
          assert.equal(true, typeof a.putGmsConfigBase === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing base', (done) => {
        try {
          a.putGmsConfigBase(null, null, null, (data, error) => {
            try {
              const displayE = 'base is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-putGmsConfigBase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing gmsConfigUpdate', (done) => {
        try {
          a.putGmsConfigBase('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'gmsConfigUpdate is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-putGmsConfigBase', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hostnameByGet - errors', () => {
      it('should have a hostnameByGet function', (done) => {
        try {
          assert.equal(true, typeof a.hostnameByGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#licenseByGet - errors', () => {
      it('should have a licenseByGet function', (done) => {
        try {
          assert.equal(true, typeof a.licenseByGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#licenseByPut - errors', () => {
      it('should have a licenseByPut function', (done) => {
        try {
          assert.equal(true, typeof a.licenseByPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licenseKey', (done) => {
        try {
          a.licenseByPut(null, (data, error) => {
            try {
              const displayE = 'licenseKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-licenseByPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#validateKey - errors', () => {
      it('should have a validateKey function', (done) => {
        try {
          assert.equal(true, typeof a.validateKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing licenseKey', (done) => {
        try {
          a.validateKey(null, (data, error) => {
            try {
              const displayE = 'licenseKey is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-validateKey', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#osInfo - errors', () => {
      it('should have a osInfo function', (done) => {
        try {
          assert.equal(true, typeof a.osInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsRemoteAuthGet - errors', () => {
      it('should have a gmsRemoteAuthGet function', (done) => {
        try {
          assert.equal(true, typeof a.gmsRemoteAuthGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsRemoteAuthPost - errors', () => {
      it('should have a gmsRemoteAuthPost function', (done) => {
        try {
          assert.equal(true, typeof a.gmsRemoteAuthPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing remoteAuthData', (done) => {
        try {
          a.gmsRemoteAuthPost(null, (data, error) => {
            try {
              const displayE = 'remoteAuthData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-gmsRemoteAuthPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sMTP - errors', () => {
      it('should have a sMTP function', (done) => {
        try {
          assert.equal(true, typeof a.sMTP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGmsSMTP - errors', () => {
      it('should have a getGmsSMTP function', (done) => {
        try {
          assert.equal(true, typeof a.getGmsSMTP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sMTPPost - errors', () => {
      it('should have a sMTPPost function', (done) => {
        try {
          assert.equal(true, typeof a.sMTPPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sMTPDetails', (done) => {
        try {
          a.sMTPPost(null, (data, error) => {
            try {
              const displayE = 'sMTPDetails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sMTPPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delUnverifiedEmails - errors', () => {
      it('should have a delUnverifiedEmails function', (done) => {
        try {
          assert.equal(true, typeof a.delUnverifiedEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing emails', (done) => {
        try {
          a.delUnverifiedEmails(null, (data, error) => {
            try {
              const displayE = 'emails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-delUnverifiedEmails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sendVerificationEmail - errors', () => {
      it('should have a sendVerificationEmail function', (done) => {
        try {
          assert.equal(true, typeof a.sendVerificationEmail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing address', (done) => {
        try {
          a.sendVerificationEmail(null, (data, error) => {
            try {
              const displayE = 'address is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sendVerificationEmail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sMTPTestMail - errors', () => {
      it('should have a sMTPTestMail function', (done) => {
        try {
          assert.equal(true, typeof a.sMTPTestMail === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sMTPDetails', (done) => {
        try {
          a.sMTPTestMail(null, (data, error) => {
            try {
              const displayE = 'sMTPDetails is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sMTPTestMail', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#listUnverifiedEmails - errors', () => {
      it('should have a listUnverifiedEmails function', (done) => {
        try {
          assert.equal(true, typeof a.listUnverifiedEmails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#verifyEmailAddress - errors', () => {
      it('should have a verifyEmailAddress function', (done) => {
        try {
          assert.equal(true, typeof a.verifyEmailAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.verifyEmailAddress(null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-verifyEmailAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsBriefInfo - errors', () => {
      it('should have a gmsBriefInfo function', (done) => {
        try {
          assert.equal(true, typeof a.gmsBriefInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#hello - errors', () => {
      it('should have a hello function', (done) => {
        try {
          assert.equal(true, typeof a.hello === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsServerInfo - errors', () => {
      it('should have a gmsServerInfo function', (done) => {
        try {
          assert.equal(true, typeof a.gmsServerInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsPingInfo - errors', () => {
      it('should have a gmsPingInfo function', (done) => {
        try {
          assert.equal(true, typeof a.gmsPingInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#haGroupsGet - errors', () => {
      it('should have a haGroupsGet function', (done) => {
        try {
          assert.equal(true, typeof a.haGroupsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#haGroupsPost - errors', () => {
      it('should have a haGroupsPost function', (done) => {
        try {
          assert.equal(true, typeof a.haGroupsPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing haGroups', (done) => {
        try {
          a.haGroupsPost(null, (data, error) => {
            try {
              const displayE = 'haGroups is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-haGroupsPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#healthSummary - errors', () => {
      it('should have a healthSummary function', (done) => {
        try {
          assert.equal(true, typeof a.healthSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing healthSummaryGetPostBody', (done) => {
        try {
          a.healthSummary(null, (data, error) => {
            try {
              const displayE = 'healthSummaryGetPostBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-healthSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHealthAlarmPeriodSummary - errors', () => {
      it('should have a getHealthAlarmPeriodSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getHealthAlarmPeriodSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing from', (done) => {
        try {
          a.getHealthAlarmPeriodSummary(null, null, null, (data, error) => {
            try {
              const displayE = 'from is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getHealthAlarmPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing to', (done) => {
        try {
          a.getHealthAlarmPeriodSummary('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'to is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getHealthAlarmPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceId', (done) => {
        try {
          a.getHealthAlarmPeriodSummary('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applianceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getHealthAlarmPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jitterPeriodSummary - errors', () => {
      it('should have a jitterPeriodSummary function', (done) => {
        try {
          assert.equal(true, typeof a.jitterPeriodSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing time', (done) => {
        try {
          a.jitterPeriodSummary(null, null, null, (data, error) => {
            try {
              const displayE = 'time is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-jitterPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.jitterPeriodSummary('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-jitterPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceId', (done) => {
        try {
          a.jitterPeriodSummary('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applianceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-jitterPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#latencyPeriodSummary - errors', () => {
      it('should have a latencyPeriodSummary function', (done) => {
        try {
          assert.equal(true, typeof a.latencyPeriodSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing time', (done) => {
        try {
          a.latencyPeriodSummary(null, null, null, (data, error) => {
            try {
              const displayE = 'time is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-latencyPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.latencyPeriodSummary('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-latencyPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceId', (done) => {
        try {
          a.latencyPeriodSummary('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applianceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-latencyPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#lossPeriodSummary - errors', () => {
      it('should have a lossPeriodSummary function', (done) => {
        try {
          assert.equal(true, typeof a.lossPeriodSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing time', (done) => {
        try {
          a.lossPeriodSummary(null, null, null, (data, error) => {
            try {
              const displayE = 'time is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-lossPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.lossPeriodSummary('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-lossPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceId', (done) => {
        try {
          a.lossPeriodSummary('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applianceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-lossPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mosPeriodSummary - errors', () => {
      it('should have a mosPeriodSummary function', (done) => {
        try {
          assert.equal(true, typeof a.mosPeriodSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing time', (done) => {
        try {
          a.mosPeriodSummary(null, null, null, (data, error) => {
            try {
              const displayE = 'time is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mosPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayId', (done) => {
        try {
          a.mosPeriodSummary('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'overlayId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mosPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceId', (done) => {
        try {
          a.mosPeriodSummary('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'applianceId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mosPeriodSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyHostname - errors', () => {
      it('should have a applyHostname function', (done) => {
        try {
          assert.equal(true, typeof a.applyHostname === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.applyHostname(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyHostname', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostName', (done) => {
        try {
          a.applyHostname('fakeparam', null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyHostname', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#idleTime - errors', () => {
      it('should have a idleTime function', (done) => {
        try {
          assert.equal(true, typeof a.idleTime === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIdleIncrement - errors', () => {
      it('should have a getIdleIncrement function', (done) => {
        try {
          assert.equal(true, typeof a.getIdleIncrement === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ikeless - errors', () => {
      it('should have a ikeless function', (done) => {
        try {
          assert.equal(true, typeof a.ikeless === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyLabelsToAppliance - errors', () => {
      it('should have a applyLabelsToAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.applyLabelsToAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.applyLabelsToAppliance(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyLabelsToAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interfaceState - errors', () => {
      it('should have a interfaceState function', (done) => {
        try {
          assert.equal(true, typeof a.interfaceState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.interfaceState(null, null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.interfaceState('fakeparam', null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#licensing - errors', () => {
      it('should have a licensing function', (done) => {
        try {
          assert.equal(true, typeof a.licensing === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicensePortalAppliance - errors', () => {
      it('should have a getLicensePortalAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.getLicensePortalAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#grant - errors', () => {
      it('should have a grant function', (done) => {
        try {
          assert.equal(true, typeof a.grant === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.grant(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-grant', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteLicenseToken - errors', () => {
      it('should have a deleteLicenseToken function', (done) => {
        try {
          assert.equal(true, typeof a.deleteLicenseToken === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.deleteLicenseToken(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteLicenseToken', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLicensePortalApplianceRevokeNePk - errors', () => {
      it('should have a postLicensePortalApplianceRevokeNePk function', (done) => {
        try {
          assert.equal(true, typeof a.postLicensePortalApplianceRevokeNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.postLicensePortalApplianceRevokeNePk(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postLicensePortalApplianceRevokeNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLicensePortalEcNePk - errors', () => {
      it('should have a postLicensePortalEcNePk function', (done) => {
        try {
          assert.equal(true, typeof a.postLicensePortalEcNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.postLicensePortalEcNePk(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postLicensePortalEcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceInfo', (done) => {
        try {
          a.postLicensePortalEcNePk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'applianceInfo is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postLicensePortalEcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicensePortalSummary - errors', () => {
      it('should have a getLicensePortalSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getLicensePortalSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLicenseVx - errors', () => {
      it('should have a getLicenseVx function', (done) => {
        try {
          assert.equal(true, typeof a.getLicenseVx === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#linkIntegrityTestRun - errors', () => {
      it('should have a linkIntegrityTestRun function', (done) => {
        try {
          assert.equal(true, typeof a.linkIntegrityTestRun === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing linkIntegrityTestRun', (done) => {
        try {
          a.linkIntegrityTestRun(null, (data, error) => {
            try {
              const displayE = 'linkIntegrityTestRun is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-linkIntegrityTestRun', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#linkIntegrityStatus - errors', () => {
      it('should have a linkIntegrityStatus function', (done) => {
        try {
          assert.equal(true, typeof a.linkIntegrityStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.linkIntegrityStatus(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-linkIntegrityStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addressLookup - errors', () => {
      it('should have a addressLookup function', (done) => {
        try {
          assert.equal(true, typeof a.addressLookup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing address', (done) => {
        try {
          a.addressLookup(null, (data, error) => {
            try {
              const displayE = 'address is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-addressLookup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLoggingSettings - errors', () => {
      it('should have a getLoggingSettings function', (done) => {
        try {
          assert.equal(true, typeof a.getLoggingSettings === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getLoggingSettings(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getLoggingSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.getLoggingSettings('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getLoggingSettings', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sessions - errors', () => {
      it('should have a sessions function', (done) => {
        try {
          assert.equal(true, typeof a.sessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMaintenanceMode - errors', () => {
      it('should have a getMaintenanceMode function', (done) => {
        try {
          assert.equal(true, typeof a.getMaintenanceMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setMaintenanceMode - errors', () => {
      it('should have a setMaintenanceMode function', (done) => {
        try {
          assert.equal(true, typeof a.setMaintenanceMode === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing maintenanceModePostBody', (done) => {
        try {
          a.setMaintenanceMode(null, (data, error) => {
            try {
              const displayE = 'maintenanceModePostBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-setMaintenanceMode', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUploadedMap - errors', () => {
      it('should have a deleteUploadedMap function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUploadedMap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageName', (done) => {
        try {
          a.deleteUploadedMap(null, (data, error) => {
            try {
              const displayE = 'imageName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteUploadedMap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUploadedMaps - errors', () => {
      it('should have a getUploadedMaps function', (done) => {
        try {
          assert.equal(true, typeof a.getUploadedMaps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multicastConfigGet - errors', () => {
      it('should have a multicastConfigGet function', (done) => {
        try {
          assert.equal(true, typeof a.multicastConfigGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.multicastConfigGet(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-multicastConfigGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.multicastConfigGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-multicastConfigGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multicastEnableGet - errors', () => {
      it('should have a multicastEnableGet function', (done) => {
        try {
          assert.equal(true, typeof a.multicastEnableGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.multicastEnableGet(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-multicastEnableGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.multicastEnableGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-multicastEnableGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multicastInterfaceStateGet - errors', () => {
      it('should have a multicastInterfaceStateGet function', (done) => {
        try {
          assert.equal(true, typeof a.multicastInterfaceStateGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.multicastInterfaceStateGet(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-multicastInterfaceStateGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multicastNeighborStateGet - errors', () => {
      it('should have a multicastNeighborStateGet function', (done) => {
        try {
          assert.equal(true, typeof a.multicastNeighborStateGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.multicastNeighborStateGet(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-multicastNeighborStateGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multicastRouteStateGet - errors', () => {
      it('should have a multicastRouteStateGet function', (done) => {
        try {
          assert.equal(true, typeof a.multicastRouteStateGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.multicastRouteStateGet(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-multicastRouteStateGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNat - errors', () => {
      it('should have a getAllNat function', (done) => {
        try {
          assert.equal(true, typeof a.getAllNat === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getAllNat(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getAllNat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.getAllNat('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getAllNat', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#branchNatMapsGet - errors', () => {
      it('should have a branchNatMapsGet function', (done) => {
        try {
          assert.equal(true, typeof a.branchNatMapsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.branchNatMapsGet(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-branchNatMapsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.branchNatMapsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-branchNatMapsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllNatPools - errors', () => {
      it('should have a getAllNatPools function', (done) => {
        try {
          assert.equal(true, typeof a.getAllNatPools === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getAllNatPools(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getAllNatPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.getAllNatPools('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getAllNatPools', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetFlowData - errors', () => {
      it('should have a getNetFlowData function', (done) => {
        try {
          assert.equal(true, typeof a.getNetFlowData === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getNetFlowData(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getNetFlowData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.getNetFlowData('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getNetFlowData', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#eraseNmPost - errors', () => {
      it('should have a eraseNmPost function', (done) => {
        try {
          assert.equal(true, typeof a.eraseNmPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceKeys', (done) => {
        try {
          a.eraseNmPost(null, (data, error) => {
            try {
              const displayE = 'applianceKeys is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-eraseNmPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delNotification - errors', () => {
      it('should have a delNotification function', (done) => {
        try {
          assert.equal(true, typeof a.delNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNotification - errors', () => {
      it('should have a getNotification function', (done) => {
        try {
          assert.equal(true, typeof a.getNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNotification - errors', () => {
      it('should have a postNotification function', (done) => {
        try {
          assert.equal(true, typeof a.postNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing notificationConfig', (done) => {
        try {
          a.postNotification(null, (data, error) => {
            try {
              const displayE = 'notificationConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getInterfaceConfigDta - errors', () => {
      it('should have a getInterfaceConfigDta function', (done) => {
        try {
          assert.equal(true, typeof a.getInterfaceConfigDta === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getInterfaceConfigDta(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getInterfaceConfigDta', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOSPFSystemConfig - errors', () => {
      it('should have a getOSPFSystemConfig function', (done) => {
        try {
          assert.equal(true, typeof a.getOSPFSystemConfig === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getOSPFSystemConfig(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getOSPFSystemConfig', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#oSPFInterfaceStateObj - errors', () => {
      it('should have a oSPFInterfaceStateObj function', (done) => {
        try {
          assert.equal(true, typeof a.oSPFInterfaceStateObj === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.oSPFInterfaceStateObj(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-oSPFInterfaceStateObj', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#oSPFNeighborsStateObj - errors', () => {
      it('should have a oSPFNeighborsStateObj function', (done) => {
        try {
          assert.equal(true, typeof a.oSPFNeighborsStateObj === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.oSPFNeighborsStateObj(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-oSPFNeighborsStateObj', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#oSPFStateObj - errors', () => {
      it('should have a oSPFStateObj function', (done) => {
        try {
          assert.equal(true, typeof a.oSPFStateObj === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.oSPFStateObj(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-oSPFStateObj', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOverlayMngProps - errors', () => {
      it('should have a getOverlayMngProps function', (done) => {
        try {
          assert.equal(true, typeof a.getOverlayMngProps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setOverlayMngProps - errors', () => {
      it('should have a setOverlayMngProps function', (done) => {
        try {
          assert.equal(true, typeof a.setOverlayMngProps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing overlayManagerProperties', (done) => {
        try {
          a.setOverlayMngProps(null, (data, error) => {
            try {
              const displayE = 'overlayManagerProperties is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-setOverlayMngProps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portForwardingGet - errors', () => {
      it('should have a portForwardingGet function', (done) => {
        try {
          assert.equal(true, typeof a.portForwardingGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.portForwardingGet(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portForwardingGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProfilesGet - errors', () => {
      it('should have a portProfilesGet function', (done) => {
        try {
          assert.equal(true, typeof a.portProfilesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProfilesPost - errors', () => {
      it('should have a portProfilesPost function', (done) => {
        try {
          assert.equal(true, typeof a.portProfilesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portProfilesPost', (done) => {
        try {
          a.portProfilesPost(null, (data, error) => {
            try {
              const displayE = 'portProfilesPost is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProfilesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProfilesDelete - errors', () => {
      it('should have a portProfilesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.portProfilesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portProfileId', (done) => {
        try {
          a.portProfilesDelete(null, (data, error) => {
            try {
              const displayE = 'portProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProfilesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPortProfilesConfigPortProfileId - errors', () => {
      it('should have a getPortProfilesConfigPortProfileId function', (done) => {
        try {
          assert.equal(true, typeof a.getPortProfilesConfigPortProfileId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portProfileId', (done) => {
        try {
          a.getPortProfilesConfigPortProfileId(null, (data, error) => {
            try {
              const displayE = 'portProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getPortProfilesConfigPortProfileId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProfilesPut - errors', () => {
      it('should have a portProfilesPut function', (done) => {
        try {
          assert.equal(true, typeof a.portProfilesPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portProfileId', (done) => {
        try {
          a.portProfilesPut(null, null, (data, error) => {
            try {
              const displayE = 'portProfileId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProfilesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portProfilesPost', (done) => {
        try {
          a.portProfilesPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'portProfilesPost is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProfilesPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProfilesLabelInUseGet - errors', () => {
      it('should have a portProfilesLabelInUseGet function', (done) => {
        try {
          assert.equal(true, typeof a.portProfilesLabelInUseGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelId', (done) => {
        try {
          a.portProfilesLabelInUseGet(null, null, (data, error) => {
            try {
              const displayE = 'labelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProfilesLabelInUseGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing labelSide', (done) => {
        try {
          a.portProfilesLabelInUseGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'labelSide is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portProfilesLabelInUseGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllApplianceAccessGroups - errors', () => {
      it('should have a getAllApplianceAccessGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getAllApplianceAccessGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrUpdateApplianceAccessGroup - errors', () => {
      it('should have a createOrUpdateApplianceAccessGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createOrUpdateApplianceAccessGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createOrUpdateApplianceAccessGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-createOrUpdateApplianceAccessGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteApplianceAccessGroupByName - errors', () => {
      it('should have a deleteApplianceAccessGroupByName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteApplianceAccessGroupByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceAccessGroupName', (done) => {
        try {
          a.deleteApplianceAccessGroupByName(null, (data, error) => {
            try {
              const displayE = 'applianceAccessGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteApplianceAccessGroupByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getApplianceAccessGroupByName - errors', () => {
      it('should have a getApplianceAccessGroupByName function', (done) => {
        try {
          assert.equal(true, typeof a.getApplianceAccessGroupByName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceAccessGroupName', (done) => {
        try {
          a.getApplianceAccessGroupByName(null, (data, error) => {
            try {
              const displayE = 'applianceAccessGroupName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getApplianceAccessGroupByName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRbacAssignments - errors', () => {
      it('should have a getAllRbacAssignments function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRbacAssignments === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createOrUpdateRbacAssignment - errors', () => {
      it('should have a createOrUpdateRbacAssignment function', (done) => {
        try {
          assert.equal(true, typeof a.createOrUpdateRbacAssignment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createOrUpdateRbacAssignment(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-createOrUpdateRbacAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRbacAssignment - errors', () => {
      it('should have a deleteRbacAssignment function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRbacAssignment === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.deleteRbacAssignment(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteRbacAssignment', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRbacAssignmentByUsername - errors', () => {
      it('should have a getRbacAssignmentByUsername function', (done) => {
        try {
          assert.equal(true, typeof a.getRbacAssignmentByUsername === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.getRbacAssignmentByUsername(null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRbacAssignmentByUsername', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRoles - errors', () => {
      it('should have a getAllRoles function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRoles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saveRole - errors', () => {
      it('should have a saveRole function', (done) => {
        try {
          assert.equal(true, typeof a.saveRole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.saveRole(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-saveRole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAssignedMenus - errors', () => {
      it('should have a getAssignedMenus function', (done) => {
        try {
          assert.equal(true, typeof a.getAssignedMenus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteRoleByRoleName - errors', () => {
      it('should have a deleteRoleByRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteRoleByRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.deleteRoleByRoleName(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteRoleByRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRoleByRoleName - errors', () => {
      it('should have a getRoleByRoleName function', (done) => {
        try {
          assert.equal(true, typeof a.getRoleByRoleName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing roleName', (done) => {
        try {
          a.getRoleByRoleName(null, (data, error) => {
            try {
              const displayE = 'roleName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRoleByRoleName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reachabilityFromAppliance - errors', () => {
      it('should have a reachabilityFromAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.reachabilityFromAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.reachabilityFromAppliance(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-reachabilityFromAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#reachabilityFromGMS - errors', () => {
      it('should have a reachabilityFromGMS function', (done) => {
        try {
          assert.equal(true, typeof a.reachabilityFromGMS === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.reachabilityFromGMS(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-reachabilityFromGMS', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#realtimeStats - errors', () => {
      it('should have a realtimeStats function', (done) => {
        try {
          assert.equal(true, typeof a.realtimeStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.realtimeStats(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-realtimeStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.realtimeStats('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-realtimeStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionGet - errors', () => {
      it('should have a regionGet function', (done) => {
        try {
          assert.equal(true, typeof a.regionGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionPost - errors', () => {
      it('should have a regionPost function', (done) => {
        try {
          assert.equal(true, typeof a.regionPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.regionPost(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-regionPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionAssociationGET - errors', () => {
      it('should have a regionAssociationGET function', (done) => {
        try {
          assert.equal(true, typeof a.regionAssociationGET === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionAssociationPost - errors', () => {
      it('should have a regionAssociationPost function', (done) => {
        try {
          assert.equal(true, typeof a.regionAssociationPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.regionAssociationPost(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-regionAssociationPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRegionsAppliancesNePkNePK - errors', () => {
      it('should have a getRegionsAppliancesNePkNePK function', (done) => {
        try {
          assert.equal(true, typeof a.getRegionsAppliancesNePkNePK === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePK', (done) => {
        try {
          a.getRegionsAppliancesNePkNePK(null, (data, error) => {
            try {
              const displayE = 'nePK is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRegionsAppliancesNePkNePK', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRegionsAppliancesRegionIdRegionId - errors', () => {
      it('should have a getRegionsAppliancesRegionIdRegionId function', (done) => {
        try {
          assert.equal(true, typeof a.getRegionsAppliancesRegionIdRegionId === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionId', (done) => {
        try {
          a.getRegionsAppliancesRegionIdRegionId(null, (data, error) => {
            try {
              const displayE = 'regionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRegionsAppliancesRegionIdRegionId', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionAssociationPut - errors', () => {
      it('should have a regionAssociationPut function', (done) => {
        try {
          assert.equal(true, typeof a.regionAssociationPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePK', (done) => {
        try {
          a.regionAssociationPut(null, null, (data, error) => {
            try {
              const displayE = 'nePK is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-regionAssociationPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.regionAssociationPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-regionAssociationPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionDelete - errors', () => {
      it('should have a regionDelete function', (done) => {
        try {
          assert.equal(true, typeof a.regionDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionId', (done) => {
        try {
          a.regionDelete(null, (data, error) => {
            try {
              const displayE = 'regionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-regionDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionSingleGet - errors', () => {
      it('should have a regionSingleGet function', (done) => {
        try {
          assert.equal(true, typeof a.regionSingleGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionId', (done) => {
        try {
          a.regionSingleGet(null, (data, error) => {
            try {
              const displayE = 'regionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-regionSingleGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#regionPut - errors', () => {
      it('should have a regionPut function', (done) => {
        try {
          assert.equal(true, typeof a.regionPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing regionId', (done) => {
        try {
          a.regionPut(null, null, (data, error) => {
            try {
              const displayE = 'regionId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-regionPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.regionPut('fakeparam', null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-regionPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releases - errors', () => {
      it('should have a releases function', (done) => {
        try {
          assert.equal(true, typeof a.releases === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing filter', (done) => {
        try {
          a.releases(null, (data, error) => {
            try {
              const displayE = 'filter is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-releases', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#releaseNotifications - errors', () => {
      it('should have a releaseNotifications function', (done) => {
        try {
          assert.equal(true, typeof a.releaseNotifications === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#delayReleaseNotification - errors', () => {
      it('should have a delayReleaseNotification function', (done) => {
        try {
          assert.equal(true, typeof a.delayReleaseNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.delayReleaseNotification(null, null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-delayReleaseNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing delayRequest', (done) => {
        try {
          a.delayReleaseNotification('fakeparam', null, (data, error) => {
            try {
              const displayE = 'delayRequest is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-delayReleaseNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dismissReleaseNotification - errors', () => {
      it('should have a dismissReleaseNotification function', (done) => {
        try {
          assert.equal(true, typeof a.dismissReleaseNotification === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing version', (done) => {
        try {
          a.dismissReleaseNotification(null, (data, error) => {
            try {
              const displayE = 'version is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dismissReleaseNotification', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remoteLogReceiverGet - errors', () => {
      it('should have a remoteLogReceiverGet function', (done) => {
        try {
          assert.equal(true, typeof a.remoteLogReceiverGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remoteLogReceiverAdd - errors', () => {
      it('should have a remoteLogReceiverAdd function', (done) => {
        try {
          assert.equal(true, typeof a.remoteLogReceiverAdd === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.remoteLogReceiverAdd(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-remoteLogReceiverAdd', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remoteLogReceiverPut - errors', () => {
      it('should have a remoteLogReceiverPut function', (done) => {
        try {
          assert.equal(true, typeof a.remoteLogReceiverPut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing requestBody', (done) => {
        try {
          a.remoteLogReceiverPut(null, (data, error) => {
            try {
              const displayE = 'requestBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-remoteLogReceiverPut', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remoteLogReceiverDelete - errors', () => {
      it('should have a remoteLogReceiverDelete function', (done) => {
        try {
          assert.equal(true, typeof a.remoteLogReceiverDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing receiverId', (done) => {
        try {
          a.remoteLogReceiverDelete(null, (data, error) => {
            try {
              const displayE = 'receiverId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-remoteLogReceiverDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#remoteLogReceiverSubscribe - errors', () => {
      it('should have a remoteLogReceiverSubscribe function', (done) => {
        try {
          assert.equal(true, typeof a.remoteLogReceiverSubscribe === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing receiverId', (done) => {
        try {
          a.remoteLogReceiverSubscribe(null, null, (data, error) => {
            try {
              const displayE = 'receiverId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-remoteLogReceiverSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fetchingParam', (done) => {
        try {
          a.remoteLogReceiverSubscribe('fakeparam', null, (data, error) => {
            try {
              const displayE = 'fetchingParam is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-remoteLogReceiverSubscribe', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dns - errors', () => {
      it('should have a dns function', (done) => {
        try {
          assert.equal(true, typeof a.dns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.dns(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.dns('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restApiConfigGet - errors', () => {
      it('should have a restApiConfigGet function', (done) => {
        try {
          assert.equal(true, typeof a.restApiConfigGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restApiConfigPost - errors', () => {
      it('should have a restApiConfigPost function', (done) => {
        try {
          assert.equal(true, typeof a.restApiConfigPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing restApiConfigPost', (done) => {
        try {
          a.restApiConfigPost(null, (data, error) => {
            try {
              const displayE = 'restApiConfigPost is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-restApiConfigPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestRequestTimeStatsSummary - errors', () => {
      it('should have a getRestRequestTimeStatsSummary function', (done) => {
        try {
          assert.equal(true, typeof a.getRestRequestTimeStatsSummary === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getRestRequestTimeStatsSummary(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resource', (done) => {
        try {
          a.getRestRequestTimeStatsSummary('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portalWS', (done) => {
        try {
          a.getRestRequestTimeStatsSummary('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'portalWS is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing timedout', (done) => {
        try {
          a.getRestRequestTimeStatsSummary('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'timedout is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing from', (done) => {
        try {
          a.getRestRequestTimeStatsSummary('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'from is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing to', (done) => {
        try {
          a.getRestRequestTimeStatsSummary('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'to is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsSummary', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRestRequestTimeStatsDetails - errors', () => {
      it('should have a getRestRequestTimeStatsDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getRestRequestTimeStatsDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getRestRequestTimeStatsDetails(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resource', (done) => {
        try {
          a.getRestRequestTimeStatsDetails('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'resource is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing portalWS', (done) => {
        try {
          a.getRestRequestTimeStatsDetails('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'portalWS is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing method', (done) => {
        try {
          a.getRestRequestTimeStatsDetails('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'method is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing from', (done) => {
        try {
          a.getRestRequestTimeStatsDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'from is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing to', (done) => {
        try {
          a.getRestRequestTimeStatsDetails('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'to is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getRestRequestTimeStatsDetails', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#applyRmaWizard - errors', () => {
      it('should have a applyRmaWizard function', (done) => {
        try {
          assert.equal(true, typeof a.applyRmaWizard === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applyRmaWizard', (done) => {
        try {
          a.applyRmaWizard(null, null, (data, error) => {
            try {
              const displayE = 'applyRmaWizard is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyRmaWizard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.applyRmaWizard('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-applyRmaWizard', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#samap - errors', () => {
      it('should have a samap function', (done) => {
        try {
          assert.equal(true, typeof a.samap === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.samap(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-samap', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityMaps - errors', () => {
      it('should have a securityMaps function', (done) => {
        try {
          assert.equal(true, typeof a.securityMaps === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.securityMaps(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityMaps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.securityMaps('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityMaps', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSessionActiveSessions - errors', () => {
      it('should have a getSessionActiveSessions function', (done) => {
        try {
          assert.equal(true, typeof a.getSessionActiveSessions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#snmpGet - errors', () => {
      it('should have a snmpGet function', (done) => {
        try {
          assert.equal(true, typeof a.snmpGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.snmpGet(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-snmpGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.snmpGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-snmpGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalAccountKeyChangeCount - errors', () => {
      it('should have a spPortalAccountKeyChangeCount function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalAccountKeyChangeCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalAccountKeyChangeStatus - errors', () => {
      it('should have a spPortalAccountKeyChangeStatus function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalAccountKeyChangeStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalAccountKeyGeneratePut - errors', () => {
      it('should have a spPortalAccountKeyGeneratePut function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalAccountKeyGeneratePut === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalECSPLicenseAssign - errors', () => {
      it('should have a spPortalECSPLicenseAssign function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalECSPLicenseAssign === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spPortalECSPLicenseAssign', (done) => {
        try {
          a.spPortalECSPLicenseAssign(null, (data, error) => {
            try {
              const displayE = 'spPortalECSPLicenseAssign is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-spPortalECSPLicenseAssign', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalECSPLicensesGet - errors', () => {
      it('should have a spPortalECSPLicensesGet function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalECSPLicensesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalECSPLicenseUnassign - errors', () => {
      it('should have a spPortalECSPLicenseUnassign function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalECSPLicenseUnassign === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spPortalECSPLicenseUnassign', (done) => {
        try {
          a.spPortalECSPLicenseUnassign(null, (data, error) => {
            try {
              const displayE = 'spPortalECSPLicenseUnassign is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-spPortalECSPLicenseUnassign', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalAccountLicenseFeatureGet - errors', () => {
      it('should have a spPortalAccountLicenseFeatureGet function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalAccountLicenseFeatureGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalAccountLicenseTypeGet - errors', () => {
      it('should have a spPortalAccountLicenseTypeGet function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalAccountLicenseTypeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalDeleteOldKey - errors', () => {
      it('should have a spPortalDeleteOldKey function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalDeleteOldKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#checkApplianceReachabilityUsingWSGet - errors', () => {
      it('should have a checkApplianceReachabilityUsingWSGet function', (done) => {
        try {
          assert.equal(true, typeof a.checkApplianceReachabilityUsingWSGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.checkApplianceReachabilityUsingWSGet(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-checkApplianceReachabilityUsingWSGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appGroupTmpGet - errors', () => {
      it('should have a appGroupTmpGet function', (done) => {
        try {
          assert.equal(true, typeof a.appGroupTmpGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appGroupTmpHashCodeGet - errors', () => {
      it('should have a appGroupTmpHashCodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.appGroupTmpHashCodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#compoundTmpGet - errors', () => {
      it('should have a compoundTmpGet function', (done) => {
        try {
          assert.equal(true, typeof a.compoundTmpGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#compoundTmpHashCodeGet - errors', () => {
      it('should have a compoundTmpHashCodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.compoundTmpHashCodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalConfigGet - errors', () => {
      it('should have a spPortalConfigGet function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalConfigGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalConfigPost - errors', () => {
      it('should have a spPortalConfigPost function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalConfigPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spPortalConfig', (done) => {
        try {
          a.spPortalConfigPost(null, (data, error) => {
            try {
              const displayE = 'spPortalConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-spPortalConfigPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connectivityGet - errors', () => {
      it('should have a connectivityGet function', (done) => {
        try {
          assert.equal(true, typeof a.connectivityGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalCreateCasePost - errors', () => {
      it('should have a spPortalCreateCasePost function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalCreateCasePost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing spPortalCreateCaseWithPortal', (done) => {
        try {
          a.spPortalCreateCasePost(null, (data, error) => {
            try {
              const displayE = 'spPortalCreateCaseWithPortal is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-spPortalCreateCasePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsTmpGet - errors', () => {
      it('should have a dnsTmpGet function', (done) => {
        try {
          assert.equal(true, typeof a.dnsTmpGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dnsTmpHashCodeGet - errors', () => {
      it('should have a dnsTmpHashCodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.dnsTmpHashCodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#geoLocationPost - errors', () => {
      it('should have a geoLocationPost function', (done) => {
        try {
          assert.equal(true, typeof a.geoLocationPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing geoLocationPostBody', (done) => {
        try {
          a.geoLocationPost(null, (data, error) => {
            try {
              const displayE = 'geoLocationPostBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-geoLocationPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#geoLocationGet - errors', () => {
      it('should have a geoLocationGet function', (done) => {
        try {
          assert.equal(true, typeof a.geoLocationGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.geoLocationGet(null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-geoLocationGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipIntelligenceTmpGet - errors', () => {
      it('should have a ipIntelligenceTmpGet function', (done) => {
        try {
          assert.equal(true, typeof a.ipIntelligenceTmpGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing start', (done) => {
        try {
          a.ipIntelligenceTmpGet(null, null, (data, error) => {
            try {
              const displayE = 'start is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-ipIntelligenceTmpGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.ipIntelligenceTmpGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-ipIntelligenceTmpGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipIntelligenceTmpTimeGet - errors', () => {
      it('should have a ipIntelligenceTmpTimeGet function', (done) => {
        try {
          assert.equal(true, typeof a.ipIntelligenceTmpTimeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSpPortalInternetDbIpIntelligenceSearch - errors', () => {
      it('should have a getSpPortalInternetDbIpIntelligenceSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getSpPortalInternetDbIpIntelligenceSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipIntelligenceTmpTotal - errors', () => {
      it('should have a ipIntelligenceTmpTotal function', (done) => {
        try {
          assert.equal(true, typeof a.ipIntelligenceTmpTotal === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceIdToSaasIdGet - errors', () => {
      it('should have a serviceIdToSaasIdGet function', (done) => {
        try {
          assert.equal(true, typeof a.serviceIdToSaasIdGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceIdToSaasIdCountGet - errors', () => {
      it('should have a serviceIdToSaasIdCountGet function', (done) => {
        try {
          assert.equal(true, typeof a.serviceIdToSaasIdCountGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceIdToSaasIdCountriesGet - errors', () => {
      it('should have a serviceIdToSaasIdCountriesGet function', (done) => {
        try {
          assert.equal(true, typeof a.serviceIdToSaasIdCountriesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#serviceIdToSaasIdSaasAppGet - errors', () => {
      it('should have a serviceIdToSaasIdSaasAppGet function', (done) => {
        try {
          assert.equal(true, typeof a.serviceIdToSaasIdSaasAppGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#ipProtocolNumbersGet - errors', () => {
      it('should have a ipProtocolNumbersGet function', (done) => {
        try {
          assert.equal(true, typeof a.ipProtocolNumbersGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meterFlowTmpGet - errors', () => {
      it('should have a meterFlowTmpGet function', (done) => {
        try {
          assert.equal(true, typeof a.meterFlowTmpGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#meterFlowTmpHashCodeGet - errors', () => {
      it('should have a meterFlowTmpHashCodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.meterFlowTmpHashCodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProtocolTmpGet - errors', () => {
      it('should have a portProtocolTmpGet function', (done) => {
        try {
          assert.equal(true, typeof a.portProtocolTmpGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portProtocolTmpHashCodeGet - errors', () => {
      it('should have a portProtocolTmpHashCodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.portProtocolTmpHashCodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsPortalRegistrationGet - errors', () => {
      it('should have a gmsPortalRegistrationGet function', (done) => {
        try {
          assert.equal(true, typeof a.gmsPortalRegistrationGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#gmsPortalRegistrationPost - errors', () => {
      it('should have a gmsPortalRegistrationPost function', (done) => {
        try {
          assert.equal(true, typeof a.gmsPortalRegistrationPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saasTmpGet - errors', () => {
      it('should have a saasTmpGet function', (done) => {
        try {
          assert.equal(true, typeof a.saasTmpGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#saasTmpHashCodeGet - errors', () => {
      it('should have a saasTmpHashCodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.saasTmpHashCodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#spPortalStatusGet - errors', () => {
      it('should have a spPortalStatusGet function', (done) => {
        try {
          assert.equal(true, typeof a.spPortalStatusGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tcpUdpPortsGet - errors', () => {
      it('should have a tcpUdpPortsGet function', (done) => {
        try {
          assert.equal(true, typeof a.tcpUdpPortsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#topSitesGet - errors', () => {
      it('should have a topSitesGet function', (done) => {
        try {
          assert.equal(true, typeof a.topSitesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tbTmpGet - errors', () => {
      it('should have a tbTmpGet function', (done) => {
        try {
          assert.equal(true, typeof a.tbTmpGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tbTmpHashCodeGet - errors', () => {
      it('should have a tbTmpHashCodeGet function', (done) => {
        try {
          assert.equal(true, typeof a.tbTmpHashCodeGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sslCertsGet - errors', () => {
      it('should have a sslCertsGet function', (done) => {
        try {
          assert.equal(true, typeof a.sslCertsGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.sslCertsGet(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslCertsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.sslCertsGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslCertsGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sslCACertGetTextPost - errors', () => {
      it('should have a sslCACertGetTextPost function', (done) => {
        try {
          assert.equal(true, typeof a.sslCACertGetTextPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sslCACertsFile', (done) => {
        try {
          a.sslCACertGetTextPost(null, (data, error) => {
            try {
              const displayE = 'sslCACertsFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslCACertGetTextPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sslCACertValidation - errors', () => {
      it('should have a sslCACertValidation function', (done) => {
        try {
          assert.equal(true, typeof a.sslCACertValidation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sslCACertValidation', (done) => {
        try {
          a.sslCACertValidation(null, (data, error) => {
            try {
              const displayE = 'sslCACertValidation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslCACertValidation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sslCACerts - errors', () => {
      it('should have a sslCACerts function', (done) => {
        try {
          assert.equal(true, typeof a.sslCACerts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.sslCACerts(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslCACerts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.sslCACerts('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslCACerts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sslCertGetInfoPost - errors', () => {
      it('should have a sslCertGetInfoPost function', (done) => {
        try {
          assert.equal(true, typeof a.sslCertGetInfoPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sslCertGetInfoPost', (done) => {
        try {
          a.sslCertGetInfoPost(null, (data, error) => {
            try {
              const displayE = 'sslCertGetInfoPost is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslCertGetInfoPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sslCertGetTextPost - errors', () => {
      it('should have a sslCertGetTextPost function', (done) => {
        try {
          assert.equal(true, typeof a.sslCertGetTextPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sslCertGetTextPost', (done) => {
        try {
          a.sslCertGetTextPost(null, (data, error) => {
            try {
              const displayE = 'sslCertGetTextPost is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslCertGetTextPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sslSubstituteCertValidation - errors', () => {
      it('should have a sslSubstituteCertValidation function', (done) => {
        try {
          assert.equal(true, typeof a.sslSubstituteCertValidation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing sslSubstituteCertValidation', (done) => {
        try {
          a.sslSubstituteCertValidation(null, (data, error) => {
            try {
              const displayE = 'sslSubstituteCertValidation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslSubstituteCertValidation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#sslSubstituteCertGet - errors', () => {
      it('should have a sslSubstituteCertGet function', (done) => {
        try {
          assert.equal(true, typeof a.sslSubstituteCertGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.sslSubstituteCertGet(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslSubstituteCertGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.sslSubstituteCertGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-sslSubstituteCertGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateTunnel - errors', () => {
      it('should have a statsAggregateTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.statsAggregateTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsAggregateTunnel(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsAggregateTunnel('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsAggregateTunnel('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsAggregateAppliance - errors', () => {
      it('should have a postStatsAggregateAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsAggregateAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsAggregateAppliance(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsAggregateAppliance('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsAggregateAppliance('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsAggregateAppliance('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateApplianceNePk - errors', () => {
      it('should have a getStatsAggregateApplianceNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateApplianceNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateApplianceNePk(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplianceNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateApplianceNePk('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplianceNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateApplianceNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplianceNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateApplianceNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplianceNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateApplication - errors', () => {
      it('should have a statsAggregateApplication function', (done) => {
        try {
          assert.equal(true, typeof a.statsAggregateApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsAggregateApplication(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsAggregateApplication('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsAggregateApplication('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsAggregateApplication - errors', () => {
      it('should have a postStatsAggregateApplication function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsAggregateApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsAggregateApplication(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsAggregateApplication('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsAggregateApplication('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsAggregateApplication('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateApplicationNePk - errors', () => {
      it('should have a getStatsAggregateApplicationNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateApplicationNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateApplicationNePk(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplicationNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateApplicationNePk('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplicationNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateApplicationNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplicationNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateApplicationNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplicationNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateApplication2 - errors', () => {
      it('should have a getStatsAggregateApplication2 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateApplication2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateApplication2(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateApplication2('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsAggregateApplication2 - errors', () => {
      it('should have a postStatsAggregateApplication2 function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsAggregateApplication2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsAggregateApplication2(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsAggregateApplication2('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsAggregateApplication2('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateApplication2NePk - errors', () => {
      it('should have a getStatsAggregateApplication2NePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateApplication2NePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateApplication2NePk(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplication2NePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateApplication2NePk('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplication2NePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateApplication2NePk('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateApplication2NePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#boostAggregateStats - errors', () => {
      it('should have a boostAggregateStats function', (done) => {
        try {
          assert.equal(true, typeof a.boostAggregateStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.boostAggregateStats(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-boostAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.boostAggregateStats('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-boostAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.boostAggregateStats('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-boostAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.boostAggregateStats('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-boostAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateBoostNePk - errors', () => {
      it('should have a getStatsAggregateBoostNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateBoostNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateBoostNePk(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateBoostNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateBoostNePk('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateBoostNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateBoostNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateBoostNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateBoostNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateBoostNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#dNSStatsAggregate - errors', () => {
      it('should have a dNSStatsAggregate function', (done) => {
        try {
          assert.equal(true, typeof a.dNSStatsAggregate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.dNSStatsAggregate(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dNSStatsAggregate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.dNSStatsAggregate('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-dNSStatsAggregate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateDns - errors', () => {
      it('should have a statsAggregateDns function', (done) => {
        try {
          assert.equal(true, typeof a.statsAggregateDns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.statsAggregateDns(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateDns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsAggregateDns('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateDns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsAggregateDns('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateDns', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateDnsNePk - errors', () => {
      it('should have a getStatsAggregateDnsNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateDnsNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateDnsNePk(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDnsNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateDnsNePk('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDnsNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateDnsNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDnsNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateDrcTunnel - errors', () => {
      it('should have a statsAggregateDrcTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.statsAggregateDrcTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsAggregateDrcTunnel(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateDrcTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsAggregateDrcTunnel('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateDrcTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsAggregateDrcTunnel('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateDrcTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsDrcAggregateTunnel - errors', () => {
      it('should have a statsDrcAggregateTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.statsDrcAggregateTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.statsDrcAggregateTunnel(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsDrcAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsDrcAggregateTunnel('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsDrcAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsDrcAggregateTunnel('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsDrcAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsDrcAggregateTunnel('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsDrcAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateDrcNePk - errors', () => {
      it('should have a getStatsAggregateDrcNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateDrcNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateDrcNePk(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDrcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateDrcNePk('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDrcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateDrcNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDrcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateDrcNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDrcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateDscp - errors', () => {
      it('should have a statsAggregateDscp function', (done) => {
        try {
          assert.equal(true, typeof a.statsAggregateDscp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsAggregateDscp(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsAggregateDscp('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsAggregateDscp('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsAggregateDscp - errors', () => {
      it('should have a postStatsAggregateDscp function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsAggregateDscp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsAggregateDscp(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsAggregateDscp('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsAggregateDscp('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsAggregateDscp('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateDscpNePk - errors', () => {
      it('should have a getStatsAggregateDscpNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateDscpNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateDscpNePk(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateDscpNePk('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateDscpNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.getStatsAggregateDscpNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateDscpNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateFlow - errors', () => {
      it('should have a statsAggregateFlow function', (done) => {
        try {
          assert.equal(true, typeof a.statsAggregateFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsAggregateFlow(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsAggregateFlow('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsAggregateFlow('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsAggregateFlow - errors', () => {
      it('should have a postStatsAggregateFlow function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsAggregateFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsAggregateFlow(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsAggregateFlow('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsAggregateFlow('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsAggregateFlow('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getActiveFlowCounts - errors', () => {
      it('should have a getActiveFlowCounts function', (done) => {
        try {
          assert.equal(true, typeof a.getActiveFlowCounts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.getActiveFlowCounts(null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getActiveFlowCounts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateFlowNePk - errors', () => {
      it('should have a getStatsAggregateFlowNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateFlowNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateFlowNePk(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateFlowNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateFlowNePk('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateFlowNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateFlowNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateFlowNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateFlowNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateFlowNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interfaceAggregateStats - errors', () => {
      it('should have a interfaceAggregateStats function', (done) => {
        try {
          assert.equal(true, typeof a.interfaceAggregateStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.interfaceAggregateStats(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.interfaceAggregateStats('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.interfaceAggregateStats('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.interfaceAggregateStats('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsAggregateInterface - errors', () => {
      it('should have a postStatsAggregateInterface function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsAggregateInterface === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsAggregateInterface(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsAggregateInterface('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsAggregateInterface('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsAggregateInterface('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.postStatsAggregateInterface('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateInterface', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interfaceOverlayTransportAggregateStats - errors', () => {
      it('should have a interfaceOverlayTransportAggregateStats function', (done) => {
        try {
          assert.equal(true, typeof a.interfaceOverlayTransportAggregateStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.interfaceOverlayTransportAggregateStats(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceOverlayTransportAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.interfaceOverlayTransportAggregateStats('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceOverlayTransportAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.interfaceOverlayTransportAggregateStats('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceOverlayTransportAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.interfaceOverlayTransportAggregateStats('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceOverlayTransportAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#jitterStatsAggregate - errors', () => {
      it('should have a jitterStatsAggregate function', (done) => {
        try {
          assert.equal(true, typeof a.jitterStatsAggregate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.jitterStatsAggregate(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-jitterStatsAggregate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.jitterStatsAggregate('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-jitterStatsAggregate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.jitterStatsAggregate('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-jitterStatsAggregate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateJitter - errors', () => {
      it('should have a statsAggregateJitter function', (done) => {
        try {
          assert.equal(true, typeof a.statsAggregateJitter === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.statsAggregateJitter(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateJitter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsAggregateJitter('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateJitter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsAggregateJitter('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateJitter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsAggregateJitter('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateJitter', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateJitterNePk - errors', () => {
      it('should have a getStatsAggregateJitterNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateJitterNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateJitterNePk(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateJitterNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateJitterNePk('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateJitterNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateJitterNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateJitterNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateJitterNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateJitterNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mosAggregateStats - errors', () => {
      it('should have a mosAggregateStats function', (done) => {
        try {
          assert.equal(true, typeof a.mosAggregateStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.mosAggregateStats(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mosAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.mosAggregateStats('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mosAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.mosAggregateStats('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mosAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.mosAggregateStats('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mosAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateMosNePk - errors', () => {
      it('should have a getStatsAggregateMosNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateMosNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateMosNePk(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateMosNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateMosNePk('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateMosNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateMosNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateMosNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateMosNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateMosNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#portStatsAggregate - errors', () => {
      it('should have a portStatsAggregate function', (done) => {
        try {
          assert.equal(true, typeof a.portStatsAggregate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.portStatsAggregate(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portStatsAggregate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.portStatsAggregate('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-portStatsAggregate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregatePorts - errors', () => {
      it('should have a statsAggregatePorts function', (done) => {
        try {
          assert.equal(true, typeof a.statsAggregatePorts === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.statsAggregatePorts(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregatePorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsAggregatePorts('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregatePorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsAggregatePorts('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregatePorts', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregatePortsNePk - errors', () => {
      it('should have a getStatsAggregatePortsNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregatePortsNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregatePortsNePk(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregatePortsNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregatePortsNePk('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregatePortsNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityPolicyAggregateStats - errors', () => {
      it('should have a securityPolicyAggregateStats function', (done) => {
        try {
          assert.equal(true, typeof a.securityPolicyAggregateStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.securityPolicyAggregateStats(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityPolicyAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.securityPolicyAggregateStats('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityPolicyAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.securityPolicyAggregateStats('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityPolicyAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.securityPolicyAggregateStats('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityPolicyAggregateStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateSecurityPolicyNePk - errors', () => {
      it('should have a getStatsAggregateSecurityPolicyNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateSecurityPolicyNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateSecurityPolicyNePk(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateSecurityPolicyNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateSecurityPolicyNePk('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateSecurityPolicyNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateSecurityPolicyNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateSecurityPolicyNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateSecurityPolicyNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateSecurityPolicyNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromZone', (done) => {
        try {
          a.getStatsAggregateSecurityPolicyNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'fromZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateSecurityPolicyNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing toZone', (done) => {
        try {
          a.getStatsAggregateSecurityPolicyNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'toZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateSecurityPolicyNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#topTalkersStatsAggregate - errors', () => {
      it('should have a topTalkersStatsAggregate function', (done) => {
        try {
          assert.equal(true, typeof a.topTalkersStatsAggregate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.topTalkersStatsAggregate(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-topTalkersStatsAggregate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.topTalkersStatsAggregate('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-topTalkersStatsAggregate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateTopTalkers - errors', () => {
      it('should have a statsAggregateTopTalkers function', (done) => {
        try {
          assert.equal(true, typeof a.statsAggregateTopTalkers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.statsAggregateTopTalkers(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateTopTalkers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsAggregateTopTalkers('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateTopTalkers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsAggregateTopTalkers('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateTopTalkers', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTopTalkersSplitNePk - errors', () => {
      it('should have a getStatsAggregateTopTalkersSplitNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateTopTalkersSplitNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateTopTalkersSplitNePk(null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTopTalkersSplitNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateTopTalkersSplitNePk('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTopTalkersSplitNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateTopTalkersSplitNePk('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTopTalkersSplitNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTopTalkersNePk - errors', () => {
      it('should have a getStatsAggregateTopTalkersNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateTopTalkersNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateTopTalkersNePk(null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTopTalkersNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateTopTalkersNePk('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTopTalkersNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateTopTalkersNePk('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTopTalkersNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#trafficBehavior - errors', () => {
      it('should have a trafficBehavior function', (done) => {
        try {
          assert.equal(true, typeof a.trafficBehavior === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.trafficBehavior(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-trafficBehavior', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.trafficBehavior('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-trafficBehavior', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsAggregateTrafficBehavior - errors', () => {
      it('should have a postStatsAggregateTrafficBehavior function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsAggregateTrafficBehavior === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsAggregateTrafficBehavior(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTrafficBehavior', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsAggregateTrafficBehavior('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTrafficBehavior', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsAggregateTrafficBehavior('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTrafficBehavior', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTrafficBehaviorNePk - errors', () => {
      it('should have a getStatsAggregateTrafficBehaviorNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateTrafficBehaviorNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateTrafficBehaviorNePk(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTrafficBehaviorNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateTrafficBehaviorNePk('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTrafficBehaviorNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateTrafficBehaviorNePk('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTrafficBehaviorNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsAggregateTrafficClass - errors', () => {
      it('should have a statsAggregateTrafficClass function', (done) => {
        try {
          assert.equal(true, typeof a.statsAggregateTrafficClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsAggregateTrafficClass(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsAggregateTrafficClass('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsAggregateTrafficClass('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsAggregateTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsAggregateTrafficClass - errors', () => {
      it('should have a postStatsAggregateTrafficClass function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsAggregateTrafficClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsAggregateTrafficClass(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsAggregateTrafficClass('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsAggregateTrafficClass('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsAggregateTrafficClass('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTrafficClassNePk - errors', () => {
      it('should have a getStatsAggregateTrafficClassNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateTrafficClassNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateTrafficClassNePk(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTrafficClassNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateTrafficClassNePk('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTrafficClassNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateTrafficClassNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTrafficClassNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateTrafficClassNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTrafficClassNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTunnel - errors', () => {
      it('should have a getStatsAggregateTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateTunnel(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateTunnel('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateTunnel('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsAggregateTunnel - errors', () => {
      it('should have a postStatsAggregateTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsAggregateTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsAggregateTunnel(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsAggregateTunnel('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsAggregateTunnel('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsAggregateTunnel('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsAggregateTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsAggregateTunnelNePk - errors', () => {
      it('should have a getStatsAggregateTunnelNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsAggregateTunnelNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsAggregateTunnelNePk(null, null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTunnelNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsAggregateTunnelNePk('fakeparam', null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTunnelNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsAggregateTunnelNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTunnelNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsAggregateTunnelNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsAggregateTunnelNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsDnsInfo - errors', () => {
      it('should have a statsDnsInfo function', (done) => {
        try {
          assert.equal(true, typeof a.statsDnsInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ip', (done) => {
        try {
          a.statsDnsInfo(null, (data, error) => {
            try {
              const displayE = 'ip is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsDnsInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesAppliance - errors', () => {
      it('should have a statsTimeseriesAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.statsTimeseriesAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsTimeseriesAppliance(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsTimeseriesAppliance('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsTimeseriesAppliance('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.statsTimeseriesAppliance('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsTimeseriesAppliance - errors', () => {
      it('should have a postStatsTimeseriesAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsTimeseriesAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsTimeseriesAppliance(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsTimeseriesAppliance('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsTimeseriesAppliance('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsTimeseriesAppliance('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.postStatsTimeseriesAppliance('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesApplianceNePk - errors', () => {
      it('should have a getStatsTimeseriesApplianceNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsTimeseriesApplianceNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsTimeseriesApplianceNePk(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplianceNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsTimeseriesApplianceNePk('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplianceNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsTimeseriesApplianceNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplianceNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsTimeseriesApplianceNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplianceNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.getStatsTimeseriesApplianceNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplianceNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsOfApplianceProcessState - errors', () => {
      it('should have a statsOfApplianceProcessState function', (done) => {
        try {
          assert.equal(true, typeof a.statsOfApplianceProcessState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.statsOfApplianceProcessState(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsOfApplianceProcessState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesApplication - errors', () => {
      it('should have a statsTimeseriesApplication function', (done) => {
        try {
          assert.equal(true, typeof a.statsTimeseriesApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsTimeseriesApplication(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsTimeseriesApplication('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsTimeseriesApplication('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.statsTimeseriesApplication('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing application', (done) => {
        try {
          a.statsTimeseriesApplication('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'application is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsTimeseriesApplication - errors', () => {
      it('should have a postStatsTimeseriesApplication function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsTimeseriesApplication === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsTimeseriesApplication(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsTimeseriesApplication('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsTimeseriesApplication('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsTimeseriesApplication('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.postStatsTimeseriesApplication('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing application', (done) => {
        try {
          a.postStatsTimeseriesApplication('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'application is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesApplication', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesApplicationNePk - errors', () => {
      it('should have a getStatsTimeseriesApplicationNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsTimeseriesApplicationNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsTimeseriesApplicationNePk(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplicationNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsTimeseriesApplicationNePk('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplicationNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsTimeseriesApplicationNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplicationNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsTimeseriesApplicationNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplicationNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.getStatsTimeseriesApplicationNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplicationNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing application', (done) => {
        try {
          a.getStatsTimeseriesApplicationNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'application is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplicationNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesApplication2 - errors', () => {
      it('should have a getStatsTimeseriesApplication2 function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsTimeseriesApplication2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsTimeseriesApplication2(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsTimeseriesApplication2('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing application', (done) => {
        try {
          a.getStatsTimeseriesApplication2('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'application is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsTimeseriesApplication2 - errors', () => {
      it('should have a postStatsTimeseriesApplication2 function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsTimeseriesApplication2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsTimeseriesApplication2(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsTimeseriesApplication2('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsTimeseriesApplication2('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing application', (done) => {
        try {
          a.postStatsTimeseriesApplication2('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'application is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesApplication2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesApplication2NePk - errors', () => {
      it('should have a getStatsTimeseriesApplication2NePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsTimeseriesApplication2NePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsTimeseriesApplication2NePk(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplication2NePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsTimeseriesApplication2NePk('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplication2NePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsTimeseriesApplication2NePk('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplication2NePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing application', (done) => {
        try {
          a.getStatsTimeseriesApplication2NePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'application is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesApplication2NePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#boostTimeSeriesStats - errors', () => {
      it('should have a boostTimeSeriesStats function', (done) => {
        try {
          assert.equal(true, typeof a.boostTimeSeriesStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.boostTimeSeriesStats(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-boostTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.boostTimeSeriesStats('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-boostTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.boostTimeSeriesStats('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-boostTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.boostTimeSeriesStats('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-boostTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesDrcTunnel - errors', () => {
      it('should have a statsTimeseriesDrcTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.statsTimeseriesDrcTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsTimeseriesDrcTunnel(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesDrcTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsTimeseriesDrcTunnel('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesDrcTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsTimeseriesDrcTunnel('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesDrcTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsTimeseriesDrc - errors', () => {
      it('should have a postStatsTimeseriesDrc function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsTimeseriesDrc === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsTimeseriesDrc(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesDrc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsTimeseriesDrc('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesDrc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsTimeseriesDrc('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesDrc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsTimeseriesDrc('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesDrc', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesDrcNePk - errors', () => {
      it('should have a getStatsTimeseriesDrcNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsTimeseriesDrcNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsTimeseriesDrcNePk(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDrcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsTimeseriesDrcNePk('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDrcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsTimeseriesDrcNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDrcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsTimeseriesDrcNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDrcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelName', (done) => {
        try {
          a.getStatsTimeseriesDrcNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tunnelName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDrcNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesDscp - errors', () => {
      it('should have a statsTimeseriesDscp function', (done) => {
        try {
          assert.equal(true, typeof a.statsTimeseriesDscp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsTimeseriesDscp(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsTimeseriesDscp('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsTimeseriesDscp('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.statsTimeseriesDscp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dscp', (done) => {
        try {
          a.statsTimeseriesDscp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'dscp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsTimeseriesDscp - errors', () => {
      it('should have a postStatsTimeseriesDscp function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsTimeseriesDscp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsTimeseriesDscp(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsTimeseriesDscp('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsTimeseriesDscp('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsTimeseriesDscp('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.postStatsTimeseriesDscp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dscp', (done) => {
        try {
          a.postStatsTimeseriesDscp('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'dscp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesDscp', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesDscpNePk - errors', () => {
      it('should have a getStatsTimeseriesDscpNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsTimeseriesDscpNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsTimeseriesDscpNePk(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsTimeseriesDscpNePk('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsTimeseriesDscpNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsTimeseriesDscpNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.getStatsTimeseriesDscpNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing dscp', (done) => {
        try {
          a.getStatsTimeseriesDscpNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'dscp is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesDscpNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesFlow - errors', () => {
      it('should have a statsTimeseriesFlow function', (done) => {
        try {
          assert.equal(true, typeof a.statsTimeseriesFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsTimeseriesFlow(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsTimeseriesFlow('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsTimeseriesFlow('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.statsTimeseriesFlow('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowType', (done) => {
        try {
          a.statsTimeseriesFlow('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'flowType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsTimeseriesFlow - errors', () => {
      it('should have a postStatsTimeseriesFlow function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsTimeseriesFlow === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsTimeseriesFlow(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsTimeseriesFlow('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsTimeseriesFlow('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsTimeseriesFlow('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.postStatsTimeseriesFlow('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowType', (done) => {
        try {
          a.postStatsTimeseriesFlow('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'flowType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesFlow', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesFlowNePk - errors', () => {
      it('should have a getStatsTimeseriesFlowNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsTimeseriesFlowNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsTimeseriesFlowNePk(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesFlowNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsTimeseriesFlowNePk('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesFlowNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsTimeseriesFlowNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesFlowNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsTimeseriesFlowNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesFlowNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.getStatsTimeseriesFlowNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesFlowNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing flowType', (done) => {
        try {
          a.getStatsTimeseriesFlowNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'flowType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesFlowNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interfaceTimeseriesStats - errors', () => {
      it('should have a interfaceTimeseriesStats function', (done) => {
        try {
          assert.equal(true, typeof a.interfaceTimeseriesStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.interfaceTimeseriesStats(null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.interfaceTimeseriesStats('fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.interfaceTimeseriesStats('fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.interfaceTimeseriesStats('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.interfaceTimeseriesStats('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#interfaceOverlayTransportTimeseriesStats - errors', () => {
      it('should have a interfaceOverlayTransportTimeseriesStats function', (done) => {
        try {
          assert.equal(true, typeof a.interfaceOverlayTransportTimeseriesStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.interfaceOverlayTransportTimeseriesStats(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceOverlayTransportTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.interfaceOverlayTransportTimeseriesStats('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceOverlayTransportTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.interfaceOverlayTransportTimeseriesStats('fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceOverlayTransportTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.interfaceOverlayTransportTimeseriesStats('fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-interfaceOverlayTransportTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesInternalDrops - errors', () => {
      it('should have a statsTimeseriesInternalDrops function', (done) => {
        try {
          assert.equal(true, typeof a.statsTimeseriesInternalDrops === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.statsTimeseriesInternalDrops(null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesInternalDrops', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsTimeseriesInternalDrops('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesInternalDrops', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsTimeseriesInternalDrops('fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesInternalDrops', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsTimeseriesInternalDrops('fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesInternalDrops', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesTunnel - errors', () => {
      it('should have a statsTimeseriesTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.statsTimeseriesTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsTimeseriesTunnel(null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsTimeseriesTunnel('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing key', (done) => {
        try {
          a.statsTimeseriesTunnel('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'key is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#mOSTimeSeriesStats - errors', () => {
      it('should have a mOSTimeSeriesStats function', (done) => {
        try {
          assert.equal(true, typeof a.mOSTimeSeriesStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.mOSTimeSeriesStats(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mOSTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.mOSTimeSeriesStats('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mOSTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.mOSTimeSeriesStats('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mOSTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.mOSTimeSeriesStats('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mOSTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnel', (done) => {
        try {
          a.mOSTimeSeriesStats('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'tunnel is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-mOSTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#securityPolicyTimeSeriesStats - errors', () => {
      it('should have a securityPolicyTimeSeriesStats function', (done) => {
        try {
          assert.equal(true, typeof a.securityPolicyTimeSeriesStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.securityPolicyTimeSeriesStats(null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityPolicyTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.securityPolicyTimeSeriesStats('fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityPolicyTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.securityPolicyTimeSeriesStats('fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityPolicyTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.securityPolicyTimeSeriesStats('fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityPolicyTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fromZone', (done) => {
        try {
          a.securityPolicyTimeSeriesStats('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'fromZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityPolicyTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing toZone', (done) => {
        try {
          a.securityPolicyTimeSeriesStats('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'toZone is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-securityPolicyTimeSeriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#shaperTimeseriesStats - errors', () => {
      it('should have a shaperTimeseriesStats function', (done) => {
        try {
          assert.equal(true, typeof a.shaperTimeseriesStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.shaperTimeseriesStats('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-shaperTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.shaperTimeseriesStats('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-shaperTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.shaperTimeseriesStats('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-shaperTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficClass', (done) => {
        try {
          a.shaperTimeseriesStats('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficClass is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-shaperTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing direction', (done) => {
        try {
          a.shaperTimeseriesStats('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'direction is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-shaperTimeseriesStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsTimeseriesShaper - errors', () => {
      it('should have a postStatsTimeseriesShaper function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsTimeseriesShaper === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsTimeseriesShaper(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesShaper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsTimeseriesShaper('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesShaper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsTimeseriesShaper('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesShaper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsTimeseriesShaper('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesShaper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficClass', (done) => {
        try {
          a.postStatsTimeseriesShaper('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficClass is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesShaper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing direction', (done) => {
        try {
          a.postStatsTimeseriesShaper('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'direction is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesShaper', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#statsTimeseriesTrafficClass - errors', () => {
      it('should have a statsTimeseriesTrafficClass function', (done) => {
        try {
          assert.equal(true, typeof a.statsTimeseriesTrafficClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.statsTimeseriesTrafficClass(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.statsTimeseriesTrafficClass('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.statsTimeseriesTrafficClass('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.statsTimeseriesTrafficClass('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficClass', (done) => {
        try {
          a.statsTimeseriesTrafficClass('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficClass is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-statsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsTimeseriesTrafficClass - errors', () => {
      it('should have a postStatsTimeseriesTrafficClass function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsTimeseriesTrafficClass === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsTimeseriesTrafficClass(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsTimeseriesTrafficClass('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsTimeseriesTrafficClass('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsTimeseriesTrafficClass('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.postStatsTimeseriesTrafficClass('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficClass', (done) => {
        try {
          a.postStatsTimeseriesTrafficClass('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficClass is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesTrafficClass', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesTrafficClassNePk - errors', () => {
      it('should have a getStatsTimeseriesTrafficClassNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsTimeseriesTrafficClassNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsTimeseriesTrafficClassNePk(null, null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTrafficClassNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsTimeseriesTrafficClassNePk('fakeparam', null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTrafficClassNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsTimeseriesTrafficClassNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTrafficClassNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsTimeseriesTrafficClassNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTrafficClassNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficType', (done) => {
        try {
          a.getStatsTimeseriesTrafficClassNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficType is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTrafficClassNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing trafficClass', (done) => {
        try {
          a.getStatsTimeseriesTrafficClassNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'trafficClass is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTrafficClassNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesTunnel - errors', () => {
      it('should have a getStatsTimeseriesTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsTimeseriesTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsTimeseriesTunnel(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsTimeseriesTunnel('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsTimeseriesTunnel('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postStatsTimeseriesTunnel - errors', () => {
      it('should have a postStatsTimeseriesTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.postStatsTimeseriesTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceIDs', (done) => {
        try {
          a.postStatsTimeseriesTunnel(null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'applianceIDs is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.postStatsTimeseriesTunnel('fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.postStatsTimeseriesTunnel('fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.postStatsTimeseriesTunnel('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postStatsTimeseriesTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getStatsTimeseriesTunnelNePk - errors', () => {
      it('should have a getStatsTimeseriesTunnelNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getStatsTimeseriesTunnelNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getStatsTimeseriesTunnelNePk(null, null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTunnelNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing startTime', (done) => {
        try {
          a.getStatsTimeseriesTunnelNePk('fakeparam', null, null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'startTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTunnelNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing endTime', (done) => {
        try {
          a.getStatsTimeseriesTunnelNePk('fakeparam', 'fakeparam', null, null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'endTime is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTunnelNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing granularity', (done) => {
        try {
          a.getStatsTimeseriesTunnelNePk('fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'granularity is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTunnelNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelName', (done) => {
        try {
          a.getStatsTimeseriesTunnelNePk('fakeparam', 'fakeparam', 'fakeparam', 'fakeparam', null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'tunnelName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getStatsTimeseriesTunnelNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#confSubnets - errors', () => {
      it('should have a confSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.confSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.confSubnets(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-confSubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing operation', (done) => {
        try {
          a.confSubnets('fakeparam', null, (data, error) => {
            try {
              const displayE = 'operation is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-confSubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubnetsForDiscoveredAppliance - errors', () => {
      it('should have a getSubnetsForDiscoveredAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.getSubnetsForDiscoveredAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discoveredId', (done) => {
        try {
          a.getSubnetsForDiscoveredAppliance(null, (data, error) => {
            try {
              const displayE = 'discoveredId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getSubnetsForDiscoveredAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#setSubnetSharingOptions - errors', () => {
      it('should have a setSubnetSharingOptions function', (done) => {
        try {
          assert.equal(true, typeof a.setSubnetSharingOptions === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.setSubnetSharingOptions(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-setSubnetSharingOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subnetsSystemJson', (done) => {
        try {
          a.setSubnetSharingOptions('fakeparam', null, (data, error) => {
            try {
              const displayE = 'subnetsSystemJson is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-setSubnetSharingOptions', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSubnets - errors', () => {
      it('should have a getSubnets function', (done) => {
        try {
          assert.equal(true, typeof a.getSubnets === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing getCachedData', (done) => {
        try {
          a.getSubnets(null, null, null, (data, error) => {
            try {
              const displayE = 'getCachedData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getSubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getSubnets('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getSubnets', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appSystemDeployInfo - errors', () => {
      it('should have a appSystemDeployInfo function', (done) => {
        try {
          assert.equal(true, typeof a.appSystemDeployInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.appSystemDeployInfo(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appSystemDeployInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.appSystemDeployInfo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appSystemDeployInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#discoveredAppSystemDeployInfo - errors', () => {
      it('should have a discoveredAppSystemDeployInfo function', (done) => {
        try {
          assert.equal(true, typeof a.discoveredAppSystemDeployInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing discoveredId', (done) => {
        try {
          a.discoveredAppSystemDeployInfo(null, (data, error) => {
            try {
              const displayE = 'discoveredId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-discoveredAppSystemDeployInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#appSystemStateInfo - errors', () => {
      it('should have a appSystemStateInfo function', (done) => {
        try {
          assert.equal(true, typeof a.appSystemStateInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.appSystemStateInfo(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appSystemStateInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.appSystemStateInfo('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-appSystemStateInfo', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelTca - errors', () => {
      it('should have a getTunnelTca function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnelTca === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getTunnelTca(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTunnelTca', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.getTunnelTca('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTunnelTca', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSystemTca - errors', () => {
      it('should have a getSystemTca function', (done) => {
        try {
          assert.equal(true, typeof a.getSystemTca === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getSystemTca(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getSystemTca', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.getSystemTca('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getSystemTca', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tcpdumpRun - errors', () => {
      it('should have a tcpdumpRun function', (done) => {
        try {
          assert.equal(true, typeof a.tcpdumpRun === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing postRunConfig', (done) => {
        try {
          a.tcpdumpRun(null, (data, error) => {
            try {
              const displayE = 'postRunConfig is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-tcpdumpRun', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tcpdumpStatus - errors', () => {
      it('should have a tcpdumpStatus function', (done) => {
        try {
          assert.equal(true, typeof a.tcpdumpStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.tcpdumpStatus(null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-tcpdumpStatus', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTcpdumpTcpdumpStatus - errors', () => {
      it('should have a getTcpdumpTcpdumpStatus function', (done) => {
        try {
          assert.equal(true, typeof a.getTcpdumpTcpdumpStatus === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associationAll - errors', () => {
      it('should have a associationAll function', (done) => {
        try {
          assert.equal(true, typeof a.associationAll === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associationOne - errors', () => {
      it('should have a associationOne function', (done) => {
        try {
          assert.equal(true, typeof a.associationOne === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.associationOne(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-associationOne', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#associationOnePost - errors', () => {
      it('should have a associationOnePost function', (done) => {
        try {
          assert.equal(true, typeof a.associationOnePost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.associationOnePost(null, null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-associationOnePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.associationOnePost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-associationOnePost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#template - errors', () => {
      it('should have a template function', (done) => {
        try {
          assert.equal(true, typeof a.template === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.template(null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-template', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateHistoryNePk - errors', () => {
      it('should have a getTemplateHistoryNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateHistoryNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getTemplateHistoryNePk(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTemplateHistoryNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing latestOnly', (done) => {
        try {
          a.getTemplateHistoryNePk('fakeparam', null, (data, error) => {
            try {
              const displayE = 'latestOnly is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTemplateHistoryNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTemplateTemplateCreate - errors', () => {
      it('should have a postTemplateTemplateCreate function', (done) => {
        try {
          assert.equal(true, typeof a.postTemplateTemplateCreate === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postTemplateTemplateCreate(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postTemplateTemplateCreate', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateTemplateGroups - errors', () => {
      it('should have a getTemplateTemplateGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateTemplateGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteTemplateTemplateGroupsTemplateGroup - errors', () => {
      it('should have a deleteTemplateTemplateGroupsTemplateGroup function', (done) => {
        try {
          assert.equal(true, typeof a.deleteTemplateTemplateGroupsTemplateGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateGroup', (done) => {
        try {
          a.deleteTemplateTemplateGroupsTemplateGroup(null, (data, error) => {
            try {
              const displayE = 'templateGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteTemplateTemplateGroupsTemplateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateTemplateGroupsTemplateGroup - errors', () => {
      it('should have a getTemplateTemplateGroupsTemplateGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateTemplateGroupsTemplateGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateGroup', (done) => {
        try {
          a.getTemplateTemplateGroupsTemplateGroup(null, (data, error) => {
            try {
              const displayE = 'templateGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTemplateTemplateGroupsTemplateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTemplateTemplateGroupsTemplateGroup - errors', () => {
      it('should have a postTemplateTemplateGroupsTemplateGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postTemplateTemplateGroupsTemplateGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateGroup', (done) => {
        try {
          a.postTemplateTemplateGroupsTemplateGroup(null, null, (data, error) => {
            try {
              const displayE = 'templateGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postTemplateTemplateGroupsTemplateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postTemplateTemplateGroupsTemplateGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postTemplateTemplateGroupsTemplateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupPrioGet - errors', () => {
      it('should have a groupPrioGet function', (done) => {
        try {
          assert.equal(true, typeof a.groupPrioGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#groupPrioPost - errors', () => {
      it('should have a groupPrioPost function', (done) => {
        try {
          assert.equal(true, typeof a.groupPrioPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.groupPrioPost(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-groupPrioPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTemplateTemplateSelectionTemplateGroup - errors', () => {
      it('should have a getTemplateTemplateSelectionTemplateGroup function', (done) => {
        try {
          assert.equal(true, typeof a.getTemplateTemplateSelectionTemplateGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateGroup', (done) => {
        try {
          a.getTemplateTemplateSelectionTemplateGroup(null, (data, error) => {
            try {
              const displayE = 'templateGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTemplateTemplateSelectionTemplateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postTemplateTemplateSelectionTemplateGroup - errors', () => {
      it('should have a postTemplateTemplateSelectionTemplateGroup function', (done) => {
        try {
          assert.equal(true, typeof a.postTemplateTemplateSelectionTemplateGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing templateGroup', (done) => {
        try {
          a.postTemplateTemplateSelectionTemplateGroup(null, null, (data, error) => {
            try {
              const displayE = 'templateGroup is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postTemplateTemplateSelectionTemplateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.postTemplateTemplateSelectionTemplateGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postTemplateTemplateSelectionTemplateGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#rdPartyLicenses - errors', () => {
      it('should have a rdPartyLicenses function', (done) => {
        try {
          assert.equal(true, typeof a.rdPartyLicenses === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerConfigurationGet - errors', () => {
      it('should have a zscalerConfigurationGet function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerConfigurationGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerConfigurationPost - errors', () => {
      it('should have a zscalerConfigurationPost function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerConfigurationPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing configuration', (done) => {
        try {
          a.zscalerConfigurationPost(null, (data, error) => {
            try {
              const displayE = 'configuration is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-zscalerConfigurationPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerConnectivityGet - errors', () => {
      it('should have a zscalerConnectivityGet function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerConnectivityGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerDefaultTunnelSettingGet - errors', () => {
      it('should have a zscalerDefaultTunnelSettingGet function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerDefaultTunnelSettingGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerPauseOrchestrationGet - errors', () => {
      it('should have a zscalerPauseOrchestrationGet function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerPauseOrchestrationGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerPauseOrchestrationPost - errors', () => {
      it('should have a zscalerPauseOrchestrationPost function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerPauseOrchestrationPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zscalerPauseObj', (done) => {
        try {
          a.zscalerPauseOrchestrationPost(null, (data, error) => {
            try {
              const displayE = 'zscalerPauseObj is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-zscalerPauseOrchestrationPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerRemoteEndpointExceptionGet - errors', () => {
      it('should have a zscalerRemoteEndpointExceptionGet function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerRemoteEndpointExceptionGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerSubscriptionDelete - errors', () => {
      it('should have a zscalerSubscriptionDelete function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerSubscriptionDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerSubscriptionGet - errors', () => {
      it('should have a zscalerSubscriptionGet function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerSubscriptionGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerSubscriptionPost - errors', () => {
      it('should have a zscalerSubscriptionPost function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerSubscriptionPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing subscription', (done) => {
        try {
          a.zscalerSubscriptionPost(null, (data, error) => {
            try {
              const displayE = 'subscription is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-zscalerSubscriptionPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zscalerVpnLocationExceptionPost - errors', () => {
      it('should have a zscalerVpnLocationExceptionPost function', (done) => {
        try {
          assert.equal(true, typeof a.zscalerVpnLocationExceptionPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceList', (done) => {
        try {
          a.zscalerVpnLocationExceptionPost(null, (data, error) => {
            try {
              const displayE = 'applianceList is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-zscalerVpnLocationExceptionPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBondedTunnelsConfigurationAndStates - errors', () => {
      it('should have a getBondedTunnelsConfigurationAndStates function', (done) => {
        try {
          assert.equal(true, typeof a.getBondedTunnelsConfigurationAndStates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neIds', (done) => {
        try {
          a.getBondedTunnelsConfigurationAndStates(null, null, (data, error) => {
            try {
              const displayE = 'neIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getBondedTunnelsConfigurationAndStates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBondedTunnelsStates - errors', () => {
      it('should have a getBondedTunnelsStates function', (done) => {
        try {
          assert.equal(true, typeof a.getBondedTunnelsStates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neIds', (done) => {
        try {
          a.getBondedTunnelsStates(null, (data, error) => {
            try {
              const displayE = 'neIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getBondedTunnelsStates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelsConfigurationAndStates - errors', () => {
      it('should have a getTunnelsConfigurationAndStates function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnelsConfigurationAndStates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ids', (done) => {
        try {
          a.getTunnelsConfigurationAndStates(null, null, (data, error) => {
            try {
              const displayE = 'ids is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTunnelsConfigurationAndStates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelsStates - errors', () => {
      it('should have a getTunnelsStates function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnelsStates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neIds', (done) => {
        try {
          a.getTunnelsStates(null, null, (data, error) => {
            try {
              const displayE = 'neIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTunnelsStates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#initiateTraceroute - errors', () => {
      it('should have a initiateTraceroute function', (done) => {
        try {
          assert.equal(true, typeof a.initiateTraceroute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.initiateTraceroute(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-initiateTraceroute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.initiateTraceroute('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-initiateTraceroute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tracerouteState - errors', () => {
      it('should have a tracerouteState function', (done) => {
        try {
          assert.equal(true, typeof a.tracerouteState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing id', (done) => {
        try {
          a.tracerouteState(null, null, (data, error) => {
            try {
              const displayE = 'id is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-tracerouteState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.tracerouteState('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-tracerouteState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getThirdPartyTunnelsConfigurationAndStates - errors', () => {
      it('should have a getThirdPartyTunnelsConfigurationAndStates function', (done) => {
        try {
          assert.equal(true, typeof a.getThirdPartyTunnelsConfigurationAndStates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neIds', (done) => {
        try {
          a.getThirdPartyTunnelsConfigurationAndStates(null, null, (data, error) => {
            try {
              const displayE = 'neIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getThirdPartyTunnelsConfigurationAndStates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getThirdPartyTunnelsStates - errors', () => {
      it('should have a getThirdPartyTunnelsStates function', (done) => {
        try {
          assert.equal(true, typeof a.getThirdPartyTunnelsStates === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neIds', (done) => {
        try {
          a.getThirdPartyTunnelsStates(null, null, (data, error) => {
            try {
              const displayE = 'neIds is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getThirdPartyTunnelsStates', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelsCount - errors', () => {
      it('should have a tunnelsCount function', (done) => {
        try {
          assert.equal(true, typeof a.tunnelsCount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing metaData', (done) => {
        try {
          a.tunnelsCount(null, (data, error) => {
            try {
              const displayE = 'metaData is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-tunnelsCount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAllBondedTunnels - errors', () => {
      it('should have a searchAllBondedTunnels function', (done) => {
        try {
          assert.equal(true, typeof a.searchAllBondedTunnels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.searchAllBondedTunnels(null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-searchAllBondedTunnels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchBondedTunnelsForAppliance - errors', () => {
      it('should have a searchBondedTunnelsForAppliance function', (done) => {
        try {
          assert.equal(true, typeof a.searchBondedTunnelsForAppliance === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.searchBondedTunnelsForAppliance(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-searchBondedTunnelsForAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.searchBondedTunnelsForAppliance('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-searchBondedTunnelsForAppliance', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOneBondedTunnel - errors', () => {
      it('should have a getOneBondedTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.getOneBondedTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing bondedTunnelId', (done) => {
        try {
          a.getOneBondedTunnel(null, null, (data, error) => {
            try {
              const displayE = 'bondedTunnelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getOneBondedTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getOneBondedTunnel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getOneBondedTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getBondedTunnelsWithPhysicalTunnels - errors', () => {
      it('should have a getBondedTunnelsWithPhysicalTunnels function', (done) => {
        try {
          assert.equal(true, typeof a.getBondedTunnelsWithPhysicalTunnels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getBondedTunnelsWithPhysicalTunnels(null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getBondedTunnelsWithPhysicalTunnels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing physicalTunnelId', (done) => {
        try {
          a.getBondedTunnelsWithPhysicalTunnels('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'physicalTunnelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getBondedTunnelsWithPhysicalTunnels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnelsBetweenAppliances - errors', () => {
      it('should have a getTunnelsBetweenAppliances function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnelsBetweenAppliances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing ids', (done) => {
        try {
          a.getTunnelsBetweenAppliances(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'ids is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTunnelsBetweenAppliances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getTunnelsBetweenAppliances('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTunnelsBetweenAppliances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAllPassThroughTunnels - errors', () => {
      it('should have a searchAllPassThroughTunnels function', (done) => {
        try {
          assert.equal(true, typeof a.searchAllPassThroughTunnels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.searchAllPassThroughTunnels(null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-searchAllPassThroughTunnels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAppliancePassthroughTunnels - errors', () => {
      it('should have a searchAppliancePassthroughTunnels function', (done) => {
        try {
          assert.equal(true, typeof a.searchAppliancePassthroughTunnels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.searchAppliancePassthroughTunnels(null, null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-searchAppliancePassthroughTunnels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.searchAppliancePassthroughTunnels('fakeparam', null, null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-searchAppliancePassthroughTunnels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPassThroughTunnel - errors', () => {
      it('should have a getPassThroughTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.getPassThroughTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getPassThroughTunnel(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getPassThroughTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing passThroughTunnelId', (done) => {
        try {
          a.getPassThroughTunnel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'passThroughTunnelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getPassThroughTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#searchAllPhysicalTunnels - errors', () => {
      it('should have a searchAllPhysicalTunnels function', (done) => {
        try {
          assert.equal(true, typeof a.searchAllPhysicalTunnels === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.searchAllPhysicalTunnels(null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-searchAllPhysicalTunnels', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTunnels2PhysicalNePk - errors', () => {
      it('should have a getTunnels2PhysicalNePk function', (done) => {
        try {
          assert.equal(true, typeof a.getTunnels2PhysicalNePk === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getTunnels2PhysicalNePk(null, null, null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTunnels2PhysicalNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing limit', (done) => {
        try {
          a.getTunnels2PhysicalNePk('fakeparam', null, null, null, (data, error) => {
            try {
              const displayE = 'limit is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getTunnels2PhysicalNePk', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getOnePhysicalTunnel - errors', () => {
      it('should have a getOnePhysicalTunnel function', (done) => {
        try {
          assert.equal(true, typeof a.getOnePhysicalTunnel === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nePk', (done) => {
        try {
          a.getOnePhysicalTunnel(null, null, (data, error) => {
            try {
              const displayE = 'nePk is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getOnePhysicalTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing tunnelId', (done) => {
        try {
          a.getOnePhysicalTunnel('fakeparam', null, (data, error) => {
            try {
              const displayE = 'tunnelId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getOnePhysicalTunnel', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelsDeploymentInfo - errors', () => {
      it('should have a tunnelsDeploymentInfo function', (done) => {
        try {
          assert.equal(true, typeof a.tunnelsDeploymentInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#tunnelsOverlayInfo - errors', () => {
      it('should have a tunnelsOverlayInfo function', (done) => {
        try {
          assert.equal(true, typeof a.tunnelsOverlayInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#passThroughTunnelsInfo - errors', () => {
      it('should have a passThroughTunnelsInfo function', (done) => {
        try {
          assert.equal(true, typeof a.passThroughTunnelsInfo === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#uiUsageStats - errors', () => {
      it('should have a uiUsageStats function', (done) => {
        try {
          assert.equal(true, typeof a.uiUsageStats === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing uiName', (done) => {
        try {
          a.uiUsageStats(null, (data, error) => {
            try {
              const displayE = 'uiName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-uiUsageStats', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#upgradeAppliances - errors', () => {
      it('should have a upgradeAppliances function', (done) => {
        try {
          assert.equal(true, typeof a.upgradeAppliances === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing applianceUpgradeEntry', (done) => {
        try {
          a.upgradeAppliances(null, (data, error) => {
            try {
              const displayE = 'applianceUpgradeEntry is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-upgradeAppliances', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUserAccount - errors', () => {
      it('should have a getUserAccount function', (done) => {
        try {
          assert.equal(true, typeof a.getUserAccount === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.getUserAccount(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getUserAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.getUserAccount('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getUserAccount', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#users - errors', () => {
      it('should have a users function', (done) => {
        try {
          assert.equal(true, typeof a.users === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#forgotPassword - errors', () => {
      it('should have a forgotPassword function', (done) => {
        try {
          assert.equal(true, typeof a.forgotPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing forgotPasswordBody', (done) => {
        try {
          a.forgotPassword(null, (data, error) => {
            try {
              const displayE = 'forgotPasswordBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-forgotPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#newTfaKey - errors', () => {
      it('should have a newTfaKey function', (done) => {
        try {
          assert.equal(true, typeof a.newTfaKey === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#resetPassword - errors', () => {
      it('should have a resetPassword function', (done) => {
        try {
          assert.equal(true, typeof a.resetPassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing resetPasswordBody', (done) => {
        try {
          a.resetPassword(null, (data, error) => {
            try {
              const displayE = 'resetPasswordBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-resetPassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersNewUser - errors', () => {
      it('should have a postUsersNewUser function', (done) => {
        try {
          assert.equal(true, typeof a.postUsersNewUser === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing newUser', (done) => {
        try {
          a.postUsersNewUser(null, null, (data, error) => {
            try {
              const displayE = 'newUser is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postUsersNewUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing saveUpdateUserBody', (done) => {
        try {
          a.postUsersNewUser('fakeparam', null, (data, error) => {
            try {
              const displayE = 'saveUpdateUserBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postUsersNewUser', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteUsersUserIdUserName - errors', () => {
      it('should have a deleteUsersUserIdUserName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteUsersUserIdUserName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userId', (done) => {
        try {
          a.deleteUsersUserIdUserName(null, null, (data, error) => {
            try {
              const displayE = 'userId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteUsersUserIdUserName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userName', (done) => {
        try {
          a.deleteUsersUserIdUserName('fakeparam', null, (data, error) => {
            try {
              const displayE = 'userName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-deleteUsersUserIdUserName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getUsersUserName - errors', () => {
      it('should have a getUsersUserName function', (done) => {
        try {
          assert.equal(true, typeof a.getUsersUserName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing userName', (done) => {
        try {
          a.getUsersUserName(null, (data, error) => {
            try {
              const displayE = 'userName is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-getUsersUserName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postUsersUsernamePassword - errors', () => {
      it('should have a postUsersUsernamePassword function', (done) => {
        try {
          assert.equal(true, typeof a.postUsersUsernamePassword === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing username', (done) => {
        try {
          a.postUsersUsernamePassword(null, null, (data, error) => {
            try {
              const displayE = 'username is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postUsersUsernamePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing changePasswordBody', (done) => {
        try {
          a.postUsersUsernamePassword('fakeparam', null, (data, error) => {
            try {
              const displayE = 'changePasswordBody is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-postUsersUsernamePassword', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#loopbackGet - errors', () => {
      it('should have a loopbackGet function', (done) => {
        try {
          assert.equal(true, typeof a.loopbackGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.loopbackGet(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-loopbackGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.loopbackGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-loopbackGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vtiGet - errors', () => {
      it('should have a vtiGet function', (done) => {
        try {
          assert.equal(true, typeof a.vtiGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.vtiGet(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-vtiGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.vtiGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-vtiGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vrrpGet - errors', () => {
      it('should have a vrrpGet function', (done) => {
        try {
          assert.equal(true, typeof a.vrrpGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.vrrpGet(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-vrrpGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.vrrpGet('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-vrrpGet', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vXOAImagesGet - errors', () => {
      it('should have a vXOAImagesGet function', (done) => {
        try {
          assert.equal(true, typeof a.vXOAImagesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#vXOAImagesDelete - errors', () => {
      it('should have a vXOAImagesDelete function', (done) => {
        try {
          assert.equal(true, typeof a.vXOAImagesDelete === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing imageFile', (done) => {
        try {
          a.vXOAImagesDelete(null, (data, error) => {
            try {
              const displayE = 'imageFile is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-vXOAImagesDelete', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wccpConfigGroup - errors', () => {
      it('should have a wccpConfigGroup function', (done) => {
        try {
          assert.equal(true, typeof a.wccpConfigGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.wccpConfigGroup(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-wccpConfigGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.wccpConfigGroup('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-wccpConfigGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wccpConfigSystem - errors', () => {
      it('should have a wccpConfigSystem function', (done) => {
        try {
          assert.equal(true, typeof a.wccpConfigSystem === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.wccpConfigSystem(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-wccpConfigSystem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.wccpConfigSystem('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-wccpConfigSystem', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#wccpState - errors', () => {
      it('should have a wccpState function', (done) => {
        try {
          assert.equal(true, typeof a.wccpState === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing neId', (done) => {
        try {
          a.wccpState(null, null, (data, error) => {
            try {
              const displayE = 'neId is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-wccpState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing cached', (done) => {
        try {
          a.wccpState('fakeparam', null, (data, error) => {
            try {
              const displayE = 'cached is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-wccpState', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zonesGet - errors', () => {
      it('should have a zonesGet function', (done) => {
        try {
          assert.equal(true, typeof a.zonesGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#zonesPost - errors', () => {
      it('should have a zonesPost function', (done) => {
        try {
          assert.equal(true, typeof a.zonesPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zones', (done) => {
        try {
          a.zonesPost(null, null, (data, error) => {
            try {
              const displayE = 'zones is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-zonesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing deleteDependencies', (done) => {
        try {
          a.zonesPost('fakeparam', null, (data, error) => {
            try {
              const displayE = 'deleteDependencies is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-zonesPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#nextIdGet - errors', () => {
      it('should have a nextIdGet function', (done) => {
        try {
          assert.equal(true, typeof a.nextIdGet === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#nextIdPost - errors', () => {
      it('should have a nextIdPost function', (done) => {
        try {
          assert.equal(true, typeof a.nextIdPost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing nextIdPost', (done) => {
        try {
          a.nextIdPost(null, (data, error) => {
            try {
              const displayE = 'nextIdPost is required';
              runErrorAsserts(data, error, 'AD.300', 'Test-silverpeak-adapter-nextIdPost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
