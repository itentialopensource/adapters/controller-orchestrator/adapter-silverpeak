# Silver Peak SD-WAN Orchestrator

Vendor: Aruba Networks
Homepage: https://www.arubanetworks.com/

Product: EdgeConnect SD-WAN Orchestrator
Product Page: https://www.arubanetworks.com/products/sd-wan/edgeconnect/

## Introduction
We classify Silver Peak SD-WAN Orchestrator into the Data Center and Network Services domains as it provides acentralized management and optimization for enterprise networks.

## Why Integrate
The Silver Peak SD-WAN Orchestrator adapter from Itential is used to integrate the Itential Automation Platform (IAP) with Silver Peak SD-WAN Orchestrator. With this adapter you have the ability to perform operations on items such as:

- Region
- Appliance
- Host
- Deployment

## Additional Product Documentation
[Aruba/SilverPeak SD-WAN Orchestrator API Documentation](https://developer.arubanetworks.com/aruba-edgeconnect/reference/acl1)