
## 0.6.4 [10-15-2024]

* Changes made at 2024.10.14_21:17PM

See merge request itentialopensource/adapters/adapter-silverpeak!19

---

## 0.6.3 [09-17-2024]

* add workshop and fix vulnerabilities

See merge request itentialopensource/adapters/adapter-silverpeak!17

---

## 0.6.2 [08-15-2024]

* Changes made at 2024.08.14_19:32PM

See merge request itentialopensource/adapters/adapter-silverpeak!16

---

## 0.6.1 [08-07-2024]

* Changes made at 2024.08.06_21:35PM

See merge request itentialopensource/adapters/adapter-silverpeak!15

---

## 0.6.0 [07-17-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!14

---

## 0.5.4 [03-28-2024]

* Changes made at 2024.03.28_13:45PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!12

---

## 0.5.3 [03-15-2024]

* Update metadata.json

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!11

---

## 0.5.2 [03-11-2024]

* Changes made at 2024.03.11_16:01PM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!10

---

## 0.5.1 [02-28-2024]

* Changes made at 2024.02.28_11:31AM

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!9

---

## 0.5.0 [12-29-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!8

---

## 0.4.0 [05-22-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!7

---

## 0.3.3 [03-15-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!6

---

## 0.3.2 [07-10-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!5

---

## 0.3.1 [01-14-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!4

---

## 0.3.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!3

---

## 0.2.0 [10-29-2019]

- Update the adapter to the latest foundation and make this adapter a public adapter

See merge request itentialopensource/adapters/controller-orchestrator/adapter-silverpeak!2

---
